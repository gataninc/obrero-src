/*
 *  "Fusion"  touchscreen driver
 *	
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/input.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/workqueue.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/semaphore.h>
#include <asm/bitops.h>
#include <asm/io.h>
#include <asm/irq.h>
#include <asm/atomic.h>
#include <asm/uaccess.h>
#include <linux/gpio.h>
#include "fusion_def.h"

#include "fusion.h"

// data structures for the character device interface
typedef struct 
{
    dev_t tpDevNo;
    struct cdev tpDev;
} fusionChrDev;

fusionChrDev chrDev;
// END char device details.

static atomic_t touchCount = ATOMIC_INIT(0);
static atomic_t tpResetCnt = ATOMIC_INIT(0);

// semaphore for serialising access to device in the worker threads.
DECLARE_MUTEX(sem_dev);

/* disable all multi-touch processing for now, we are not using it.. */
//#define _MT_

/* taking short-cuts.. using an arch specific call to get immr */
extern phys_addr_t get_immrbase(void);

/* offsets to GPIO registers, needed to configure GPIO0 as interrupt */
#define GPIO0_OFFSET     0x1100

/* physical memory address of FPGA register space */
#define FPGA_REG_ADDR  0xE5000000

static unsigned char *reset_ptr;    // pointer to fpga regs
#define   RESET_REG              6  // offset to reset register
#define   FPGA_RST_FUSION_TP  0x20  // write this to reg. to reset tp

typedef struct gpio_reg_set
{
    volatile u32 gpDIR;
    volatile u32 gpODR;
    volatile u32 gpDAT;
    volatile u32 gpIER;
    volatile u32 gpIMR;
    volatile u32 gpICR1;
    volatile u32 gpICR2;
} gpio_regs;

static gpio_regs *gpio;
static int fusion_done_init = 0;

#define PANEL_10Z8

#ifdef PANEL_10Z8 /* 10" panel */
static struct fusion_info bing_info = { 2275, 1275, 1 };
#else  /* assume 7" panel */
static struct fusion_info bing_info = { 1500, 900, 0 };
#endif

static struct fusion_info *fusion_info = NULL;

static struct fusion_data fusion;

static unsigned short normal_i2c[] = { FUSION_I2C_SLAVE_ADDR, I2C_CLIENT_END };

static struct i2c_driver fusion_i2c_drv;

static unsigned long last_intr_jiffy;
static unsigned int dev_intr_cnt = 0;
static unsigned long irq_enables, irq_disables;
static int tp_reset_dbg, tp_reset_trace; // debug enables.
static int tp_reset_delay = 750; // milli-seconds, default.
static int do_tp_reset = 0;

enum { reset_start, reset_pulse, reset_end };
static void reset_func(unsigned long data)
{
    static int tmr_state = reset_start;
    struct fusion_data *data_ptr = (struct fusion_data *)data;
    if (tmr_state == reset_start)
    {
        if (tp_reset_trace) printk("== RESET TP(%d) ==\n", tp_reset_delay);
        //reset_ptr[RESET_REG] |= FPGA_RST_FUSION_TP;
        setbits8((reset_ptr+RESET_REG), FPGA_RST_FUSION_TP);
        data_ptr->reset_timer.expires = (jiffies + msecs_to_jiffies(tp_reset_delay));
        tmr_state = reset_pulse;
        add_timer(&(data_ptr->reset_timer));
    }
    else if (tmr_state == reset_pulse)
    {
        //reset_ptr[RESET_REG] &= ~(FPGA_RST_FUSION_TP);
        clrbits8((reset_ptr+RESET_REG), FPGA_RST_FUSION_TP);
        data_ptr->reset_timer.expires = (jiffies + msecs_to_jiffies(250));
        tmr_state = reset_end;
        add_timer(&(data_ptr->reset_timer));
    }
    else if (tmr_state == reset_end)
    {
        tmr_state = reset_start;
        do_tp_reset = 0;
        atomic_inc(&tpResetCnt);
        if (tp_reset_dbg) printk("== RESET TP DONE == \n");
	    if (data_ptr->irq)
        {
            irq_enables++;
		    enable_irq(data_ptr->irq);
        }
    }
}

static int fusion_write_u8(u8 addr, u8 data) 
{
	return i2c_smbus_write_byte_data(fusion.client, addr, data);
}

static int fusion_read_u8(u8 addr)
{
	return i2c_smbus_read_byte_data(fusion.client, addr);
}

static int fusion_read_block(u8 addr, u8 len, u8 *data)
{
#if 0
	/* When i2c_smbus_read_i2c_block_data() takes a block length parameter, we can do
	 * this. lm-sensors lists hints this has been fixed, but I can't tell whether it
	 * was or will be merged upstream. */

	return i2c_smbus_read_i2c_block_data(&fusion.client, addr, data);
#else
	u8 msgbuf0[1] = { addr };
	u16 slave = fusion.client->addr;
	u16 flags = fusion.client->flags;
	struct i2c_msg msg[2] = { { slave, flags, 1, msgbuf0 },
				  { slave, flags | I2C_M_RD, len, data }
	};

	return i2c_transfer(fusion.client->adapter, msg, ARRAY_SIZE(msg));
#endif
}

static int fusion_register_input(void)
{
	int ret;
	struct input_dev *dev;

	dev = fusion.input = input_allocate_device();
	if (dev == NULL)
		return -ENOMEM;

	dev->name = fusion.client->name;

	set_bit(EV_KEY, dev->evbit);
	set_bit(EV_ABS, dev->evbit);
	dev->keybit[BIT_WORD(BTN_TOUCH)] = BIT_MASK(BTN_TOUCH);
#ifdef _MT_
	input_set_abs_params(dev, ABS_MT_POSITION_X, 0, fusion_info->xres-1, 0, 0);
	input_set_abs_params(dev, ABS_MT_POSITION_Y, 0, fusion_info->yres-1, 0, 0);
	input_set_abs_params(dev, ABS_MT_TOUCH_MAJOR, 0, 255, 0, 0);
	input_set_abs_params(dev, ABS_MT_WIDTH_MAJOR, 0, 15, 0, 0);
#else
	input_set_abs_params(dev, ABS_X, 0, fusion_info->xres-1, 0, 0);
	input_set_abs_params(dev, ABS_Y, 0, fusion_info->yres-1, 0, 0);
#endif
	ret = input_register_device(dev);
	if (ret < 0)
		goto bail1;

	return 0;

bail1:
	input_free_device(dev);
	return ret;
}

#define WC_RETRY_COUNT 		3
static int fusion_write_complete(void)
{
	int ret, i;

	for(i=0; i<WC_RETRY_COUNT; i++)
	{
		ret = fusion_write_u8(FUSION_SCAN_COMPLETE, 0);
		if(ret == 0)
			break;
		else
			dev_err(&fusion.client->dev, "Write complete failed(%d): %d\n", i, ret);
	}
	return ret;
}

#define DATA_START	FUSION_DATA_INFO
#define	DATA_END	FUSION_SEC_TIDTS
#define DATA_LEN	(DATA_END - DATA_START + 1)
#define DATA_OFF(x)	((x) - DATA_START)

static int fusion_read_sensor(void)
{
	int ret;
	u8 data[DATA_LEN];

#define DATA(x) (data[DATA_OFF(x)])
	/* To ensure data coherency, read the sensor with a single transaction. */
	ret = fusion_read_block(DATA_START, DATA_LEN, data);
	if (ret < 0) {
		dev_err(&fusion.client->dev,
			"Read block failed: %d\n", ret);
		/* Clear fusion interrupt */
        /* 08/10/2012: do not attempt to write to device, 
           attempt recovery by resetting touch controller  */
		/* fusion_write_complete(); */ 
		return ret;
	}

	fusion.f_num = DATA(FUSION_DATA_INFO)&0x03;
	
	fusion.y1 = DATA(FUSION_POS_X1_HI) << 8;
	fusion.y1 |= DATA(FUSION_POS_X1_LO);
	fusion.x1 = DATA(FUSION_POS_Y1_HI) << 8;
	fusion.x1 |= DATA(FUSION_POS_Y1_LO);
	fusion.z1 = DATA(FUSION_FIR_PRESS);
	fusion.tip1 = DATA(FUSION_FIR_TIDTS)&0x0f;
	fusion.tid1 = (DATA(FUSION_FIR_TIDTS)&0xf0)>>4;
	
#ifndef _MT_
	fusion.y2 = DATA(FUSION_POS_X2_HI) << 8;
	fusion.y2 |= DATA(FUSION_POS_X2_LO);
	fusion.x2 = DATA(FUSION_POS_Y2_HI) << 8;
	fusion.x2 |= DATA(FUSION_POS_Y2_LO);
	fusion.z2 = DATA(FUSION_SEC_PRESS);
	fusion.tip2 = DATA(FUSION_SEC_TIDTS)&0x0f;
	fusion.tid2 =(DATA(FUSION_SEC_TIDTS)&0xf0)>>4;
#endif
#undef DATA
	/* Clear fusion interrupt */
	//return fusion_write_complete();
    return 0;
}

#define val_cut_max(x, max, reverse)	\
do					\
{					\
	if(x > max)			\
		x = max;		\
	if(reverse)			\
		x = (max) - (x);	\
}					\
while(0)

static unsigned long next_reset_jiffy; // for delay between successive resets..
static void tp_hang_func(struct work_struct *work)
{
    struct fusion_data *data = &fusion;
    static int err_cnt = 0;
    static int lock_fail, rd_cnt, err_reset;
    static int ev_active;
    static unsigned int last_intr_cnt;
    int ret;
    if (tp_reset_dbg)
    {
    if ((rd_cnt % 60) == 0)
        printk("== tp_hang_func: lk fail %d count %d err_rst %d\n",
                        lock_fail, rd_cnt, err_reset);
    }
    if (tp_reset_trace) printk("<== %s ", __func__);
    if (down_trylock(&sem_dev) != 0)
    {
        lock_fail++;
        goto out_hang_func;
    }
    // don't read device if it is not time for next reset..
    if (time_before(jiffies, next_reset_jiffy))
    {
        up(&sem_dev);
        goto out_hang_func;
    }
    if (!do_tp_reset)
    {
        rd_cnt++;
        ret = fusion_read_u8(FUSION_SCAN_COMPLETE/*FUSION_VIESION_INFO_LO*/);
        if (tp_reset_trace) printk("scan_comp: %d", ret);
        if (ret == 1)  // device reports a touch event..
        {
            if (ev_active)
            {
                if (last_intr_cnt != dev_intr_cnt) // interrupts are still active..
                    ev_active = 0;
                else
                {
                u32 reg;
                reg = in_be32(&gpio->gpIER);
                if (tp_reset_trace) printk("int. NOT active %u - %u (IE %lu ID %lu IER 0x%X)", 
                                dev_intr_cnt, last_intr_cnt, irq_enables, irq_disables, reg);
                do_tp_reset = 1;
                if (data->irq)
                {
                    irq_disables++;
                    disable_irq_nosync(data->irq);
                }
                data->reset_timer.expires = (jiffies + msecs_to_jiffies(1));
                add_timer(&(data->reset_timer));
                ev_active = 0;
                err_reset++;
                next_reset_jiffy = (jiffies + (6 * HZ));
                }
            }
            else
            {
                ev_active++;
                last_intr_cnt = dev_intr_cnt;
            }
        }
        else
            ev_active = 0;
        if (ret < 0)
            err_cnt++;
        else
            err_cnt = 0;
        if (err_cnt > 1) // two errors in series, initiate reset
        {
            do_tp_reset = 1;
	        if (data->irq)
            {
                irq_disables++;
		        disable_irq_nosync(data->irq);
            }
            data->reset_timer.expires = (jiffies + msecs_to_jiffies(1));
            add_timer(&(data->reset_timer));
            err_cnt = 0;
            err_reset++;
            next_reset_jiffy = (jiffies + (6 * HZ));
        }
    }
    up(&sem_dev);
out_hang_func:
    if (tp_reset_trace) printk("  ==>\n");
    data->dev_check_timer.expires = (jiffies + msecs_to_jiffies(1000));
    add_timer(&(data->dev_check_timer));
}

static DECLARE_WORK(fusion_check, tp_hang_func);
static void dev_check_func(unsigned long data)
{
    // if last interrupt occurred within last second, do not test for hang
    if (time_before(jiffies, (last_intr_jiffy+HZ)))
    {
        fusion.dev_check_timer.expires = (jiffies + msecs_to_jiffies(1000));
        add_timer(&(fusion.dev_check_timer));
    }
    else
	    queue_work(fusion.dev_check_q, &fusion_check);
}

static void fusion_wq(struct work_struct *work)
{
    u32 reg;
	struct input_dev *dev = fusion.input;
	int save_points = 0;
	int x1 = 0, y1 = 0, z1 = 0, x2 = 0, y2 = 0, z2 = 0;
    static int fusionRdFails = 0;
    static int prev_resets = 0;
    static int spuriusInt = 0;
    static int rdErr = 0;
    int num_resets;

    reg = in_be32(&gpio->gpIER);
    if ((reg & 0x40000000) == 0) /* spurious interrupt?? */
    {
	    if (fusion.irq)
        {
            irq_enables++;
		    enable_irq(fusion.irq);
        }
        spuriusInt++;
        return;
    }
    out_be32(&gpio->gpIER, reg);
    dev_intr_cnt++;
    if ((dev_intr_cnt  % 200) == 0)
    {
        unsigned int tCount = atomic_read(&touchCount);
        printk("== fusiontp: ints %u rdFail %d spurious %d tCount %u\n", dev_intr_cnt, 
                                                fusionRdFails, spuriusInt, tCount);
    }
    num_resets = atomic_read(&tpResetCnt);
    if (prev_resets != num_resets)
    {
        printk("\n== fusiontp: touch controller RESET(%d)\n", num_resets);
        prev_resets = num_resets;
    }
    down(&sem_dev);
    if (do_tp_reset)
    {
        up(&sem_dev);
        return;
    }
	if (fusion_read_sensor() < 0)
    {
        fusionRdFails++;
        rdErr++;
        if ((rdErr > 2) && (!do_tp_reset))
        {
            if (tp_reset_dbg) printk("== TP_RESET_INTR ==\n");
            do_tp_reset = 1;
            /* don't enable interrupt, try to recover by resetting the chip */
            fusion.reset_timer.expires = (jiffies + msecs_to_jiffies(1));
            add_timer(&fusion.reset_timer);
        }
        else
        {
	        if (fusion.irq)
            {
                irq_enables++;
		        enable_irq(fusion.irq);
            }
        }
        up(&sem_dev);
		return;
    }
    else
        rdErr = 0;

	val_cut_max(fusion.x1, fusion_info->xres-1, fusion_info->xy_reverse);
	val_cut_max(fusion.y1, fusion_info->yres-1, fusion_info->xy_reverse);
#ifndef _MT_
	val_cut_max(fusion.x2, fusion_info->xres-1, fusion_info->xy_reverse);
	val_cut_max(fusion.y2, fusion_info->yres-1, fusion_info->xy_reverse);
#endif
	if(fusion.tip1 == 1)
	{
		if(fusion.tid1 == 1)
		{
			/* first point */
			x1 = fusion.x1;
			y1 = fusion.y1;
			z1 = fusion.z1;
			save_points |= FUSION_SAVE_PT1;
		}
#ifndef _MT_
		else if(fusion.tid1 == 2)
		{
			/* second point ABS_DISTANCE second point pressure, BTN_2 second point touch */
			x2 = fusion.x1;
			y2 = fusion.y1;
			z2 = fusion.z1;
			save_points |= FUSION_SAVE_PT2;
		}
#endif
	}
#ifndef _MT_
	if(fusion.tip2 == 1)
	{
		if(fusion.tid2 == 2)
		{
			/* second point ABS_DISTANCE second point pressure, BTN_2 second point touch */
			x2 = fusion.x2;
			y2 = fusion.y2;
			z2 = fusion.z2;
			save_points |= FUSION_SAVE_PT2;
		}
		else if(fusion.tid2 == 1)/* maybe this will never happen */
		{
			/* first point */
			x1 = fusion.x2;
			y1 = fusion.y2;
			z1 = fusion.z2;
			save_points |= FUSION_SAVE_PT1;
		}
	}
#endif
#ifdef _MT_
	input_report_abs(dev, ABS_MT_TOUCH_MAJOR, z1);
	input_report_abs(dev, ABS_MT_WIDTH_MAJOR, 1);
	input_report_abs(dev, ABS_MT_POSITION_X, x1);
	input_report_abs(dev, ABS_MT_POSITION_Y, y1);
	input_mt_sync(dev);
	input_report_abs(dev, ABS_MT_TOUCH_MAJOR, z2);
	input_report_abs(dev, ABS_MT_WIDTH_MAJOR, 2);
	input_report_abs(dev, ABS_MT_POSITION_X, x2);
	input_report_abs(dev, ABS_MT_POSITION_Y, y2);
	input_mt_sync(dev);
#else
    if ((x1) || (y1))
    {
        input_report_abs(dev, ABS_X, x1);
        input_report_abs(dev, ABS_Y, y1);
        input_report_key(dev, BTN_TOUCH, 1); /* button press */
    }
    else
        input_report_key(dev, BTN_TOUCH, 0); /* button release */
#endif

	input_sync(dev);
	fusion_write_complete();
    up(&sem_dev);
    // increment touch count for use by userland
    atomic_inc(&touchCount);
	if (fusion.irq)
    {
        irq_enables++;
		enable_irq(fusion.irq);
    }
}

static DECLARE_WORK(fusion_work, fusion_wq);

static irqreturn_t fusion_interrupt(int irq, void *dev_id)
{
	if (fusion.irq)
    {
        irq_disables++;
		disable_irq_nosync(fusion.irq);
    }
    last_intr_jiffy = jiffies;
    /* clear GPIO event register too.. */
	queue_work(fusion.workq, &fusion_work);
	return IRQ_HANDLED;
}

const static u8* g_ver_product[4] = {
	"10Z8", "70Z7", "43Z6", ""
};

static int fusion_probe(struct i2c_client *i2c, const struct i2c_device_id *id)
{
    int ret;
    u8 ver_product, ver_id;
    u32 version;
    
    /* Attach the I2C client */
    fusion.client =  i2c;
    i2c_set_clientdata(i2c, &fusion);
    printk(KERN_INFO "Fusion :Touchscreen registered with bus id (%d) "
                    "with slave address 0x%x, irq 0x%x\n",
                     i2c_adapter_id(fusion.client->adapter),
                    fusion.client->addr, fusion.client->irq);

    fusion.irq = fusion.client->irq;

    /* Read out a lot of registers */
    ret = fusion_read_u8(FUSION_VIESION_INFO_LO);
    if (ret < 0) {
            dev_err(&i2c->dev, "query failed: %d\n", ret);
            ret = 0;
            goto bail1;
    }
    ver_product = (((u8)ret) & 0xc0) >> 6;
    version = (10 + ((((u32)ret)&0x30) >> 4)) * 100000;
    version += (((u32)ret)&0xf) * 1000;
    /* Read out a lot of registers */
    ret = fusion_read_u8(FUSION_VIESION_INFO);
    if (ret < 0) {
            dev_err(&i2c->dev, "query failed: %d\n", ret);
            ret = 0;
            goto bail1;
    }
    ver_id = ((u8)(ret) & 0x6) >> 1;
    version += ((((u32)ret) & 0xf8) >> 3) * 10;
    version += (((u32)ret) & 0x1) + 1; /* 0 is build 1, 1 is build 2 */
    printk(KERN_INFO "Fusion version product %s(%d)\n", g_ver_product[ver_product] ,ver_product);
    printk(KERN_INFO "Fusion version id %s(%d)\n", ver_id ? "1.4" : "1.0", ver_id);
    printk(KERN_INFO "Fusion version series (%d)\n", version);

    /* Register the input device. */
    ret = fusion_register_input();
    if (ret < 0) {
            dev_err(&i2c->dev, "can't register input: %d\n", ret);
            ret = 0;
            goto bail1;
    }

    /* make sure gpio1 is set to input */
    //reg = in_be32(&gpio->gpDIR);
    //reg &= (~0xF0000000);
    //out_be32(&gpio->gpDIR, reg);
    clrbits32(&gpio->gpDIR, 0x40000000);

    /* set bit 30 to enable interrupts on GPIO1 */
    //out_be32(&gpio->gpIMR, 0x40000000);
    setbits32(&gpio->gpIMR, 0x40000000);

    /* set bits 29-28 to 01 to enable interrupt gen. on 
       low to high change on GPIO1 */
    //out_be32(&gpio->gpICR1, 0x10000000); /* low-to-high.. */
    setbits32(&gpio->gpICR1, 0x10000000);

    /* clear the irq first */
    fusion_write_complete();

    /* Register for the interrupt and enable it. Our handler will
     *  start getting invoked after this call. */
    ret = request_irq(fusion.irq, fusion_interrupt, IRQF_SHARED,
                      i2c->name, &fusion);
    if (ret < 0) {
            dev_err(&i2c->dev, "can't get irq %d: %d\n", fusion.irq, ret);
            goto bail2;
    }
    fusion_done_init = 1;
    return 0;
bail2:
    input_unregister_device(fusion.input);

bail1:
    return ret;
}

static int fusion_remove(struct i2c_client *i2c)
{
    if (fusion_done_init)
    {
        free_irq(fusion.irq, &fusion);
        input_unregister_device(fusion.input);
        i2c_set_clientdata(i2c, NULL);
    }
    return 0;
}

static int fusion_detect(struct i2c_client *client, int kind,
			  struct i2c_board_info *info)
{
	struct i2c_adapter *adapter = client->adapter;
	if (!i2c_check_functionality(adapter, I2C_FUNC_SMBUS_BYTE))
		return -ENODEV;
    return 0;
}

static struct i2c_device_id fusion_id[] = {
        {"fusiontp", 0x10},
        {},
};

MODULE_DEVICE_TABLE(i2c, fusion_id);

static const struct i2c_client_address_data addr_data = {		\
	.normal_i2c	= normal_i2c,					\
	.probe		= 0,					\
	.ignore		= 0,					\
	.forces		= 0,					\
};

static struct i2c_driver fusion_i2c_drv = {
        .driver = {
                .name   = "fusion",
        },
        .detect         = fusion_detect,
        .probe          = fusion_probe,
        .remove         = fusion_remove,
        .id_table       = fusion_id,
        .address_data   = &addr_data,
};

/* char device code */
ssize_t tpdev_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos)
{
    // XXX: Assuming only ONE task will read at a time. Even without a 
    // serialising lock, we should be OK since we use atomic_read..
    // Irrespective of number of bytes asked, just return the count always..
    int val = atomic_read(&touchCount);
    int dataSz = sizeof(int);
    if (count < dataSz)
        return -EFAULT;
    return ((put_user(val, (int __user *)buf)) ? -EFAULT : dataSz);
}

int tpdev_ioctl(struct inode *inode, struct file *filp,
                    unsigned int cmd, unsigned long arg)
{
    int ret = 0;
    int intVal;
    //printk("%s: cmd %x\n", __func__, cmd);
    // validate cmd..
    if (_IOC_TYPE(cmd) != TPDEV_IOCTL_MAGIC)
    {
        printk("%s: !magic %d\n", __func__, _IOC_TYPE(cmd));
        return -ENOTTY;
    }
    if (_IOC_NR(cmd) > TPDEV_IOCTL_MAX)
    {
        printk("%s: >max %d(%d)\n", __func__, _IOC_NR(cmd), TPDEV_IOCTL_MAX);
        return -ENOTTY;
    }
    // we will use put_user/get_user so access_ok is not needed
    switch (cmd)
    {
        case TPDEV_GET_TPRSTCNT:
            intVal = atomic_read(&tpResetCnt);
            ret = put_user(intVal, (int __user *)arg);
            break;
        case TPDEV_GET_TPRSTSTAT:
            intVal = atomic_read(&tpResetCnt);
            ret = put_user(intVal, (int __user *)arg);
            break;
        case TPDEV_SET_DBGFLAG:
            ret = get_user(tp_reset_dbg, (int __user *)arg);
            break;
        case TPDEV_SET_TRACEFLAG:
            ret = get_user(tp_reset_trace, (int __user *)arg);
            break;
        case TPDEV_SET_RESET_DELAY:
            ret = get_user(intVal, (int __user *)arg);
            if (ret == 0)
            {
                if ((intVal >= 100) && (intVal <= 5000))
                    tp_reset_delay = intVal;
                else
                    ret = -ERANGE;
            }
            break;
        default:
            ret = -ENOTTY;
            break;
    }
    return ret;
}

static struct file_operations fusion_tpdev_fops = {
    .owner = THIS_MODULE,
    .read  = tpdev_read,
    .ioctl = tpdev_ioctl,
};
/* END char device code */

static int __init fusion_init( void )
{
	int ret;
    unsigned int immr_base;
	static char drvname[] = "fusion_tp";
	memset(&fusion, 0, sizeof(fusion));

    fusion_info = &bing_info;
    printk("Fusion Info: %d x %d, %d\n", bing_info.xres, bing_info.yres, bing_info.xy_reverse);

	/* Create a worker thread for doing interrupt processing. i2c read/write calls can 
       sleep and so cannot be used from timer functions.. so use a workq */
	fusion.workq = create_singlethread_workqueue("tp_work");
	if (fusion.workq == NULL) {
		printk(KERN_ERR "driver %s, can't create work queue 1\n", drvname);
		ret = -ENOMEM;
		goto bail1;
	}

	/* Create a worker thread to periodically check for tp hang */
	fusion.dev_check_q = create_singlethread_workqueue("tp_dev_check");
	if (fusion.dev_check_q == NULL) {
		printk(KERN_ERR "driver %s, can't create work queue 2\n", drvname);
		ret = -ENOMEM;
		goto bail2;
	}

    /* initialize timer used for recovery reset of touch controller */
    init_timer(&fusion.reset_timer);
    fusion.reset_timer.data     = (unsigned long)(&fusion);
    fusion.reset_timer.function = reset_func;

    /* initialize timer used for checking for device hang */
    init_timer(&fusion.dev_check_timer);
    fusion.dev_check_timer.data     = (unsigned long)(&fusion);
    fusion.dev_check_timer.function = dev_check_func;

    /* map gpio registers */
    immr_base = get_immrbase();
    gpio = (gpio_regs *)ioremap((immr_base + GPIO0_OFFSET), 0x80);
    printk("IMMR 0x%x gpio 0x%X\n", immr_base, (u32)gpio);

    /* map FPGA memory to have access to reset register of touch controller */
    reset_ptr = (unsigned char *)ioremap(FPGA_REG_ADDR, 0x10);
    printk("FPGA Regs: 0x%X\n", (unsigned int)reset_ptr);

	/* Probe for Fusion on I2C. */
	ret = i2c_add_driver(&fusion_i2c_drv);
	if (ret < 0) {
		printk(KERN_ERR "driver %s, cant add i2c driver: %d\n", drvname, ret);
		goto bail2;
	}
    // create a char device for keeping track of touch events from userland
    if (alloc_chrdev_region(&chrDev.tpDevNo, 0, 1, "fusion_tpdev") < 0)
    {
        ret = -ENODEV;
        goto bail2;
    }
    /* register with kernel */
    cdev_init(&chrDev.tpDev, &fusion_tpdev_fops);
    if (cdev_add(&chrDev.tpDev, chrDev.tpDevNo, 1) < 0)
    {
        ret = -ENODEV;
        goto bail3;
    }
    // get hang check timer going after 10 seconds..
    next_reset_jiffy = last_intr_jiffy = jiffies;
    fusion.dev_check_timer.expires = (jiffies + msecs_to_jiffies(10000));
    add_timer(&(fusion.dev_check_timer));
	return 0;

bail3:
    unregister_chrdev_region(chrDev.tpDevNo, 1);
	destroy_workqueue(fusion.dev_check_q);
bail2:
	destroy_workqueue(fusion.workq);
bail1:
	return ret;
}

static void __exit fusion_exit( void )
{
    msleep(100);
    del_timer_sync(&fusion.reset_timer);
    del_timer_sync(&fusion.dev_check_timer);
	destroy_workqueue(fusion.workq);
	destroy_workqueue(fusion.dev_check_q);
	i2c_del_driver(&fusion_i2c_drv);
    unregister_chrdev_region(chrDev.tpDevNo, 1);
}
module_init(fusion_init);
module_exit(fusion_exit);

MODULE_DESCRIPTION("Fusion Touchscreen Driver");
MODULE_LICENSE("GPL");

