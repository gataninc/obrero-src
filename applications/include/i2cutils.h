#ifndef __I2C_UTILS_H
#define __I2C_UTILS_H

int i2cmux_select_chan(int chan_no);
int getMacAddress(uint8_t *macAddr);
int setMacAddress(uint8_t *macAddr);

#define I2C_MUX_ADDR         0x77
#define USB_HUB_ADDR         0x2C
#define TEMP_SENSOR_ADDR     0x4C
#define I2C_EEPROM_ADDR      0x50
#define UCD_9081_ADDR        0x61

#define I2C_MUX_BUS          2

#define EEPROM_MUX_CHAN      8
#define TMP421_MUX_CHAN      8
#define UCD_9081_CHAN        8
#define USB_HUB_MUX_CHAN     6
#define MCP3424_CHAN         2

#define SIZE_M24C0X    256   // bytes, size of ID eeprom.

// Length of manufacturing info fields
#define LEN_COMPANY_NAME   16
#define LEN_PART_NO        16
#define LEN_BOARD_REV      8
#define LEN_BOARD_ENG_REV  8
#define LEN_BOARD_SERIAL   16
#define LEN_PROD_NAME      16
#define LEN_PROD_MODEL     16
#define LEN_PROD_SERIAL    16
#define LEN_ASSM_PART_NO   16
#define LEN_ASSM_REV       8
#define LEN_ASSM_ENG_REV   8
#define LEN_ASSM_SERIAL    16
#define LEN_EXP_FIELD1     16
#define LEN_EXP_FIELD2     16
#define LEN_CSUM           1
#define MFG_DATA_SIZE      \
    ((LEN_MFG_LONG_ENTRY * 8) + (LEN_MFG_SHORT_ENTRY * 4))

#define LEN_MFG_LONG_ENTRY  (16)
#define LEN_MFG_SHORT_ENTRY (8)

#define EEPROM_OFFSET_PCA  (0)

/* PCA: company name, part no, 2x revs, serial no */
#define EEPROM_OFFSET_PROD  (EEPROM_OFFSET_PCA + \
                ((LEN_MFG_LONG_ENTRY * 3) + (LEN_MFG_SHORT_ENTRY * 2)))

/* PRODUCT: prod. name, prod. mode, prod. serial */
#define EEPROM_OFFSET_ASSM  (EEPROM_OFFSET_PROD + ((LEN_MFG_LONG_ENTRY * 3)))

// MFG INFO: All fields filled with leading spaces.
// NOTICE NOTICE
// Fields are not written in the suggested order because only PCA 
// information was implemented first and then product and then assembly..
// END NOTICE NOTICE
// Fields are stored in eeprom in the following order
// one extra byte for null termination
typedef struct
{
    char mfgCompanyName[LEN_MFG_LONG_ENTRY+1];
    char mfgBrdPartNo[LEN_MFG_LONG_ENTRY+1];
    char mfgBrdRev[LEN_MFG_SHORT_ENTRY+1];
    char mfgBrdEngRev[LEN_MFG_SHORT_ENTRY+1];
    char mfgBrdSerial[LEN_MFG_LONG_ENTRY+1];
    char mfgProdName[LEN_MFG_LONG_ENTRY+1];
    char mfgProdModel[LEN_MFG_LONG_ENTRY+1];
    char mfgProdSerial[LEN_MFG_LONG_ENTRY+1];
    char mfgAssmPartNo[LEN_MFG_LONG_ENTRY+1];
    char mfgAssmRev[LEN_MFG_SHORT_ENTRY+1];
    char mfgAssmEngRev[LEN_MFG_SHORT_ENTRY+1];
    char mfgAssmSerial[LEN_MFG_LONG_ENTRY+1];
} ObreroMfgInfo;

/* board temp sensor */
typedef enum
{
    INTERNAL = 0,
    REMOTE1  = 1,
    REMOTE2  = 2
} temp_channel;

// UCD9081
#define UCD9081_USRCFG_ADDR     0x1080
#define UCD9081_USRCFG_SIZE     128
#define UCD9081_DEVCFG_ADDR     0xE000
#define UCD9081_DEVCFG_SIZE     512
#define UCD9081_NUM_RAIL_VOLTS  8

int init_temp_sensor(int bus, int devAddr);
int read_temperature(int bus, int devAddr, double *pTemp, temp_channel channel);
int read_raw_temperature(int bus, int devAddr, temp_channel chan, unsigned char *hiByte);
int set_correction_factor(int bus, int devAddr, unsigned char factor, temp_channel channel);
int get_sensor_id(int bus, int devAddr, unsigned char *pvendor, unsigned char *pdevice);
int readMfgData(uint32_t offset, ObreroMfgInfo *mfgInfo);
int writeMfgData(uint32_t offset, char *company, char *partNo, char *rev, char *engRev, char *serial);
int writeMfgData2(int offset, char *prodName, char *prodModel, 
                                                        char *prodSerial);
int setLCDBrightness(int bus, int devAddr, int bVal);
// UCD9081
int read_ucd9081_version(int bus, int devAddr, uint8_t *ver);
int read_ucd9081_status(int bus, int devAddr, uint8_t *status);
int read_ucd9081_railstatus(int bus, int devAddr, uint8_t *rail1, uint8_t *rail2);
int read_ucd9081_railvoltages(int bus, int devAddr, uint8_t *voltages);
int ucd9081_flashlog_exists(int bus, int devAddr);
int read_ucd9081_logs(int bus, int devAddr, uint8_t err_buf[]);

#endif //__I2C_UTILS_H
