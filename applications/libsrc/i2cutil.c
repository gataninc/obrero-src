/*************************************************
 *
 * Author: Roice Joseph, based on Ciro's i2c-lib
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <syslog.h>
#include <i2c-dev.h>
#include "bing_common.h"
#include "utils.h"
#include "i2cutils.h"
#include "eeprom.h"

/* Bing board EEPROM device is M24C02, a 2Kbit device from STMicro. 
   The temperature monitor is TMP421 from TI */

#define EEPROM_MAC_OFFSET    (SIZE_M24C0X-12) /* keep two copies? of MAC */
/*
 * Program i2c mux chip so that it selects devices on the specified channel 
 * for subsequent access.
 * Bus and channel are 1 based indices.
 */
int i2cmux_select_chan(int chan_no)
{
    int hI2C;
    int i2c_bus = 2;
    struct i2c_msg message;
    struct i2c_rdwr_ioctl_data ctrl;
    unsigned char v_reg[1];

    if ((chan_no < 1) || (chan_no > MAX_MUX_CHAN))
        return -1;
    if ((hI2C = open(get_i2c_bus(i2c_bus), O_RDWR)) < 0)
    {
        perror("open");
        return -2;
    }
    /* set-up ioctl parameters */
    ctrl.msgs  = &message;
    ctrl.nmsgs = 1;

    message.addr   = I2C_MUX_ADDR;
    message.flags  = 0;
    message.len    = 1;
    message.buf    = (char *)&v_reg[0];
    v_reg[0]       = 1 << (chan_no -1);
    if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        close(hI2C);
        return -3;
    }
    /* temporary: read back enabled channel and print info */
#if 0
    message.flags  = I2C_M_RD;
    message.len    = 1;
    if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
        perror("read ioctl");
    fprintf(stderr, "mux: reg 0x%x\n", (uint32_t)v_reg[0]);
#endif
    close(hI2C);
    return 0;
}

int getMacAddress(uint8_t *macAddr)
{
    int ret = 0;
    if ((ret = i2cmux_select_chan(EEPROM_MUX_CHAN)) != 0)
    {
        logPrint("Error mux_select_chan(ch %d), %d\n", EEPROM_MUX_CHAN, ret);
        return ret;
    }
    ret = eeprom_read_m24c0x(I2C_MUX_BUS, I2C_EEPROM_ADDR, EEPROM_MAC_OFFSET, 
                             EPROM_MAC_ADDR_LEN, macAddr);
    if (ret != EPROM_MAC_ADDR_LEN)
        return ret;
    return 0;
}

int setMacAddress(uint8_t *macAddr)
{
    int ret = 0;
    if ((ret = i2cmux_select_chan(EEPROM_MUX_CHAN)) != 0)
    {
        logPrint("Error mux_select_chan(ch %d), %d\n", EEPROM_MUX_CHAN, ret);
        return ret;
    }
    ret = eeprom_write_m24c0x(I2C_MUX_BUS, I2C_EEPROM_ADDR, EEPROM_MAC_OFFSET,
                              EPROM_MAC_ADDR_LEN, macAddr);
    if (ret != EPROM_MAC_ADDR_LEN)
        return ret;
    return 0;
}

/*
 * Read mfg data(assembly no, revision and serial number) from specified eeprom.
 * parameter dataBuf must be a buffer of size MFG_DATA_SIZE.
 */
static int readMfgDataFromEprom(int bus, int devAddr, uint32_t offset, unsigned char dataBuf[])
{
    int count, err;
    uint8_t cSum = 0;
    if ((offset + MFG_DATA_SIZE) >= SIZE_M24C0X)
    {
        fprintf(stderr, "Error: %d greater than eeprom size\n", (offset+MFG_DATA_SIZE));
        return -1;
    }
    if ((err = eeprom_read_m24c0x(bus, devAddr, 0, MFG_DATA_SIZE, dataBuf)) != 
            MFG_DATA_SIZE)
    {
        fprintf(stderr, "Error reading eeprom(0x%X), err %d\n", devAddr, err);
        return err;
    }
    //prettyPrint(dataBuf, MFG_DATA_SIZE);
#if 0 // no checksums now..
    for (count = 0; count < (MFG_DATA_SIZE-1); count++)
        cSum += dataBuf[count];
    if (cSum != dataBuf[MFG_DATA_SIZE-1])
        fprintf(stderr, "Csum Error! %X != %X\n", cSum, dataBuf[MFG_DATA_SIZE-2]);
#endif
    return 0;
}

int readMfgData(uint32_t offset, ObreroMfgInfo *mfgInfo)
{
    uint8_t buffer[MFG_DATA_SIZE];
    int ret, i, offs = 0;
    if ((ret = i2cmux_select_chan(EEPROM_MUX_CHAN)) != 0)
    {
        logPrint("Error mux_select_chan(ch %d), %d\n", EEPROM_MUX_CHAN, ret);
        return ret;
    }
    if ((ret = readMfgDataFromEprom(I2C_MUX_BUS, I2C_EEPROM_ADDR, offset, &buffer[0])) != 0)
        return ret;
    // remove un-printable chars..
    for (i = 0; i < MFG_DATA_SIZE; i++)
    {
        if ((buffer[i] < ' ') || (buffer[i] > 126))
            buffer[i] = '.';
    }
    // format data
    offs = 0;
    memcpy(mfgInfo->mfgCompanyName, &buffer[offs], LEN_MFG_LONG_ENTRY);
    mfgInfo->mfgCompanyName[LEN_MFG_LONG_ENTRY] = '\0';
    offs += LEN_MFG_LONG_ENTRY;

    memcpy(mfgInfo->mfgBrdPartNo, &buffer[offs], LEN_MFG_LONG_ENTRY);
    mfgInfo->mfgBrdPartNo[LEN_MFG_LONG_ENTRY] = '\0';
    offs += LEN_MFG_LONG_ENTRY;

    memcpy(mfgInfo->mfgBrdRev, &buffer[offs], LEN_MFG_SHORT_ENTRY);
    mfgInfo->mfgBrdRev[LEN_MFG_SHORT_ENTRY] = '\0';
    offs += LEN_MFG_SHORT_ENTRY;

    memcpy(mfgInfo->mfgBrdEngRev, &buffer[offs], LEN_MFG_SHORT_ENTRY);
    mfgInfo->mfgBrdEngRev[LEN_MFG_SHORT_ENTRY] = '\0';
    offs += LEN_MFG_SHORT_ENTRY;

    memcpy(mfgInfo->mfgBrdSerial, &buffer[offs], LEN_MFG_LONG_ENTRY);
    mfgInfo->mfgBrdSerial[LEN_MFG_LONG_ENTRY] = '\0';
    offs += LEN_MFG_LONG_ENTRY;

    memcpy(mfgInfo->mfgProdName, &buffer[offs], LEN_MFG_LONG_ENTRY);
    mfgInfo->mfgProdName[LEN_MFG_LONG_ENTRY] = '\0';
    offs += LEN_MFG_LONG_ENTRY;

    memcpy(mfgInfo->mfgProdModel, &buffer[offs], LEN_MFG_LONG_ENTRY);
    mfgInfo->mfgProdModel[LEN_MFG_LONG_ENTRY] = '\0';
    offs += LEN_MFG_LONG_ENTRY;

    memcpy(mfgInfo->mfgProdSerial, &buffer[offs], LEN_MFG_LONG_ENTRY);
    mfgInfo->mfgProdSerial[LEN_MFG_LONG_ENTRY] = '\0';
    offs += LEN_MFG_LONG_ENTRY;

    memcpy(mfgInfo->mfgAssmPartNo, &buffer[offs], LEN_MFG_LONG_ENTRY);
    mfgInfo->mfgAssmPartNo[LEN_MFG_LONG_ENTRY] = '\0';
    offs += LEN_MFG_LONG_ENTRY;

    memcpy(mfgInfo->mfgAssmRev, &buffer[offs], LEN_MFG_SHORT_ENTRY);
    mfgInfo->mfgAssmRev[LEN_MFG_SHORT_ENTRY] = '\0';
    offs += LEN_MFG_SHORT_ENTRY;

    memcpy(mfgInfo->mfgAssmEngRev, &buffer[offs], LEN_MFG_SHORT_ENTRY);
    mfgInfo->mfgAssmEngRev[LEN_MFG_SHORT_ENTRY] = '\0';
    offs += LEN_MFG_SHORT_ENTRY;

    memcpy(mfgInfo->mfgAssmSerial, &buffer[offs], LEN_MFG_LONG_ENTRY);
    mfgInfo->mfgAssmPartNo[LEN_MFG_LONG_ENTRY] = '\0';
    offs += LEN_MFG_LONG_ENTRY;

    return 0;
}

static int writeMfgDataToEprom(int bus, int devAddr, uint32_t offset, unsigned char dataBuf[])
{
    uint8_t cSum = 0;
    int err, i;
    /* compute check sum */
    // Always write MFG DATA Size
    for (i = 0; i < (MFG_DATA_SIZE-1); i++) // exclude cksum
        cSum += dataBuf[i];
    /* Append checksum to end of buffer */
    dataBuf[MFG_DATA_SIZE-1] = cSum;
    fprintf(stderr, "Update mfg data\n");
    if ((err = eeprom_write_m24c0x(bus, devAddr, offset, MFG_DATA_SIZE, 
                                                dataBuf)) != MFG_DATA_SIZE)
    {
        fprintf(stderr, "Error updating eeprom(0x%X), err %d\n", devAddr, err);
        return err;
    }
    return 0;
}

/*
 *  NEW EEPROM FORMAT
 *  Company Name     :  16 Bytes: filled with leading ASCII space character
 *  Board P/N        :  16 Bytes: filled with leading ASCII space character
 *  Board Rev        :   8 Bytes: filled with leading ASCII space character
 *  Board Eng Rev    :   8 Bytes: filled with leading ASCII space character
 *  Board S/N        :  16 Bytes: filled with leading ASCII space character
 *  Product Name     :  16 Bytes: filled with leading ASCII space character
 *  Product Model    :  16 Bytes: filled with leading ASCII space character
 *  Product Model    :  16 Bytes: filled with leading ASCII space character
 *  Expansion Field 1:  16 Bytes: filled with leading ASCII space character
 *  Expansion Field 2:  16 Bytes: filled with leading ASCII space character
 *
 *  Product Name and Product Model values are not filled in by this routine.
 *  Expansion fields not used.
 */
int writeMfgData(uint32_t offset, char *company, char *partNo, char *rev,
                        char *engRev, char *serial)
{
    char buffer[MFG_DATA_SIZE+1]; /* one extra for null termination */
    int ret, offs, len;
    /* format strings to match eeprom format */
    if ((!company) || strlen(company) >= LEN_COMPANY_NAME)
    {
        fprintf(stderr, "Error: incorrect length for company\n");
        return -1;
    }
    if ((!partNo) || strlen(partNo) >= LEN_PART_NO)
    {
        fprintf(stderr, "Error: incorrect length for Part No.\n");
        return -1;
    }
    if (partNo[4] != '.') /* validate format */
    {
        fprintf(stderr, "Error: incorrect format for Part No.\n");
        return -1;
    }
    if ((!rev) || strlen(rev) >= LEN_BOARD_REV)
    {
        fprintf(stderr, "Error: incorrect length for board rev.\n");
        return -1;
    }
    if ((!engRev) || strlen(engRev) >= LEN_BOARD_ENG_REV)
    {
        fprintf(stderr, "Error: incorrect length for board eng. rev.\n");
        return -1;
    }
    if ((!serial) || strlen(serial) >= LEN_BOARD_SERIAL)
    {
        fprintf(stderr, "Error: incorrect length for serial number\n");
        return -1;
    }
    offs = 0;
    len = sprintf(&(buffer[offs]), "%16s", company);
    offs += len;
    len = sprintf(&(buffer[offs]), "%16s", partNo);
    offs += len;
    len = sprintf(&(buffer[offs]), "%8s", rev);
    offs += len;
    len = sprintf(&(buffer[offs]), "%8s", engRev);
    offs += len;
    len = sprintf(&(buffer[offs]), "%16s", serial);
    offs += len;
    if ((offset+offs) > MFG_DATA_SIZE)
        { printf("Error: MFG total len = %d\n", len); fflush(stdout); }
    { printf(":%s:\n", buffer); fflush(stdout); }
    if ((ret = i2cmux_select_chan(EEPROM_MUX_CHAN)) != 0)
    {
        logPrint("Error mux_select_chan(ch %d), %d\n", EEPROM_MUX_CHAN, ret);
        return ret;
    }
    if ((ret = eeprom_write_m24c0x(I2C_MUX_BUS, I2C_EEPROM_ADDR, offset, offs, 
                                        (unsigned char *)buffer)) != offs)
    {
        fprintf(stderr, "Error updating eeprom(0x%X), err %d\n", 
                                                I2C_EEPROM_ADDR, ret);
        return ret;
    }
    return 0;
}

int writeMfgData2(int offset, char *prodName, char *prodModel, 
                                                        char *prodSerial)
{
    char buffer[MFG_DATA_SIZE+1]; /* one extra for null termination */
    int ret, offs, len;
    /* format strings to match eeprom format */
    if ((!prodName) || strlen(prodName) >= LEN_PROD_NAME)
    {
        fprintf(stderr, "Error: incorrect length for product name.\n");
        return -1;
    }
    if ((!prodModel) || strlen(prodModel) >= LEN_PROD_MODEL)
    {
        fprintf(stderr, "Error: incorrect length for product model.\n");
        return -1;
    }
    if ((!prodSerial) || strlen(prodSerial) >= LEN_PROD_SERIAL)
    {
        fprintf(stderr, "Error: incorrect length for product serial.\n");
        return -1;
    }
    offs = 0;
    len = sprintf(&(buffer[offs]), "%16s", prodName);
    offs += len;
    len = sprintf(&(buffer[offs]), "%16s", prodModel);
    offs += len;
    len = sprintf(&(buffer[offs]), "%16s", prodSerial);
    offs += len;
    if ((offset+offs) > MFG_DATA_SIZE)
        { printf("Error: MFG total len = %d\n", len); fflush(stdout); }
    { printf(":%s:\n", buffer); fflush(stdout); }
    if ((ret = i2cmux_select_chan(EEPROM_MUX_CHAN)) != 0)
    {
        logPrint("Error mux_select_chan(ch %d), %d\n", EEPROM_MUX_CHAN, ret);
        return ret;
    }
    if ((ret = eeprom_write_m24c0x(I2C_MUX_BUS, I2C_EEPROM_ADDR, offset, offs, 
                                        (unsigned char *)buffer)) != offs)
    {
        fprintf(stderr, "Error updating eeprom(0x%X), err %d\n", 
                                                I2C_EEPROM_ADDR, ret);
        return ret;
    }
    return 0;
}

int writeMfgData3(int offset, char *assmPartNo, char *rev, char *engRev, 
                                    char *assmSerial) 
{
    char buffer[MFG_DATA_SIZE+1]; /* one extra for null termination */
    int ret, offs, len;
    /* format strings to match eeprom format */
    if ((!assmPartNo) || strlen(assmPartNo) >= LEN_ASSM_PART_NO)
    {
        fprintf(stderr, "Error: incorrect length for part number.\n");
        return -1;
    }
    if ((!rev) || strlen(rev) >= LEN_ASSM_REV)
    {
        fprintf(stderr, "Error: incorrect length for revision.\n");
        return -1;
    }
    if ((!engRev) || strlen(engRev) >= LEN_ASSM_ENG_REV)
    {
        fprintf(stderr, "Error: incorrect length for eng. revision.\n");
        return -1;
    }
    if ((!assmSerial) || strlen(assmSerial) >= LEN_ASSM_SERIAL)
    {
        fprintf(stderr, "Error: incorrect length for serial number.\n");
        return -1;
    }
    offs = 0;
    len = sprintf(&(buffer[offs]), "%16s", assmPartNo);
    offs += len;
    len = sprintf(&(buffer[offs]), "%8s", rev);
    offs += len;
    len = sprintf(&(buffer[offs]), "%8s", engRev);
    offs += len;
    len = sprintf(&(buffer[offs]), "%16s", assmSerial);
    offs += len;
    if ((offset+offs) > MFG_DATA_SIZE)
        { printf("Error: MFG total len = %d\n", len); fflush(stdout); }
    { printf(":%s:\n", buffer); fflush(stdout); }
    if ((ret = i2cmux_select_chan(EEPROM_MUX_CHAN)) != 0)
    {
        logPrint("Error mux_select_chan(ch %d), %d\n", EEPROM_MUX_CHAN, ret);
        return ret;
    }
    if ((ret = eeprom_write_m24c0x(I2C_MUX_BUS, I2C_EEPROM_ADDR, offset, offs, 
                                        (unsigned char *)buffer)) != offs)
    {
        fprintf(stderr, "Error updating eeprom(0x%X), err %d\n", 
                                                I2C_EEPROM_ADDR, ret);
        return ret;
    }
    return 0;
}

// write to LTC2626 device, 12bit single channel DAC
// bVal must be between 0 and 0xFFF
int setLCDBrightness(int bus, int devAddr, int bVal)
{
    int hI2C;
    struct i2c_msg message;
    struct i2c_rdwr_ioctl_data ctrl;
    unsigned char v_reg[3];

    if ((hI2C = open(get_i2c_bus(bus), O_RDWR)) < 0)
    {
        perror("open");
        return -2;
    }
    /* set-up ioctl parameters */
    ctrl.msgs  = &message;
    ctrl.nmsgs = 1;

    message.addr   = devAddr;
    message.flags  = 0;
    message.len    = 3;
    message.buf    = (char *)&v_reg[0];
    bVal &= 0xFFF;
    v_reg[0] = 0x30;
    v_reg[1] = ((bVal & 0xf00) >> 4) | ((bVal & 0xf0) >> 4);
    v_reg[2] = ((bVal & 0x0f) << 4);
printf("0x%x 0x%X 0x%X\n", v_reg[0], v_reg[1], v_reg[2]);fflush(stdout);
    if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        close(hI2C);
        return -3;
    }
    close(hI2C);
    return 0;
}
