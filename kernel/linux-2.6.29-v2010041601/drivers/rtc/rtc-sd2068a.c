/*
 * rtc-ds1307.c - RTC driver for some mostly-compatible I2C chips.
 *
 *  Copyright (C) 2005 James Chapman (ds1337 core)
 *  Copyright (C) 2006 David Brownell
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/i2c.h>
#include <linux/string.h>
#include <linux/rtc.h>
#include <linux/bcd.h>



/* We can't determine type by probing, but if we expect pre-Linux code
 * to have set the chip up as a clock (turning on the oscillator and
 * setting the date and time), Linux can ignore the non-clock features.
 * That's a natural job for a factory or repair bench.
 *
 * This is currently a simple no-alarms driver.  If your board has the
 * alarm irq wired up on a ds1337 or ds1339, and you want to use that,
 * then look at the rtc-rs5c372 driver for code to steal...
 */

#define	BCD2BIN(val)	bcd2bin(val)
#define	BIN2BCD(val)	bin2bcd(val)
static int sd2068_uie_on_off(struct device *dev,unsigned char enable);
#define		ENABLE_ALARM_INT		0
#if 1
#define	RTC_DS1307_DEBUG()
#else
#define	RTC_DS1307_DEBUG()	printk("%s() line:%d\n",__FUNCTION__,__LINE__)
#endif
/* RTC registers don't differ much, except for the century flag */
enum{
SD2608_REG_SEC=0x00,
SD2608_REG_MIN,
SD2608_REG_HOUR,
SD2608_REG_DAY_OF_WEEK,
SD2608_REG_DAY_OF_MONTH,
SD2608_REG_MONTH,
SD2608_REG_YEAR
};
#define	SD2068_HOUR_12_24_CONTROL	0x80
#define	SD2068_CTR2_WRTC1			0x80
#define	SD2068_CTR1_WRTC2_WRTC3		0x84

/* Other registers (control, status, alarms, trickle charge, NVRAM, etc)
 * start at 7, and they differ a LOT. Only control and status matter for
 * basic RTC date and time functionality; be careful using them.
 */



struct sd2068_struct {
	u8			reg_addr;
	bool			has_nvram;
	u8			regs[8];
	u8 	msb_buf[8];
	u8	ctrl_reg[4];
	//enum ds_type		type;
	struct i2c_msg		msg[4];
	struct i2c_client	*client;
	struct i2c_client	dev;
	struct rtc_device	*rtc;
};
/*

struct chip_desc {
	char			name[9];
	unsigned		nvram56:1;
	unsigned		alarm:1;
	enum ds_type		type;
};

static const struct chip_desc chips[] = { {
	.name		= "ds1307",
	.type		= ds_1307,
	.nvram56	= 1,
}, {
	.name		= "ds1337",
	.type		= ds_1337,
	.alarm		= 1,
}, {
	.name		= "ds1338",
	.type		= ds_1338,
	.nvram56	= 1,
}, {
	.name		= "ds1339",
	.type		= ds_1339,
	.alarm		= 1,
}, {
	.name		= "ds1340",
	.type		= ds_1340,
}, {
	.name		= "m41t00",
	.type		= m41t00,
}, };

static inline const struct chip_desc *find_chip(const char *s)
{
	unsigned i;

	for (i = 0; i < ARRAY_SIZE(chips); i++)
		if (strnicmp(s, chips[i].name, sizeof chips[i].name) == 0)
			return &chips[i];
	return NULL;
}
*/
static int sd2068_get_time(struct device *dev, struct rtc_time *t)
{
	struct sd2068_struct *sd2068 = dev_get_drvdata(dev);
	unsigned char		tmp;

	/* read the RTC date and time registers all at once */
	sd2068->msg[1].flags = I2C_M_RD;
	sd2068->msg[1].len = 7;

	tmp = i2c_transfer(to_i2c_adapter(sd2068->client->dev.parent),
			sd2068->msg, 2);
	if (tmp != 2) {
		dev_err(dev, "%s error %d\n", "read", tmp);
		return -EIO;
	}
/*
	printk("%s:%02x %02x %02x %02x %02x %02x %02x\n",
			"read",
			sd2068->regs[0], sd2068->regs[1],
			sd2068->regs[2], sd2068->regs[3],
			sd2068->regs[4], sd2068->regs[5],
			sd2068->regs[6]);
*/
	t->tm_sec = BCD2BIN(sd2068->regs[SD2608_REG_SEC] & 0x7f);
	t->tm_min = BCD2BIN(sd2068->regs[SD2608_REG_MIN] & 0x7f);
	tmp = sd2068->regs[SD2608_REG_HOUR] & 0x3f;
	t->tm_hour =BCD2BIN (tmp&(~SD2068_HOUR_12_24_CONTROL));
	t->tm_wday = BCD2BIN(sd2068->regs[SD2608_REG_DAY_OF_WEEK] & 0x07) ;
	t->tm_mday =BCD2BIN (sd2068->regs[SD2608_REG_DAY_OF_MONTH] & 0x3f);
	tmp = sd2068->regs[SD2608_REG_MONTH] & 0x1f;
	t->tm_mon =BCD2BIN (tmp);

	
	t->tm_year = BCD2BIN(sd2068->regs[SD2608_REG_YEAR])+2000-1900;
#if 0
	printk(KERN_ERR "%s:secs=%d, mins=%d, "
		"hours=%d, mday=%d, mon=%d, year=%d, wday=%d\n",
		"read", t->tm_sec, t->tm_min,
		t->tm_hour, t->tm_mday,
		t->tm_mon, t->tm_year, t->tm_wday);
#endif
	/* initial clock setting can be undefined */
	return rtc_valid_tm(t);
}

static int sd2068_set_time(struct device *dev, struct rtc_time *t)
{
	struct sd2068_struct *sd2068 = dev_get_drvdata(dev);
	int		result;
	int		tmp;
	u8		*buf = sd2068->regs;
#if 0
	printk(KERN_ERR"%s:secs=%d, mins=%d, "
		"hours=%d, mday=%d, mon=%d, year=%d, wday=%d\n",
		"write", t->tm_sec, t->tm_min,
		t->tm_hour, t->tm_mday,
		t->tm_mon, t->tm_year, t->tm_wday);
#endif
	*buf++ = 0;		/* first register addr */
	buf[SD2608_REG_SEC] = BIN2BCD(t->tm_sec);
	buf[SD2608_REG_MIN] =BIN2BCD (t->tm_min);
	buf[SD2608_REG_HOUR] = BIN2BCD(t->tm_hour)|SD2068_HOUR_12_24_CONTROL;
	buf[SD2608_REG_DAY_OF_WEEK] = BIN2BCD(t->tm_wday);
	buf[SD2608_REG_DAY_OF_MONTH] = BIN2BCD(t->tm_mday);
	buf[SD2608_REG_MONTH] = BIN2BCD(t->tm_mon );

	/* assume 20YY not 19YY */
	if((t->tm_year+1900>2100)||(t->tm_year+1900<2000))
	{
		printk(KERN_ERR"rtc range is from 2000 to 2100.\n");
		return -1;
	}
	tmp = t->tm_year+1900-2000;
	buf[SD2608_REG_YEAR] =BIN2BCD (tmp);

	
	sd2068->msg[1].flags = 0;
	sd2068->msg[1].len = 8;

	dev_dbg(dev, "%s: %02x %02x %02x %02x %02x %02x %02x\n",
		"write", buf[0], buf[1], buf[2], buf[3],
		buf[4], buf[5], buf[6]);
	sd2068_uie_on_off(dev,1);
	result = i2c_transfer(to_i2c_adapter(sd2068->client->dev.parent),
			&sd2068->msg[1], 1);
	sd2068_uie_on_off(dev,0);
	if (result != 1) {
		dev_err(dev, "%s error %d\n", "write", tmp);
		return -EIO;
	}
	return 0;
}

/*********
0:disable
1:enable
*************/
static int sd2068_uie_on_off(struct device *dev,unsigned char enable)
{
	struct sd2068_struct *sd2068 = dev_get_drvdata(dev);
	int tmp;
	u8	buf[2];
	sd2068->msg[2].flags = 0;
	sd2068->msg[2].len = 1;	
	sd2068->msg[2].buf[0]=0x0f;
	
	sd2068->msg[3].flags = I2C_M_RD;
	sd2068->msg[3].len = 2;
	tmp = i2c_transfer(to_i2c_adapter(sd2068->client->dev.parent),
			&sd2068->msg[2], 2);
	if(tmp!=2)
	{
		printk(KERN_ERR"%s() %d read failed.\n",__FUNCTION__,__LINE__);
		return -1;
	}
	buf[0]=sd2068->msg[3].buf[0];
	buf[1]=sd2068->msg[3].buf[1];
	//printk("buf[0]=0x%02x buf[1]=0x%02x\n",buf[0],buf[1]);
	sd2068->msg[3].flags = 0;
	sd2068->msg[3].len = 2;	
	if(enable)
	{		
		sd2068->msg[3].buf[0]=0x10;
		sd2068->msg[3].buf[1]=buf[1]|SD2068_CTR2_WRTC1;	
		tmp = i2c_transfer(to_i2c_adapter(sd2068->client->dev.parent),
			&sd2068->msg[3], 1);
		RTC_DS1307_DEBUG();
		if(tmp!=1)
		{
			goto failed;
		}
		sd2068->msg[3].buf[0]=0x0f;
		sd2068->msg[3].buf[1]=buf[0]|SD2068_CTR1_WRTC2_WRTC3;
		tmp = i2c_transfer(to_i2c_adapter(sd2068->client->dev.parent),
			&sd2068->msg[3], 1);
		
		if(tmp!=1)
		{
			goto failed;
		}
		RTC_DS1307_DEBUG();
	}
	else
	{
		sd2068->msg[3].buf[0]=0x0f;
		sd2068->msg[3].buf[1]=buf[0]&(~SD2068_CTR1_WRTC2_WRTC3);
		tmp = i2c_transfer(to_i2c_adapter(sd2068->client->dev.parent),
			&sd2068->msg[3], 1);
		RTC_DS1307_DEBUG();
		if(tmp!=1)
		{
			goto failed;
		}
		RTC_DS1307_DEBUG();
		sd2068->msg[3].buf[0]=0x10;
		sd2068->msg[3].buf[1]=buf[1]&(~SD2068_CTR2_WRTC1);	
		tmp = i2c_transfer(to_i2c_adapter(sd2068->client->dev.parent),
			&sd2068->msg[3], 1);
		if(tmp!=1)
		{
			goto failed;
		}
		RTC_DS1307_DEBUG();
	}

	return 0;
failed:
	printk(KERN_ERR"%s() %d read failed.\n",__FUNCTION__,__LINE__);
		return -1;
	
	
}

static int sd2068_rtc_ioctl(struct device *dev, unsigned int cmd,
			     unsigned long arg)
{
	struct sd2068_struct *sd2068 = dev_get_drvdata(dev);
	//struct mpc5121_rtc_regs __iomem *regs = rtc->regs;
	RTC_DS1307_DEBUG();
	//printk("cmd:0x%08x\n",cmd);
	switch (cmd) {
		/* alarm interrupt */
	case RTC_AIE_ON:
		//out_8(&regs->alm_enable, 1);
		//rtc->wkalarm.enabled = 1;
		break;
	case RTC_AIE_OFF:
		//out_8(&regs->alm_enable, 0);
		//rtc->wkalarm.enabled = 0;
		break;

		/* update interrupt */

	case RTC_UIE_ON:
		//RTC_DS1307_DEBUG();
		return -ENOTTY;
	case RTC_UIE_OFF:
		//out_8(&regs->int_enable, in_8(&regs->int_enable) & ~0x1);
		//RTC_DS1307_DEBUG();
		return -ENOTTY;
	case RTC_RD_TIME:
		RTC_DS1307_DEBUG();
		{
			struct rtc_time rtc_tm;
						
			memset(&rtc_tm, 0, sizeof (struct rtc_time));
			sd2068_get_time(dev,&rtc_tm);						
			if (copy_to_user((struct rtc_time*)arg, &rtc_tm, sizeof(struct rtc_time)))
				return -EFAULT;	
			return 0;
		}
		break;
	case RTC_SET_TIME:
		RTC_DS1307_DEBUG();
		{
			struct rtc_time rtc_tm;
			unsigned char mon, day, hrs, min, sec, leap_yr;
			unsigned int yrs;

			if (!capable(CAP_SYS_TIME))
				return -EPERM;

			if (copy_from_user(&rtc_tm, (struct rtc_time*)arg, sizeof(struct rtc_time)))
				return -EFAULT;
#if 0
		printk( "%s:secs=%d, mins=%d, "
		"hours=%d, mday=%d, mon=%d, year=%d, wday=%d\n",
		"write",rtc_tm.tm_sec, rtc_tm.tm_min,
		rtc_tm.tm_hour,rtc_tm.tm_mday,
		rtc_tm.tm_mon, rtc_tm.tm_year, rtc_tm.tm_wday);
#endif
			sd2068_set_time(dev,&rtc_tm);
			
			return 0;
		}
		break;
		
		/* no periodic interrupts */
	case RTC_IRQP_READ:
	case RTC_IRQP_SET:
		return -ENOTTY;

	default:
		return -ENOIOCTLCMD;
	}
	return 0;
}

static const struct rtc_class_ops sd2068_rtc_ops = {
	.read_time	= sd2068_get_time,
	.set_time	= sd2068_set_time,
	.ioctl = sd2068_rtc_ioctl,
};

/*----------------------------------------------------------------------*/

#define NVRAM_SIZE	56

/*----------------------------------------------------------------------*/

static struct i2c_driver sd2608_driver;
#if ENABLE_ALARM_INT
static int sd2068_set_alarm(struct device *dev,unsigned int irq)
{
	struct sd2068_struct *sd2068 = dev_get_drvdata(dev);
	//int		result;
	int		tmp;
	sd2068->msg[2].flags = 0;
	sd2068->msg[2].len = 1;	
	sd2068->msg[2].buf[0]=0x0e;
	
	sd2068->msg[3].flags = I2C_M_RD;
	sd2068->msg[3].len = 1;
	tmp = i2c_transfer(to_i2c_adapter(sd2068->client->dev.parent),
			&sd2068->msg[2], 2);
	if(tmp!=2)
	{
		printk(KERN_ERR"%s() %d read failed.\n",__FUNCTION__,__LINE__);
		return -1;
	}
	tmp=sd2068->msg[3].buf[0];	
	tmp|=irq;
	tmp&=0x7f;
	//tmp|=0x40;
	//tmp=(tmp)?(tmp|0x80):tmp;
	sd2068_uie_on_off(dev,1);
	sd2068->msg[2].flags = 0;
	sd2068->msg[2].len = 2;	
	sd2068->msg[2].buf[0]=0x0e;
	sd2068->msg[2].buf[1]=tmp;
	tmp = i2c_transfer(to_i2c_adapter(sd2068->client->dev.parent),
			&sd2068->msg[2], 1);
	if(tmp!=1)
	{
		printk(KERN_ERR"%s() %d read failed.\n",__FUNCTION__,__LINE__);
		goto setfailed;
	}

	sd2068->msg[2].flags = 0;
	sd2068->msg[2].len = 2;	
	sd2068->msg[2].buf[0]=0x07;
	sd2068->msg[2].buf[1]=0x20;
	tmp = i2c_transfer(to_i2c_adapter(sd2068->client->dev.parent),
			&sd2068->msg[2], 1);
	if(tmp!=1)
	{
		printk(KERN_ERR"%s() %d read failed.\n",__FUNCTION__,__LINE__);
		goto setfailed;
	}
	sd2068_uie_on_off(dev,0);

	sd2068->msg[2].flags = 0;
	sd2068->msg[2].len = 1;	
	sd2068->msg[2].buf[0]=0x0e;
	
	sd2068->msg[3].flags = I2C_M_RD;
	sd2068->msg[3].len = 1;
	tmp = i2c_transfer(to_i2c_adapter(sd2068->client->dev.parent),
			&sd2068->msg[2], 2);
	if(tmp!=2)
	{
		printk(KERN_ERR"%s() %d read failed.\n",__FUNCTION__,__LINE__);
		goto setfailed;
	}
	printk(KERN_ERR"%s() %d read [0x0e]=0x%02x\n",__FUNCTION__,__LINE__,sd2068->msg[3].buf[0]);

	sd2068->msg[2].flags = 0;
	sd2068->msg[2].len = 1;	
	sd2068->msg[2].buf[0]=0x07;
	
	sd2068->msg[3].flags = I2C_M_RD;
	sd2068->msg[3].len = 1;
	tmp = i2c_transfer(to_i2c_adapter(sd2068->client->dev.parent),
			&sd2068->msg[2], 2);
	if(tmp!=2)
	{
		printk(KERN_ERR"%s() %d read failed.\n",__FUNCTION__,__LINE__);
		goto setfailed;
	}
	printk(KERN_ERR"%s() %d read [0x07]=0x%02x\n",__FUNCTION__,__LINE__,sd2068->msg[3].buf[0]);
	return 0;
setfailed:
	sd2068_uie_on_off(dev,0);
	return -1;
}

static int sd2068_set_alarm_mode(struct device *dev,unsigned int mode)
{
	struct sd2068_struct *sd2068 = dev_get_drvdata(dev);
	//int		result;
	int		tmp;
	sd2068->msg[2].flags = 0;
	sd2068->msg[2].len = 1;	
	sd2068->msg[2].buf[0]=0x10;
	
	sd2068->msg[3].flags = I2C_M_RD;
	sd2068->msg[3].len = 1;
	tmp = i2c_transfer(to_i2c_adapter(sd2068->client->dev.parent),
			&sd2068->msg[2], 2);
	if(tmp!=2)
	{
		printk(KERN_ERR"%s() %d read failed.\n",__FUNCTION__,__LINE__);
		return -1;
	}
	tmp=sd2068->msg[3].buf[0];	
	tmp&=0xcf;
	tmp|=mode<<4;
	tmp|=0x40;
	tmp|=0x02;
	sd2068_uie_on_off(dev,1);
	sd2068->msg[2].flags = 0;
	sd2068->msg[2].len = 2;	
	sd2068->msg[2].buf[0]=0x10;
	sd2068->msg[2].buf[1]=tmp;
	tmp = i2c_transfer(to_i2c_adapter(sd2068->client->dev.parent),
			&sd2068->msg[2], 1);
	if(tmp!=1)
	{
		printk(KERN_ERR"%s() %d read failed.\n",__FUNCTION__,__LINE__);
		goto setfailed;
	}
	
	sd2068_uie_on_off(dev,0);
	sd2068->msg[2].flags = 0;
	sd2068->msg[2].len = 1;	
	sd2068->msg[2].buf[0]=0x10;
	
	sd2068->msg[3].flags = I2C_M_RD;
	sd2068->msg[3].len = 1;
	tmp = i2c_transfer(to_i2c_adapter(sd2068->client->dev.parent),
			&sd2068->msg[2], 2);
	if(tmp!=2)
	{
		printk(KERN_ERR"%s() %d read failed.\n",__FUNCTION__,__LINE__);
		goto setfailed;
	}
	printk(KERN_ERR"%s() %d read [0x10]=0x%02x\n",__FUNCTION__,__LINE__,sd2068->msg[3].buf[0]);
	return 0;
setfailed:
	sd2068_uie_on_off(dev,0);
	return -1;
}
#endif

static int __devinit sd2068_probe(struct i2c_client *client)
{
	struct sd2068_struct		*sd2068;
	int			err = -ENODEV;
	int			tmp;
	
	//const struct chip_desc	*chip;
	struct i2c_adapter	*adapter = to_i2c_adapter(client->dev.parent);
	RTC_DS1307_DEBUG();
/*	
	chip = find_chip(client->name);
	if (!chip) {
		dev_err(&client->dev, "unknown chip type '%s'\n",
				client->name);
		return -ENODEV;
	}
*/	
	/*printk("chip name:%s \n",client->name);*/
	RTC_DS1307_DEBUG();
#if 0
	if (!i2c_check_functionality(adapter,
			I2C_FUNC_I2C | I2C_FUNC_SMBUS_WRITE_BYTE_DATA))
		return -EIO;
#else
	if (!i2c_check_functionality(adapter,I2C_FUNC_I2C))
		return -EIO;
#endif
	RTC_DS1307_DEBUG();

	if (!(sd2068 = kzalloc(sizeof(struct sd2068_struct), GFP_KERNEL)))
		return -ENOMEM;

	/*printk("client->addr:%d\n",client->addr);*/
	sd2068->reg_addr=0;
	sd2068->client = client;
	i2c_set_clientdata(client, sd2068);

	sd2068->msg[0].addr = client->addr;
	sd2068->msg[0].flags = 0;
	sd2068->msg[0].len = 1;
	sd2068->msg[0].buf = &sd2068->reg_addr;

	sd2068->msg[2].addr = client->addr;
	sd2068->msg[2].flags = 0;
	sd2068->msg[2].len = 1;
	sd2068->msg[2].buf = sd2068->ctrl_reg;
	sd2068->ctrl_reg[0]=0x0f;
	
	
	sd2068->msg[3].addr = client->addr;
	sd2068->msg[3].flags = I2C_M_RD;
	sd2068->msg[3].len = 2;
	sd2068->msg[3].buf = sd2068->msb_buf;

	//sd2068->msb_buf[0]=0x0f;
	//sd2068->msb_buf[1]=0;
	

	sd2068->msg[1].addr = client->addr;
	sd2068->msg[1].flags = I2C_M_RD;
	sd2068->msg[1].len = 7;
	sd2068->msg[1].buf = sd2068->regs;


#if ENABLE_ALARM_INT
	sd2068_set_alarm_mode(&client->dev,1);
	sd2068_set_alarm(&client->dev,1);
#endif

	

	

read_rtc:
	/* read RTC registers */
	RTC_DS1307_DEBUG();
	tmp = i2c_transfer(adapter, sd2068->msg, 2);
	if (tmp != 2) {
		pr_debug("read error %d\n", tmp);
		err = -EIO;
		goto exit_free;
	}

	
	RTC_DS1307_DEBUG();
	sd2068->rtc = rtc_device_register(client->name, &client->dev,
				&sd2068_rtc_ops, THIS_MODULE);
	
	if (IS_ERR(sd2068->rtc)) {
		err = PTR_ERR(sd2068->rtc);
		dev_err(&client->dev,
			"unable to register the class device\n");
		goto exit_free;
	}
	
	RTC_DS1307_DEBUG();
	printk("Register rtc:%s \n",client->name);
		
			
	return 0;
exit_bad:
	printk("%s: %02x %02x %02x %02x %02x %02x %02x %02x\n",
			"bogus register",
			sd2068->regs[0], sd2068->regs[1],
			sd2068->regs[2], sd2068->regs[3],
			sd2068->regs[4], sd2068->regs[5],
			sd2068->regs[6],sd2068->regs[7]);

exit_free:
	kfree(sd2068);
	return err;
}

static int __devexit sd2068_remove(struct i2c_client *client)
{
	struct sd2068_struct *sd2068 = i2c_get_clientdata(client);

	

	rtc_device_unregister(sd2068->rtc);
	kfree(sd2068);
	return 0;
}
static const struct i2c_device_id ds2068_id[] = {
	{ "sd2068", 0 },	
	{ }
};
MODULE_DEVICE_TABLE(i2c, ds2068_id);

static struct i2c_driver sd2068_driver = {
	.driver = {
		.name	= "rtc-sd2068",
		.owner	= THIS_MODULE,
	},
	.probe		= sd2068_probe,
	.remove		= __devexit_p(sd2068_remove),
	.id_table	= ds2068_id,
};

static int __init sd2068_init(void)
{
	RTC_DS1307_DEBUG();
	return i2c_add_driver(&sd2068_driver);
}
module_init(sd2068_init);

static void __exit ds1307_exit(void)
{
	i2c_del_driver(&sd2068_driver);
}
module_exit(ds1307_exit);

MODULE_DESCRIPTION("RTC driver for DS1307 and similar chips");
MODULE_LICENSE("GPL");
