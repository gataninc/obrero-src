#define _XOPEN_SOURCE 500
#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <libgen.h>
#include <time.h>
#include <ftw.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mount.h>
#include <errno.h>
#include <strings.h>
#include <byteswap.h>
#include "bing_common.h"
#include "utils.h"
#include "adler32.h"
#include "../upgrade/upgrade_hdr.h"

/*  Error codes: */
#define UPG_ERR_OK         (0) /* Upgrade successful */
#define UPG_ERR_FOPEN      (1) /* Error opening upgrade file  */
#define UPG_ERR_IMGINFO    (2) /* Error retrieving system image details  */
#define UPG_ERR_IMGTOOBIG  (3) /* File too big */
#define UPG_ERR_IMGSZ      (4) /* Incorrect size for system upgrade image  */
#define UPG_ERR_MALLOC     (5) /* Failed to allocate app buffer  */
#define UPG_ERR_FREAD      (6) /* Error reading system upgrade image  */
#define UPG_ERR_IMGBAD     (7) /* Upgrade image - invalid header */
#define UPG_ERR_CSUM       (8) /* Upgrade image - checksum error  */
#define UPG_ERR_IMGBADSYS  (9) /* Upgrade image - not valid for SmartSet2  */
#define UPG_ERR_IDINVALID  (10) /* Upgrade image - invalid ID  */
#define UPG_ERR_POPEN      (11) /* Error opening system partition  */
#define UPG_ERR_IMGUPD     (12) /* Error writing system upgrade image  */
#define UPG_ERR_MOUNT      (13) /* Error mounting app partition  */
#define UPG_ERR_EXTRACT    (14) /* Error extracting files from cfg archive */
#define UPG_ERR_CFGFILE    (15) /* Error in cfg archive file */
#define UPG_ERR_NOTOC      (16) /* Error not contents file in cfg archive */
#define UPG_ERR_TOC_OPEN   (17) /* error reading content file */
#define UPG_ERR_TOC_READ   (18) /* Error opening content file */
#define UPG_ERR_TOC_ERR    (19) /* Error in content file */
#define UPG_ERR_TMPDIR     (20) /* Error modifying temp. dir */
#define UPG_ERR_NO_VER     (21) /* Version number not present(personality) */ 
#define UPG_ERR_VER        (22) /* Error updating version number(personality) */
#define UPG_ERR_BKUPDIR    (23) /* Error modifying backup dir */

#define UPG_ERR_MAX        (24)

// define qDebug to output to standard error
#define qDebug(format, ...) \
  do { fprintf(stderr, format "\n", ##__VA_ARGS__); fflush(stderr); } while (0)

#define MIN_SYS_SIZE_MB  (7)   // min. file size for firmware upgrade image.
#define MIN_APP_SIZE_MB  (9)   // min. file size for gui upgrade image.
#define MAX_UPG_FSIZE    (32*1024*1024)  // Mega Bytes

#define MAX_FNAME_LEN   (64)
static char sysUpgradeFile[MAX_FNAME_LEN];
static char appUpgradeFile[MAX_FNAME_LEN];

static char sysErasePart[]   = "/dev/mtd4";
static char sysUpgradePart[] = "/dev/mtdblock4";
#define FLASH_BLK_SIZE   (512*1024)

char appPart[]     = "/dev/mtdblock5";
char appMntPt[]    = "/mnt/cdrom";
char appFSType[]   = "yaffs";
char appFileName[] = "obrero_ui.img.gz";

static int openAndReadFile(char *upgFile, uint8_t **upgBuf, uint32_t *bufLen)
{
    int fd;
    uint8_t *bufPtr = 0;
    int err = 0;
    struct stat fDet;
    // open upgrade image
    if ((fd = open(upgFile, O_RDONLY)) < 0)
    {
        err = errno;
        qDebug("%s: open: %s\n", __func__, strerror(err));
        return (UPG_ERR_FOPEN);
    }
    if (fstat(fd, &fDet) != 0)
    {
        err = errno;
        close(fd);
        qDebug("%s: stat: %s\n", __func__, strerror(err));
        return (UPG_ERR_IMGINFO);
    }
    if (fDet.st_size > MAX_UPG_FSIZE)
    {
        close(fd);
        qDebug("%s: File too big\n", __func__);
        return (UPG_ERR_IMGTOOBIG);
    }
    // allocate memory for file
    if ((bufPtr = (uint8_t *)malloc(fDet.st_size)) == 0)
    {
        close(fd);
        qDebug("%s: Error: memory allocation failure", __func__);
        return (UPG_ERR_MALLOC);
    }
    // read whole file into buffer
    if (read(fd, bufPtr, fDet.st_size) != fDet.st_size)
    {
        err = errno;
        close(fd);
        qDebug("%s: read: %s", __func__, strerror(err));
        return (UPG_ERR_FREAD);
    }
    close(fd);
    *upgBuf = bufPtr;
    *bufLen = (uint32_t)fDet.st_size;
    return 0;
}

static uLong computeCRC(uint8_t *buffer, uint32_t size)
{
    uLong crc32;
    /* init csum field */
    crc32 = adler32(0L, Z_NULL, 0);
    // now compute buffer csum
    crc32 = adler32(crc32, buffer, size);
    return crc32;
}

static int verifyImage(uint8_t *bufPtr, uint32_t bufSz, int *upgType)
{
    UpgradeImgHdr *imgHdr;
    uint8_t *tmpBuf;
    uint32_t crc32;

    if (bufSz < (sizeof(UpgradeImgHdr)))
            return UPG_ERR_IMGBAD;
    imgHdr = (UpgradeImgHdr *)bufPtr;
    if (memcmp(imgHdr->magicStr, MAGIC_STR_SS2, MAGIC_LEN) != 0)
    {
        //qDebug("Error: %s image is not Gatan upgrade image\n", __func__);
        return (UPG_ERR_IMGBAD);
    }
    tmpBuf = (bufPtr + sizeof(uLong)); // skip csum bytes
    crc32 = computeCRC(tmpBuf, (bufSz-sizeof(uLong)));
    // byte swap values that we care about..
    imgHdr->ckSum   = bswap_32(imgHdr->ckSum);
    imgHdr->sysId   = bswap_32(imgHdr->sysId);
    imgHdr->majorNo    = bswap_16(imgHdr->majorNo);
    imgHdr->minorNo    = bswap_16(imgHdr->minorNo);
    imgHdr->patchLevel = bswap_16(imgHdr->patchLevel);
    imgHdr->buildNo    = bswap_16(imgHdr->buildNo);
    imgHdr->bldType = bswap_32(imgHdr->bldType);
    imgHdr->bldDate.year = bswap_16(imgHdr->bldDate.year);

    qDebug("Header Info:");
    qDebug("check sum : %x", imgHdr->ckSum);
    qDebug("systemId  : %x", imgHdr->sysId);
    qDebug("build ver : %hu.%hu.%hu.%hu build Type: %u", 
                imgHdr->majorNo, imgHdr->minorNo, imgHdr->patchLevel,
                imgHdr->buildNo, imgHdr->bldType);
    qDebug("build date: %02d / %02d / %04d, %02d:%02d:%02d\n",
                      imgHdr->bldDate.month, imgHdr->bldDate.day, 
                      imgHdr->bldDate.year, imgHdr->bldDate.hour, 
                      imgHdr->bldDate.min, imgHdr->bldDate.sec);
    if (crc32 != imgHdr->ckSum)
    {
        qDebug("Error: checksum mis-match(hdr: %x, computed: %x\n", 
                                imgHdr->ckSum, crc32);
        return (UPG_ERR_CSUM);
    }
    // verify system id and image type
    qDebug("sysId: %x", (imgHdr->sysId & IMAGE_SMARTSET2_TYPE));
    if ((imgHdr->sysId & IMAGE_SMARTSET2_TYPE) != IMAGE_SMARTSET2_TYPE)
    {
        qDebug("Error: upgrade image is not targeted for SmartSet2");
        return (UPG_ERR_IMGBADSYS);
    }
    imgHdr->sysId &= 0xFF;
    qDebug("sysId: %x", imgHdr->sysId);
    if ((imgHdr->sysId != IMAGE_SYS_TYPE) && (imgHdr->sysId != IMAGE_APP_TYPE))
    {
        qDebug("Error: Bad system ID in header\n");
        return UPG_ERR_IDINVALID;
    }
    if ((imgHdr->sysId == IMAGE_SYS_TYPE) && 
                                ((bufSz/(1024*1024)) < MIN_SYS_SIZE_MB))
    {
        qDebug("%s: Error: invalid file size for system image", __func__);
        return UPG_ERR_IMGSZ;
    }
    if ((imgHdr->sysId == IMAGE_APP_TYPE) && 
                                ((bufSz/(1024*1024)) < MIN_APP_SIZE_MB))
    {
        qDebug("%s: Error: invalid file size for app image", __func__);
        return UPG_ERR_IMGSZ;
    }
    *upgType = imgHdr->sysId;
    return 0;

}

static int eraseSysPartition()
{
    char sysCmd[128];
    int ret;
    // Erase system image partition. Once in a while, the system appears to
    // fail to erase the blocks properly and upgrade fails.
    // The command below silently erases all blocks in the partition..
    sprintf(sysCmd, "/usr/bin/flash_eraseall -q %s", sysErasePart);
    ret = system(sysCmd);
    qDebug("%s: system returned %d", __func__, ret);
    if (ret != 0)
        return -1;
    return 0;
}

static int upgradeSystem(uint8_t *bufPtr, uint32_t bufLen)
{
    int devFd, err = 0;
    uint8_t *tmpBuf;
    uint32_t tmpLen;
    ssize_t rdLen, wrLen;
    uint32_t totLen = 0;

    if ((!bufPtr) || (bufLen < (MIN_SYS_SIZE_MB*1024*1024)))
        return UPG_ERR_IMGSZ;
    // Erase flash partition
    if (eraseSysPartition() != 0)
    {
        free(bufPtr);
        qDebug("%s: Error erase system partition", __func__);
        return UPG_ERR_POPEN;
    }
    // Open upgrade partition and write image to it.
    if ((devFd = open(sysUpgradePart, O_WRONLY)) < 0)
    {
        err = errno;
        qDebug("%s: sys open: %s", __func__, strerror(err));
        return 21;
    }
    tmpBuf = bufPtr + sizeof(UpgradeImgHdr);
    bufLen -= sizeof(UpgradeImgHdr);
    tmpLen = bufLen;
    // Copy system image to partition
    while (1)
    {
        if (tmpLen > (FLASH_BLK_SIZE))
            rdLen = FLASH_BLK_SIZE;
        else
            rdLen = tmpLen;
        if ((wrLen = write(devFd, tmpBuf, rdLen)) < rdLen)
        {
            err = errno;    
            qDebug("%s: Update: %s", __func__, strerror(err));
            break;
        }
        tmpLen -= rdLen;
        totLen += rdLen;
        tmpBuf += rdLen;
        if (tmpLen > bufLen) // wrapped around
        {
            qDebug("!! ERROR !!: sys buffer length wrapped around!!");
            break;
        }
        if (tmpLen == 0)
            break;
    }
    if (totLen != bufLen)
    {
        qDebug("%s: Error!: write len(%u) != file len(%u)!", __func__,
                (uint32_t)totLen, (uint32_t)(bufLen));
        err = UPG_ERR_IMGUPD;
    }
    close(devFd);
    return err;
}

static int mountAppPartition()
{
    int err;
    if (mount(appPart, appMntPt, appFSType, 0, 0) != 0)
    {
        err = errno;
        qDebug("%s: mount error: %s", __func__, strerror(err));
        return -1;
    }
    return 0;
}

static int umountAppPartition()
{
    int err;
    if (umount(appMntPt) != 0)
    {
        err = errno;
        qDebug("%s: umount error: %s", __func__, strerror(err));
        return -1;
    }
    return 0;
}

static int upgradeApps(uint8_t *bufPtr, uint32_t bufLen)
{
    char upgrFile[64];
    int devFd, err = 0;
    uint8_t *tmpBuf;
    ssize_t rdLen, wrLen;
    uint32_t totLen = 0, tmpLen = 0;

    bzero((void *)upgrFile, 64);
    sprintf(upgrFile, "%s/%s", appMntPt, appFileName);
    qDebug("UpgrFile: %s", upgrFile);
    if (mountAppPartition() != 0)
        return UPG_ERR_MOUNT;
    usleep(500000);
    if (unlink(upgrFile) != 0)
    {
        err = errno;
        qDebug("%s: unlink: %s", __func__, strerror(err));
        err = 0; // reporting only
    }
    usleep(500000);
    // open upgrade partition and write image to it.
    if ((devFd = open(upgrFile, O_WRONLY|O_TRUNC|O_CREAT,
                        S_IRWXU|S_IRUSR|S_IROTH)) < 0)
    {
        err = errno;
        qDebug("%s: open: %s", __func__, strerror(err));
        umountAppPartition();
        return UPG_ERR_POPEN;
    }
    // set pointers and offsets
    tmpBuf = bufPtr + sizeof(UpgradeImgHdr);
    bufLen -= sizeof(UpgradeImgHdr);
    tmpLen = bufLen;
    // copy application archive to partition.
    while (1)
    {
        if (tmpLen > (FLASH_BLK_SIZE))
            rdLen = FLASH_BLK_SIZE;
        else
            rdLen = tmpLen;
        if ((wrLen = write(devFd, tmpBuf, rdLen)) < rdLen)
        {
            err = errno;    
            qDebug("%s: Update: %s", __func__, strerror(err));
            break;
        }
        tmpLen -= rdLen;
        totLen += rdLen;
        tmpBuf += rdLen;
        if (tmpLen > bufLen)
        {
            qDebug("!! Error !!: apps buffer length wrapped around!!");
            break;
        }
        if (tmpLen == 0)
            break;
    }
    if (totLen != bufLen)
    {
        qDebug("%s: Error!: write len(%u) != file len(%u)!", __func__,
                                        (uint32_t)totLen, (uint32_t)bufLen);
        err = UPG_ERR_IMGUPD;
    }
    close(devFd);
    usleep(10000);
    umountAppPartition();
    return err;
}

const char *libGetPVer()
{
    char verFile[] = PDEFS_VER_FILE;
    static char verStr[128] = "";
    int ret;
    ret = getStringFromFile(verFile, verStr, 128);
    if (ret != 0)
    {
        if (ret == -1)
           printf("Error: error failed to open ver. file\n");
        else  if (ret == -2)
           printf("Error: error reading  version\n");
        else
           printf("Error: unknown error(%d)\n", ret);
    }
    return verStr;
}

#define MAX_CFGFILE_SIZE (1024*1024)  // 1MB
static char cfgTmpDir[] = "/mnt/nv/cfg_tmp";
static char contentFile[] = "gatan_cfg.txt";
static char cfgLogFile[] = "/tmp/cfg_upg.log";
static char pDefsDir[] = "/mnt/nv/personalities";
static char pBackup[]  = "backup.0"; // last backup
static char sBackup[]  = "backup.1"; // backup before last
static char ActivePFile[] = "Active Personality";
static int  cpFiles = 0;
// copy each file found by nftw to personality folder
static int copyCfgFile(const char *fpath, const struct stat *sb, int tflag,
                            struct FTW *ftwbuf)
{
    const char *fName;
    char dstFile[256];
    int err = 0;
    if (tflag == FTW_F) // regular file
    {
        fName = (fpath+ftwbuf->base);
        printf("%s -> %s\n", fpath, fName);
        if (strcmp(fName, contentFile) == 0) // don't copy content file
            return 0;
        snprintf(dstFile, 255, "%s/%s", pDefsDir, fName);
        dstFile[255] = '\0';
        printf("rename %s -> %s\n", fName, dstFile);
        err = rename(fName, dstFile);
        if (err != 0)
        {
            err = errno;
            qDebug("%s: error copying file <%s>\n", __func__, strerror(err));
            return UPG_ERR_IMGUPD;
        }
        cpFiles++;
    }
    return 0;
}

static int updatePdefsVersion(char *vStr) 
{
    FILE *fp;
    int err;
    if ((!vStr) || (*vStr == '\0'))
        return -1;
    if ((fp = fopen(PDEFS_VER_FILE, "w")) == NULL)
    {
        err = errno;
        qDebug("%s: open - %s", __func__, strerror(err));
        return -1;
    }
    if ((err = fprintf(fp, "%s\n", vStr)) < (int)strlen(vStr))
        err = errno;
    fclose(fp);
    if (err < 0)
    {
        qDebug("%s: write - %s", __func__, strerror(err));
        return -1;
    }
    return 0;
}

// Count regular files in the personality directory and return count
static volatile int numPDefFiles = 0;
static int countRegFiles(const char *fpath, const struct stat *sb, int tflag,
                            struct FTW *ftwbuf)
{
    const char *fName;
    int err = 0;

    //printf("FPATH: %s, level: %d\n", fpath, ftwbuf->level);

    // do not count files in sub-directories
    if ((tflag == FTW_D) && (ftwbuf->level > 0)) 
        return FTW_SKIP_SUBTREE;

    if (tflag == FTW_F) // regular file
    {
        fName = (fpath+ftwbuf->base);
        printf("%s -> %s\n", fpath, fName);
        if (strcmp(fName, ActivePFile) == 0) // skip active personality
            ;        
        else
            numPDefFiles++;
    }
    return FTW_CONTINUE;
}

static int countFiles()
{
    int err = 0;
    numPDefFiles = 0;
    err = nftw(pDefsDir, countRegFiles, 2, (FTW_PHYS|FTW_ACTIONRETVAL));
    if (err == -1)
    {
        err = errno;
        qDebug("%s: %s\n", __func__, strerror(err));
        return 0;
    }
    return numPDefFiles;
}

static int backupPDefs(const char *fpath, const struct stat *sb, int tflag,
                            struct FTW *ftwbuf)
{
    const char *fName;
    char srcFile[256];
    char backupFile[256];
    int err = 0;

    // do not copy files in sub-directories
    if ((tflag == FTW_D) && (ftwbuf->level > 0)) 
        return FTW_SKIP_SUBTREE;

    if (tflag == FTW_F) // regular file
    {
        fName = (fpath+ftwbuf->base);
        printf("%s -> %s\n", fpath, fName);
        if (strcmp(fName, ActivePFile) == 0) // don't back up active pfile
            return 0;
        snprintf(backupFile, 255, "%s/%s/%s", pDefsDir, pBackup, fName);
        backupFile[255] = '\0';
        snprintf(srcFile, 255, "%s/%s", pDefsDir, fName);
        srcFile[255] = '\0';
        printf("\trename %s -> %s\n", srcFile, backupFile);
        err = rename(srcFile, backupFile);
        if (err != 0)
        {
            err = errno;
            qDebug("%s: error copying file <%s>\n", __func__, strerror(err));
            return UPG_ERR_IMGUPD;
        }
    }
    return 0;
}

static int backupPDefVersion(char *bDir)
{
    const char *pVer;
    const char pDefVer[] = "Unknown    01/01/2016";
    char verFile[128];
    FILE *fp;
    int err = 0;

    pVer = libGetPVer();
    if (strlen(pVer) < 2)
        pVer = pDefVer;
    printf("PDefs V: [%s]\n", pVer);
    snprintf(verFile, 127, "%s/version.txt", bDir);
    verFile[127] = '\0';
    if ((fp = fopen(verFile, "w")) == NULL)
    {
        err = errno;
        qDebug("%s: open - %s", __func__, strerror(err));
        return -1;
    }
    if ((err = fprintf(fp, "%s\n", pVer)) < (int)strlen(pVer))
        err = errno;
    else
        err = 0; // do not leave no. of char. printed in the return value.
    fclose(fp);
    if (err != 0)
    {
        qDebug("%s: write - %s", __func__, strerror(err));
        return -1;
    }
    return 0;
}

/*
 * Move all personality files in /mnt/nv/personalities to backup directory
 * Remove previous backup if present and rename existing backup.
 */
static int doBackupPDefs()
{
    char backupDir[128];
    char pBackupDir[128];
    char cmdStr[128];
    struct stat sBuf;
    int pBExist = 0, bExist = 0;
    int pCount = 0, err = 0;
    // some house keeping stuff
    // Make sure there are file[s] to backup
    if ((pCount = countFiles()) == 0)
    {
        qDebug("%s: nothing to backup/delete\n", __func__);
        return 0;
    }
    qDebug("%s: Found %d personality files to backup\n", __func__, pCount);
    // backup directories.
    sprintf(backupDir, "%s/%s", pDefsDir, pBackup);
    sprintf(pBackupDir, "%s/%s", pDefsDir, sBackup);
    // see which directories exist
    if (stat(backupDir, &sBuf) == 0)
        bExist = 1;
    if (stat(pBackupDir, &sBuf) == 0)
        pBExist = 1;
    if (bExist)
    {
        if (pBExist)
        {
            qDebug("%s: Remove %s\n", __func__, pBackupDir);
            // remove previous backup.
            snprintf(cmdStr,127,"rm -rf %s >>%s 2>&1", pBackupDir, cfgLogFile);
            cmdStr[127] = '\0';
            system(cmdStr);
        }
        // rename current backup to previous backup.
        qDebug("%s: Rename %s\n", __func__, backupDir);
        if (rename(backupDir, pBackupDir) != 0)
        {
            err = errno;
            qDebug("%s: rename backup error<%s>\n", backupDir, strerror(err));
            return UPG_ERR_IMGUPD;
        }
    }
    // Backup current files and copy pdefs version
    // Create backup directory
    qDebug("%s: Create %s\n", __func__, backupDir);
    if (mkdir(backupDir, 0644) != 0)
    {
        qDebug("ConfigFile: failed to create backup dir");
        return UPG_ERR_BKUPDIR;
    }
    // Backup files now..
    err = nftw(pDefsDir, backupPDefs, 2, (FTW_PHYS|FTW_ACTIONRETVAL));
    if (err == -1)
    {
        err = errno;
        qDebug("%s: %s\n", __func__, strerror(err));
        return UPG_ERR_FREAD;
    }
    if (err < 0)
        return UPG_ERR_FREAD;
    return backupPDefVersion(backupDir);
}

enum { CMD_UNKNOWN, CMD_COPY, CMD_DEL, CMD_VER };
static int lineNo = 1;
static char upgVersion[128];
static int upgVerFound = 0;

static int doCfgOp(char *line)
{
    char *lPtr, *cPtr, *fName;
    int i, err;
    int cmd = CMD_UNKNOWN;
    struct stat sBuf;

    // strip CR/LF
    lPtr = line + (strlen(line)-1);
    while (lPtr > line)
    {
        if ((*lPtr == '\n') || (*lPtr == '\r'))
            *lPtr = '\0';
        lPtr--;
    }
    // strip leading/trailing spaces.
    if ((lPtr = stripWhiteSpace(line)) == 0)
        return UPG_ERR_OK;
    printf("[%s]\n", lPtr);
    // extract the command, seperator is tab or space
    if ((cPtr = index(lPtr, '\t')) == 0)
        cPtr = index(lPtr, ' ');
    if (cPtr == 0) // no space or tab??
    {
        printf("line: %d, No seperator found(space or tab)\n", lineNo);
        return UPG_ERR_TOC_ERR;
    }
    *cPtr = '\0';
    //printf("%s\n", lPtr);
#if 0
    if (strcasecmp(lPtr, "copy") == 0)
        cmd = CMD_COPY;
    else 
#endif
    if (strcasecmp(lPtr, "delete") == 0)
        cmd = CMD_DEL;
    else if (strcasecmp(lPtr, "version") == 0)
        cmd = CMD_VER;
    else 
    {
        printf("line: %d, Invalid first field\n", lineNo);
        return UPG_ERR_TOC_ERR;
    }
    *cPtr = ' ';
    fName = stripWhiteSpace(cPtr);
    if (! fName)
    {
        printf("line: %d, No path/version specified\n", lineNo);
        return UPG_ERR_TOC_ERR;
    }
    if (cmd == CMD_VER) 
    {
        if (fName[0] != '\0') // non-empty string..
        {
            strncpy(upgVersion, fName, 127);
            upgVersion[127] = '\0';
            upgVerFound = 1;
            return UPG_ERR_OK;
        }
        else
            return UPG_ERR_VER;
    }
    //printf("CMD: %s FILE: %s\n", ((cmd == CMD_COPY) ? "copy" : "del"),
    //                                            fName);
    // check for some invalid chars in filename..
    if ((index(fName, ';') != 0) || (strstr(fName, "..") != 0) ||
            (index(fName, '\t') != 0) || (index(fName, '\\') != 0))
    {
        printf("line: %d, Invalid path/filename\n", lineNo);
        return UPG_ERR_TOC_ERR;
    }
    // ok: seems to be fine., do the action
    if (cmd == CMD_DEL)
    {
        if ((fName[0] == '*') || (strcmp(fName, "*.*") == 0))
            doBackupPDefs();
        else
        {
            if (strncmp(fName, pDefsDir, strlen(pDefsDir)) != 0)
            {
                printf("line: %d, Invalid path prefix\n", lineNo);
                return UPG_ERR_TOC_ERR;
            }
            unlink(fName); // expect it to work always..
        }
    }
    else
    {
        if (strncmp(fName, pDefsDir, strlen(pDefsDir)) != 0)
        {
            printf("line: %d, Invalid path prefix\n", lineNo);
            return UPG_ERR_TOC_ERR;
        }
        // make sure file is present in archive etc
        if ((cPtr = basename(fName)) == 0)
            return UPG_ERR_TOC_ERR;
        if (stat(cPtr, &sBuf) != 0)
            return UPG_ERR_TOC_ERR;
        if (! (S_ISREG(sBuf.st_mode)))
            return UPG_ERR_TOC_ERR;
        //printf("exec: rename %s %s\n", cPtr,fName);
        if (rename(cPtr, fName) != 0)
        {
            err = errno;
            qDebug("%s: file copy error <%s>\n", fName, strerror(err));
            return UPG_ERR_IMGUPD;
        }
    }
    return UPG_ERR_OK;
}

// process entries in contentFile[]
static int doProcContent(int fileSz)
{
    int err = UPG_ERR_OK;
    int errCnt = 0;
    // content file size = 0, special case: just copy files in the archive
    // to the personality file directory
    if (fileSz == 0) 
    {
        cpFiles = 0;
        if ((err = nftw(cfgTmpDir, copyCfgFile, 2, FTW_PHYS)) == -1)
        {
            err = errno;
            qDebug("%s: %s\n", __func__, strerror(err));
            return UPG_ERR_FREAD;
        }
        if (err > 0)
            return err; // value returned by copy fuction
        return UPG_ERR_OK;
    }
    upgVersion[0] = '\0';
    // 10/20/2015:
    // "version" entry is mandatory in content file now. 
    // All files present in archive are copied, no entries needed
    // in content file.
    // Do actions specified in contents file..
    {
        // open file and read contents one line at a time and process entry
        FILE *fp;
        char *line = 0;
        size_t len = 0;
        ssize_t rdBytes;
        if ((fp = fopen(contentFile, "r")) == NULL)
            return UPG_ERR_TOC_OPEN;
        lineNo = 1;
        while ((rdBytes = getline(&line, &len, fp)) != -1)
        {
            if ((line[0] == '#') || 
                    ((line[0] == ' ') && (line[1] == '#'))) // skip comments
                continue;
            err = doCfgOp(line);
            if (err != UPG_ERR_OK)
               errCnt++;
            lineNo++;
        }
        if (errCnt != 0)
            err = UPG_ERR_TOC_ERR;
        if (err == UPG_ERR_OK) // no other error happened..
        {
            if (! feof(fp))
            {
                perror("getline");
                err = UPG_ERR_TOC_READ;
            }
        }
        fclose(fp);
        if (line)
            free(line);
    }
//return 0;
    if ((err == 0) && (upgVerFound == 0))
        err = UPG_ERR_NO_VER;
    if (err != 0)
        return err;
    // now copy the personality files
    cpFiles = 0;
    err = nftw(cfgTmpDir, copyCfgFile, 2, FTW_PHYS);
    if (cpFiles > 0)
        updatePdefsVersion(upgVersion);
    if (err == -1)
    {
        err = errno;
        qDebug("%s: %s\n", __func__, strerror(err));
        return UPG_ERR_FREAD;
    }
    if (err > 0)
        return err; // value returned by copy fuction
    return UPG_ERR_OK;
}

// Use the 7zDec program to try extract file contents to a directory. If that 
// fails, report error and exit. This also acts as a validation of the archive.
// Locate the contents.txt file and do actions based on entries in the file.
// Report failure/success.
// Following types of entries supported in contents.txt now(08/31/2015)
// DELETE  <full path to file>
// COPY    <full path to file>
// Firmware will do the following for these entries:
// DELETE: deletes the file located at the specified path.
// COPY  : copy file from archive to specified path. The filename in archive 
//         and the path must match.
// imgName: must be full(absolute) path to the upgrade image
static int upgradeCfgFiles(char *imgName, int Len)
{
    char cmdStr[128];
    struct stat sBuf;
    int err;
    // first validate the incoming file.
    snprintf(cmdStr, 127, "7zDec t %s >>%s 2>&1", imgName, cfgLogFile);
    cmdStr[127] = '\0';
    err = system(cmdStr);
    if (err != 0)
    {
        qDebug("Upgrade: Invalid file");
        return UPG_ERR_IMGBAD;
    }
    if (Len > MAX_CFGFILE_SIZE)
    {
        qDebug("Upgrade: config file too large.");
        return UPG_ERR_IMGBAD;
    }
    // remove temp. directory
    snprintf(cmdStr, 127, "rm -rf %s >>%s 2>&1", cfgTmpDir, cfgLogFile);
    cmdStr[127] = '\0';
    system(cmdStr);
    if ((err = stat(cfgTmpDir, &sBuf)) == 0) // dir. still exists?
    {
        qDebug("ConfigFile: failed to remove temp. dir");
        return UPG_ERR_TMPDIR;
    }
    // err != 0: make sure error is what we expect
    err = errno;
    if (err != ENOENT)
    {
        qDebug("ConfigFile: failed to remove temp. dir");
        return UPG_ERR_TMPDIR;
    }
    if (mkdir(cfgTmpDir, 0644) != 0)
    {
        qDebug("ConfigFile: failed to create temp. dir");
        return UPG_ERR_TMPDIR;
    }
    if (chdir(cfgTmpDir) != 0)
    {
        qDebug("ConfigFile: failed to chdir to temp. dir");
        return UPG_ERR_TMPDIR;
    }
    snprintf(cmdStr, 127, "7zDec e %s >>%s 2>&1", imgName, cfgLogFile);
    cmdStr[127] = '\0';
    err = system(cmdStr);
    if (err != 0)
    {
        qDebug("ConfigFile: failed to extract files from upgrade archive");
        return UPG_ERR_EXTRACT;
    }
    // Make sure contents file is present and act on its contents.
    snprintf(cmdStr, 127, "%s/%s", cfgTmpDir, contentFile);
    cmdStr[127] = '\0';
    if ((err = stat(cmdStr, &sBuf)) != 0)
        return UPG_ERR_NOTOC;
    // make sure it is a regular file.
    if (! (S_ISREG(sBuf.st_mode)))
        return UPG_ERR_NOTOC;
    return doProcContent(sBuf.st_size);
}

// Work for sw upgrade task:
// Mount /dev/sda1 on /mnt/src type vfat
// Determine type of upgrade file(firmware(sys) or gui(app) and copy
// it to the right location in flash.
int doSwUpgrade(char *imgName)
{
    int fileMap;
    int ret = 0;
    int upgType;
    uint8_t *bufPtr = 0;
    uint32_t bufLen = 0;

    if ((ret = openAndReadFile(imgName, &bufPtr, &bufLen)) < 0)
        return ret;
    if ((ret = verifyImage(bufPtr, bufLen, &upgType)) != 0)
    {
        free(bufPtr);
        if ((ret == UPG_ERR_IMGBAD) || (ret == UPG_ERR_IMGSZ))
            // may be a config archive : 7zip file..
            ret = upgradeCfgFiles(imgName, bufLen);
        goto doneUpgrade;
    }
    if (upgType == IMAGE_SYS_TYPE) 
    {
        ret = upgradeSystem(bufPtr, bufLen);
        if (ret != 0)
            qDebug("Error: upgrade of system image failed");
    }
    else if (upgType == IMAGE_APP_TYPE)
    {
        ret = upgradeApps(bufPtr, bufLen);
        if (ret != 0)
            qDebug("Error: upgrade of apps image failed");
    }
    else
        ret =  UPG_ERR_IMGBAD;
    free(bufPtr);
doneUpgrade:
    return ret;
}
