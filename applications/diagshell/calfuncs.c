#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <signal.h>
#include <setjmp.h>
#include <time.h>
#include <math.h>
#include <byteswap.h>
#include "bing_common.h"
#include "regs.h"
#include "i2cutils.h"
#include "utils.h"
#include "sysutils.h"
#include "sharedmem.h"
#include "dmmfuncs.h"
#include "minIni.h"
#include "sys_ini.h"

#define REV_100MOHM_BRD  (11)
/*
 * Functions used for calibration/tests during mfg setup
 * All functions based on documentation from AlistairM
 */

int mcp3424Read(int bus, int devAddr, int chan, int gain, float *val);
int mcp3424ReadRaw(int bus, int devAddr, int chan, int gain, int *val);

extern volatile sysParams *pCache;

enum { CAL_LOG_MSG, CAL_LOG_WARN, CAL_LOG_ERR };
#define GTC_IDEAL (42.15226337)

#define DO_INI_SAVE
// strings to save settings into ini file.
static char sectStr[64];
static char itemStr[64];
static char fltVal[64];

// globals..
static float T_cjc_cal_start, T_cjc_cal_end;
static float T_pcb_cal_start, T_pcb_cal_end;
static double V_cjc_eeprom_pwr_v; // stored in mV
static int pauseOnError = 0;

// Gather a list of errors and print out at end(JohnP 02/25/2016)
static int errIdx = 0;
#define MAX_ERR_IDX      (50)
#define MAX_ERR_STR_LEN (128)
static char calErrors[MAX_ERR_IDX][MAX_ERR_STR_LEN];

// output test results into a log file, (JohnP 02/25/2016)
static char tmpFile[] = "/mnt/nv/cal_log.tmp"; // temporary file, later renamed.
static char logFile[128] = { 0 };
static FILE *logFP = 0;

// log a message to screen. The level is the level of indent for each message
// level 0 is no indent, 1 is x spaces, 2 is 2x spaces etc.
static void logCalMsg(int msgType, int level, char *fmt, ...)
{
    char msgStr[128];
    char indentStr[32];
    va_list ap;
    int indent = 3; // spaces
    int nl = 0;
    va_start(ap, fmt);
    vsnprintf(msgStr, 128, fmt, ap);
    va_end(ap);
    msgStr[127] = '\0';
    if (msgStr[0] == '\n')
        nl = 1;
    if (level < 0)
        level = 0;
    if (level > 5)
        level = 5;
    if (level == 0)
        indentStr[0] = '\0';
    else
    {
        if (nl)
            snprintf(indentStr, 31, "\n%*s", (indent*level), " ");
        else
            snprintf(indentStr, 31, "%*s", (indent*level), " ");
        indentStr[31] = '\0';
    }
    switch (msgType)
    {
        case CAL_LOG_WARN:
            if (nl)
            {
                printf("%sWarning: %s\n\n", indentStr, &(msgStr[1]));
                if (logFP)
                    fprintf(logFP,"%sWarning: %s\n\n",indentStr,&(msgStr[1]));
            }
            else
            {
                printf("\n%sWarning: %s\n\n", indentStr, msgStr);
                if (logFP)
                    fprintf(logFP, "\n%sWarning: %s\n\n", indentStr, msgStr);
            }
            fflush(stdout);
            break;
        case CAL_LOG_ERR:
            if (nl)
            {
                printf("%sError: %s\n\n", indentStr, &(msgStr[1]));
                if (logFP)
                    fprintf(logFP, "%sError: %s\n\n", indentStr, &(msgStr[1]));
            } 
            else
            {
                printf("\n%sError: %s\n\n", indentStr, msgStr);
                if (logFP)
                    fprintf(logFP, "\n%sError: %s\n\n", indentStr, msgStr);
            }
            fflush(stdout);
            break;
        case CAL_LOG_MSG:
            if (nl)
            {
                printf("%s%s\n", indentStr, &(msgStr[1]));
                if (logFP)
                    fprintf(logFP, "%s%s\n", indentStr, &(msgStr[1]));
            }
            else
            {
                printf("%s%s\n", indentStr, msgStr);
                if (logFP)
                    fprintf(logFP, "%s%s\n", indentStr, msgStr);
            }
            fflush(stdout);
            break;
    }
    // Collect all calibration error strings for summary print.
    if (strstr(msgStr, "CAL_ERR_") != 0)
    {
        if (errIdx < MAX_ERR_IDX)
        {
            char *eStr = &(calErrors[errIdx][0]);
            if (nl)
                strncpy(eStr, (msgStr+1), 127);
            else
                strncpy(eStr, msgStr, 127);
            eStr[127] = '\0';
            if (msgStr[strlen(msgStr)-1] == '\n')
                eStr[strlen(eStr)-1] = '\0';
            errIdx++;
        }
    }
}

static void pauseTest()
{
    int c;
    if (pauseOnError)
    {
        printf("\nPress RETURN to continue: ");
        fflush(stdout);
        fflush(stdin);
        c = fgetc(stdin);
    }
}

static void prtReg(int fl)
{
    uint16_t regs[40];
    int numRegs = 8;
    int i;
    if (fl)
    {
    readCtrlReg(0, numRegs, &(regs[0]));
    printf("REGS: [ ");
    for (i = 0; i < numRegs; i++)
        printf("%d.0x%X  ", i, regs[i]);
    printf("]\n");fflush(stdout);
    }
}

// idx 0 contains pre-amp ctrl A voltage, idx 1 ctrl B voltage and 
// idx 2 for ctrl C.
static int findDongleId(double *preAmpV, int *dIdx)
{
    // pre-amp ctrl A
    *dIdx = 0;
    if ((preAmpV[0] >= 0.724) && (preAmpV[0] <= 0.958))
        return 13;
    if ((preAmpV[0] >= 1.448) && (preAmpV[0] <= 1.916))
        return 7;
    if ((preAmpV[0] >= 2.17) && (preAmpV[0] <= 2.875))
        return 1;
    if ((preAmpV[0] >= -0.958) && (preAmpV[0] <= -0.724))
        return 14;
    if ((preAmpV[0] >= -1.916) && (preAmpV[0] <= -1.448))
        return 8;
    if ((preAmpV[0] >= -2.875) && (preAmpV[0] <= -2.17))
        return 2;

    // pre-amp ctrl B
    *dIdx = 1;
    if ((preAmpV[1] >= 0.724) && (preAmpV[1] <= 0.958))
        return 15;
    if ((preAmpV[1] >= 1.448) && (preAmpV[1] <= 1.916))
        return 9;
    if ((preAmpV[1] >= 2.17) && (preAmpV[1] <= 2.875))
        return 3;
    if ((preAmpV[1] >= -0.958) && (preAmpV[1] <= -0.724))
        return 16;
    if ((preAmpV[1] >= -1.916) && (preAmpV[1] <= -1.448))
        return 10;
    if ((preAmpV[1] >= -2.875) && (preAmpV[1] <= -2.17))
        return 4;

    // pre-amp ctrl C
    *dIdx = 2;
    if ((preAmpV[2] >= 0.724) && (preAmpV[2] <= 0.958))
        return 17;
    if ((preAmpV[2] >= 1.448) && (preAmpV[2] <= 1.916))
        return 11;
    if ((preAmpV[2] >= 2.17) && (preAmpV[2] <= 2.875))
        return 5;
    if ((preAmpV[2] >= -0.958) && (preAmpV[2] <= -0.724))
        return 18;
    if ((preAmpV[2] >= -1.916) && (preAmpV[2] <= -1.448))
        return 12;
    if ((preAmpV[2] >= -2.875) && (preAmpV[2] <= -2.17))
        return 6;

    return 0;
}

static int tryRegLock(int tries)
{
    int i;
    tries = ((tries <= 0) ? 1 : tries);
    for (i = 0; i < tries; i++)
    {
        if (lockCtrlReg() == 0)
            break;
        usleep(100000);
    }
    if (i >= tries)
        return -1;
    return 0;
}

static int getAvgTADCFpgaVal(int chan, int numCycles, int scale2p5V, 
                                                                double *V)
{
    uint8_t reg = 1;
    static prevChan = 0;
    switch (chan)
    {
        case T_ADC_CHAN_PREAMP:
            reg = 0x41;
            break;
        case T_ADC_CHAN_DRTD:
            reg = 0x21;
            break;
        case T_ADC_CHAN_TC:
            reg = 0x1;
            break;
    }
    if (prevChan != chan)
    {
        setCtrlRegBits(37, 0x61, reg);
        usleep(200000); // wait a few cycles
    }
    else
        usleep(25000); 
    prevChan = chan;
    return getAvgTADCFpgaVoltage(chan, numCycles, scale2p5V, V);
}

static int checkDongleID(int ID)
{
    int i, dID, dIdx;;
    double preAmpV[3];
    double V;
    // set PRE-AMP CTRL A,B & C low
    setCtrlRegBits(3, 0xffff, 0x1);  // drtd_sample  must be 01b
prtReg(0);
    getAvgTADCVolt(T_ADC_CHAN_PREAMP, 1000, 0, 0, &V);
    V *= 1000.0; // convert to mV
    if (fabs(V) > 50.01)  // must be -50mV - +50mV
    {
        logCalMsg(CAL_LOG_MSG, 2,
          "\nCAL_ERR_001: Error: checkDongleID: Voltage out of range(%0.4f)\n",
                    V);
        return -1;
    }
    // set each pre-amp ctrl high and measure pre-amp voltage
    // idx 0: pre-amp ctrl A, 1: ctrl B, 2: ctrl C
    for (i = 0; i < 3; i++)
    {
        setCtrlRegBits(3, 0xffff, ((0x10 << i)| 0x1));  
        usleep(30000); // at least 10ms
prtReg(0);
        getAvgTADCVolt(T_ADC_CHAN_PREAMP, 1000, 0, 0, &(preAmpV[i]));
        usleep(10000); 
    }
    setCtrlRegBits(3, 0xffff, 0x1);  // restore register content
prtReg(0);
    logCalMsg(CAL_LOG_MSG, 2, "Dongle Id: %d [ %0.6f : %0.6f : %0.6f (V)]",
               ID, preAmpV[0], preAmpV[1], preAmpV[2]);
    dID = findDongleId(preAmpV, &dIdx);
    if (dID == 0)
    {
        logCalMsg(CAL_LOG_MSG, 2,
          "\nCAL_ERR_002: Error: checkDongleID: Failed to find ID[%0.3f:%0.3f:%0.3f]\n", preAmpV[0], preAmpV[1], preAmpV[2]);
        return -1;
    }
    if (ID != dID)
    {
        logCalMsg(CAL_LOG_MSG, 2,
                "\nCAL_ERR_004: Error: checkDongleID: Incorrect dongle[%d:%d]", 
                        dID, ID);
        return -1;
    }
    preAmpV[i] *= 1000.0; // convert to mV
    // check to make sure non-high entries are within -50 - +50 mV
    for (i = 0; i < 3; i++)
    {
        if (dIdx != i) // skip the 'high' value
        {
            if (fabs(preAmpV[i]) > 50.01)
                break;
        }
    }
    if (i < 3)
    {
        logCalMsg(CAL_LOG_MSG, 2,
                "\nCAL_ERR_003: Error: checkDongleID: Voltage out of range(%d. %0.5f)", i, preAmpV[i]);
        return -1;
    }
    return 0;
}

// IDN: Keysight Technologies,34465A,MY54501210,A.02.08-02.37-02.08-00.49-02-01
static void getDmmModel(char *idnStr, char *dmmModel, int len)
{
    char *sPtr, *ePtr;
    int mLen;
    dmmModel[0] = '\0';
    if ((sPtr = strchr(idnStr, ',')) != 0) // ',' before model string
    {
        ePtr = strchr((sPtr+1), ',');     // ',' after model string
        mLen = ePtr - sPtr; // has space for '\0'
        if (mLen < len)
            len = mLen;
        strncpy(dmmModel, (sPtr+1), len);
        dmmModel[len-1] = '\0';
    }
}

// uptime format:  +0,+0,+0,+59. (days,hours,min,sec)
static int getDMMUpTime(char *reply, int *day, int *hr, int *min, int *sec)
{
    if (sscanf(reply, "%d,%d,%d,%d", day, hr, min, sec) != 4)
    {
        logCalMsg(CAL_LOG_ERR, 2, "verifyDMM: Uptime: unable to parse(%s)", reply);
        return -1;
    }
    return 0;
}

// calibration date format: +2015,+6,+23. (year,month,day)
static int checkDMMCalDate(char *calDate, char *cDate)
{
    int calYr, calM, calD;
    int yr, m, d;
    time_t tCal, tNow;
    struct tm tmVal;
    int tDiff;
    if (sscanf(calDate, "%d,%d,%d", &calYr, &calM, &calD) != 3)
    {
        logCalMsg(CAL_LOG_ERR, 2, "verifyDMM: calDate: unable to parse(%s)", calDate);
        return -1;
    }
    if (sscanf(cDate, "%d,%d,%d", &yr, &m, &d) != 3)
    {
        logCalMsg(CAL_LOG_ERR, 2, "verifyDMM: Date: unable to parse(%s)", cDate);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "DMM: Cal. date: %d/%d/%d, Today: %d/%d/%d",
            calM, calD, calYr, m, d, yr);
    memset(&tmVal, 0, sizeof(struct tm));
    // convert these to time_t format to find the difference in time
    tmVal.tm_mday = calD;
    tmVal.tm_mon  = calM - 1; // range is 0 - 11
    tmVal.tm_year = calYr - 1900;
    tCal = mktime(&tmVal);
    if (tCal <= 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "verifyDMM: calDate: error mktime(%d:%d:%d)", 
                                tmVal.tm_mday, tmVal.tm_mon, tmVal.tm_year);
        return -1;
    }

    tmVal.tm_mday = d;
    tmVal.tm_mon  = m - 1; // range is 0 - 11
    tmVal.tm_year = yr - 1900;
    tNow = mktime(&tmVal);
    if (tNow <= 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "verifyDMM: Date: error mktime(%d:%d:%d)", 
                                tmVal.tm_mday, tmVal.tm_mon, tmVal.tm_year);
        return -1;
    }
    tDiff = (int)(tNow - tCal);
    if (tDiff > (364 * 24 * 60 * 60))  // one year in seconds
        logCalMsg(CAL_LOG_WARN, 2, "verifyDMM: DMM calibration was done %d days back.", (tDiff / (24 * 60 * 60)));
    else
    {
        logCalMsg(CAL_LOG_MSG, 2, "verifyDMM: DMM calibrated %d days back", 
                        (tDiff / (24 * 60 * 60))); 
    }     
    return 0;
}

static int verifyDMM(int dmmSock)
{
    char reply[256];
    char cDate[256];
    char dmmModel[32];
    // reset DMM
    if (writeDMMString(dmmSock, "*RST\n") <= 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "verifyDMM: Failed to RESET DMM");
        return -1;
    }
    usleep(100000);

    // retrieve DMM identity string(see next line for sample)
    // Keysight Technologies,34465A,MY54501210,A.02.08-02.37-02.08-00.49-02-01
    if (getDMMString(dmmSock, "*IDN?\n", reply, 256) <= 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "verifyDMM: Failed to retrieve DMM identity");
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "DMM IDN: %s", reply);
    getDmmModel(reply, dmmModel, 32); 
    logCalMsg(CAL_LOG_MSG, 2, "DMM MODEL: %s", dmmModel);

    // TODO: check DMM models
    

    if (strcmp(dmmModel, "34465A") == 0)
    {
    // get DMM uptime, format:  +0,+0,+0,+59. (days,hours,min,sec)
    if (getDMMString(dmmSock, "SYST:UPTIME?\n", reply, 256) > 0)
    {
        int day, hr, min, sec;
        if (getDMMUpTime(reply, &day, &hr, &min, &sec) != 0)
            return -1; // message already logged
        if ((day > 0) || (hr > 0)) // more than 1 hour of uptime..
            logCalMsg(CAL_LOG_MSG, 2, "DMM uptime is %d days %dh %dm %ds", day, hr, min, sec);
        else
            logCalMsg(CAL_LOG_WARN, 2, "verifyDMM: DMM uptime < 1 hour[%dm:%ds]", min, sec); 
    }
    else // may be this model does not support it..
        logCalMsg(CAL_LOG_ERR, 2, "verifyDMM: Failed to retrieve DMM uptime");

    // retrieve DMM calibration date and make sure it not older than 1yr
    // format: +2015,+6,+23. (year,month,day)
    if (getDMMString(dmmSock, "SYST:DATE?\n", cDate, 256) <= 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "verifyDMM: Failed to retrieve DMM date");
        return -1;
    }
    if (getDMMString(dmmSock, "CAL:DATE?\n", reply, 256) <= 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "verifyDMM: Failed to retrieve DMM calibration date");
        return -1;
    }
    if (checkDMMCalDate(reply, cDate) != 0) // message already logged.
        return -1;
    }

    // check input selector and make sure it is front(returns string "FRON")
    if (getDMMString(dmmSock, "ROUT:TERM?\n", reply, 256) <= 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "verifyDMM: Failed to retrieve DMM input selector");
        return -1;
    }
    if (strncmp(reply, "FRON", 4) != 0)
    {
        logCalMsg(CAL_LOG_MSG, 2, 
          "\nCAL_ERR_020: Error: DMM not set for FRONT panel control[%s]\n",reply);
        return -1;
    }
    return 0;
}

static int readPCBTemp(float *pcbT)
{
    float pcbV, pcbR;
    int i;
    computePCBTemp(&pcbV, &pcbR, pcbT);
    return 0;
}

static int setMCP4728(int ch, int gain, float V)
{
    int bus = 2, devAddr = 0x61;
    int muxChan = 1;
    int ret = 0;
    if (lockI2CBus(bus) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "setMCP4728: failed to acquire i2c lock");
        return -1;
    }
    if ((ret = i2cmux_select_chan(muxChan)) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "setMCP4728: select mux channel(%d)\n", ret);
        unlockI2CBus(bus);
        return -1;
    }
    ret = mcp4728Write(bus, devAddr, ch, gain, V);
    unlockI2CBus(bus);
    if (ret != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "setMCP4728 write (%d)", ret);
        return -1;
    }
    return 0;
}

static int setMCP4726(float V)
{
    int bus = 2, devAddr = 0x60;
    int muxChan = 3;
    int ret = 0;
    if (lockI2CBus(bus) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "setMCP4726: failed to acquire i2c lock");
        return -1;
    }
    if ((ret = i2cmux_select_chan(muxChan)) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "setMCP4726: select mux channel(%d)\n", ret);
        unlockI2CBus(bus);
        return -1;
    }
    ret = mcp47x6Write(bus, devAddr, V);
    unlockI2CBus(bus);
    if (ret != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "setMCP4726 write (%d)", ret);
        return -1;
    }
    return 0;
}

/* 
 * Default setup and pre-checks:
 * 1. Configure Obrero signals: set register bits.
 * 2. Setup clkgen
 * 3. Set values in DACs(MCP4728 & DAC8560)
 * 4. Check Dongle ID
 * 5. Verify and log DMM type, setup, uptimes, Temperatures etc
 */
#define DAYS(intVal) (intVal / (60 * 60 * 24))
#define HRS(intVal)  ((intVal / (60 * 60)) % 24)
#define MIN(intVal)  ((intVal / 60) % 60)
#define SEC(intVal)  (intVal % 60)
int dodefaultSetup(int dmmSock, int dongleId)
{
    int ret = 0;
    int i;
    int intVal;
    setCtrlRegBits(0, 0xffff, 0x200); // IO_DRV_EN
    // clkgen command holds i2c lock
    ret = system("/bin/dsh clkgen config /fpga/register_map_clkgen.txt >/tmp/clkgen.msg 2>&1");
    if (ret != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "preCheck: config clkgen failed");
        return -1;
    }
    // set all other signals
    setCtrlRegBits(1, 0xffff, 0xC0);  // heat_v_range[1:0] 11b
    setCtrlRegBits(2, 0xffff, 0x3);   // drtd_cal_sel[1:0] 11b
    setCtrlRegBits(3, 0xffff, 0x1);   // drtd_sample  1b
    setCtrlRegBits(4, 0xffff, 0x0);  
    setCtrlRegBits(5, 0xffff, 0x0);  
    // set MCP4728 to gain 2x and 2.56V for channels 1 and 2
    setMCP4728(1, 2, 2.56);
    usleep(20000);
    setMCP4728(2, 2, 2.56);
    usleep(10000);
    setHeaterDAC(0.0); // set heater output to 0.

    ret = checkDongleID(dongleId);
#if 1 //TODO: enable this check
    if ((dongleId != 7) && (ret != 0)) // TODO
        return -1;
#endif
    if ((intVal = getSysUpTime()) < 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "preCheck: Failed to get system uptime");
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "DUT uptime is %d days %dh %dm %ds",
            DAYS(intVal), HRS(intVal), MIN(intVal), SEC(intVal));
    // make sure uptime is at least 30 mts
    if (intVal < (30*60))
        logCalMsg(CAL_LOG_WARN, 2, "preCheck: DUT uptime is < 30 min.");

    // DMM checks
    if (verifyDMM(dmmSock) != 0)
        return -1;

    // log Obrero PCB temp
    if (readPCBTemp(&T_pcb_cal_start) != 0)
        return -1;
    logCalMsg(CAL_LOG_MSG, 2, "T pcb_cal_start: PCB Temp: %0.2f", T_pcb_cal_start);
    if (dongleId == 1)
    {
        if ((T_pcb_cal_start < 25.0) || (T_pcb_cal_start > 50.0))
        {
            logCalMsg(CAL_LOG_MSG, 2, 
                    "\nCAL_ERR_043: Error: PCB temperature not within range\n");
//            return -1;
        }
    }
 
    // Check mains sync TODO

    return 0;
}

static int readCJCTemp(float *cjcTemp)
{
    int bus = 2, devAddr = 0x68;
    float temp;
    int ret = 0, j;
    int gain = 2;
    setCtrlRegBits(0, 0x400, 0x400);
    usleep(120000);
    // read CJC temperature and log it
    // read i2c device channel 1 to get voltage.
    ret = mcp3424Read(bus, devAddr, 1, gain, &temp);
    if (ret != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "Read CJC temperature(%d)", ret);
        return -1;
    }
    *cjcTemp = temp/10.0; // 10mv/degree, @0degC, o/p is 0V
    return 0;
}
static int readCJCTempDelay(float *cjcTemp, unsigned int waitUs)
{
    int bus = 2, devAddr = 0x68;
    float temp;
    int ret = 0, j;
    int gain = 2;
    setCtrlRegBits(0, 0x400, 0x400);
    usleep(waitUs); // wait 30 seconds
    // read CJC temperature and log it
    // read i2c device channel 1 to get voltage.
    ret = mcp3424Read(bus, devAddr, 1, gain, &temp);
    if (ret != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "Read CJC temperature(%d)", ret);
        return -1;
    }
    *cjcTemp = temp/10.0; // 10mv/degree, @0degC, o/p is 0V
    return 0;
}


static int readCJCVolt(int gain, float *cjcV)
{
    int bus = 2, devAddr = 0x68;
    int ret = 0;
    // read i2c device channel 1 to get voltage.
    ret = mcp3424Read(bus, devAddr, 1, gain, cjcV);
    if (ret != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "Read CJC voltage(%d)", ret);
        return -1;
    }
    return 0;
}

int calReadCJCRawVal(int *rawVal)
{
    int bus = 2, devAddr = 0x68;
    int ret = 0;
    int gain = 2;
    // read i2c device channel 1 to get voltage.
    ret = mcp3424ReadRaw(bus, devAddr, 1, gain, rawVal);
    if (ret != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "Read CJC register(%d)", ret);
        return -1;
    }
    return 0;
}

static int mfgBrdRev; // to store board(PCA) revision number 
static void printBoardDetails(int dongleId)
{
    int ret, i, j;
    ObreroMfgInfo mfgInfo;
    char mfgBrdRevStr[24];
    struct tm *tmP;
    time_t t;
    char brdSerial[17], *bP;
    if (tryLockI2C(I2C_MUX_BUS) != 0)
        return;
    ret = readMfgData(0, &mfgInfo);
    unlockI2CBus(I2C_MUX_BUS);
    // make sure serial number is clean.
    for (i = 0, j = 0; i < 16; i++)
    {
        if ((mfgInfo.mfgBrdSerial[i] >= ' ') && (mfgInfo.mfgBrdSerial[i] < 126))
        {
            brdSerial[j] = mfgInfo.mfgBrdSerial[i];
            j++;
        }
    }
    brdSerial[j] = '\0';
    bP = stripWhiteSpace(brdSerial);
    if (ret == 0)
    {
        logCalMsg(CAL_LOG_MSG, 2, "\nBoard: Serial [%16s]  Rev [%8s]\n",
                bP, mfgInfo.mfgBrdRev);
    }
    // generate log file name..
    time(&t);
    tmP = gmtime(&t);
    // filename format: 1905_serialnumber_DongleTestNumber_mmddyyyy
    if (tmP != 0)
    {
        // filename format: 1905_serialnumber_DongleTestNumber_mmddyyyy
        snprintf(logFile, 127, 
                "/mnt/nv/1905_%s_D%d_%02d%02d%04d_%02d%02d%02d.log",
                bP, dongleId, (tmP->tm_mon+1), tmP->tm_mday, 
                (1900+tmP->tm_year), tmP->tm_hour, tmP->tm_min, tmP->tm_sec);
        logFile[127] = '\0';
        {printf("LogFile: [%s]\n", logFile);fflush(stdout);}
    }
    // make sure board revision is clean and copy to global
    for (i = 0; i < 8; i++)
    {
        if ((mfgInfo.mfgBrdRev[i] < ' ') || (mfgInfo.mfgBrdRev[i] > 126))
            mfgInfo.mfgBrdRev[i] = ' ';
    }
    bP = stripWhiteSpace(mfgInfo.mfgBrdRev);
    if (*bP != '\0')
    {
        strncpy(mfgBrdRevStr, bP, 8);
        mfgBrdRevStr[8] = '\0';
    }
    else
    {
        mfgBrdRevStr[0] = '0';
        mfgBrdRevStr[1] = '\0';
    }
    mfgBrdRev = atoi(mfgBrdRevStr);
    {printf("PCA Rev: [%d]\n", mfgBrdRev);fflush(stdout);}
}

static int doPreChecks(int dmmSock, int dongleId, int chkCJC)
{
    logCalMsg(CAL_LOG_MSG, 1, "\npre-checks:");
    float minT = 20.0, maxT = 40.0;

    // default set-up and pre-check
    if (dodefaultSetup(dmmSock, dongleId) != 0)
        return -1;

    if (chkCJC)
    {
        // check and log CJC temperature
        // wait for 30 seconds after turning on CJC power before reading
        if (readCJCTempDelay(&T_cjc_cal_start, 30000000) != 0)
            return -1;
        logCalMsg(CAL_LOG_MSG, 2, "T cjc_cal_start %0.2f C", T_cjc_cal_start);
        // range changed to 20-40 on suggestion from AlistairM
        if ((T_cjc_cal_start < minT) || (T_cjc_cal_start > maxT))
        {
            logCalMsg(CAL_LOG_MSG, 2, 
                    "\nCAL_ERR_005: CJC temperature outside range(%0.2f - %02f C)\n", minT, maxT);
            return -1;
        }
    }

    printBoardDetails(dongleId); // DongleID for log file creation

    return 0;
}

int doEndSettings()
{
    int i;
    logCalMsg(CAL_LOG_MSG, 1, "\nendSettings:");
    setHeaterDAC(0.0);    // set heater output to 0.
    // set Aux_Analog output to 0
    setMCP4726(0.0);
    // set channel 1 and 2 of MPC4728 to 0.0(power on default)
    setMCP4728(1, 1, 0.0);
    setMCP4728(2, 1, 0.0);

    setCtrlRegBits(1, 0xffff, 0xC0);  // heat_v_range[1:0] 11b
    usleep(120000);

    setCtrlRegBits(0, 0xffff, 0x200);
    setCtrlRegBits(2, 0xffff, 0x3);
    setCtrlRegBits(3, 0xffff, 0x1);
    setCtrlRegBits(4, 0xffff, 0x0);  
    setCtrlRegBits(5, 0xffff, 0x0);  

    return 0;
}

// Quick and easy way to setup DMM. Have config strings for each settings in
// an array.
enum 
{
    DMM_100mVDC_100NPLC = 0,
    DMM_10VDC_10NPLC,
    DMM_10VDC_100NPLC,
    DMM_1VDC_100NPLC,
    DMM_1GOHM_100NPLC,
    DMM_100VAC_20HZ,
    DMM_100mVAC_20HZ,
    DMM_10KOHM_100NPLC,
    DMM_1VDC_10NPLC,
    DMM_100mVDC_10NPLC,
    DMM_100OHM_10NPLC,
    DMM_100VDC_10NPLC,
    DMM_1MOHM_1NPLC,
    DMM_100MOHM_1NPLC,
    DMM_10KOHM_10NPLC,
    DMM_100MOHM_100NPLC,
    // Add new settings above this. 
    // NOTICE: Update the range array after adding any setting
    MAX_DMM_SETTINGS,
};

// Range of value expected for each DMM setting. Report error if DMM reads a 
// value outside this range.

typedef struct tagDMMReadRange
{
    double minVal;
    double maxVal;
} DMMReadRange;

static DMMReadRange DMMValidRange[] =
{
    /* DMM_100mVDC_100NPLC */ { -5e2,   5e2   },
    /* DMM_10VDC_10NPLC    */ { -100.0, 100.0 }, 
    /* DMM_10VDC_100NPLC   */ { -100.0, 100.0 }, 
    /* DMM_1VDC_100NPLC    */ { -10.0,  10.0  }, 
    /* DMM_1GOHM_100NPLC   */ { -2e5,   2e9   },
    /* DMM_100VAC_20HZ     */ { -1e3,   1e3   },
    /* DMM_100mVAC_20HZ    */ { -1.0,   1.00  },
    /* DMM_10KOHM_100NPLC  */ { -1e5,   1e6   },
    /* DMM_1VDC_10NPLC     */ { -10.0,  10.0  },
    /* DMM_100mVDC_10NPLC  */ { -1.0,   1.00  },
    /* DMM_100OHM_10NPLC   */ { -1e3,   1e3   },
    /* DMM_100VDC_10NPLC   */ { -1e3,   1e3   },
    /* DMM_1MOHM_1NPLC     */ { -1e3,   1e8   },
    /* DMM_100MOHM_1NPLC   */ { 0.00,   0.00  }, // do not error check
    /* DMM_10KOHM_10NPLC   */ { -1e5,   1e8   },
    /* DMM_100MOHM_100NPLC */ { 0.00,   0.00  }, // do not error check
};

// 100mV, 100NPLC, hi-Z on
char *dmm100mVDC100NPLCStr[] = 
{
    "CONF:VOLT:DC 100 mV\n",
    "SENS:VOLT:DC:NPLC 100\n",
    "VOLTAGE:DC:IMPEDANCE:AUTO 1\n",
};

// 10VDC, 100 NPLC, hi-Z
char *dmm10VDC100NPLCStr[] = 
{
    "CONF:VOLT:DC 10 V\n",
    "SENS:VOLT:DC:NPLC 100\n",
    "VOLTAGE:DC:IMPEDANCE:AUTO 1\n",
};

// 10VDC, 10NPLC, hi-Z on
char *dmm10VDC10NPLCStr[] = 
{
    "CONF:VOLT:DC 10 V\n",
    "SENS:VOLT:DC:NPLC 10\n",
    "VOLTAGE:DC:IMPEDANCE:AUTO 1\n",
};

// 1V, 100NPLC, hi-Z on
char *dmm1VDC100NPLCStr[] = 
{
    "CONF:VOLT:DC 1 V\n",
    "SENS:VOLT:DC:NPLC 100\n",
    "VOLTAGE:DC:IMPEDANCE:AUTO 1\n",
};

// 1GOhm, 100 NPLC
char *dmm1GOhm100NPLCStr[] =
{
    "CONF:RESISTANCE 1000000000\n",
    "SENSE:RESISTANCE:NPLC 100\n",
    "SAMPLE:COUNT 1\n",
};

// 1MOhm, 1 NPLC
char *dmm1MOhm1NPLCStr[] =
{
    "CONF:RESISTANCE 1000000\n",
    "SENSE:RESISTANCE:NPLC 1\n",
    "SAMPLE:COUNT 1\n",
};

// 100MOhm, 1 NPLC
char *dmm100MOhm1NPLCStr[] =
{
    "CONF:RESISTANCE 100000000\n",
    "SENSE:RESISTANCE:NPLC 1\n",
    "SAMPLE:COUNT 1\n",
};

// 100MOhm, 100 NPLC
char *dmm100MOhm100NPLCStr[] =
{
    "CONF:RESISTANCE 100000000\n",
    "SENSE:RESISTANCE:NPLC 100\n",
    "SAMPLE:COUNT 1\n",
};

// 10KOhm, 100 NPLC
char *dmm10KOhm100NPLCStr[] =
{
    "CONF:RESISTANCE 10000\n",
    "SENSE:RESISTANCE:NPLC 100\n",
    "SAMPLE:COUNT 1\n",
};

// 10KOhm, 10 NPLC
char *dmm10KOhm10NPLCStr[] =
{
    "CONF:RESISTANCE 10000\n",
    "SENSE:RESISTANCE:NPLC 10\n",
    "SAMPLE:COUNT 1\n",
};

// 100VAC, 20Hz(medium) bandwidth
char *dmm100VAC20HZStr[] = 
{
    "CONF:VOLT:AC 100V\n",
    "SENSE:VOLT:AC:BANDWIDTH 20Hz\n",
    "SAMPLE:COUNT 1\n",
};

// 100mVAC, 20hz(medium) bandwidth
char *dmm100mVAC20HStr[] = 
{
    "CONF:VOLT:AC 100mV\n",
    "SENSE:VOLT:AC:BANDWIDTH 20Hz\n",
    "SAMPLE:COUNT 1\n",
};

// 1VDC, 10NPLC
char *dmm1VDC10NPLCStr[] = 
{
    "CONF:VOLT:DC 1 V\n",
    "SENS:VOLT:DC:NPLC 10\n",
    "VOLTAGE:DC:IMPEDANCE:AUTO 1\n",
};

// 100mV DC, 10NPLC
char *dmm100mVDC10NPLCStr[] = 
{
    "CONF:VOLT:DC 100 mV\n",
    "SENS:VOLT:DC:NPLC 10\n",
    "VOLTAGE:DC:IMPEDANCE:AUTO 1\n",
};

// 100 Ohms, 10NPLC
char *dmm100Ohm10NPLCStr[] = 
{
    "CONF:RESISTANCE 100\n",
    "SENSE:RESISTANCE:NPLC 10\n",
    "SAMPLE:COUNT 1\n",
};

// 100VDC, 10NPLC
char *dmm100VDC10NPLCStr[] = 
{
    "CONF:VOLT:DC 100 V\n",
    "SENS:VOLT:DC:NPLC 10\n",
    "VOLTAGE:DC:IMPEDANCE:AUTO 1\n",
};

static int curDMMSetIdx;
static int doDMMSetup(int dmmSock, int setting)
{
    char **setArray;
    int i;
    if (setting >= MAX_DMM_SETTINGS)
    {
        logCalMsg(CAL_LOG_ERR,2,"Invalid index for DMM setting(%d)\n", setting);
        return -1;
    }
    switch (setting)
    {
        case DMM_100mVDC_100NPLC:
            setArray = dmm100mVDC100NPLCStr;
            break;
        case DMM_10VDC_10NPLC:
            setArray = dmm10VDC10NPLCStr;
            break;
        case DMM_10VDC_100NPLC:
            setArray = dmm10VDC100NPLCStr;
            break;
        case DMM_1VDC_100NPLC:
            setArray = dmm1VDC100NPLCStr;
            break;
        case DMM_1GOHM_100NPLC:
            setArray = dmm1GOhm100NPLCStr;
            break;
        case DMM_100VAC_20HZ:
            setArray = dmm100VAC20HZStr;
            break;
        case DMM_100mVAC_20HZ:
            setArray = dmm100mVAC20HStr;
            break;
        case DMM_10KOHM_100NPLC:
            setArray = dmm10KOhm100NPLCStr;
            break;
        case DMM_10KOHM_10NPLC:
            setArray = dmm10KOhm10NPLCStr;
            break;
        case DMM_1VDC_10NPLC:
            setArray = dmm1VDC10NPLCStr;
            break;
        case DMM_100mVDC_10NPLC:
            setArray = dmm100mVDC10NPLCStr;
            break;
        case DMM_100OHM_10NPLC:
            setArray = dmm100Ohm10NPLCStr;
            break;
        case DMM_100VDC_10NPLC:
            setArray = dmm100VDC10NPLCStr;
            break;
        case DMM_1MOHM_1NPLC:
            setArray = dmm1MOhm1NPLCStr;
            break;
        case DMM_100MOHM_1NPLC:
            setArray = dmm100MOhm1NPLCStr;
            break;
        case DMM_100MOHM_100NPLC:
            setArray = dmm100MOhm100NPLCStr;
            break;
    }
    curDMMSetIdx = setting;
    for (i = 0; i < 3; i++)
    {
        if (writeDMMString(dmmSock, setArray[i]) <= 0)
            break;
        usleep(10000);
    }
    if (i < 3)
        goto errDMMSetup;
    // read only one value at a time
    if (writeDMMString(dmmSock, "SAMPLE:COUNT 1\n") <= 0)
        goto errDMMSetup;
    usleep(10000);
    // TODO: verify values
    return 0;
errDMMSetup:
    logCalMsg(CAL_LOG_ERR, 2, "Failed to setup DMM");
    return -1;
}

// log ERRORS from DMM
static void checkDMMErrors(dmmSock)
{
    char reply[256];
    char *sPtr;
    int err;
    if (getDMMString(dmmSock, "SYST:ERR?\n", reply, 256) <= 0)
    {
        logCalMsg(CAL_LOG_ERR,2,"checkDMMErrors: failed to read error status");
        return;
    }
    if ((sPtr = index(reply, ',')) != 0)
    {
        *sPtr = '\0';
        if (convToInt(reply, &err) == 0)
        {
            if (err != 0)
                logCalMsg(CAL_LOG_ERR, 2, "checkDMMErrors: ERROR %s", reply);
            else
                logCalMsg(CAL_LOG_MSG, 2, "checkDMMErrors: No Errors");
        }
        else
        logCalMsg(CAL_LOG_ERR, 2, "checkDMMErrors: (1)parse error (%s)", reply);
    }
    else
        logCalMsg(CAL_LOG_ERR, 2, "checkDMMErrors: (2)parse error (%s)", reply);
    return;
}

static int dbgDMMR = 0;
static int getDMMReading(int dmmSock, double *V)
{
    char reply[256];
    enum { value_ok, value_too_low, value_too_high };
    int vState = value_ok;
    if (getDMMString(dmmSock, "READ?\n", reply, 256) <= 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "%s: Failed to read DMM", __func__);
        return -1;
    }
    if (dbgDMMR)
        logPrint("= DMM Replly: %s\n", reply);
    if (cvtToDouble(reply, V) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "%s: Failed to convert value", __func__);
        return -1;
    }
    if ((curDMMSetIdx == DMM_100MOHM_1NPLC) || (curDMMSetIdx == DMM_100MOHM_100NPLC))
        goto DMMReadDone;
#if 0
    if (fabs(*V) < DMMValidRange[curDMMSetIdx].minVal)
        vState = value_too_low;
    if (fabs(*V) > DMMValidRange[curDMMSetIdx].maxVal)
        vState = value_too_high;
#else
    if ((*V) < DMMValidRange[curDMMSetIdx].minVal)
        vState = value_too_low;
    if ((*V) > DMMValidRange[curDMMSetIdx].maxVal)
        vState = value_too_high;
#endif
    if (vState != value_ok)
    {
//logCalMsg(CAL_LOG_MSG, 2, "DMM: %0.10f[curIdx: %d, %0.10f - %0.10f]\n", 
//    (*V), curDMMSetIdx, DMMValidRange[curDMMSetIdx].minVal, 
//    DMMValidRange[curDMMSetIdx].maxVal);
        logPrint("      Value read is too ");
        if (vState == value_too_low)
            logPrint("LOW [%0.8f]\n", (*V));
        else
            logPrint("HIGH [%0.6f]\n", (*V));
        logCalMsg(CAL_LOG_ERR, 2,
                "DMM may not be connected properly to Dongle."); 
        logCalMsg(CAL_LOG_MSG, 2, 
                "Please check connections and re-run the calibration routine.");
        return -1;
    }
DMMReadDone:
    return 0;
}

static int adjustDMMFor100uA_Stim(int dmmSock, float tgt_mV, float minV, 
                                        float maxV, float *dacV)
{
    int ret = 0;
    double V;
    double low, high, mid;
    int gain = 2, bus = 2;
    int ch = 1;
    int devAddr = 0x61;
    low = minV;  high = maxV;
    for (; ;)
    {
        mid = low + ((high - low) / 2.0);
        // set MPC4728 to x2 gain and apply voltage
        if ((ret = mcp4728Write(bus, devAddr, ch, gain, mid)) != 0)
        {
            logCalMsg(CAL_LOG_ERR, 2, "DRTD100uA_Stim_Cal: failed to write to MPC4728(%d)", ret);
            return ret;
        }
        usleep(10000);
        if ((ret = mcp4728Write(bus, devAddr, (ch+1), gain, mid)) != 0)
        {
            logCalMsg(CAL_LOG_ERR, 2, "DRTD100uA_Stim_Cal: failed to write to MPC4728(%d)", ret);
            return ret;
        }
        usleep(10000);
        if (getDMMReading(dmmSock, &V) != 0)
        {
           logCalMsg(CAL_LOG_ERR,2, "DRTD_100uA_Stimulus: Failed to read voltage");
            ret = -1;
            break;
        }
        V *= 1000.0;
#if 1
        logPrint("adjDMMFor100uAStim: l: %0.4f  h: %0.4f  m: %0.2f -> %0.4f mV[%0.4f mV]\n", 
                            __func__, low, high, mid, V, tgt_mV);
#endif
        if ((fabs(V - tgt_mV)) <= 0.0050) // 0.01% of 50mV
        {
            *dacV = (float)mid;
            break;
        }
        if (fabs(V) > fabs(tgt_mV))
            low = mid;
        else if (fabs(V) < fabs(tgt_mV))
            high = mid;
        if (fabs(high - low) < 0.010)
        {
            *dacV = (float)mid;
            logPrint("%s: Exit at l: %0.4f  h: %0.4f\n", __func__, low, high);
            break;
        }
    }
    return ret;
}

static int stim100uAOK = 0;
static int doDRTD_100uA_Stimulus_Calibration(int dmmSock, int *eSkips)
{
    float dacV;
    int bus = 2, muxChan = 1;
    int ret = 0;

    logCalMsg(CAL_LOG_MSG, 1, "\nDRTD 100uA Stimulus Calibration:");

    // set DMM to 100mV DC range, 100NPLC avg and hi-z(hi-imp) input
    if (doDMMSetup(dmmSock, DMM_100mVDC_100NPLC) != 0)
        return -1;

    setCtrlRegBits(0, 0x400, 0x0);   // disable CJC_PWR
    setDRTDGain(GAIN_X21);           // set gain to 21/1.01
    setDRTDCalSel1(DRTD_CAL_NORM);   // external DRTD
    if (lockI2CBus(bus) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "Failed to acquire lock i2c bus\n");
        return -1;
    }
    if ((ret = i2cmux_select_chan(muxChan)) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "Selecting mux. chan.(%d)\n", ret);
        unlockI2CBus(bus);
        return -1;
    }
    // do calibration for +100uA stimulus
    setStimulusCurrent(STIM_100uA, 0);  // set stimulus to +100uA
prtReg(0);
    if ((ret = adjustDMMFor100uA_Stim(dmmSock, 50.0, 0.0, 3.3, &dacV)) != 0)
        goto err100uAStim;
    logCalMsg(CAL_LOG_MSG, 2, "DAC stim_trim_100u_p: %0.3f V", dacV);
    if ((dacV < MIN_TRIM_VOLTS) || (dacV > MAX_TRIM_VOLTS))
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_054: DRTD +100uA Stimulus, invalid DAC voltage\n");
        pauseTest();
        if (!(*eSkips))
            goto err100uAStim;
        (*eSkips)--;
    }
#ifdef DO_INI_SAVE
    else
    {
        // Save value to persistent storage
        sprintf(itemStr, "%s", STIM_TRIM_100U_P);
        sprintf(fltVal, "%0.10f", dacV);
        if (ini_puts(sectStr, itemStr, fltVal, SYS_INI_FILE) != 1)
            logCalMsg(CAL_LOG_ERR, 2, 
                    "Failed to save calibration value(Stim_Trim_100u_P)");
        stim100uAOK++;
    }
#endif

    // do calibration for -100uA stimulus
    setStimulusCurrent(STIM_100uA_N, 0);  // set stimulus to -100uA
prtReg(0);
    if ((ret = adjustDMMFor100uA_Stim(dmmSock, -50.0, 0.0, 3.3, &dacV)) != 0)
        goto err100uAStim;
    unlockI2CBus(bus);
    logCalMsg(CAL_LOG_MSG,  2, "DAC stim_trim_100u_m: %0.3f V", dacV);
    if ((dacV < MIN_TRIM_VOLTS) || (dacV > MAX_TRIM_VOLTS))
    {
        logCalMsg(CAL_LOG_MSG, 2, 
          "\nCAL_ERR_054: DRTD -100uA Stimulus, invalid DAC voltage(%0.3f)\n",
                    dacV);
        pauseTest();
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
    }
#ifdef DO_INI_SAVE
    else
    {
        // Save value to persistent storage
        sprintf(itemStr, "%s", STIM_TRIM_100U_N);
        sprintf(fltVal, "%0.8f", dacV);
        if (ini_puts(sectStr, itemStr, fltVal, SYS_INI_FILE) != 1)
            logCalMsg(CAL_LOG_ERR, 2, 
                        "Failed to save calibration value(Stim_Trim_100u_N)");
        stim100uAOK++;
    }
#endif
    return 0;
err100uAStim:
    unlockI2CBus(bus);
    return -1;
}

static int doTCGainStep(int dmmSock, double *gVal, int *eSkips)
{
    double srcZero, adcZero;
    double srcMax, adcMax;
    double GtcIdeal = GTC_IDEAL;

    setCtrlRegBits(0, 0x400, 0x0);  // disable CJC power
    setVoltageRange(0.0);           // set o/p range to 0.0
    setHeaterDACRegVal(0);          // set heater dac o/p to 0x0
    usleep(550000);
prtReg(0);
    // DMM set to 100mV reading in previous step..
    if (getDMMReading(dmmSock, &srcZero) != 0)
    {
        logCalMsg(CAL_LOG_ERR,  2, "TCGainCalibration: Failed to read srcZero");
        return -1;
    }
    srcZero *= 1000.0;
    logCalMsg(CAL_LOG_MSG,  2, "V source_zero : %0.6f mV", srcZero);
    if (fabs(srcZero) > 10e-3) // 10uV, srcZero in mV
    {
        logCalMsg(CAL_LOG_MSG, 2,  
              "\nCAL_ERR_012: TCGainCalibration: V source_zero out of range\n");
        pauseTest();
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
    }
    getAvgTADCFpgaVal(T_ADC_CHAN_TC, 10, 1, &adcZero);
    adcZero *= 1000.0;
    adcZero /= GtcIdeal;
    logCalMsg(CAL_LOG_MSG,  2, "V adc_zero : %0.6f mV", adcZero);
    if (fabs(adcZero) > 118e-3) // 118uV, adcZero in mV
    {
        logCalMsg(CAL_LOG_MSG, 2,  
              "\nCAL_ERR_013: TCGainCalibration: V adc_zero out of range\n");
        pauseTest();
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
    }

    setCtrlRegBits(0, 0x400, 0x400); // enable CJC power
    setVoltageRange(40.0);           // set o/p range to 40.0
    setHeaterDACRegVal(34562);       // set heater dac o/p,val.from.cal.routine
    usleep(550000);
prtReg(0);
    if (getDMMReading(dmmSock, &srcMax) != 0)
    {
        logCalMsg(CAL_LOG_ERR,  2, "TCGainCalibration: Failed to read srcMax");
        return -1;
    }
    srcMax *= 1000.0; // converto to mV
    logCalMsg(CAL_LOG_MSG,  2, "V source_max : %0.6f mV", srcMax);
    {
        double low, high;
        double ideal = 50.0;
        double offs = ideal * 3.0 /100.0; // 3% of 50mV
        low = ideal - offs;  
        high = ideal + offs;
        if ((srcMax < low) || (srcMax > high))  // srcMax in mV
        {
            logCalMsg(CAL_LOG_MSG, 2,  
               "\nCAL_ERR_006: TCGainCalibration: V source_max out of range\n");
            pauseTest();
            if (!(*eSkips))
                return -1;
            (*eSkips)--;
        }
    }
    getAvgTADCFpgaVal(T_ADC_CHAN_TC, 10, 1, &adcMax);
    adcMax *= 1000.0; // convert to mV
    logCalMsg(CAL_LOG_MSG,  2, "V adc_max : %0.6f mV\n", adcMax);
    // find gain value
    *gVal = (adcMax - adcZero) / (srcMax - srcZero);
    return 0;
}

int tcGainOK = 0;
#define TC_GAIN_CNT (10)
static int doTCGainCalibration(int dmmSock, int *eSkips)
{
    int i;
    double gV[TC_GAIN_CNT];
    double Gtpos, Gtneg, tmp, diff;
    double Gtc;
    double GtcIdeal = GTC_IDEAL;

    logCalMsg(CAL_LOG_MSG, 1, "\nTC gain calibration:");

    // New calibration routine for D1(Oct16,2015:Alistair May)
    memset(&gV, 0, (TC_GAIN_CNT * sizeof(double)));

    setStimulusCurrent(STIM_0uA, 1); // set stimulus to 0uA
    setCtrlRegBits(1, 0x1, 0x1);  // enable bake out
    usleep(500000);

    for (i = 0; i < TC_GAIN_CNT; i++)
    {
        if (doTCGainStep(dmmSock, &(gV[i]), eSkips) != 0)
            break;
        usleep(10000);
    }
    if (i < TC_GAIN_CNT)
    {
        if (!(*eSkips))
            goto errTCGain;
tcGainOK--;
        (*eSkips)--;
    }
    // compute G t_pos
    for (i = 0, tmp = 0.0; i < TC_GAIN_CNT; i++)
    {
        logCalMsg(CAL_LOG_MSG,  2, "+TCGainCalibration[%d] %0.6f", i, gV[i]);
        tmp += gV[i];
    }
    Gtpos = tmp / (float)TC_GAIN_CNT;
    // Check that each voltage is is within 0.005% of Gtpos...
    diff = (Gtpos * 0.005) / 100.0;
    logCalMsg(CAL_LOG_MSG,  2, "Gtpos %0.4f\n", Gtpos);
    for (i = 0; i < TC_GAIN_CNT; i++)
    {
        if ((gV[i] < (Gtpos - diff)) || (gV[i] > (Gtpos + diff)))
        {
            logCalMsg(CAL_LOG_MSG, 2, 
            "\nCAL_ERR_009: TCGainCalibration: gain values not within range (%0.4f [%0.4f - %0.4f])\n", gV[i], (Gtpos-diff), (Gtpos+diff));
            pauseTest();
            break;
        }
    }
    if (i < TC_GAIN_CNT)
    {
        if (!(*eSkips))
            goto errTCGain;
tcGainOK--;
        (*eSkips)--;
    }
    Gtc = Gtpos;
    logCalMsg(CAL_LOG_MSG,  2, "Gtc %0.6f\n", Gtc);

    // make sure Gtc is within 0.75% of ideal value:GtcIdeal
    diff = (GtcIdeal * 0.755) / 100.0;

    if ((Gtc < (GtcIdeal - diff)) || (Gtc > (GtcIdeal + diff)))
    {

        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_011: TCGainCalibration: Gtc not within 0.75%% of ideal[%0.6f]\n", GtcIdeal);
        pauseTest();

        if (!(*eSkips))
            goto errTCGain;

tcGainOK--;
        (*eSkips)--;
    }

    setHeaterDACRegVal(0);
    setVoltageRange(0.0);

    if (tcGainOK == 0)
    {
#ifdef DO_INI_SAVE
        // save Gtc to persistent storage
        sprintf(itemStr, "%s", TC_GTC_GAIN);
        sprintf(fltVal, "%0.10f", Gtc);
        if (ini_puts(sectStr, itemStr, fltVal, SYS_INI_FILE) != 1)
            logCalMsg(CAL_LOG_ERR, 2, "Failed to save calibration value(Gtc)");
#endif
        // set shared variable, start using this value from now on
        pCache->tcGTC = Gtc;
    }
    return 0;
errTCGain:
    setHeaterDACRegVal(0);
    setVoltageRange(0.0);
    return -1;
}

static int doCheckTCZero(int dmmSock, int *eSkips)
{
    double tcV;

    logCalMsg(CAL_LOG_MSG, 1, "\nCheck TC Zero:");

    setCtrlRegBits(1, 0x1, 0x1);     // enable bake out
    setCtrlRegBits(0, 0x400, 0x400); // enable CJC power
    setVoltageRange(40.0);           // set o/p range to 0.0
    setHeaterDACRegVal(34562);       // set heater dac o/p to 22.05V
    setCtrlRegBits(3, 0x4, 0x4);     // set TC_ZERO to 1
    usleep(550000);
prtReg(0);
    // measure TC voltage
    getAvgTADCFpgaVal(T_ADC_CHAN_TC, 10, 0, &tcV);
    logCalMsg(CAL_LOG_MSG,  2, "V tc_zero %0.8f V", tcV);
    // set TC_ZERO to 0
    setCtrlRegBits(3, 0x4, 0x0);
    setHeaterDACRegVal(0);
    setVoltageRange(0.0);
prtReg(0);
    if (fabs(tcV) > 118e-6)
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_007: checkTCZero: voltage not within range");
        pauseTest();
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
    }
    else
    {
#ifdef DO_INI_SAVE
        // Save value to persistent storage
        sprintf(itemStr, "%s", TC_ZERO_V);
        sprintf(fltVal, "%0.10f", tcV);
        if (ini_puts(sectStr, itemStr, fltVal, SYS_INI_FILE) != 1)
            logCalMsg(CAL_LOG_ERR, 2, 
                                "Failed to save calibration value(TC_Zero)");
#endif
        // set shared variable, start using this value
        pCache->tcVZero = tcV; 
    }
    return 0;
}

static int doTCGainCheck(int dmmSock, int *eSkips)
{
    double vSrcChkP50, vADCChkP50;
    double vSrcChkP25, vADCChkP25;
    double vSrcChkP0, vADCChkP0;
    double vSrcChkN50, vADCChkN50;
    double avgVal, propErr, err;
    double offsErr = 10e-3; // 10uV, 10e-3 mV

    logCalMsg(CAL_LOG_MSG, 1, "\nTC Gain Check:");

    // P50
    setCtrlRegBits(1, 0x1, 0x1);     // enable bake out
    setCtrlRegBits(0, 0x400, 0x400); // enable CJC power
    setVoltageRange(40.0);           // set o/p range to 0.0
    setHeaterDACRegVal(34562);       // set heater dac o/p to 22.05V
    //usleep(550000);
    usleep(2100000); // 2.1 secs
prtReg(0);
    if (getDMMReading(dmmSock, &vSrcChkP50) != 0)
    {
        logCalMsg(CAL_LOG_ERR,  2, "TCGainCheck: Failed to read Vsrc_check_p50");
        return -1;
    }
    vSrcChkP50 *= 1000.0;  // cvt to mV
    logCalMsg(CAL_LOG_MSG,  2, "V_src_check_p50  %0.6f mV", vSrcChkP50);
    err = 50.0 * 3.01 / 100.0;
    if ((vSrcChkP50 < ( 50 - err)) || (vSrcChkP50 > (50 + err)))
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_006: Error V_src_check_p50 not within range\n");
        pauseTest();
        if (!(*eSkips))
            goto errTCGainCheck;
        (*eSkips)--;
    }

    getAvgTADCFpgaVal(T_ADC_CHAN_TC, 10, 0, &vADCChkP50);
    vADCChkP50 -= pCache->tcVZero; // assuming this was computed fine.. TODO
    vADCChkP50 *= 1000.0; // convert to mV
    logCalMsg(CAL_LOG_MSG,  2, "V_adc_check_p50  %0.6f mV", vADCChkP50);

    avgVal = (vADCChkP50 + vSrcChkP50) / 2.0;
    propErr = avgVal * 0.01 / 100.0;
    //err = propErr + offsErr;
    err = 30e-3; // 30uV = 30-e3mV, BobE 03/01/2016
//logPrint("avgVal %0.6f propErr %0.6f err %0.6f\n", avgVal, propErr, err);
    if (fabs(vADCChkP50 - vSrcChkP50) > err) 
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_021: Error v_src_p50 & v_adc_p50 does not match(%0.6f mV)\n", err);
        pauseTest();
        if (!(*eSkips))
            goto errTCGainCheck;
        (*eSkips)--;
    }

    // P25
    setHeaterDACRegVal(17359);       // set heater dac o/p to 11.025V
    usleep(550000);
prtReg(0);
    if (getDMMReading(dmmSock, &vSrcChkP25) != 0)
    {
        logCalMsg(CAL_LOG_ERR,  2,"TCGainCheck: Failed to read Vsrc_check_p25");
        return -1;
    }
    vSrcChkP25 *= 1000.0;  // cvt to mV
    logCalMsg(CAL_LOG_MSG,  2, "V_src_check_p25  %0.6f mV", vSrcChkP25);

    err = 25.0 * 3.01 / 100.0;
    if ((vSrcChkP25 < ( 25 - err)) || (vSrcChkP25 > (25 + err)))
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_102: Error V_src_check_p25 not within range\n");
        pauseTest();
        if (!(*eSkips))
            goto errTCGainCheck;
        (*eSkips)--;
    }

    getAvgTADCFpgaVal(T_ADC_CHAN_TC, 10, 0, &vADCChkP25);
    vADCChkP25 -= pCache->tcVZero; // assuming this was computed OK
    vADCChkP25 *= 1000.0; // convert to mV
    logCalMsg(CAL_LOG_MSG,  2, "V adc_check_p25  %0.6f mV", vADCChkP25);

    avgVal = (vADCChkP25 + vSrcChkP25) / 2.0;
    propErr = avgVal * 0.01 / 100.0;
    //err = propErr + offsErr;
    err = 18e-3; // 18uV = 18-e3mV, BobE 03/01/2016
//logPrint("avgVal %0.6f propErr %0.6f err %0.6f\n", avgVal, propErr, err);
    if (fabs(vADCChkP25 - vSrcChkP25) > err) 
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_022: Error v_src_p25 & v_adc_p25 does not match(%0.6f mV)\n", err);
        pauseTest();
        if (!(*eSkips))
            goto errTCGainCheck;
        (*eSkips)--;
    }

    // P0
    setHeaterDACRegVal(156);       // set heater dac o/p to 0V
    setCtrlRegBits(1, 0x1, 0x0);   // disable bake out
    usleep(550000);
prtReg(0);
    if (getDMMReading(dmmSock, &vSrcChkP0) != 0)
    {
        logCalMsg(CAL_LOG_ERR,  2,"TCGainCheck: Failed to read Vsrc_check_p0");
        return -1;
    }
    vSrcChkP0 *= 1000.0;  // cvt to mV
    logCalMsg(CAL_LOG_MSG,  2, "V_src_check_p0  %0.6f mV", vSrcChkP0);

    if (fabs(vSrcChkP0) > 5e-3)  // 5uV in mV
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_103: Error V src_check_p0 not within range\n");
        pauseTest();
        if (!(*eSkips))
            goto errTCGainCheck;
        (*eSkips)--;
    }

    getAvgTADCFpgaVal(T_ADC_CHAN_TC, 10, 0, &vADCChkP0);
    vADCChkP0 -= pCache->tcVZero; // assuming this was computed fine.. TODO
    vADCChkP0 *= 1000.0; // convert to mV
    logCalMsg(CAL_LOG_MSG,  2, "V_adc_check_p0  %0.6f mV", vADCChkP0);
    err = 5e-3; // 5uV = 0.005mV
    if (fabs(vADCChkP0 - vSrcChkP0) > err)
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_023: Error V_src_p0 & V_adc_p0 not within range(%0.6f mV)\n", err);
        pauseTest();
        if (!(*eSkips))
            goto errTCGainCheck;
        (*eSkips)--;
    }

    // N50
    setCtrlRegBits(0, 0x400, 0x0);    // disable CJC power
    setStimulusCurrent(STIM_100uA_N, 1); // set stimulus to -100uA
    usleep(550000);
prtReg(0);
    if (getDMMReading(dmmSock, &vSrcChkN50) != 0)
    {
        logCalMsg(CAL_LOG_ERR,  2, "TCGainCheck: Failed to read Vsrc_check_N50");
        return -1;
    }
    vSrcChkN50 *= 1000.0;  // cvt to mV
    logCalMsg(CAL_LOG_MSG,  2, "V_src_check_N50  %0.6f mV", vSrcChkN50);

    err = -50.0 * 3.01 / 100.0;
    if ((vSrcChkN50 < (-50.0 + err)) || (vSrcChkN50 > (-50.0 - err)))
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_104: Error V_src_check_N50 not within range\n");
        pauseTest();
        if (!(*eSkips))
            goto errTCGainCheck;
        (*eSkips)--;
    }

    getAvgTADCFpgaVal(T_ADC_CHAN_TC, 10, 0, &vADCChkN50);
    vADCChkN50 -= pCache->tcVZero; // assuming this was computed fine.. TODO
    vADCChkN50 *= 1000.0; // convert to mV
    logCalMsg(CAL_LOG_MSG,  2, "V_adc_check_N50  %0.6f mV", vADCChkN50);

    avgVal = (vADCChkN50 + vSrcChkN50) / 2.0;
    propErr = avgVal * 5.0 / 100.0;
    err = propErr;
    if (fabs(vADCChkN50 - vSrcChkN50) > fabs(err)) 
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_021: Error v_src_N50 & v_adc_N50 not within range(%0.6f mV)\n", err);
        pauseTest();
        if (!(*eSkips))
            goto errTCGainCheck;
        (*eSkips)--;
    }

    setHeaterDACRegVal(0);
    setVoltageRange(0.0);
    return 0;
errTCGainCheck:
    setHeaterDACRegVal(0);
    setVoltageRange(0.0);
    return -1;
}

static int doMissingTCBiasCheck(int dmmSock, int *eSkips)
{
    double vTCBiasOff;
    double vTCBiasOn;
    double iTCBias;
    double Rcal_500 = 500.0; // Ohms

    logCalMsg(CAL_LOG_MSG, 1, "\nMissing TC Bias Check:");
    setCtrlRegBits(3, 0x2, 0x0);     // set TC_BIAS_EN to 0
    setCtrlRegBits(1, 0x1, 0x0);     // disable bake out
    setCtrlRegBits(0, 0x400, 0x400); // turn on CJC power
    usleep(110000);
    if (getDMMReading(dmmSock, &vTCBiasOff) != 0)
    {
        logCalMsg(CAL_LOG_ERR,  2, "TCBiasCheck: Failed to read V tc_bias_off");
        return -1;
    }
    //vTCBiasOff *= 1000.0;  // cvt to mV
    logCalMsg(CAL_LOG_MSG,  2, "V tc_bias_off %0.6f mV", (vTCBiasOff*1000.0));
    if (fabs(vTCBiasOff) > 4e-6) // 4uV, vTCBiasOff is in V
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_026: Error V tc_bias_off not within +/-4uV\n");
        pauseTest();
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
    }
    // set TC_BIAS_EN to 1
    setCtrlRegBits(3, 0x2, 0x2);
    usleep(110000);
    if (getDMMReading(dmmSock, &vTCBiasOn) != 0)
    {
        logCalMsg(CAL_LOG_ERR,  2, "TCBiasCheck: Failed to read V tc_bias_on");
        return -1;
    }
    //vTCBiasOn *= 1000.0;  // cvt to mV
    logCalMsg(CAL_LOG_MSG,  2, "V tc_bias_on %0.6f mV", (vTCBiasOn*1000.0));
    // set TC_BIAS_EN to 0
    setCtrlRegBits(3, 0x2, 0x0);
    iTCBias = (vTCBiasOn - vTCBiasOff) / Rcal_500;
    iTCBias *= 1e9; // convert to nA
    logCalMsg(CAL_LOG_MSG,  2, "I tc_bias %0.6f nA", iTCBias);
    if ((iTCBias < -129.99) || (iTCBias > -120.01)) // between -130nA and -120nA
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_027: Error I tc_bias not within -130uA - -120nA\n");
        pauseTest();
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
    }
#ifdef DO_INI_SAVE
    else
    {
        // save to persistent storage
        sprintf(itemStr, "%s", TC_BIAS_I);
        sprintf(fltVal, "%0.10f", iTCBias); // stored as micro-Amps
        if (ini_puts(sectStr, itemStr, fltVal, SYS_INI_FILE) != 1)
            logCalMsg(CAL_LOG_ERR, 2, 
                                "Failed to save calibration value(ItcBias)");
    }
#endif
    return 0;
}

static double findStdDeviation(double *dVal, int nSamples)
{
    int i;
    double sum = 0.0, avg = 0.0, sum1 = 0.0;
    double var = 0.0, sd = 0.0;
    for (i = 0; i < nSamples; i++)
        sum += dVal[i];
    avg = sum / (double)nSamples;
    for (i = 0; i < nSamples; i++)
        sum1  += pow((dVal[i] - avg), 2.0);
    var = sum1 / (double) nSamples;
    sd = sqrt(var);
    return sd;
}

#define NSAMPLES (1000)
static uint32_t tADCRawV[NSAMPLES];
static double tADCV[NSAMPLES];
static int doTCAmpNoiseCheck(int dmmSock, int *eSkips)
{
    int i;
    double minV = 2.60, maxV = -2.60;
    double SD;
    double tcVZero = pCache->tcVZero;
    logCalMsg(CAL_LOG_MSG, 1, "\nTC Amp. and ADC Noise Check:");
    setCtrlRegBits(3, 0x4, 0x4); // set TC_ZERO to 1
    usleep(10000);
    readT_ADCValues(T_ADC_CHAN_TC, NSAMPLES, &(tADCRawV[0]));
    setCtrlRegBits(3, 0x4, 0x0); // set TC_ZERO to 0
    pCache->tcVZero       = 0.0;
    pCache->tcVMiss       = 0.0;
    // use only Gtc to scale voltage
    for (i = 0; i < NSAMPLES; i++)
        tADCV[i] = scaleTADCVal(T_ADC_CHAN_TC, tADCRawV[i]);
    // compute std. deviation of this set..
    SD = findStdDeviation(tADCV, NSAMPLES);
    logCalMsg(CAL_LOG_MSG, 2, "V tc_noise_rms %0.6f", SD);
    // compute min and max.
    for (i = 0; i < NSAMPLES; i++)
    {
        if (tADCV[i] < minV)
            minV = tADCV[i];
        if (tADCV[i] > maxV)
            maxV = tADCV[i];
    }
    logCalMsg(CAL_LOG_MSG, 2, "V tc_noise_pp %0.6f", (maxV - minV));
    //logCalMsg(CAL_LOG_MSG, 2, "tcVZero %0.8f", pCache->tcVZero);
    pCache->tcVZero = tcVZero; //restore setting.
    pCache->tcVMiss = (211.0 / 1000000.0);  // use approx vMiss:211uV
    //logCalMsg(CAL_LOG_MSG, 2, "        %0.8f", pCache->tcVZero);
    return 0;
}

static int doInputOffsetCurrentCheck(int dmmSock, int *eSkips)
{
    double vSrcIOS, vAdcIOS;

    logCalMsg(CAL_LOG_MSG, 1, "\nInput Offset Current Check:");

    // turn-on CJC_PWR and set TC_ZERO to 0
    setCtrlRegBits(0, 0x400, 0x400);
    setCtrlRegBits(0, 0x4, 0x0);
    usleep(550000);
    if (getDMMReading(dmmSock, &vSrcIOS) != 0)
    {
        logCalMsg(CAL_LOG_ERR,  2, "IO_OffsetCurrent: Failed to read V src_ios");
        goto errTCAmpNoise;
    }
    vSrcIOS *= 1000.0;  // cvt to mV
    logCalMsg(CAL_LOG_MSG,  2, "V source_ios %0.6f mV", vSrcIOS);
    if (fabs(vSrcIOS) > 4e-3) // 4uV, vSrcIOS is in mV
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_014: Error V source_ios not within range(+/-4uV)\n");
        pauseTest();
        if (!(*eSkips))
            goto errTCAmpNoise;
        (*eSkips)--;
    }
    getAvgTADCFpgaVal(T_ADC_CHAN_TC, 10, 0, &vAdcIOS);
    vAdcIOS *= 1000.0; // convert to mV
    logCalMsg(CAL_LOG_MSG,  2, "V adc_ios %0.6f mV", vAdcIOS);
    vAdcIOS -= (pCache->tcVZero*1000.0); // subtract TC VZero in mV
    logCalMsg(CAL_LOG_MSG,  2, "       => %0.6f mV", vAdcIOS);
    if (fabs(vAdcIOS) > 10e-3) // 3uV, vSrcIOS is in mV
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_015: Error V adc_ios not within range(+/-10uV)\n");
        pauseTest();
        if (!(*eSkips))
            goto errTCAmpNoise;
        (*eSkips)--;
    }
    setCtrlRegBits(0, 0x400, 0x0);
    return 0;
errTCAmpNoise:
    setCtrlRegBits(0, 0x400, 0x0);
    return -1;
}

static int doEepromDataCheck(int dmmSock, int *eSkips)
{
    uint8_t regVal;
    uint8_t r;
    int pass = 0;
    //uint16_t cRegs[10];

    logCalMsg(CAL_LOG_MSG, 1, "\nEEPROM Data Check:");
    setCtrlRegBits(0, 0x400, 0x400);
    usleep(120000);
    readStatReg(4, 1, &regVal); 
    //readCtrlReg(0, 10, &cRegs[0]);
    //logPrint("S Reg4: 0x%X\n", regVal);
    //logPrint("C Regs 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x\n",
    //        cRegs[0], cRegs[1], cRegs[2], cRegs[3], cRegs[4], 
    //        cRegs[5], cRegs[6], cRegs[7], cRegs[8], cRegs[9]); 
    // NOTE: NOTE: BING inverts this signal ::
    if (! (regVal & 0x40))
    {
        logCalMsg(CAL_LOG_MSG,  2, "\nCAL_ERR_028: Error eeprom_data not high\n");
        pauseTest();
        setCtrlRegBits(0, 0x400, 0x0);
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
    }
    else
        pass++;
    setCtrlRegBits(0, 0x400, 0x0);
    usleep(120000);
    readStatReg(4, 1, &regVal); // TODO: check the right offset and bit pos..
    //logPrint("Reg4: 0x%X\n", regVal);
    if (regVal & 0x40) 
    {
        logCalMsg(CAL_LOG_MSG,  2, "\nCAL_ERR_028: Error eeprom_data not low\n");
        pauseTest();
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
    }
    else
        pass++;
    if (pass == 2)
        logCalMsg(CAL_LOG_MSG, 2, "EEPROM Data Check: PASS\n");
    return 0;
}

static int doDRTDOpenTest(int dmmSock, int *eSkips)
{
    double rtdV;
    double diff;

    logCalMsg(CAL_LOG_MSG, 1, "\nDRTD Open Test:");

    // turn-on CJC_PWR, stimulus 1uA, DRTD gain to 1/1.01
    setCtrlRegBits(0, 0x400, 0x400);
    setStimulusCurrent(STIM_1uA, 1);
    setDRTDGain(GAIN_X1);
    usleep(1000000);
    getAvgTADCFpgaVal(T_ADC_CHAN_DRTD, 1, 0, &rtdV);
    logCalMsg(CAL_LOG_MSG,  2, "DRTD at 1uA %0.7f V", rtdV);
    diff = rtdV - 2.50000;
    if (diff < 1e-6)
    {
        logCalMsg(CAL_LOG_MSG,  2, "\nCAL_ERR_030: DRTD Open 1uA Test failure\n");
        pauseTest();
        if (!(*eSkips))
            goto errDRTDOpen;
        (*eSkips)--;
    }
    setStimulusCurrent(STIM_1uA_N, 1);
    usleep(1000000);
    getAvgTADCFpgaVal(T_ADC_CHAN_DRTD, 1, 0, &rtdV);
    logCalMsg(CAL_LOG_MSG,  2, "DRTD at -1uA %0.7f V", rtdV);
    diff = rtdV - 2.50000;
    if (diff < 1e-6)
    {
        logCalMsg(CAL_LOG_MSG, 2,  "\nCAL_ERR_031: DRTD Open -1uA Test failure\n");
        pauseTest();
        if (!(*eSkips))
            goto errDRTDOpen;
        (*eSkips)--;
    }
    setCtrlRegBits(0, 0x400, 0x0);
    return 0;
errDRTDOpen:
    setCtrlRegBits(0, 0x400, 0x0);
    return -1;
}

static int doVerify500OhmR(int dmmSock, int *eSkips) 
{
    double R;
    logCalMsg(CAL_LOG_MSG, 1, "\nVerify 500 Ohm Dongle Resistor:");

    if (doDMMSetup(dmmSock, DMM_10KOHM_10NPLC) != 0)
        return -1;
    setCtrlRegBits(0, 0x400, 0x400); // turn on CJC power
    setVoltageRange(0.0);            // set o/p range to 0.0
    setHeaterDACRegVal(0);           // set heater dac o/p to 0
    setCtrlRegBits(1, 0x1, 0x0);     // disable bake out
    usleep(120000);
    if (getDMMReading(dmmSock, &R) != 0)
    {
        logCalMsg(CAL_LOG_ERR,2, "%s: Failed to read Resistance", __func__);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "RDongle1_500R_Check: %0.6f", R);
    if ((R < 499.7490) || (R > 500.751))
    {
        logCalMsg(CAL_LOG_ERR, 2, "Resistor Value: %0.6f", R);
        logCalMsg(CAL_LOG_MSG, 2,  
                "CAL_ERR_101: Verification of 500 Ohm Resistor failed\n", R);
        pauseTest();
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
    }
    else
    {
        logCalMsg(CAL_LOG_MSG, 2, "Resistor value : %0.6f [ PASS ]\n", R);
    }
    return 0;
}

static int doPostChecks(int dmmSock, int dongleId)
{
    float minT = 19.90, maxT = 40.01;
    if (dongleId == 1)
    {
        logCalMsg(CAL_LOG_MSG, 1, "\nPostChecks:");
        // check and log CJC temperature
        if (readCJCTemp(&T_cjc_cal_end) != 0)
            return -1;
        logCalMsg(CAL_LOG_MSG,  2, "T cjc_cal_end %0.2f C", T_cjc_cal_end);
        if ((T_cjc_cal_end < minT) || (T_cjc_cal_end > maxT))
        {
            logCalMsg(CAL_LOG_MSG, 2,  
                    "\nCAL_ERR_016: CJC temperature outside range(20-40 C)\n");
            return -1;
        }
        if (fabs(T_cjc_cal_start - T_cjc_cal_end) > 1.510)
        {
            logCalMsg(CAL_LOG_MSG, 2,
                                "\nCAL_ERR_017: Dongle temp. not stable\n");
        }
        // PCB temperature
        if (readPCBTemp(&T_pcb_cal_end) != 0)
            return -1;
        logCalMsg(CAL_LOG_MSG, 2,  "T pcb_cal_end: PCB Temp: %0.2f", T_pcb_cal_end);
        if (fabs(T_pcb_cal_start - T_pcb_cal_end) > 3.010)
        {
            logCalMsg(CAL_LOG_MSG, 2,  "\nCAL_ERR_018: PCB temp. not stable\n");
            return -1;
        }
    }
    return 0;
}

static int doDongleOneChecks(int dmmSock, int errSkips)
{
    int dongleId = 1;
    double GtcIdeal = GTC_IDEAL;

    logCalMsg(CAL_LOG_MSG, 0, "---- Dongle 1 Checks:");
    if (tryRegLock(5) < 0)
    {
        logCalMsg(CAL_LOG_ERR, 1, "dongle1Check: unable to get reg. lock");
        return -1;
    }

    if (doPreChecks(dmmSock, dongleId, 1) != 0)
        goto errDongle1;

    // DRTD 100uA stimulus current calibration
    if (doDRTD_100uA_Stimulus_Calibration(dmmSock, &errSkips) != 0)
        goto errDongle1;

    // TODO: useful for debug, if stimulus calibration did not work,
    // set some sane values.
    if (stim100uAOK != 2)
    {
        printf("set MCP4728\n");
        setMCP4728(1, 2, 2.56);
        usleep(20000);
        setMCP4728(2, 2, 2.56);
        usleep(10000);
    }

    // set tcVMiss, tcVZero and tcGTC to 1.0 for gain computation
    pCache->tcVZero       = 0.0;
    pCache->tcVMiss       = 0.0; 
    pCache->tcGTC         = 1.0000; 
     
    if (doTCGainCalibration(dmmSock, &errSkips) != 0)
        goto errDongle1;

    // restore values..
    pCache->tcVZero       = 0.0;
    pCache->tcVMiss       = (211.0 / 1000000.0);  // use approx vMiss:211uV
    if (tcGainOK < 0) // gain calibration failed...
    {
        logPrint("Restore tcGTC..\n");
        pCache->tcGTC         = GtcIdeal;
    }

    // check TC_ZERO
    if (doCheckTCZero(dmmSock, &errSkips) != 0)
        goto errDongle1;

    // TC Gtc gain check
    if (doTCGainCheck(dmmSock, &errSkips) != 0)
        goto errDongle1;

    // Missing TC bias
    if (doMissingTCBiasCheck(dmmSock, &errSkips) != 0)
        goto errDongle1;

    // TC Amp and ADC noise check
    if (doTCAmpNoiseCheck(dmmSock, &errSkips) != 0)
        goto errDongle1;

    // Input offset current check
    if (doInputOffsetCurrentCheck(dmmSock, &errSkips) != 0)
        goto errDongle1;

    // eeprom_data check
    if (doEepromDataCheck(dmmSock, &errSkips) != 0)
        goto errDongle1;

    // DRTD Open Circuit test
    if (doDRTDOpenTest(dmmSock, &errSkips) != 0)
        goto errDongle1;

    // Verification of 500Ohm dongle resistor
    if (doVerify500OhmR(dmmSock, &errSkips) != 0)
        goto errDongle1;

    // post-checks
    if (doPostChecks(dmmSock, 1) != 0)
        goto errDongle1;

    doEndSettings();
    checkDMMErrors(dmmSock);
    logCalMsg(CAL_LOG_MSG, 0, "---- END Dongle 1 Checks:");
    return 0;

errDongle1:
    doEndSettings();
    unlockCtrlReg();
    checkDMMErrors(dmmSock);
    logCalMsg(CAL_LOG_MSG, 0, "---- END Dongle 1 Checks:");
    return -1;
}

static int doCJCZeroOffsetChk(int dmmSock, int *eSkips) 
{
    uint16_t statReg;
    double dmmV;
    float curr, V;
    uint32_t rawCurr, rawV;
    int gain = 4;
    logCalMsg(CAL_LOG_MSG, 1, "\nCJC Zero Offset Check:");
    // set DMM to 100mV DC range, 100NPLC avg and hi-z(hi-imp) input
    if (doDMMSetup(dmmSock, DMM_100mVDC_100NPLC) != 0)
        return -1;
    setCtrlRegBits(0, 0x400, 0x0); // turn off CJC power
    setVoltageRange(40);
    setIRange(HEAT_IRANGE_200mA); 
    setHeaterDAC(40.0);
    usleep(100000);
prtReg(0);
    // check #ILIMIT flag and heater current..
    readStatRegFull(0, 1, &statReg);
    if (statReg & 0x40) // #ilimit asserted
    {
        logCalMsg(CAL_LOG_MSG, 2,
          "\nCAL_ERR_035: Error: CJCZeroOffsCheck: ILIMIT# set\n");
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
    }
    getHeaterValues(&V, &rawV, &curr, &rawCurr);
    if (curr > 0.02500) // > 25mA
    {
        logCalMsg(CAL_LOG_MSG, 2,
          "\nCAL_ERR_036: Error: CJCZeroOffsCheck: heater current > 25mA(%0.4f)\n", curr);
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
    }
    usleep(250000);
    if (getDMMReading(dmmSock, &dmmV) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "CJCZeroOffsCheck: Failed to read voltage");
        return -1;
    }
    dmmV *= 1000.0; // convert to mV
    logCalMsg(CAL_LOG_MSG, 2, "DMM V: %0.6f mV", dmmV);
    if (fabs(dmmV) > 100e-3) // not within -100uV - +100uV, dmmV in mV
    {
        logCalMsg(CAL_LOG_MSG, 2,
          "\nCAL_ERR_032: Error: CJCZeroOffsCheck: DMM V not within range[%0.5f mV]\n", dmmV);
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
    }
    if (readCJCVolt(gain, &V) != 0) // returns mV in param V
        return -1;
    logCalMsg(CAL_LOG_MSG, 2, "CJC V: %0.6f mV", V);
    // check range 0 - 1mV, 11/26/15 Alistair.M
    if ((V < -0.0000010) || ( V > 1.001))
    {
        logCalMsg(CAL_LOG_MSG, 2,
          "\nCAL_ERR_033: Error: CJCZeroOffsCheck: CJC_SIGP not within range[0..+1mV]\n", V);
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
    }
    return 0;
}

static int doCJCPwrCheck(int dmmSock, int *eSkips)
{
    double diff;
    logCalMsg(CAL_LOG_MSG, 1, "\nCJC 5V Power Check:");

    setCtrlRegBits(0, 0x400, 0x400); // turn on CJC power
    // set DMM to 1V DC range, 10NPLC avg and hi-z(hi-imp) input
    // TODO: set DMM to 10V for now.. reading is not right at this time
    if (doDMMSetup(dmmSock, DMM_10VDC_10NPLC) != 0)
        return -1;
    usleep(10000);
prtReg(0);
    if (getDMMReading(dmmSock, &V_cjc_eeprom_pwr_v) != 0)
    {
        //setCtrlRegBits(0, 0x400, 0x0); // turn on CJC power
        logCalMsg(CAL_LOG_ERR, 2, "CJC5VPwrCheck: Failed to read voltage");
        return -1;
    }
    //setCtrlRegBits(0, 0x400, 0x0); // turn off CJC power
    V_cjc_eeprom_pwr_v *= 1000.0; // convert to mV
    logCalMsg(CAL_LOG_MSG, 2, "V cjc_eeprom_pwr_v %0.3f mV\n", 
                                            V_cjc_eeprom_pwr_v);
    diff = (5000.0 * 3.5)/100.0; // 3.5 % of 5V in mV
    if ((V_cjc_eeprom_pwr_v < (5000.0-diff)) || 
                                (V_cjc_eeprom_pwr_v > (5000.0+diff)))
    {
        logCalMsg(CAL_LOG_MSG, 2,
          "\nCAL_ERR_032: Error: CJC5VPwrCheck: DMM V not within range[%0.5f]\n", V_cjc_eeprom_pwr_v);
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
    }
    return 0;
}

static int GcjcOK = 0;
static int doCJCADCGainCal(int dmmSock, int *eSkips)
{
    double Vcjc_test_v;
    double epromFact, diff;
    int adcRaw;
    double Gcjc;
    double maxVRange;
    int gain = 4;
    float cjcV;

    logCalMsg(CAL_LOG_MSG, 1, "\nCJC ADC Gain Calibration:");

    // set DMM to 1V DC range, 100NPLC avg and hi-z(hi-imp) input
    // TODO: set DMM to 10V DC range now, something not quite right..
    if (doDMMSetup(dmmSock, DMM_1VDC_100NPLC) != 0)
        return -1;
//setCtrlRegBits(0, 0x400, 0x0); // turn off CJC power
    //setVoltageRange(0);
    setHeaterDAC(0.0);
    usleep(120000);
prtReg(0);
    if (getDMMReading(dmmSock, &Vcjc_test_v) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "CJAdcGainCal: Failed to read voltage");
        return -1;
    }
    Vcjc_test_v *= 1000.0;
    logCalMsg(CAL_LOG_MSG, 2, "V cjc_test_v %0.6f mV [448mV +/-5%]\n",
                                                        Vcjc_test_v);
    // check against cjc eeprom voltage, must be within +/-1.5%
    epromFact = V_cjc_eeprom_pwr_v / 11.1481;
    diff = (epromFact * 1.50) / 100.0;
    if ((Vcjc_test_v < (epromFact - diff)) || (Vcjc_test_v >(epromFact + diff)))
    {
        logCalMsg(CAL_LOG_MSG, 2,
           "\nCAL_ERR_114: Error: CJAdcGainCal: cjc_test_v not within range\n");
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
    }
    if (calReadCJCRawVal(&adcRaw) < 0)
        return -1;
    adcRaw &= 0x3FFFF;
    Gcjc = Vcjc_test_v / (double)adcRaw; // volts per lsb
    maxVRange = (Gcjc * 0x3FFFF) / gain; // all 18 bits.., scaled by gain
    //logPrint("ADC Register 0x%x, maxV %0.6f\n", adcRaw, maxVRange);
    logCalMsg(CAL_LOG_MSG, 2, "Gcjc %0.6f", Gcjc);
    // check maxVRange within 0.45%  of 0.512 mV
    diff = 512.0 * 0.45 / 100.0;
    if ((maxVRange < (512.0 - diff)) || (maxVRange > (512.0 + diff)))
    {
        logCalMsg(CAL_LOG_MSG, 2, "\nCAL_ERR_037: Cold Junction ADC Gain\n");
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
        GcjcOK--;
    }
    if (calReadCJCRawVal(&adcRaw) < 0)
        return -1;
    cjcV = adcRaw * Gcjc;
    logCalMsg(CAL_LOG_MSG, 2, "CJC Voltage: %0.6f", cjcV);
    if (fabs(cjcV - Vcjc_test_v) > 200e-3)  // mV
    {
        logCalMsg(CAL_LOG_MSG, 2, "\nCAL_ERR_040: Cold Junction ADC Gain\n");
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
        GcjcOK--;
    }
    setCtrlRegBits(0, 0x400, 0x0); // turn off CJC power
#ifdef DO_INI_SAVE
    // save Gcjc value to persistent storage
    if (GcjcOK == 0)
    {
        sprintf(itemStr, "%s", CJC_ADC_GAIN);
        sprintf(fltVal, "%0.10f", Gcjc); 
        if (ini_puts(sectStr, itemStr, fltVal, SYS_INI_FILE) != 1)
            logCalMsg(CAL_LOG_ERR, 2, 
                            "Failed to save calibration value(CJC ADC Gain)");
    }
#endif
    return 0;
}

static int doPreAmpVoltageCheck(int dmmSock, int *eSkips)
{
    double V;
    double multFact = 550.098;
    double pMin = 10.90, pMax = 12.70;
    double nMin = -12.70, nMax = -10.90;

    logCalMsg(CAL_LOG_MSG, 1, "\nPreAmp Voltage Check:");
    //setVoltageRange(0);
    setHeaterDAC(0.0);
    usleep(120000);
prtReg(0);
    // TODO: restore values after done??
    pCache->tcVMiss = 0.0;
    pCache->tcVZero = 0.0;
    getAvgTADCFpgaVal(T_ADC_CHAN_TC, 10, 0, &V);
    logCalMsg(CAL_LOG_MSG, 2, "TC V1: %0.6f", V);
    V *= multFact;
    logCalMsg(CAL_LOG_MSG, 2, "    => %0.5f", V);
    if ((V < pMin) || (V  > pMax))
    {
        logCalMsg(CAL_LOG_MSG, 2,
           "\nCAL_ERR_038: Error: PreAmpVoltageCheck: V not within [%0.2f - %0.2f]\n", pMin, pMax);
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
    }
    setHeaterDAC(40.0);
    usleep(120000);
prtReg(0);
    getAvgTADCFpgaVal(T_ADC_CHAN_TC, 10, 0, &V);
    logCalMsg(CAL_LOG_MSG, 2, "TC V2: %0.6f", V);
    V *= multFact;
    logCalMsg(CAL_LOG_MSG, 2, "    => %0.5f", V);
    if ((V < nMin) || (V  > nMax))
    {
        logCalMsg(CAL_LOG_MSG, 2,
           "\nCAL_ERR_039: Error: PreAmpVoltageCheck: V not within [%0.2f - %0.2f]\n", nMin, nMax);
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
    }
    setHeaterDAC(0.0);
    setVoltageRange(0);
prtReg(0);
    return 0;
}

static int doDRTDZeroOhmCheck(int dmmSock, int *eSkips)
{
    double Vdrtd_z_p, Vdrtd_z_n;

    logCalMsg(CAL_LOG_MSG, 1, "\nDRTD Zero Ohm Check:");

    setStimulusCurrent(STIM_100uA, 1);  // set stimulus to +100uA
prtReg(0);
    setDRTDGain(GAIN_X210);          // set gain to 210/1.01
prtReg(0);
    setDRTDCalSel1(DRTD_CAL_NORM);   // external DRTD
    usleep(120000);
prtReg(0);
    getAvgTADCFpgaVal(T_ADC_CHAN_DRTD, 10, 0, &Vdrtd_z_p);
    Vdrtd_z_p *= 1e6; // uV
    // divide by 210/1.01
    Vdrtd_z_p /= (210.0/1.01);
    logCalMsg(CAL_LOG_MSG, 2, "V drtd_z_test_p %0.6f uV RTI\n", Vdrtd_z_p);

    setStimulusCurrent(STIM_100uA_N, 1);  // set stimulus to -100uA
    usleep(120000);
prtReg(0);
    // Gain now should be 210/1.01
    getAvgTADCFpgaVal(T_ADC_CHAN_DRTD, 10, 0, &Vdrtd_z_n);
    Vdrtd_z_n *= 1e6; // uV
    // divide by 210/1.01
    Vdrtd_z_n /= (210.0/1.01);
    logCalMsg(CAL_LOG_MSG, 2, "V drtd_z_test_n %0.6f uV RTI\n", Vdrtd_z_n);

    if (fabs(Vdrtd_z_p) > 50.001) // 50uV
    {
        logCalMsg(CAL_LOG_MSG, 2,
           "\nCAL_ERR_041: Error: DRTDZeroOhmCheck: Vdrtd_z_test_p not within range\n");
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
    }
    if (fabs(Vdrtd_z_n) > 50.001) // 50uV
    {
        logCalMsg(CAL_LOG_MSG, 2,
           "\nCAL_ERR_041: Error: DRTDZeroOhmCheck: Vdrtd_z_test_n not within range\n");
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
    }
    if ((fabs((Vdrtd_z_p - Vdrtd_z_n) / 2)) > 16.001) // 16uV
    {
        logCalMsg(CAL_LOG_MSG, 2,
           "\nCAL_ERR_042: Error: DRTDZeroOhmCheck: avg above 16uV\n");
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
    }
    return 0;
}

static int doDongleTwoChecks(int dmmSock, int errSkips)
{
    int dongleId = 2;

    logCalMsg(CAL_LOG_MSG, 0, "---- Dongle 2 Checks:");
    if (tryRegLock(5) < 0)
    {
        logCalMsg(CAL_LOG_ERR, 1, "dongle2Check: unable to get reg. lock");
        return -1;
    }

    if (doPreChecks(dmmSock, dongleId, 0) != 0)
        goto errDongle2;

    if (doCJCZeroOffsetChk(dmmSock, &errSkips) != 0)
        goto errDongle2;

    if (doCJCPwrCheck(dmmSock, &errSkips) != 0)
        goto errDongle2;

    if (doCJCADCGainCal(dmmSock, &errSkips) != 0)
        goto errDongle2;

    if (doPreAmpVoltageCheck(dmmSock, &errSkips) != 0)
        goto errDongle2;

    if (doDRTDZeroOhmCheck(dmmSock, &errSkips) != 0)
        goto errDongle2;

    // post-checks
    if (doPostChecks(dmmSock, 2) != 0)
        goto errDongle2;

    doEndSettings();
    unlockCtrlReg();
    checkDMMErrors(dmmSock);
    logCalMsg(CAL_LOG_MSG, 0, "---- END Dongle 2 Checks:");
    return 0;
errDongle2:
    doEndSettings();
    unlockCtrlReg();
    checkDMMErrors(dmmSock);
    logCalMsg(CAL_LOG_MSG, 0, "---- END Dongle 2 Checks:");
    return -1;
}

#define NUM_DRTD_SAMPLES (10)
static int doDiodeSensorGain(int dmmSock, int *eSkips)
{
    double Vadc_zero, Vsrc_zero;
    double Vadc_max, Vsrc_max;
    double G1DrtdSamp[NUM_DRTD_SAMPLES], G1drtd, diff;
    int numSamples = NUM_DRTD_SAMPLES;
    double ideal = 1.0 / 1.01;
    int i;
double V;

    logCalMsg(CAL_LOG_MSG, 1, "\nDiode Sensor Gain Calibration:");

    if (doDMMSetup(dmmSock, DMM_100mVDC_100NPLC) != 0)
        return -1;

    for (i = 0; i < numSamples; i++)
    {
        if (doDMMSetup(dmmSock, DMM_100mVDC_100NPLC) != 0)
            return -1;
        setDRTDCalSel1(DRTD_CAL_NORM);   // external DRTD
        setStimulusCurrent(STIM_0uA, 1);    // set stimulus to 0uA
        setDRTDGain(GAIN_X1);            // set gain to 1/1.01
        usleep(550000);
prtReg(0);
        if (getDMMReading(dmmSock, &Vsrc_zero) != 0)
        {
            logCalMsg(CAL_LOG_ERR, 2, 
                                "DiodeSensorGain: Failed to read Vsrc_zero");
            return -1;
        }
        Vsrc_zero *= 1000.0; // V => mV
        logCalMsg(CAL_LOG_MSG, 2, "V src_zero %0.6f mV", Vsrc_zero);
        if (fabs(Vsrc_zero) > 1.0100)
        {
            logCalMsg(CAL_LOG_MSG, 2,
              "\nCAL_ERR_045: Diode Sensor Gain: Vsrc_zero not within range\n");
            if (!(*eSkips))
                return -1;
            (*eSkips)--;
        }
        getAvgTADCFpgaVal(T_ADC_CHAN_DRTD, 10, 1, &Vadc_zero);
        Vadc_zero *= 1000.0; // V => mV
        logCalMsg(CAL_LOG_MSG, 2, "V adc_zero %0.6f mV", Vadc_zero);

        setStimulusCurrent(STIM_100uA, 1);    // set stimulus to 100uA
        usleep(550000);
prtReg(0);
        if (doDMMSetup(dmmSock, DMM_10VDC_100NPLC) != 0)
            return -1;
        if (getDMMReading(dmmSock, &Vsrc_max) != 0)
        {
            logCalMsg(CAL_LOG_ERR,2,"DiodeSensorGain: Failed to read Vsrc_max");
            return -1;
        }
        Vsrc_max *= 1000.0; // V => mV
        logCalMsg(CAL_LOG_MSG, 2, "V src_max %0.6f mV", Vsrc_max);
        if ((Vsrc_max < 1895)  || (Vsrc_max > 2105)) // 2000 +/- 5% mV
        {
            logCalMsg(CAL_LOG_MSG, 2,
               "\nCAL_ERR_044: Diode Sensor Gain: Vsrc_max not within range\n");
            if (!(*eSkips))
                return -1;
            (*eSkips)--;
        }
        getAvgTADCFpgaVal(T_ADC_CHAN_DRTD, 10, 1, &Vadc_max);
        Vadc_max *= 1000.0; // V => mV
        logCalMsg(CAL_LOG_MSG, 2, "V adc_max %0.6f mV", Vadc_max);
        G1DrtdSamp[i] = (Vadc_max - Vadc_zero) / (Vsrc_max - Vsrc_zero);
        logCalMsg(CAL_LOG_MSG, 2, "G1Drtd(%d) %0.6f\n", i, G1DrtdSamp[i]);
    }
    G1drtd = 0.0;
    for (i = 0; i < numSamples; i++)
        G1drtd += G1DrtdSamp[i];
    G1drtd /= numSamples;
    logCalMsg(CAL_LOG_MSG, 2, "G1Drtd Average: %0.6f\n", G1drtd);
    diff = G1drtd * 0.005/ 100.0;
    for (i = 0; i < numSamples; i++)
    {
        if (fabs(G1DrtdSamp[i] - G1drtd) > diff)
            break;
    }
    if (i < numSamples) // range error
    {
        logCalMsg(CAL_LOG_MSG, 2,
               "\nCAL_ERR_046: Diode Sensor Gain: G1drtd not within 0.005%% of average\n");
            if (!(*eSkips))
                return -1;
            (*eSkips)--;
    }
    diff = ideal * 0.6510 / 100.0; // 0.65% of ideal(1/1.01)
    if (fabs(G1drtd - ideal) > diff)
    {
        logCalMsg(CAL_LOG_MSG, 2,
               "\nCAL_ERR_047: Diode Sensor Gain: G1drtd not within 0.65%% of ideal(%0.6f)\n", ideal);
            if (!(*eSkips))
                return -1;
            (*eSkips)--;
    }
#ifdef DO_INI_SAVE
    else
    //  save value to persistent storage
    {
        sprintf(itemStr, "%s", DIODE_SENSOR_GAIN);
        sprintf(fltVal, "%0.10f", G1drtd); 
        if (ini_puts(sectStr, itemStr, fltVal, SYS_INI_FILE) != 1)
            logCalMsg(CAL_LOG_ERR, 2, 
                            "Failed to save calibration value(G1DRTD)");
    }
#endif
    logCalMsg(CAL_LOG_MSG, 2, "G1Drtd: %0.6f\n", G1drtd);
    return 0;
}

static int calibrateTrimVoltage(int dmmSock, double tgtmV, double *StimV) 
{
    int ret = 0;
    double V;
    double low = 0.0, high = 3.30, mid;
    int gain = 2, bus = 2;
    int ch = 1;
    int devAddr = 0x61;
    for (; ;)
    {
        mid = low + ((high - low) / 2.0);
        // set MPC4728 to x2 gain and apply voltage
        if ((ret = mcp4728Write(bus, devAddr, ch, gain, mid)) != 0)
        {
            logCalMsg(CAL_LOG_ERR, 2, "%s: failed to write to MPC4728(%d)", __func__, ret);
            return ret;
        }
        usleep(50000);
        if ((ret = mcp4728Write(bus, devAddr, (ch+1), gain, mid)) != 0)
        {
            logCalMsg(CAL_LOG_ERR, 2, "%s: failed to write to MPC4728(%d)", __func__, ret);
            return ret;
        }
        usleep(50000);
        if (getDMMReading(dmmSock, &V) != 0)
        {
            logCalMsg(CAL_LOG_ERR,2, "%s: Failed to read voltage", __func__);
            ret = -1;
            break;
        }
        V *= 1000.0;
        logPrint("%s: l: %0.4f  h: %0.4f  m: %0.2f -> %0.4f mV\n", 
                            __func__, low, high, mid, V);
        if ((fabs(V - tgtmV)) <= 0.0100) // close enough?
        {
            *StimV = mid;
            break;
        }
        if (fabs(V) > fabs(tgtmV))
            low = mid;
        else if (fabs(V) < fabs(tgtmV))
            high = mid;
        if (fabs(high - low) < 0.010)
        {
            *StimV = mid;
            logPrint("%s: Exit at l: %0.4f  h: %0.4f\n", __func__, low, high);
            break;
        }
    }
    return ret;
}

enum { CAL_STIM1uA, CAL_STIM10uA, CAL_STIM100uA };
static int doDRTDStimulusCalibration(int dmmSock, int *eSkips, int eStim)
{
    int stimVal[2];
    double stimV[2];
    char *strStimV;
    char *errStr;
    double pStimV, nStimV;
    int bus = 2, muxChan = 1;
    int ret = 0;
    switch (eStim)
    {
        case CAL_STIM1uA:
            stimVal[0] = STIM_1uA;
            stimVal[1] = STIM_1uA_N;
            stimV[0] = +20.0;  // mV
            stimV[1] = -20.0;  // mV
            strStimV = "1uA";
            errStr = "CAL_ERR_048";
            break;
        case CAL_STIM10uA:
            stimVal[0] = STIM_10uA;
            stimVal[1] = STIM_10uA_N;
            stimV[0] = +200.0;  // mV
            stimV[1] = -200.0;  // mV
            strStimV = "10uA";
            errStr = "CAL_ERR_053";
            break;
        case CAL_STIM100uA:
#if 0 // TODO:
            stimVal[0] = STIM_100uA;
            stimVal[1] = STIM_100uA_N;
            stimV[0] = +50.0;  // mV
            stimV[1] = -50.0;  // mV
            strStimV = "100uA";
            break;
#endif
        default:
            return -1;
    }

    if (lockI2CBus(bus) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "Failed to acquire lock i2c bus\n");
        return -1;
    }
    if ((ret = i2cmux_select_chan(muxChan)) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "Selecting mux. chan.(%d)\n", ret);
        unlockI2CBus(bus);
        return -1;
    }
    setStimulusCurrent(stimVal[0], 0);  // set +ve stimulus
    if (calibrateTrimVoltage(dmmSock, stimV[0], &pStimV) != 0)
    {
        logCalMsg(CAL_LOG_MSG, 2, "\nError: stimulus trim cal %s\n", strStimV);
        if (! (*eSkips))
            goto errStimCal;
        (*eSkips)--;
    }
    //pStimV *= 1000.0; 
    logCalMsg(CAL_LOG_MSG, 2, "DACstim_trim_%s_p %0.3f V", strStimV, pStimV);
    if ((pStimV < 1.4900) || (pStimV > 3.1100))
    {
        logCalMsg(CAL_LOG_MSG, 2,
                "\n%s: Stimulus trim cal %s\n", errStr, strStimV);
        if (! (*eSkips))
            goto errStimCal;
        (*eSkips)--;
    }
#ifdef DO_INI_SAVE
    else
    {
        //  save value to persistent storage
        char *itemPtr;
logPrint("Save P Stim ");
        if (stimVal[0] == STIM_1uA)
            itemPtr = STIM_TRIM_1U_P;
        else if (stimVal[0] == STIM_10uA)
            itemPtr = STIM_TRIM_10U_P;
        else
            itemPtr = STIM_TRIM_100U_P;
        sprintf(itemStr, "%s", itemPtr);
        sprintf(fltVal, "%0.10f", pStimV); 
logPrint("ItemPtr: %s, fltVal %s\n", itemPtr, fltVal);
        if (ini_puts(sectStr, itemStr, fltVal, SYS_INI_FILE) != 1)
            logCalMsg(CAL_LOG_ERR, 2, 
                            "Failed to save calibration value(%s)",strStimV);
    }
#endif
    setStimulusCurrent(stimVal[1], 0);  // set -ve stimulus
    if (calibrateTrimVoltage(dmmSock, stimV[1], &nStimV) != 0)
    {
        logCalMsg(CAL_LOG_MSG, 2, "\nError: stimulus trim cal -%s\n", strStimV);
        if (! (*eSkips))
            goto errStimCal;
        (*eSkips)--;
    }
    unlockI2CBus(bus);
    //nStimV *= 1000.0;
    logCalMsg(CAL_LOG_MSG, 2, "DACstim_trim_%s_m %0.3f V", strStimV, nStimV);
    if ((nStimV < 1.4900) || (nStimV > 3.1100))
    {
        logCalMsg(CAL_LOG_MSG, 2,
                "\n%s: Stimulus trim cal -%s\n", errStr, strStimV);
        if (! (*eSkips))
            goto errStimCal;
        (*eSkips)--;
    }
#ifdef DO_INI_SAVE
    else
    {
        //  save value to persistent storage
        char *itemPtr;
logPrint("Save N Stim ");
        if (stimVal[1] == STIM_1uA_N)
            itemPtr = STIM_TRIM_1U_N;
        else if (stimVal[1] == STIM_10uA_N)
            itemPtr = STIM_TRIM_10U_N;
        else
            itemPtr = STIM_TRIM_100U_N;
        sprintf(itemStr, "%s", itemPtr);
        sprintf(fltVal, "%0.10f", nStimV); 
logPrint("ItemPtr: %s, fltVal %s\n", itemPtr, fltVal);
        if (ini_puts(sectStr, itemStr, fltVal, SYS_INI_FILE) != 1)
            logCalMsg(CAL_LOG_ERR, 2, 
                            "Failed to save calibration value(%s)",strStimV);
    }
#endif
    return 0;
errStimCal:
    unlockI2CBus(bus);
    return -1;
}

static doDRTDStimulus1uACalibration(int dmmSock, int *eSkips)
{
    logCalMsg(CAL_LOG_MSG, 1, "\nDRTD Stimulus 1uA Calibration:");

    if (doDMMSetup(dmmSock, DMM_100mVDC_100NPLC) != 0)
        return -1;
    usleep(100000);
    if (doDRTDStimulusCalibration(dmmSock, eSkips, CAL_STIM1uA) != 0)
        return -1;
    return 0;
}

static int doDRTDStimulus10uACalibration(int dmmSock, int *eSkips)
{
    logCalMsg(CAL_LOG_MSG, 1, "\nDRTD Stimulus 10uA Calibration:");

    // set DMM to 1V DC range, 100NPLC avg and hi-z(hi-imp) input
    if (doDMMSetup(dmmSock, DMM_1VDC_100NPLC) != 0)
        return -1;
    usleep(100000);
    if (doDRTDStimulusCalibration(dmmSock, eSkips, CAL_STIM10uA) != 0)
        return -1;
    return 0;
}

static int doTCInputResistanceTest(int dmmSock, int *eSkips)
{
    double Vtc_leak;
    logCalMsg(CAL_LOG_MSG, 1, "\nTC Input Resistance Test:");

    getAvgTADCFpgaVal(T_ADC_CHAN_TC, 10, 0, &Vtc_leak);
    Vtc_leak *= 1000.0; // V => mV
    logCalMsg(CAL_LOG_MSG, 2, "Vtc_leak: %0.6f mV", Vtc_leak);
    if (Vtc_leak > 56.010)
    {
        logCalMsg(CAL_LOG_MSG, 2, "\nCAL_ERR_051: TC Input Resistance Test");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    if (Vtc_leak < 24.990)
    {
        logCalMsg(CAL_LOG_MSG, 2, "\nCAL_ERR_052: TC Input Resistance Test");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    return 0;
}

static int doGndChassisConnTest(int dmmSock, int *eSkips)
{
    float Vgnd_chassis_off; // in mV
    float Vgnd_chassis_on;  // in mV
    float Vgnd_chassis_diff;
    float Rgnd_chassis;
    int gain = 4;

    logCalMsg(CAL_LOG_MSG, 1, "\nGround Chassis Conn. Test:");

    setCtrlRegBits(0, 0x400, 0x0);   // disable CJC_PWR
    usleep(20000);
    if (readCJCVolt(gain, &Vgnd_chassis_off) != 0)
        return -1;
    logCalMsg(CAL_LOG_MSG, 2, "V gnd_chassis_off: %0.6f mV", Vgnd_chassis_off);
    if (fabs(Vgnd_chassis_off) > 1.001)
    {
        logCalMsg(CAL_LOG_MSG, 2, "\nCAL_ERR_049: Ground chassis conn. test");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    setCtrlRegBits(0, 0x400, 0x400);   // enable CJC_PWR
    usleep(20000);
    if (readCJCVolt(gain, &Vgnd_chassis_on) != 0)
        return -1;
    logCalMsg(CAL_LOG_MSG, 2, "V gnd_chassis_on %0.6f mV", Vgnd_chassis_on);

    Vgnd_chassis_diff = Vgnd_chassis_on - Vgnd_chassis_off;
    logCalMsg(CAL_LOG_MSG, 2, "V gnd_chassis_diff %0.6f", Vgnd_chassis_diff);

    // convert mV to V for computation..
    Rgnd_chassis = (Vgnd_chassis_diff/1000.0) / 
                        ((5.0 - (Vgnd_chassis_diff/1000.0)) / 150.0);
    logCalMsg(CAL_LOG_MSG, 2, "R gnd_chassis : %0.4f Ohms\n", Rgnd_chassis);
    if (Rgnd_chassis > 0.2510) 
    {
        logCalMsg(CAL_LOG_MSG, 2, "\nCAL_ERR_049: Ground chassis conn. test");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    if (Rgnd_chassis < 2e-3)
    {
        logCalMsg(CAL_LOG_MSG, 2, "\nCAL_ERR_095: Ground chassis conn. test");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    return 0;
}

static int doDongleThreeChecks(int dmmSock, int errSkips)
{
    int dongleId = 3;

    logCalMsg(CAL_LOG_MSG, 0, "---- Dongle 3 Checks:");
    if (tryRegLock(5) < 0)
    {
        logCalMsg(CAL_LOG_ERR, 1, "dongle3Check: unable to get reg. lock");
        closeDMM(dmmSock);
        return -1;
    }
    if (doPreChecks(dmmSock, dongleId, 0) != 0)
        goto errDongle3;
    if (doDiodeSensorGain(dmmSock, &errSkips) != 0)
        goto errDongle3;
    if (doDRTDStimulus1uACalibration(dmmSock, &errSkips) != 0)
        goto errDongle3;
    if (doDRTDStimulus10uACalibration(dmmSock, &errSkips) != 0)
        goto errDongle3;
    if (doTCInputResistanceTest(dmmSock, &errSkips) != 0)
        goto errDongle3;
    if (doGndChassisConnTest(dmmSock, &errSkips) != 0)
        goto errDongle3;

    doEndSettings();
    unlockCtrlReg();
    checkDMMErrors(dmmSock);
    logCalMsg(CAL_LOG_MSG, 0, "---- END Dongle 3 Checks:");
    return 0;
errDongle3:
    doEndSettings();
    unlockCtrlReg();
    checkDMMErrors(dmmSock);
    logCalMsg(CAL_LOG_MSG, 0, "---- END Dongle 3 Checks:");
    return -1;
}

static int doRTD100OhmCheck(int dmmSock, int *eSkips)
{
    double vPrecZero, vPrecStim;
    double vRTDZero, vRTDStim;
    double rPrec = 100.00;
    double rRTD;
    double diff = 0.04001; // 0.04% of 100

    logCalMsg(CAL_LOG_MSG, 1, "\nRTD 100 Ohm Check:");
    setStimulusCurrent(STIM_0uA, 1);
    setDRTDGain(GAIN_X210); 
    setDRTDCalSel1(DRTD_CAL_R100);
    usleep(100000);
    getAvgTADCFpgaVal(T_ADC_CHAN_DRTD, 200, 0, &vPrecZero);

    setStimulusCurrent(STIM_10uA, 1);
    usleep(100000);
    getAvgTADCFpgaVal(T_ADC_CHAN_DRTD, 200, 0, &vPrecStim);

    setStimulusCurrent(STIM_0uA, 1);
    setDRTDCalSel1(DRTD_CAL_NORM);
    usleep(100000);
    getAvgTADCFpgaVal(T_ADC_CHAN_DRTD, 200, 0, &vRTDZero);
    
    setStimulusCurrent(STIM_10uA, 1);
    usleep(100000);
    getAvgTADCFpgaVal(T_ADC_CHAN_DRTD, 200, 0, &vRTDStim);
    logCalMsg(CAL_LOG_MSG, 2, 
            "VPrecZero %0.8f VPrecStim %0.6f VRTDZero %0.6f VRTDStim %0.6f",
            vPrecZero, vPrecStim, vRTDZero, vRTDStim);
    rRTD = rPrec * ( (vRTDStim - vRTDZero) / (vPrecStim - vPrecZero) );
    logCalMsg(CAL_LOG_MSG, 2, "R ext_cal_100 %0.4f Ohms", rRTD);
    if (fabs(100.000 - fabs(rRTD)) > diff)
    {
        logCalMsg(CAL_LOG_MSG, 2, "\nCAL_ERR_054: RTD 100 Ohm Check");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
#ifdef DO_INI_SAVE
    else
    {
        // Save value to persistent storage
        sprintf(itemStr, "%s", V_PREC_ZERO);
        sprintf(fltVal, "%0.10f", vPrecZero);
        if (ini_puts(sectStr, itemStr, fltVal, SYS_INI_FILE) != 1)
            logCalMsg(CAL_LOG_ERR, 2, "Failed to save calibration value(%s)",
                                                                V_PREC_ZERO);
        sprintf(itemStr, "%s", V_RTD_ZERO);
        sprintf(fltVal, "%0.10f", vRTDZero);
        if (ini_puts(sectStr, itemStr, fltVal, SYS_INI_FILE) != 1)
            logCalMsg(CAL_LOG_ERR, 2, "Failed to save calibration value(%s)",
                                                                V_RTD_ZERO);
        sprintf(itemStr, "%s", V_PREC_STIM);
        sprintf(fltVal, "%0.10f", vPrecStim);
        if (ini_puts(sectStr, itemStr, fltVal, SYS_INI_FILE) != 1)
            logCalMsg(CAL_LOG_ERR, 2, "Failed to save calibration value(%s)",
                                                                V_PREC_STIM);
    }
#endif
    return 0;
}

static int doSensorGndIsolTest(int dmmSock, int *eSkips) 
{
    double R;
    double R100K = 1e5;
    double R100M = 1e8;
    double R1p1G = 1.1e9;
    double R9M   = 9e6;
    double R11M  = 11e6;
    char *errStr = 0;

    logCalMsg(CAL_LOG_MSG, 1, "\nSensor Ground Isolation Test:");

    if (mfgBrdRev <= REV_100MOHM_BRD) // boards with R 100MOhm-1GOhm
    {
        // set DMM to 1GOhm range, 100NPLC avg.
        if (doDMMSetup(dmmSock, DMM_1GOHM_100NPLC) != 0)
            return -1;
    }
    else
    {
        // set DMM to 100MOhm range, 100NPLC avg.
        if (doDMMSetup(dmmSock, DMM_100MOHM_100NPLC) != 0)
            return -1;
    }
    sleep(1);
prtReg(0);
    if (getDMMReading(dmmSock, &R) != 0)
    {
        logCalMsg(CAL_LOG_ERR,2, "%s: Failed to read Resistance", __func__);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "R iso_tgnd_chassis: %0.2f MOhms", (R/1e6));
    if (mfgBrdRev <= REV_100MOHM_BRD) // boards with R 100MOhm-1GOhm
    {
        if ((R >= R100K) && (R <= R100M))
            errStr = "CAL_ERR_055";
        else if (R < R100K) 
            errStr = "CAL_ERR_056";
        else if (R > R1p1G) 
            errStr = "CAL_ERR_057";
    }
    else
    {
        if (R < R9M)
            errStr = "CAL_ERR_056";
        if (R > R11M)
            errStr = "CAL_ERR_057";
        /* R between 9M and 11M: OK */
    }
    if (errStr)
    {
        logCalMsg(CAL_LOG_MSG, 2,
            "%s: Sensor Ground Isolation Check failed", errStr);
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    return 0;
}

static int doHeaterGroundIsolCheck(int dmmSock, int *eSkips)
{
    double R;
    double R100K = 1e5;
    double R100M = 1e8;
    double R2p4K = 2.4e3;
    double R1p1G = 1.1e9;
    double R9M   = 9e6;
    double R11M  = 11e6;
    char *errStr = 0;

    logCalMsg(CAL_LOG_MSG, 1, "\nHeater Ground Isolation Test:");

    if (mfgBrdRev <= REV_100MOHM_BRD) // boards with R 100MOhm-1GOhm
    {
        // set DMM to 1GOhm range, 100NPLC avg.
        if (doDMMSetup(dmmSock, DMM_1GOHM_100NPLC) != 0)
            return -1;
    }
    else
    {
        // set DMM to 100MOhm range, 100NPLC avg.
        if (doDMMSetup(dmmSock, DMM_100MOHM_100NPLC) != 0)
            return -1;
    }
    sleep(1);
    setHeaterDAC(0.0);               // set heater output 0
    setVoltageRange(0);
    setCtrlRegBits(0, 0x400, 0x400); // turn on CJC power
    logCalMsg(CAL_LOG_MSG, 2, "Waiting 40 seconds...");
    sleep(40);
prtReg(0);
    if (getDMMReading(dmmSock, &R) != 0)
    {
        logCalMsg(CAL_LOG_ERR,2, "%s: Failed to read Resistance", __func__);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "R iso_hgnd_chassis: %0.2f MOhms", (R/1e6));
    if (mfgBrdRev <= REV_100MOHM_BRD)
    {
        if ((R >= R100K) && (R <= R100M))
            errStr = "CAL_ERR_060";
        if ((R >= R2p4K) && (R < R100K))
            errStr = "CAL_ERR_061";
        else if (R < R2p4K) 
            errStr = "CAL_ERR_058";
        else if (R > R1p1G) 
            errStr = "CAL_ERR_062";
    }
    else
    {
        if (R < R9M)
            errStr = "CAL_ERR_058";
        if (R > R11M)
            errStr = "CAL_ERR_062";
        /* R is between 9M and 11M: OK */
    }
    if (errStr)
    {
        logCalMsg(CAL_LOG_MSG, 2,
            "%s: Heater Ground Isolation Check failed", errStr);
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    return 0;
}

static int doHeater2ChassisConnTest(int dmmSock, int *eSkips)
{
    double R, V;
    double expR = 2490;         // 2.49KOhms
    double diff = (expR * 0.210)/100.0;  // tolerance, +/-0.2%

    logCalMsg(CAL_LOG_MSG, 1, "\nHeater2Chassis Connection Check:");

    setCtrlRegBits(1, 0x6, 0x4); // heatgnd_sel[1:0] = 10
    // heater output already set to 0.
    usleep(200000);
    if (doDMMSetup(dmmSock, DMM_100mVDC_10NPLC) != 0)
        return -1;
    if (getDMMReading(dmmSock, &V) != 0)
    {
        logCalMsg(CAL_LOG_ERR,2, "%s: Failed to read Voltage", __func__);
        return -1;
    }
    V *= 1000.0;
    logCalMsg(CAL_LOG_MSG, 2, "Voltage : %0.6f mV", V);
    if (V > 2e-1)  // V already in mV
    {
        logCalMsg(CAL_LOG_MSG, 2,
            "CAL_ERR_097: Heater 2 Chassis connection voltage failed");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
prtReg(0);
    if (doDMMSetup(dmmSock, DMM_10KOHM_100NPLC) != 0)
        return -1;
    if (getDMMReading(dmmSock, &R) != 0)
    {
        logCalMsg(CAL_LOG_ERR,2, "%s: Failed to read Resistance", __func__);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "R relay_hgnd_chassis : %0.4f KOhms", (R/1e3));
    if (fabs(expR - R) > diff)
    {
        logCalMsg(CAL_LOG_MSG, 2,
            "CAL_ERR_063: Heater 2 Chassis connection check failed");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    return 0;
}

static int doFloatingHeaterGndCMVoltageCheck(int dmmSock, int *eSkips)
{
    double V;
    logCalMsg(CAL_LOG_MSG, 1, 
                        "\nFloating Heater Ground Common Mode Voltage Check:");

    if (doDMMSetup(dmmSock, DMM_100VAC_20HZ) != 0)
        return -1;
    setCtrlRegBits(1, 0x6, 0x0); // heat_gnd_sel[1:0] = 00
    sleep(1);
prtReg(0);
    if (getDMMReading(dmmSock, &V) != 0)
    {
        logCalMsg(CAL_LOG_ERR,2, "%s: Failed to read Voltage", __func__);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "V heat_cm_float : %0.6f V", V);
    if (V > 10.00010)
    {
        logCalMsg(CAL_LOG_MSG, 2,
          "CAL_ERR_064: Floating Heater Ground Comm-Mode Voltage Check failed");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    return 0;
}

static int doBufferedHeaterGndCMNoiseCheck(int dmmSock,  int *eSkips)
{
    double Vheat_cm_buf_no_load;
    double Vheat_cm_buf_2A_load;
    float hDACv;
    float hV, hI;
    uint32_t rawV, rawI;
    int ret, cnt;
    logCalMsg(CAL_LOG_MSG, 1, 
            "\nBuffered Heater Ground common-mode noise check:");
    if (doDMMSetup(dmmSock, DMM_100mVAC_20HZ) != 0)
        return -1;
    setCtrlRegBits(1, 0x6, 0x6); // heat_gnd_sel[1:0] = 11 : buffered gnd
    // heater output already set to 0
    // CJC already on
    sleep(7);
prtReg(0);
    if (getDMMReading(dmmSock, &Vheat_cm_buf_no_load) != 0)
    {
        logCalMsg(CAL_LOG_ERR,2, "%s: Failed to read voltage", __func__);
        return -1;
    }
    Vheat_cm_buf_no_load *= 1000.0; // V -> mV
    logCalMsg(CAL_LOG_MSG, 2, "V heat_cm_buf_no_load : %0.6f mV", 
                                                        Vheat_cm_buf_no_load);
    if (Vheat_cm_buf_no_load > 1.00100)
    {
        logCalMsg(CAL_LOG_MSG, 2,
                "CAL_ERR_064: V heat_cm_buf_no_load > 2.00mV");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    setVoltageRange(10);
    setIRange(HEAT_IRANGE_2A); 
    setHeaterDAC(0.0);
    usleep(250000);
prtReg(0);
    // increase heater output by 1mV every 10mS, till current is between 
    // 2 and 2.1 A.
    hDACv = 0.000;
    for (cnt = 0; ;cnt++)
    {
        hDACv += 0.0010;
        if (hDACv > 9.999)
            break;
        setHeaterDAC(hDACv);
        getHeaterValues(&hV, &rawV, &hI, &rawI);
//if ((cnt % 100) == 0)
//logPrint("setV: %0.6f, getV: %0.6f getI: %0.6f\n", hDACv, hV, hI);
        if (hI > 2.010)
            break;
        usleep(10000);
    }
//logPrint("\nsetV: %0.6f, getV: %0.6f getI: %0.6f\n\n", hDACv, hV, hI);
    if ((hI  > 2.0010) && (hI < 2.1010))
    {
        sleep(2);
        ret = getDMMReading(dmmSock, &Vheat_cm_buf_2A_load);
        setHeaterDAC(0.0);
        setVoltageRange(0); // turn off heater power
        if (ret != 0)
        {
            logCalMsg(CAL_LOG_ERR,2, "%s: Failed to read voltage", __func__);
            return -1;
        }
        Vheat_cm_buf_2A_load *= 1000.0; // V -> mV
        logCalMsg(CAL_LOG_MSG, 2, 
                    "V heat_cm_buf_2A_load : %0.6f mV", Vheat_cm_buf_2A_load);
        if (Vheat_cm_buf_2A_load > 2.01000)
        {
            logCalMsg(CAL_LOG_MSG, 2,
                    "CAL_ERR_064: V heat_cm_buf_2A_load > 2.00mV");
            if (! (*eSkips))
                return -1;
            (*eSkips)--;
        }
    }
    else
    {
        setHeaterDAC(0.0);
        setVoltageRange(0); // turn off heater power
        logCalMsg(CAL_LOG_MSG, 2, 
                    "CAL_ERR_068: Failed to raise output to 2A - 2.1A ");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
prtReg(0);
    return 0;
}

static int doBufferedHeaterGnd0VOffset(int dmmSock, int *eSkips) 
{
    double Vbuf_gnd_offs;
    logCalMsg(CAL_LOG_MSG, 1, "\nBuffered Heater Ground 0V Offset:");
    if (doDMMSetup(dmmSock, DMM_100mVDC_100NPLC) != 0)
        return -1;
    if (getDMMReading(dmmSock, &Vbuf_gnd_offs) != 0)
    {
        logCalMsg(CAL_LOG_ERR,2, "%s: Failed to read voltage", __func__);
        return -1;
    }
prtReg(0);
    Vbuf_gnd_offs *= 1000.0; // V -> mV
    logCalMsg(CAL_LOG_MSG, 2, "V buf_gnd_offset : %0.6f mV", Vbuf_gnd_offs);
    if (Vbuf_gnd_offs > 2.0100)
    {
        logCalMsg(CAL_LOG_ERR, 2,
            "\nCAL_ERR_066: V buf_gnd_offset > 2.0mV\n");
    }
    return 0;
}

static int doDongleFourChecks(int dmmSock, int errSkips)
{
    int dongleId = 4;

    logCalMsg(CAL_LOG_MSG, 0, "---- Dongle 4 Checks:");
    if (tryRegLock(5) < 0)
    {
        logCalMsg(CAL_LOG_ERR, 1, "dongle4Check: unable to get reg. lock");
        closeDMM(dmmSock);
        return -1;
    }

    if (doPreChecks(dmmSock, dongleId, 0) != 0)
        goto errDongle4;

    if (doRTD100OhmCheck(dmmSock, &errSkips) != 0)
        goto errDongle4;
    if (doSensorGndIsolTest(dmmSock, &errSkips) != 0)
        goto errDongle4;
    if (doHeaterGroundIsolCheck(dmmSock, &errSkips) != 0)
        goto errDongle4;
    if (doHeater2ChassisConnTest(dmmSock, &errSkips) != 0)
        goto errDongle4;
    if (doFloatingHeaterGndCMVoltageCheck(dmmSock, &errSkips) != 0)
        goto errDongle4;
    if (doBufferedHeaterGndCMNoiseCheck(dmmSock, &errSkips) != 0)
        goto errDongle4;
    if (doBufferedHeaterGnd0VOffset(dmmSock, &errSkips) != 0)
        goto errDongle4;

    doEndSettings();
    unlockCtrlReg();
    checkDMMErrors(dmmSock);
    logCalMsg(CAL_LOG_MSG, 0, "---- END Dongle 4 Checks:");
    return 0;
errDongle4:
    doEndSettings();
    unlockCtrlReg();
    checkDMMErrors(dmmSock);
    logCalMsg(CAL_LOG_MSG, 0, "---- END Dongle 4 Checks:");
    return -1;
}

static int doDongleFiveChecks(int dmmSock, int errSkips)
{
    int dongleId = 5;
    logCalMsg(CAL_LOG_ERR, 0, "Checks for dongle 5 not implemented yet");
    return -1;
}

int diagSetSensorType(int idx, int sType);
int diagGetTemperature(int idx, float *temp, float *v);
char *diagGetSensorStr(int idx);
static int doTempAgreementCheck(int dmmSock, int *eSkips) 
{
    int ret;
    float tcTemp, tcV;
    float diodeT, diodeV;
    float cjcTemp;
    logCalMsg(CAL_LOG_MSG, 1, "\n Temperature Agreement Check");

    if ((ret = diagSetSensorType(0, DT_670)) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "Failed to set sensor 1 type(%d)", ret);
        return -1;
    }
    usleep(250000);
    if ((ret = diagSetSensorType(1, Type_T)) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "Failed to set sensor 2 type(%d)", ret);
        return -1;
    }
    // release lock for cjc task to update fpga registers..
    unlockCtrlReg();
    sleep(5);

    if (tryRegLock(5) < 0)
    {
        logCalMsg(CAL_LOG_ERR, 1, "%s: failed re-acquire lock", __func__);
        return -1;
    }
    if ((ret = diagGetTemperature(0, &diodeT, &diodeV)) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "Failed to read temperature 1", ret);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "Sensor 1(%s) : %0.6f K [ %0.6f V]",
                diagGetSensorStr(0), diodeT, diodeV);
    usleep(25000);
    if ((ret = diagGetTemperature(1, &tcTemp, &tcV)) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "Failed to read temperature 2", ret);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "Sensor 2(%s) : %0.6f K [ %0.6f V]",
                diagGetSensorStr(1), tcTemp, tcV);
    usleep(25000);
    if (readCJCTemp(&cjcTemp) != 0)
        return -1;
    cjcTemp += 273.15; // conv to K
    logCalMsg(CAL_LOG_MSG, 2, "LM35 Temperature : %0.6f K\n", cjcTemp);

    unlockCtrlReg();

    logCalMsg(CAL_LOG_MSG, 2, "TC - Diode: %0.6f", (tcTemp - diodeT));
    logCalMsg(CAL_LOG_MSG, 2, "LM35 - TC : %0.6f\n", (cjcTemp - tcTemp));

    sleep(5);

    if (tryRegLock(5) < 0)
    {
        logCalMsg(CAL_LOG_ERR, 1, "%s: failed re-acquire lock", __func__);
        return -1;
    }
    if ((ret = diagGetTemperature(0, &diodeT, &diodeV)) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "Failed to read temperature 1", ret);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "Sensor 1(%s) : %0.6f K [ %0.6f V]",
                diagGetSensorStr(0), diodeT, diodeV);
    usleep(25000);
    if ((ret = diagGetTemperature(1, &tcTemp, &tcV)) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "Failed to read temperature 2", ret);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "Sensor 2(%s) : %0.6f K [ %0.6f V]",
                diagGetSensorStr(1), tcTemp, tcV);
    usleep(25000);
    if (readCJCTemp(&cjcTemp) != 0)
        return -1;
    cjcTemp += 273.15; // convert to K
    logCalMsg(CAL_LOG_MSG, 2, "LM35 Temperature : %0.6f K\n", cjcTemp);

    logCalMsg(CAL_LOG_MSG, 2, "TC - Diode: %0.6f", (tcTemp - diodeT));
    logCalMsg(CAL_LOG_MSG, 2, "LM35 - TC : %0.6f\n", (cjcTemp - tcTemp));

    if (fabs(diodeT - tcTemp) > (0.015*cjcTemp + 0.5))
    {
        logCalMsg(CAL_LOG_MSG, 2,  
              "\nCAL_ERR_072: Diode and TC temperatures do not agree.\n");
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
    }

    if (fabs(cjcTemp - tcTemp) > 0.801)
    {
        logCalMsg(CAL_LOG_MSG, 2,  
              "\nCAL_ERR_072: TC and LM35 temperatures do not agree.\n");
        if (!(*eSkips))
            return -1;
        (*eSkips)--;
    }

    resetSensors();
    return 0;
}

static int doScreenConnCheck(int dmmSock, int *eSkips) 
{
    double R;
    logCalMsg(CAL_LOG_MSG, 1, "\n TC_SCREEN and DRTD_SCREEN Conn. Check");
    if (doDMMSetup(dmmSock, DMM_100OHM_10NPLC) != 0)
        return -1;
    if (getDMMReading(dmmSock, &R) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, "%s: Failed to read Resistance", __func__);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "R: %0.6f Ohms\n", R);
    if (R > 1.01)
    {
        logCalMsg(CAL_LOG_MSG, 2, "CAL_ERR_073: Resistance over 1 Ohm");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    return 0;
}

static int doDongleSixChecks(int dmmSock, int errSkips)
{
    int dongleId = 6;

    logCalMsg(CAL_LOG_MSG, 0, "---- Dongle 6 Checks:");
    if (tryRegLock(5) < 0)
    {
        logCalMsg(CAL_LOG_ERR, 1, "dongle7Check: unable to get reg. lock");
        closeDMM(dmmSock);
        return -1;
    }

    if (doPreChecks(dmmSock, dongleId, 0) != 0)
        goto errDongle6;
    if (doTempAgreementCheck(dmmSock, &errSkips) != 0)
        goto errDongle6;
    if (doScreenConnCheck(dmmSock, &errSkips) != 0)
        goto errDongle6;

    doEndSettings();
    unlockCtrlReg();
    checkDMMErrors(dmmSock);
    logCalMsg(CAL_LOG_MSG, 0, "---- END Dongle 6 Checks:");
    return 0;
errDongle6:
    doEndSettings();
    unlockCtrlReg();
    checkDMMErrors(dmmSock);
    logCalMsg(CAL_LOG_MSG, 0, "---- END Dongle 6 Checks:");
    return -1;
}

static int doHeaterOPDisableTest(int dmmSock, int *eSkips)
{
    uint16_t regVal;
    double Vheat_off_leak_max_dac;
    double Vheat_off_leak_min_dac;
    logCalMsg(CAL_LOG_MSG, 1, "\nHeater Output Disable Test");

    if (doDMMSetup(dmmSock, DMM_1VDC_10NPLC) != 0)
        return -1;
    setVoltageRange(0); // disable heater output, HEAT_VRANGE[1:0] = 11
    setCtrlRegBits(1, 0x1, 0x1); // enable BAKE
    regVal = 0xFFFF; // set full scale DAC8560, heater output
    writeCtrlRegStrobe(8, 2, (void *)&regVal, 0, 4);
    usleep(500000);
    // check HEAT_OUT_ENABLED, FPGA inverts the signal..
    readStatRegFull(0, 1, &regVal); // re-use regVal..
    //logPrint("R: 0x%X\n", regVal);
    if (regVal & 0x200) // must be low
    {
        logCalMsg(CAL_LOG_ERR, 2, "\nCAL_ERR_112: HEAT_OUT_ENABLED is HIGH\n");
        if (! (*eSkips))
            goto errHeaterOP;
        (*eSkips)--;
    }

    if (getDMMReading(dmmSock, &Vheat_off_leak_max_dac) != 0)
    {
        logCalMsg(CAL_LOG_ERR,2, "%s: Failed to read voltage", __func__);
        goto errHeaterOP;
    }
    Vheat_off_leak_max_dac *= 1000.0; // V -> mV
    logCalMsg(CAL_LOG_MSG, 2, 
                    "V heat_off_leak_max_dac %0.6f mV", Vheat_off_leak_max_dac);
    if ((Vheat_off_leak_max_dac < -50.01) ||  // range -0.05V - 0.1V
                                (Vheat_off_leak_max_dac > 1e2)) // 0.1V
    {
        logCalMsg(CAL_LOG_ERR,2, 
            "\nCAL_ERR_074: V heat_off_leak_max_dac is outside (-0.05-0.1V)\n");
        if (! (*eSkips))
            goto errHeaterOP;
        (*eSkips)--;
    }

    regVal = 0x0;
    writeCtrlRegStrobe(8, 2, (void *)&regVal, 0, 4);
    usleep(500000);
    if (getDMMReading(dmmSock, &Vheat_off_leak_min_dac) != 0)
    {
        logCalMsg(CAL_LOG_ERR,2, "%s: Failed to read voltage", __func__);
        return -1;
    }
    Vheat_off_leak_min_dac *= 1000.0; // V -> mV
    logCalMsg(CAL_LOG_MSG, 2, 
                    "V heat_off_leak_min_dac %0.6f mV", Vheat_off_leak_max_dac);
    if (fabs(Vheat_off_leak_min_dac) > 50.010) // +/- 0.05V
    {
        logCalMsg(CAL_LOG_ERR,2, 
            "\nCAL_ERR_075: V heat_off_leak_max_dac is outside (+/- 0.05V)\n");
        if (! (*eSkips))
        {
            setCtrlRegBits(1, 0x1, 0x0); // disable BAKE
            return -1;
        }
        (*eSkips)--;
    }
    //setCtrlRegBits(1, 0x1, 0x0); // disable BAKE
    return 0;
errHeaterOP:
    regVal = 0x0;
    writeCtrlRegStrobe(8, 2, (void *)&regVal, 0, 4); // turn off o/p
    //setCtrlRegBits(1, 0x1, 0x0); // disable BAKE
    return -1;
}


static double g_Vheat_max_40v_range;
static double g_Vheat_max_20v_range;
static double g_Vheat_max_10v_range;
static int heat_max_OK = 0; 
static int doHeaterMaxVRangeCheck(int dmmSock, int *eSkips)
{
    uint16_t regVal;
    double max40v = 41.90;
    double max20v = 20.90;
    double max10v = 10.40;
    double v40min = 41.23, v40max = 42.57;
    double v20min = 20.53, v20max = 21.27;
    double v10min = 10.18, v10max = 10.61;
    logCalMsg(CAL_LOG_MSG, 1, "\nHeater Max. O/P Voltage Range Check:");

    if (doDMMSetup(dmmSock, DMM_100VDC_10NPLC) != 0)
        return -1;
    setCtrlRegBits(0, 0x400, 0x0);   // disable CJC_PWR
    // bake is still enabled..
    setVoltageRange(40);
    regVal = 0xFFFF; // set full scale DAC8560, heater output
    writeCtrlRegStrobe(8, 2, (void *)&regVal, 0, 4);
    usleep(500000);
    // check HEAT_OUT_ENABLED, FPGA inverts the signal..
    readStatRegFull(0, 1, &regVal); // re-use regVal..
    usleep(10000);
    readStatRegFull(0, 1, &regVal);
    //logPrint("R: 0x%X\n", regVal);
    if (!(regVal & 0x200)) // must be high
    {
        logCalMsg(CAL_LOG_ERR, 2, "\nCAL_ERR_112: HEAT_OUT_ENABLED is LOW\n");
unlockCtrlReg();
sleep(5);
if (tryRegLock(5) < 0)
{
    logCalMsg(CAL_LOG_ERR, 1, "dongle7Check: unable to get reg. lock");
    return -1;
}
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
#if 0
{
    float hV, hI;
    uint32_t rawV, rawI;
    getHeaterValues(&hV, &rawV, &hI, &rawI);
    logPrint("Heater V: %0.6f I: %0.6f\n", hV, hI);
}
sleep(1);
#endif
    if (getDMMReading(dmmSock, &g_Vheat_max_40v_range) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, 
                    "%s: Failed to read Vheat_max_40v_range", __func__);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, 
                    "V heat_max_40v_range %0.3f V\n", g_Vheat_max_40v_range);
    if ((g_Vheat_max_40v_range < v40min) || (g_Vheat_max_40v_range > v40max))
    {
        logCalMsg(CAL_LOG_ERR, 2, "\nCAL_ERR_079: V heat_max_40v_range\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
else heat_max_OK++;

    setVoltageRange(20);
    usleep(120000);
    if (getDMMReading(dmmSock, &g_Vheat_max_20v_range) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, 
                    "%s: Failed to read Vheat_max_20v_range", __func__);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, 
                    "V heat_max_20v_range %0.3f V\n", g_Vheat_max_20v_range);
    if ((g_Vheat_max_20v_range < v20min) || (g_Vheat_max_20v_range > v20max))
    {
        logCalMsg(CAL_LOG_ERR, 2, "\nCAL_ERR_080: V heat_max_20v_range\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
else heat_max_OK++;

    setVoltageRange(10);
    usleep(120000);
    if (getDMMReading(dmmSock, &g_Vheat_max_10v_range) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, 
                    "%s: Failed to read Vheat_max_10v_range", __func__);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, 
                    "V heat_max_10v_range %0.3f V\n", g_Vheat_max_10v_range);
    if ((g_Vheat_max_10v_range < v10min) || (g_Vheat_max_10v_range > v10max))
    {
        logCalMsg(CAL_LOG_ERR, 2, "\nCAL_ERR_081: V heat_max_10v_range\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
else heat_max_OK++;
    return 0;
}

static double g_Vheat_min_40v_range;
static double g_Vheat_min_20v_range;
static double g_Vheat_min_10v_range;
static int heat_min_OK = 0;
static int doHeaterMinVRangeCheck(int dmmSock, int *eSkips)
{
    uint16_t regVal;
    double vMin = -0.164;
    double v40max = 0.166, v20max = 0.0638, v10max = 0.0129;

    logCalMsg(CAL_LOG_MSG,1,"\nHeater Min. O/P Voltage Range Check(unloaded):");

    if (doDMMSetup(dmmSock, DMM_1VDC_10NPLC) != 0)
        return -1;
    // CJC already disabled.
    // bake is still enabled..
    regVal = 0x0; // set  min. output DAC8560, heater output
    writeCtrlRegStrobe(8, 2, (void *)&regVal, 0, 4);
    setVoltageRange(40);
    usleep(500000);
    if (getDMMReading(dmmSock, &g_Vheat_min_40v_range) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, 
                    "%s: Failed to read Vheat_min_40v_range", __func__);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, 
                    "V heat_min_40v_range %0.6f V\n", g_Vheat_min_40v_range);
    if ((g_Vheat_min_40v_range < vMin) || (g_Vheat_min_40v_range > v40max))
    {
        logCalMsg(CAL_LOG_ERR, 2, "\nCAL_ERR_082: V heat_min_40v_range\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
else heat_min_OK++;

    setVoltageRange(20);
    usleep(120000);
    if (getDMMReading(dmmSock, &g_Vheat_min_20v_range) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, 
                    "%s: Failed to read Vheat_min_20v_range", __func__);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, 
                    "V heat_min_20v_range %0.6f V\n", g_Vheat_min_20v_range);
    if ((g_Vheat_min_20v_range < vMin) || (g_Vheat_min_20v_range > v20max))
    {
        logCalMsg(CAL_LOG_ERR, 2, "\nCAL_ERR_082: V heat_min_20v_range\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
else heat_min_OK++;

    setVoltageRange(10);
    usleep(120000);
    if (getDMMReading(dmmSock, &g_Vheat_min_10v_range) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, 
                    "%s: Failed to read Vheat_min_10v_range", __func__);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, 
                    "V heat_min_10v_range %0.6f V\n", g_Vheat_min_10v_range);
    if ((g_Vheat_min_10v_range < vMin) || (g_Vheat_min_10v_range > v10max))
    {
        logCalMsg(CAL_LOG_ERR, 2, "\nCAL_ERR_082: V heat_min_10v_range\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
else heat_min_OK++;
    return 0;
}

static double g_Vheat_head_40V_range;
static double g_Vheat_head_diff;
#define HEAT_OUT_99P5  (65207)
#define HEAT_OUT_100P0 (65535)
static int heat_head_OK = 0;
static int do44VRailHeaderoomCheck(int dmmSock, int *eSkips)
{
    uint16_t regVal;

    logCalMsg(CAL_LOG_MSG, 1, "\nHeater 44V Rail Headroom Check:\n");

    if (doDMMSetup(dmmSock, DMM_100VDC_10NPLC) != 0)
        return -1;
    regVal = HEAT_OUT_99P5; // 99.5% of full scale
    regVal = bswap_16(regVal);
    writeCtrlRegStrobe(8, 2, (void *)&regVal, 0, 4);
    setVoltageRange(40);
    usleep(500000);
    if (getDMMReading(dmmSock, &g_Vheat_head_40V_range) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, 
                    "%s: Failed to read Heater Voltage", __func__);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, 
            "V heat_head_40V_range: %0.6f V", g_Vheat_head_40V_range);
    g_Vheat_head_diff = g_Vheat_max_40v_range - g_Vheat_head_40V_range;
    logCalMsg(CAL_LOG_MSG, 2, "V heat_head_diff: %0.6f V", g_Vheat_head_diff);
    if ((g_Vheat_head_diff < 0.1499) || // range 0.15 - 0.25 V
        (g_Vheat_head_diff > 0.2501))
    {
        logCalMsg(CAL_LOG_ERR, 2, "\nCAL_ERR_095: V heat_head_diff\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
else heat_head_OK++;
    return 0;
}

#define HI_DAC_OUTP (64500)
double g_Vheat_high_40V_range;
double g_Vheat_high_20V_range;
double g_Vheat_high_10V_range;
int heat_high_ok = 0;
static int doHeaterVoltAtHighDAC(int dmmSock, int *eSkips)
{
    uint16_t regVal;

    logCalMsg(CAL_LOG_MSG, 1, "\nHeater Output Voltage at High DAC value:\n");

    if (doDMMSetup(dmmSock, DMM_100VDC_10NPLC) != 0)
        return -1;
    regVal = HI_DAC_OUTP; 
    regVal = bswap_16(regVal);
    setVoltageRange(40);
    writeCtrlRegStrobe(8, 2, (void *)&regVal, 0, 4);
    usleep(500000);
    if (getDMMReading(dmmSock, &g_Vheat_high_40V_range) != 0)
    {
        logCalMsg(CAL_LOG_ERR,2,"%s: Failed to read Heater Voltage",__func__);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "V heat_high_40V_range: %0.6f V", 
                                                g_Vheat_high_40V_range);
    if ((g_Vheat_high_40V_range < 40.57) || (g_Vheat_high_40V_range > 41.90))
    {
        logCalMsg(CAL_LOG_ERR, 2, "\nCAL_ERR_106: V heat_high_40V_range\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    else heat_high_ok++;

    setVoltageRange(20);
    usleep(250000);
    if (getDMMReading(dmmSock, &g_Vheat_high_20V_range) != 0)
    {
        logCalMsg(CAL_LOG_ERR,2,"%s: Failed to read Heater Voltage",__func__);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "V heat_high_20V_range: %0.6f V", 
                                                g_Vheat_high_20V_range);
    if ((g_Vheat_high_20V_range < 20.21) || (g_Vheat_high_20V_range > 20.93))
    {
        logCalMsg(CAL_LOG_ERR, 2, "\nCAL_ERR_107: V heat_high_20V_range\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    else heat_high_ok++;

    setVoltageRange(10);
    usleep(250000);
    if (getDMMReading(dmmSock, &g_Vheat_high_10V_range) != 0)
    {
        logCalMsg(CAL_LOG_ERR,2,"%s: Failed to read Heater Voltage",__func__);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "V heat_high_10V_range: %0.6f V", 
                                                g_Vheat_high_10V_range);
    if ((g_Vheat_high_10V_range < 10.02) || (g_Vheat_high_10V_range > 10.45))
    {
        logCalMsg(CAL_LOG_ERR, 2, "\nCAL_ERR_108: V heat_high_10V_range\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    else heat_high_ok++;
    return 0;
}

#define LO_DAC_OUTP (500)
double g_Vheat_low_40V_range;
double g_Vheat_low_20V_range;
double g_Vheat_low_10V_range;
int heat_low_ok = 0;
static int doHeaterVoltAtLowDAC(int dmmSock, int *eSkips)
{
    uint16_t regVal;

    logCalMsg(CAL_LOG_MSG, 1, "\nHeater Output Voltage at Low DAC value:\n");

    if (doDMMSetup(dmmSock, DMM_1VDC_10NPLC) != 0)
        return -1;
    regVal = LO_DAC_OUTP; 
    regVal = bswap_16(regVal);
    setVoltageRange(40);
    writeCtrlRegStrobe(8, 2, (void *)&regVal, 0, 4);
    usleep(500000);
    if (getDMMReading(dmmSock, &g_Vheat_low_40V_range) != 0)
    {
        logCalMsg(CAL_LOG_ERR,2,"%s: Failed to read Heater Voltage",__func__);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "V heat_low_40V_range: %0.6f V", 
                                                g_Vheat_low_40V_range);
    if ((g_Vheat_low_40V_range < -0.044) || (g_Vheat_low_40V_range > 0.489))
    {
        logCalMsg(CAL_LOG_ERR, 2, "\nCAL_ERR_109: V heat_low_40V_range\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    else heat_low_ok++;

    setVoltageRange(20);
    usleep(250000);
    if (getDMMReading(dmmSock, &g_Vheat_low_20V_range) != 0)
    {
        logCalMsg(CAL_LOG_ERR,2,"%s: Failed to read Heater Voltage",__func__);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "V heat_low_20V_range: %0.6f V", 
                                                g_Vheat_low_20V_range);
    if ((g_Vheat_low_20V_range < -0.104) || (g_Vheat_low_20V_range > 0.225))
    {
        logCalMsg(CAL_LOG_ERR, 2, "\nCAL_ERR_110: V heat_low_20V_range\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    else heat_low_ok++;

    setVoltageRange(10);
    usleep(250000);
    if (getDMMReading(dmmSock, &g_Vheat_low_10V_range) != 0)
    {
        logCalMsg(CAL_LOG_ERR,2,"%s: Failed to read Heater Voltage",__func__);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "V heat_low_10V_range: %0.6f V", 
                                                g_Vheat_low_10V_range);
    if ((g_Vheat_low_10V_range < -0.134) || (g_Vheat_low_10V_range > 0.094))
    {
        logCalMsg(CAL_LOG_ERR, 2, "\nCAL_ERR_111: V heat_low_10V_range\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    else heat_low_ok++;
    return 0;
}

static double g_DAC_40V, g_DAC_20V, g_DAC_10V;
static int doHeaterVoltageCalibration(int dmmSock, int *eSkips)
{
    logCalMsg(CAL_LOG_MSG, 1, "\nHeater Voltage Calibration:\n");
    g_DAC_40V = (HI_DAC_OUTP - LO_DAC_OUTP) /
                    (g_Vheat_high_40V_range - g_Vheat_low_40V_range);
    g_DAC_20V = (HI_DAC_OUTP - LO_DAC_OUTP) /
                    (g_Vheat_high_20V_range - g_Vheat_low_20V_range);
    g_DAC_10V = (HI_DAC_OUTP - LO_DAC_OUTP) /
                    (g_Vheat_high_10V_range - g_Vheat_low_10V_range);

    logCalMsg(CAL_LOG_MSG, 2, "Gdac_40v : %0.6f V", g_DAC_40V);
    logCalMsg(CAL_LOG_MSG, 2, "Gdac_20v : %0.6f V", g_DAC_20V);
    logCalMsg(CAL_LOG_MSG, 2, "Gdac_10v : %0.6f V", g_DAC_10V);

    // DAC = (VHeat - Vheat_min_xxV_range) * g_DAC_XXV;
    // where DAC - DAC reg. value
    //       Vheat is the required heater Voltage output
    return 0;
}

static int errPrt = 0;
static int setHDACRegVal(double v, int range)
{
    uint16_t regVal = 0;
    int dacVal; // must be signed, value could be -ve
    int rOK = 0;
#if 0
if (! errPrt)
printf("heat_high_ok %d, heat_low_ok %d, heat_head_OK %d\n", heat_high_ok, heat_low_ok, heat_head_OK);
#endif
    if ((heat_high_ok == 3) && (heat_low_ok == 3) && (heat_head_OK))
    {
        switch (range)
        {
            case 10:
                dacVal = ((v - g_Vheat_low_10V_range) * g_DAC_10V) + 500;
                rOK = 1;
                break;
            case 20:
                dacVal = ((v - g_Vheat_low_20V_range) * g_DAC_20V) + 500;
                rOK = 1;
                break;
            case 40:
                dacVal = ((v - g_Vheat_low_40V_range) * g_DAC_40V) + 500;
                rOK = 1;
                break;
            default:
                logPrint("%s: Error: invalid range\n");
                break;
        }
    }
if (! errPrt)
{
logPrint("-- %s Using calibrated V --\n", ((rOK) ? "" : "NOT"));
errPrt++;
}
    if (rOK)
    {
        if (dacVal < 0)
            regVal = 0;
        else if (dacVal > 0xFFFF)
            regVal = 0xFFFF;
        else
            regVal = dacVal;
        //logPrint("%s: %0.3f(%d) -> %hu[%d]\n", __func__, v, 
        //                                    range, regVal, dacVal);
        return setHeaterDACRegVal(regVal);
    }
    return setHeaterDAC((float)v);
}

static int doHeaterVoltageCheckUnloaded(int dmmSock, int *eSkips)
{
    double h40V, h20V, h10V;
    double d40V = 40.0;
    double d20V = 20.0;
    double d10V = 10.0;
    double  diff, V;
    logCalMsg(CAL_LOG_MSG, 1, "\nHeater Voltage Check(Unloaded):");

    if (doDMMSetup(dmmSock, DMM_100VDC_10NPLC) != 0)
        return -1;
    setVoltageRange(40);
    setHDACRegVal(d40V, 40);
    usleep(120000);

    // verify 40V output
    if (getDMMReading(dmmSock, &h40V) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, 
                    "%s: Failed to read Heater Voltage(%0.1f)", __func__, d40V);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "V heat_check_40V: %0.6f V", h40V);
    diff = d40V * 0.101 / 100.0;
    if ((h40V < (d40V - diff)) || (h40V > (d40V + diff)))
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                    "\nCAL_ERR_083: heater %0.1fV not within range\n", d40V);
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }

    // verify 20V output
    setHDACRegVal(d20V, 40);
    usleep(120000);
    if (getDMMReading(dmmSock, &h20V) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, 
                    "%s: Failed to read Heater Voltage(%0.1f)", __func__,d20V);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "V heat_check_20V: %0.6f V", h20V);
    diff = d20V * 0.101 / 100.0;
    if ((h20V < (d20V - diff)) || (h20V > (d20V + diff)))
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_084: heater %0.1fV not within range\n", d20V);
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }

    // verify 10V output
    setHDACRegVal(d10V, 40);
    usleep(120000);
    if (getDMMReading(dmmSock, &h10V) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, 
                    "%s: Failed to read Heater Voltage(%0.1f)", __func__,d10V);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "V heat_check_10V: %0.6f V", h10V);
    diff = d10V * 0.101 / 100.0;
    if ((h10V < (d10V - diff)) || (h10V > (d10V + diff)))
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_085: heater %0.1fV not within range\n", d10V);
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }

    // 0(no) heater output checks.
    // 40V range
    if (doDMMSetup(dmmSock, DMM_1VDC_10NPLC) != 0)
        return -1;
    setVoltageRange(40);
    setHDACRegVal(0.0, 40);
    usleep(120000);
    if (getDMMReading(dmmSock, &V) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, 
                    "%s: Failed to read Heater Voltage(40V,0OP)", __func__);
        return -1;
    }
    V *= 1000.0; // V -> mV
    logCalMsg(CAL_LOG_MSG, 2, "40V Range 0V O/P: %0.6f mV", V);
    if ((V < -10.0) || (V > 165.01))  // range -0.01V - 0.165V
    {
        logCalMsg(CAL_LOG_MSG, 2, "\nCAL_ERR_086: 40V range, 0V O/P\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }

    // 20V range
    setVoltageRange(20);
    setHDACRegVal(0.0, 20);
    usleep(120000);
    if (getDMMReading(dmmSock, &V) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, 
                    "%s: Failed to read Heater Voltage(20V,0OP)", __func__);
        return -1;
    }
    V *= 1000.0; // V -> mV
    logCalMsg(CAL_LOG_MSG, 2, "20V Range 0V O/P: %0.6f mV", V);
    if ((V < -10.0) || (V > 64.01))  // range -0.01V - 0.064V
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_087: heater 0 O/P, 20V range\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }

    // 10V range
    setVoltageRange(10);
    setHDACRegVal(0.0, 10);
    usleep(120000);
    if (getDMMReading(dmmSock, &V) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, 
                    "%s: Failed to read Heater Voltage(10V,0OP)", __func__);
        return -1;
    }
    V *= 1000.0; // V -> mV
    logCalMsg(CAL_LOG_MSG, 2, "10V Range 0V O/P: %0.6f mV", V);
    if ((V < -10.0) || (V > 13.01))  // range -0.01V - 0.013V
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_088: heater 0 O/P, 10V range\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    return 0;
}

static int doHeaterVoltageReadbackCheck(int dmmSock, int *eSkips)
{
    double Vheat_dmm_40v;
    double Vheat_dmm_0v2;
    uint16_t ADCHeatADC40V;
    uint16_t ADCHeatADC0V2;
    double diff;
    static int adc40VIdeal = 63530;
    static int diff40V = 635; // 1%
    static int adc0VIdeal = 318;
    static int diff0V = 100;

    logCalMsg(CAL_LOG_MSG, 1, "\nHeater Voltage Readback ADC Check:");

    // 100VDC 10NPLC
    if (doDMMSetup(dmmSock, DMM_100VDC_10NPLC) != 0)
        return -1;
    setVoltageRange(40);
    setHDACRegVal(40.0, 40);
    // HEAT_IV_SEL is set by FPGA as needed
    usleep(120000);
    if (getDMMReading(dmmSock, &Vheat_dmm_40v) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, 
                    "%s: Failed to read Heater Voltage(40V)", __func__);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "V heat_dmm_40v : %0.6f V", Vheat_dmm_40v);
    diff = 40 * 0.101 / 100.0;
    if ((Vheat_dmm_40v < (40.0 - diff)) || (Vheat_dmm_40v > (40.0 + diff)))
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_089: Vheat_dmm_40v not within range\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    // TODO: read AD7683 ADC 100 times & take average, store as ADCHeat_dmm_40v
    getHeaterAvgRaw(100, &ADCHeatADC40V);
    logCalMsg(CAL_LOG_MSG, 2, "ADC Heat_ADC_40V: %d", ADCHeatADC40V);
    if ((ADCHeatADC40V < (adc40VIdeal - diff40V)) ||
            (ADCHeatADC40V > (adc40VIdeal + diff40V)))
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_096: ADC heat_adc_40v within range\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }


    setHDACRegVal(0.2, 40);
    usleep(120000);
    // 1V DC 10NPLC
    if (doDMMSetup(dmmSock, DMM_1VDC_10NPLC) != 0)
        return -1;
    if (getDMMReading(dmmSock, &Vheat_dmm_0v2) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, 
                    "%s: Failed to read Heater Voltage(0V)", __func__);
        return -1;
    }
    logCalMsg(CAL_LOG_MSG, 2, "V heat_dmm_0v : %0.6f V", Vheat_dmm_0v2);
    if ((Vheat_dmm_0v2 < (0.2-0.05)) || (Vheat_dmm_0v2 > (0.2+0.05)))
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_090: Vheat_dmm_0v2 not within range\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    getHeaterAvgRaw(100, &ADCHeatADC0V2);
    logCalMsg(CAL_LOG_MSG, 2, "ADC_Heat_ADC_0V2: %d", ADCHeatADC0V2);
    if ((ADCHeatADC0V2 < (adc0VIdeal - diff0V)) ||
            (ADCHeatADC0V2 > (adc0VIdeal + diff0V)))
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_097: ADC heat_adc_0v2 within range\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }

    return 0;
}

static int doHeaterCurrentZeroCheck(int dmmSock, int *eSkips)
{
    float hV, Iheat_0V;
    uint32_t rawV, rawI;

    logCalMsg(CAL_LOG_MSG, 1, "\nHeater Current Zero Check:");

    // HEATE_IV_SEL is set by FPGA
    setHDACRegVal(0.0, 40);
    setVoltageRange(0);
    setIRange(HEAT_IRANGE_2A);
    usleep(100000);

    setCtrlRegBits(1, 0x1, 0x0); // disable bake
    usleep(100000);

    getHeaterValues(&hV, &rawV, &Iheat_0V, &rawI);
    Iheat_0V *= 1000.0; 
    logCalMsg(CAL_LOG_MSG, 2, "I heat_0V : %0.6f mA", Iheat_0V);
    if (Iheat_0V > 50.01) // 50mA
    {
        logCalMsg(CAL_LOG_MSG, 2, "\nCAL_ERR_098: I heat_0V not in range");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    return 0;
}

static int doBakeOutRelayCheck(int dmmSock, int *eSkips)
{
    double RVal;

    logCalMsg(CAL_LOG_MSG, 1, "\nHeater Bake-out Relay Check:");

    // use the settings from previous tests..
    if (doDMMSetup(dmmSock, DMM_100MOHM_1NPLC) != 0)
        return -1;

    if (getDMMReading(dmmSock, &RVal) != 0)
    {
        logCalMsg(CAL_LOG_ERR, 2, 
                    "%s: Failed to read Resistance", __func__);
        return -1;
    }
    //logCalMsg(CAL_LOG_MSG, 2, "Resistance: %0.6f", RVal);
    if (RVal < 1e8)  // 100MOhm
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_093: Bake-out Relay Check failed.\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    else
        logCalMsg(CAL_LOG_MSG,2, "R > 100M Ohms(PASS)");
    return 0;
}

int doDummyLoadHeatSinkTempCheck(int dmmSock, int *eSkips) 
{
    int gain = 2;
    float TheatSink;

    logCalMsg(CAL_LOG_MSG, 1, "\nDummy Load Heat Sink Temperature Check:");

    setCtrlRegBits(0, 0x400, 0x400); // turn on CJC power
    usleep(500000);
    if (readCJCTemp(&TheatSink) != 0)
        return -1;
    logCalMsg(CAL_LOG_MSG, 2, "T dummy_load: %0.3f C", TheatSink);
    if (TheatSink > 65.00) // 65 degC
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_008: Make sure that fan is running on dummy load.\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    return 0;
}

static int doHeaterOPVoltageCheck(int dmmSock, int *eSkips)
{
    double Vheat_15V_readback;
//float hV, hI;
//uint32_t rawV, rawI;

    logCalMsg(CAL_LOG_MSG, 1, "\nHeater Output Voltage Check:");

    setVoltageRange(20);
    setHDACRegVal(15.0, 20);
    usleep(120000);
    getHeaterAvg(100, &Vheat_15V_readback);
//getHeaterValues(&hV, &rawV, &hI, &rawI);
    logCalMsg(CAL_LOG_MSG, 2, "V heat_15V_readback: %0.6f", Vheat_15V_readback);
//logCalMsg(CAL_LOG_MSG, 2, "V %0.6f I %0.6f", hV, hI);
    if ((Vheat_15V_readback < 14.7990) || (Vheat_15V_readback > 15.201))
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_099: Heater O/P voltage check failed");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    return 0;
}

static int doHeaterCurrentAccuracyCheck(int dmmSock, int *eSkips)
{
    float hV, Idut_bake_15V;
    uint32_t rawV, rawI;
    double diff = 1.90 * 7.01 / 100.0;

    logCalMsg(CAL_LOG_MSG, 1, "\nHeater Current Accuracy Check:");

    setHDACRegVal(15.0, 20);
    usleep(120000);
    getHeaterValues(&hV, &rawV, &Idut_bake_15V, &rawI);
    logCalMsg(CAL_LOG_MSG, 2, "I dut_bake_15V: %0.6f A", Idut_bake_15V);
    if (fabs(1.90 - Idut_bake_15V) > diff)
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_092: Heater Current Accuracy Check failed");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    return 0;
}

static int doHeaterCurrentLimitCheck(int dmmSock, int *eSkips)
{
    float dacVal = 15.1; // start with 15.1V, it is already 15V
    uint16_t statReg;
    int errState = 0;
    float curr, V;
    uint32_t rawCurr, rawV;
    logCalMsg(CAL_LOG_MSG, 1, "\nHeater Current Limit Check:");

    // check HEAT_ILIMIT# flag
    readStatRegFull(0, 1, &statReg);
    if (statReg & 0x40) // #ilimit asserted, inverse of h/w flag
    {
        logCalMsg(CAL_LOG_MSG, 2, 
                "\nCAL_ERR_076: Heater Current limit check failed.\n");
        return -1;
    }

    for (; ;)
    {
        setHDACRegVal(dacVal, 20);
        usleep(120000);
        getHeaterValues(&V, &rawV, &curr, &rawCurr);
        readStatRegFull(0, 1, &statReg);
        if ((statReg & 0x40) != 0) // #ilimit asserted
            break;
        usleep(120000);
        dacVal += 0.1;
        if (dacVal > 20.00) // Voltage limit set to 20 in prev. func
        {
            errState = 1;
            break;
        }
    }
    logCalMsg(CAL_LOG_MSG, 2, "Heater Current: %0.5f A @ %0.5f V(dac: %02f) R:%X", 
                curr, V, dacVal, statReg);
    if (errState != 0) // #ilimit not asserted
    {
        logCalMsg(CAL_LOG_MSG, 2,
          "\nCAL_ERR_094: Error: HEAT_ILIMIT# did not go low!\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    setHDACRegVal(20.0, 20);
    usleep(20000);
    getHeaterValues(&V, &rawV, &curr, &rawCurr);
    logCalMsg(CAL_LOG_MSG, 2, "V heat_limit_7R5_load: %0.6f V", V);
    setHDACRegVal(0.0, 20);
    if (V > 20.01)
    {
        logCalMsg(CAL_LOG_MSG, 2,
          "\nCAL_ERR_077: Error: Voltage above 20.0 V\n");
        if (! (*eSkips))
            return -1;
        (*eSkips)--;
    }
    return 0;
}

static int doHeaterLoadTest(int dmmSock, int *eSkips)
{
    float TpaTemp, TheatSink;
    time_t tStart, tEnd, tNow;
#define TSECS (5*60) // number of seconds, test duration
    float V, I;
    uint32_t rawI, rawV;
//uint16_t statReg;

    logCalMsg(CAL_LOG_MSG, 1, "\nHeater Load Test:");
//{logCalMsg(CAL_LOG_MSG, 2, "SKIPPED"); return 0;}
    if (readPCBTemp(&TpaTemp) != 0)
        return -1;
    logCalMsg(CAL_LOG_MSG, 2, "T PA_Start : %0.4f C", TpaTemp);
    if (readCJCTemp(&TheatSink) != 0)
        return -1;
    logCalMsg(CAL_LOG_MSG, 2, "T heat Sink : %0.4f C", TheatSink);

    setVoltageRange(40);
    setHDACRegVal(15.0, 40);

    setCtrlRegBits(0, 0x400, 0x400); // turn on CJC power
    usleep(120000);

//readStatRegFull(0, 1, &statReg);
//logPrint("StatReg: 0x%X\n",statReg);

    tStart = tNow = time(0);
    tEnd = tStart + TSECS;
    logPrint("==\n");
    logPrint("==    Waiting 5 minutes..\n");
    logPrint("==    The FAN speed should increase during this time.\n");
    logPrint("==\n");
    while (tNow < tEnd)
    {
        // monitor PA temperature and dummy load heatsink temperature.
        if (readPCBTemp(&TpaTemp) != 0)
            break;
        if (TpaTemp > 70.01)
        {
            logCalMsg(CAL_LOG_MSG, 2,
                "\nCAL_ERR_100: Error: PA Temperature too high(%0.3f C)\n", 
                                TpaTemp);
            return -1;
        }
        if (readCJCTemp(&TheatSink) != 0)
            break;
        if (TheatSink > 85.01)
        {
            logCalMsg(CAL_LOG_MSG, 2,
             "\nCAL_ERR_XXX: Error: Heat Sink Temperature too high(%0.3f C)\n", 
                                TheatSink);
            return -1;
        }
        logPrint("\r   [ %d S ] T dummy load : %0.3f C, T pa : %0.3f C", 
                                     (tNow-tStart), TheatSink, TpaTemp);
        sleep(1);
        tNow = time(0);
    }
    // log voltage and current for debug..
    getHeaterValues(&V, &rawV, &I, &rawI);
    logCalMsg(CAL_LOG_MSG, 2, "\nV: %0.6f V I: %0.6f A", V, I);

    setHDACRegVal(0.0, 40);
    if (readPCBTemp(&TpaTemp) != 0)
        return -1;
    else
        logCalMsg(CAL_LOG_MSG, 2, "T PA_End : %0.4f C", TpaTemp);
    if (readCJCTemp(&TheatSink) != 0)
        return -1;
    else
        logCalMsg(CAL_LOG_MSG, 2, "T heatSink : %0.4f C", TheatSink);
    return 0;
}

static int doDongleSevenChecks(int dmmSock, int errSkips)
{
    int dongleId = 7;

    logCalMsg(CAL_LOG_MSG, 0, "---- Dongle 7 Checks:");
    if (tryRegLock(5) < 0)
    {
        logCalMsg(CAL_LOG_ERR, 1, "dongle7Check: unable to get reg. lock");
        closeDMM(dmmSock);
        return -1;
    }

    if (doPreChecks(dmmSock, dongleId, 0) != 0)
        goto errDongle7;
 
    if (doHeaterOPDisableTest(dmmSock, &errSkips) != 0)
        goto errDongle7;
    if (doHeaterMaxVRangeCheck(dmmSock, &errSkips) != 0)
        goto errDongle7;
    if (doHeaterMinVRangeCheck(dmmSock, &errSkips) != 0)
        goto errDongle7;
    if (do44VRailHeaderoomCheck(dmmSock, &errSkips) != 0)
        goto errDongle7;
    if (doHeaterVoltAtHighDAC(dmmSock, &errSkips) != 0)
        goto errDongle7;
    if (doHeaterVoltAtLowDAC(dmmSock, &errSkips) != 0)
        goto errDongle7;
    if (doHeaterVoltageCalibration(dmmSock, &errSkips) != 0)
        goto errDongle7;
    if (doHeaterVoltageCheckUnloaded(dmmSock, &errSkips) != 0)
        goto errDongle7;
    if (doHeaterVoltageReadbackCheck(dmmSock, &errSkips) != 0)
        goto errDongle7;
    if (doHeaterCurrentZeroCheck(dmmSock, &errSkips) != 0)
        goto errDongle7;
    if (doBakeOutRelayCheck(dmmSock, &errSkips) != 0)
        goto errDongle7;
    if (doDummyLoadHeatSinkTempCheck(dmmSock, &errSkips) != 0)
        goto errDongle7;
    if (doHeaterOPVoltageCheck(dmmSock, &errSkips) != 0)
        goto errDongle7;
    if (doHeaterCurrentAccuracyCheck(dmmSock, &errSkips) != 0)
        goto errDongle7;
    if (doHeaterCurrentLimitCheck(dmmSock, &errSkips) != 0)
        goto errDongle7;
    if (doHeaterLoadTest(dmmSock, &errSkips) != 0)
        goto errDongle7;

    doEndSettings();
    unlockCtrlReg();
    checkDMMErrors(dmmSock);
    logCalMsg(CAL_LOG_MSG, 0, "---- END Dongle 7 Checks:");
    return 0;
errDongle7:
    doEndSettings();
    unlockCtrlReg();
    checkDMMErrors(dmmSock);
    logCalMsg(CAL_LOG_MSG, 0, "---- END Dongle 7 Checks:");
    return -1;
}

jmp_buf jEnv;
void intrHandler(int dummy)
{
    longjmp(jEnv, 1);
}

int doCalFunc(int dongleId, char *IPStr, int errSkips, int pause)
{
    int ret, i;
    int dmmSock = openDMM(IPStr);
    if (dmmSock < 0)
        return -1;

    signal(SIGTSTP, SIG_IGN);
    signal(SIGINT, intrHandler);

    // setup jump buffer for error handling.
    if (setjmp(jEnv) != 0)
    {
        printf("\nInterrupt, aborting..\n");
        doEndSettings();
        unlockCtrlReg();
        closeDMM(dmmSock);
        if (logFP)
            fclose(logFP);
        return -1;
    }

    logFP = fopen(tmpFile, "w");
    if (logFP == 0)
        logCalMsg(CAL_LOG_MSG, 2, "\nERROR: Failed to open log file\n");

    pauseOnError = ((pause) ? 1 : 0);

    // init section string for storing config
    sprintf(sectStr, "%s", CAL_SECTION);
    switch (dongleId)
    {
        case 1:
            ret = doDongleOneChecks(dmmSock, errSkips);
            break;
        case 2:
            ret = doDongleTwoChecks(dmmSock, errSkips);
            break;
        case 3:
            ret = doDongleThreeChecks(dmmSock, errSkips);
            break;
        case 4:
            ret = doDongleFourChecks(dmmSock, errSkips);
            break;
        case 5:
            ret = doDongleFiveChecks(dmmSock, errSkips);
            break;
        case 6:
            ret = doDongleSixChecks(dmmSock, errSkips);
            break;
        case 7:
            ret = doDongleSevenChecks(dmmSock, errSkips);
            break;
        default:
            ret = -1;
            logCalMsg(CAL_LOG_ERR, 0, "Un-supported dongle");
            break;
    }
    closeDMM(dmmSock);
    if (errIdx > 0) // print summary error messages
    {
        printf("\nErrors:\n");
        if (logFP)
            fprintf(logFP, "\nErrors:\n");
        for (i = 0; i < errIdx; i++)
        {
            printf("   %s\n", calErrors[i]);
            if (logFP)
                fprintf(logFP, "   %s\n", calErrors[i]);
        }
        printf("\n");
        if (logFP)
            fprintf(logFP, "\n");
    }
    if (logFP)
    {
        fclose(logFP);
        // now rename file to correct name
        rename(tmpFile, logFile);
    }
    return ret;
}

//#define UNIT_TEST
volatile sysParams *pCache;
#ifdef UNIT_TEST
int main()
{
    if (initLib() != 0)
    {
        printf("Error: failed to init library!!\n");
        exit(1);
    }
    if ((pCache = getSharedMemory(IPC_KEY_SHAREDMEM, sizeof(sysParams))) == 0)
    {
        fprintf(stderr, "Error attaching shared memory\n");
        exit(1);
    }
    //doCalFunc(1, "10.13.7.121");
    doCalFunc(2, "10.13.7.220");
    return 0;
}
#endif
