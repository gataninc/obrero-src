#! /bin/sh

# script to do first time setup of Obrero system 


# set env. variables and export them to be used in expect scripts
# testing
#obreroip=10.13.8.119
#serverip=10.13.8.87
#netmask=255.255.0.0

# Actual, pvt network
obreroip=192.168.111.61
serverip=192.168.111.100
netmask=255.255.255.0

mfgImg=ObreroMfg.img
guiImg=obrero_ui.img.gz

export obreroip serverip netmask mfgImg guiImg

# make sure all needed scripts are present.
script_os="./loados.exp"
script_gui="./loadgui.exp"

if [ ! -x $script_os -o ! -x $script_gui ]; then
    echo "Error: Scripts not found!"
    exit 1
fi

# first setup boot env. and load Linux kernel to the system.

./$script_os

if [ $? -ne 0 ]; then
    echo "Error loading OS, aborting"
    exit 1
fi

# Wait for the Obrero to bootup, approx. 1 minute
echo; echo "Waiting for sytem to bootup(may take upto a minute).."
sleep 60
ping -c 1 -q $obreroip >/dev/null 2>&1
if [ $? -ne 0 ]; then
    echo; echo "Waiting for sytem to bootup..."; echo
    sleep 10
    ping -c 1 -q $obreroip >/dev/null 2>&1
    if [ $? -ne 0 ]; then
       echo; 
       echo -n "Error: Unit failed to bootup, try running setup again "
       echo "after power cycling the unit"
       echo
       exit 1
    fi
fi

echo; echo "System alive, loading GUI image"; echo

./$script_gui

if [ $? -ne 0 ]; then
    echo "Error loading GUI, aborting"
    exit 1
fi

echo; echo "Setting MAC address"; echo

FNEXTMAC=./nextmac
FNEXTMAC2=./nextmac.bak
# make sure files exist and is size > 0
if [ ! -s ${FNEXTMAC} -o ! -s ${FNEXTMAC2} ]; then
    echo; 
    echo "MAC Files do not exist. Cannot set MAC address. Set it manually."; 
    echo
else
    diff ${FNEXTMAC} ${FNEXTMAC2} >/dev/null 2>&1
    if [ $? -ne 0 ]; then
        echo
        echo "-----------------------------------"; 
        echo "Last MAC files differ. Cannot set MAC address. Set it manually."; 
	    echo -n "MAC1: "; cat ${FNEXTMAC};
	    echo -n "MAC2: "; cat ${FNEXTMAC2};
        echo "-----------------------------------"; 
        echo
    else
        nextMAC=$(cat ${FNEXTMAC})
	    newMAC=$(./obrero_next_mac $nextMAC)
        devMAC=$(./setMAC.exp $nextMAC)
	    if [ $? -ne 0 ]; then
            echo
            echo "Error: Failed to set MAC address"
            echo "Please set MAC address manually to $nextMAC"
            echo
        fi
# verify that MAC address was updated
	    if [ "$devMAC" = "$nextMAC" ]; then
            echo "MAC address updated OK"
	    else
            echo
            echo "Error: Failed to set MAC address [ $nextMAC : $devMAC ]"
            echo "Please set MAC address manually to $nextMAC"
            echo
	    fi
# update next MAC files
	    echo "$newMAC" > ${FNEXTMAC}
	    echo "$newMAC" > ${FNEXTMAC2}
    fi
fi

echo
echo "All setup DONE, power cycle the unit to verify setup."; echo
echo "Please record the following MAC address in the log book:"
echo "    MAC address of unit: $nextMAC"
echo
