#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <libgen.h>
#include <time.h>
#include <errno.h>
#include "bing_common.h"
#include "upgrade_hdr.h"
#include "adler32.h"

static uLong computeCRC(uint8_t *buffer, uint size)
{
    uLong crc32;
    /* init csum field */
    crc32 = adler32(0L, Z_NULL, 0);
    // now compute buffer csum
    crc32 = adler32(crc32, buffer, size);
    return crc32;
}


static char *bldTypeStr[]={ "Unknown", "Alpha", "Beta", "Release", "Invalid" };
int main(int argc, char *argv[])
{
    int fd, err;
    UpgradeImgHdr *imgHdr;
    uint8_t *imgBuf, *tmpBuf;
    struct stat stBuf;
    uLong crc32;
    if (argc != 2)
    {
        fprintf(stderr, "%s <upgrade image>\n", argv[0]);
        exit(1);
    }
    if ((fd = open(argv[1], O_RDONLY)) < 0)
    {
        err = errno;
        fprintf(stderr, "open: %s\n", strerror(err));
        exit(1);
    }
    if (fstat(fd, &stBuf) != 0)
    {
        err = errno;
        fprintf(stderr, "stat: %s\n", strerror(err));
        close(fd);
        exit(1);
    }
    if ((imgBuf = (uint8_t *) malloc(stBuf.st_size)) == 0)
    {
        close(fd);
        fprintf(stderr, "Error: memory allocation failure\n");
        exit(1);
    }
    if (read(fd, imgBuf, stBuf.st_size) != stBuf.st_size)
    {
        err = errno;
        close(fd);
        fprintf(stderr, "read: %s\n", strerror(err));
        exit(1);
    }
    close(fd);
    imgHdr = (UpgradeImgHdr *)imgBuf;
    if (memcmp(imgHdr->magicStr, MAGIC_STR_SS2, MAGIC_LEN) != 0)
    {
        free(imgBuf);
        fprintf(stderr, "Error: input file is not a SmartSet2 upgrade image\n");
        exit(1);
    }
    tmpBuf = (imgBuf + sizeof(uLong)); // skip csum bytes
    crc32 = computeCRC(tmpBuf, (stBuf.st_size-sizeof(uLong)));
    if (crc32 != imgHdr->ckSum)
    {
        free(imgBuf);
        fprintf(stderr, "Error: checksum mis-match(hdr: %x, computed: %x\n", 
                                imgHdr->ckSum, crc32);
        exit(1);
    }
    // OK, the file seems to be valid.. print the header bytes
    printf("Header Info:\n");
    printf("  check sum : %x\n", imgHdr->ckSum);
    printf("  systemId  : ");
    if ((imgHdr->sysId & IMAGE_SMARTSET2_TYPE) == IMAGE_SMARTSET2_TYPE)
        printf("[SmartSet2] ");
    if ((imgHdr->sysId & 0xff) == IMAGE_SYS_TYPE)
        printf("[Firmware upgrade] "); 
    if ((imgHdr->sysId & 0xff) == IMAGE_APP_TYPE)
        printf("[GUI upgrade] "); 
    printf("%x\n", imgHdr->sysId);
    printf("  build ver : %u.%u.%u.%u  build Type: %s [%u]\n", 
                imgHdr->majorNo, imgHdr->minorNo, imgHdr->patchLevel,
                imgHdr->buildNo, bldTypeStr[imgHdr->bldType], imgHdr->bldType);
    printf("  build date: %02d / %02d / %04d, %02d:%02d:%02d\n",
                      (imgHdr->bldDate.month + 1), imgHdr->bldDate.day, 
                      imgHdr->bldDate.year, imgHdr->bldDate.hour, 
                      imgHdr->bldDate.min, imgHdr->bldDate.sec);
    free(imgBuf);
    exit(0);
}
