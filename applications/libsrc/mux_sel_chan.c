#include <stdio.h>
#include <stdlib.h>

extern int i2cmux_select_chan(int chan_no);
int main(int numArgs, char *args[])
{
    int ret = 0;
    int chan;
    if (numArgs != 2)
    {
        fprintf(stderr, "Usage: %s chan\n", args[0]);
        exit(1);
    }
    chan    = atoi(args[1]);
    if ((chan  <= 0) || (chan > 8))
    {
        fprintf(stderr, "Invalid  channel number, %d\n", chan);
        exit(1);
    }
    if ((ret = i2cmux_select_chan(chan)) != 0)
        fprintf(stderr, "Error selecting channel %d\n", ret);
    exit(0);
}
