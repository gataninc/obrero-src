/*
   Refer to the document:
   http://www.mosaic-industries.com/embedded-systems/microcontroller-projects/temperature-measurement/thermocouple/calibration-table
   for more details on the implementation.
 */
#include <stdio.h>
#include <math.h>
#include "../include/tc_calibrate.h"

// Routines for converting voltage to temperature for 
// Type K & T thermo-couples

typedef struct 
{
    double T0;
    double V0;
    double P1;
    double P2;
    double P3;
    double P4;
    double Q1;
    double Q2;
    double Q3;
} CalCoeff;

#define NUM_RANGE_K_TYPE (5)
#define NUM_RANGE_T_TYPE (4)

// Type K Coefficients and ranges..
double typeK_Range_Vals[NUM_RANGE_K_TYPE*2] = 
{
    -6.404,  -3.554, // mV, -250 - -100 degC
    -3.554,   4.096, // mV, -100 - 100 degC
     4.096,  16.397, // mV, 100  - 400 degC
    16.397,  33.275, // mV, 400  - 800 degC
    33.275,  69.553, // mV, 800  - 1200 degC
};

CalCoeff typeKCal[NUM_RANGE_K_TYPE] = 
{
    { // -6.404 to  -3.554mV(-250 - -100 degC)
        ((double)-1.2147164E2), // T0
        ((double)-4.1790858E0), // V0
        ((double)3.6069513E1),  // P1
        ((double)3.0722076E1),  // P2
        ((double)7.7913860E0),  // P3
        ((double)5.2593991E-1), // P4
        ((double)9.3939547E-1), // Q1
        ((double)2.7791285E-1), // Q2
        ((double)2.5163349E-2), // Q3
    },
    { // -3.554 to 4.096mV(-100 - 100 degC)
        ((double)-8.7935962E0),   // T0
        ((double)-3.4489914E-1),  // V0
        ((double)2.5678719E+1),   // P1
        ((double)-4.9887904E-1),  // P2
        ((double)-4.4705222E-1),  // P3
        ((double)-4.4869203E-2),  // P4
        ((double)2.3893439E-4),   // Q1
        ((double)-2.0397750E-2),  // Q2
        ((double)-1.8424107E-3),  // Q3
    },
    { // 4.096 to  16.397mV(100  - 400 degC)
        ((double)3.1018976E2),    // T0
        ((double)1.2631386E1),    // V0
        ((double)2.4061949E1),    // P1
        ((double)4.0158622E0),    // P2
        ((double)2.6853917E-1),   // P3
        ((double)-9.7188544E-3),  // P4
        ((double)1.6995872E-1),   // Q1
        ((double)1.1413069E-2),   // Q2
        ((double)-3.9275155E-4),  // Q3
    },
    { // 16.397 to 33.275mV(400  - 800 degC)
        ((double)6.0572562E2),   // T0
        ((double)2.5148718E1),   // V0
        ((double)2.3539401E1),   // P1
        ((double)4.6547228E-2),  // P2
        ((double)1.3444400E-2),  // P3
        ((double)5.9236853E-4),  // P4
        ((double)8.3445513E-4),  // Q1
        ((double)4.6121445E-4),  // Q2
        ((double)2.5488122E-5),  // Q3
    },
    { // 33.275 to 69.553mV(800  - 1200 degC)
        ((double)1.0184705E3),   // T0
        ((double)4.1993851E1),   // V0
        ((double)2.5783239E1),   // P1
        ((double)-1.8363403E0),  // P2
        ((double)5.6176662E-2),  // P3
        ((double)1.8532400E-4),  // P4
        ((double)-7.4803355E-2), // Q1
        ((double)2.3841860E-3),  // Q2
        ((double)0.0),           // Q3
    },
};

// Type T Coefficients and ranges..

double typeT_Range_Vals[NUM_RANGE_T_TYPE*2] = 
{
     -6.18,  -4.648,    // mV, -250 to -150 degC
    -4.648,     0.0,    // mV, -150 to 0    degC
       0.0,   9.288,    // mV, 0    to 200  degC
     9.288,  20.872,    // mV, 200  to 400  degC
};

CalCoeff typeTCal[NUM_RANGE_T_TYPE] = 
{
    { // -6.18 to -4.648mV(-250 to -150 degC)
        ((double)-1.9243000E2),  // T0
        ((double)-5.4798963E0),  // V0
        ((double)5.9572141E1),   // P1
        ((double)1.9675733E0),   // P2
        ((double)-7.8176011E1),  // P3
        ((double)-1.0963280E1),  // P4
        ((double)2.7498092E-1),  // Q1
        ((double)-1.3768944E0),  // Q2
        ((double)-4.5209805E-1), // Q3
    },
    { // -4.648 to 0.0mV(-150 to 0 degC)
        ((double)-6.0000000E1),  // T0
        ((double)-2.1528350E0),  // V0
        ((double)3.0449332E1),   // P1
        ((double)-1.2946560E0),  // P2
        ((double)-3.0500735E0),  // P3
        ((double)-1.9226856E-1), // P4
        ((double)6.9877863E-3),  // Q1
        ((double)-1.0596207E-1), // Q2
        ((double)-1.0774995E-2), // Q3
    },
    { // 0.0 to 9.288mV(0  to 200 degC)
        ((double)1.3500000E2),   // T0
        ((double)5.9588600E0),   // V0
        ((double)2.0325591E1),   // P1
        ((double)3.3013079E0),   // P2
        ((double)1.2638462E-1),  // P3
        ((double)-8.2883695E-4), // P4
        ((double)1.7595577E-1),  // Q1
        ((double)7.9740521E-3 ), // Q2
        ((double)0.0),           // Q3
    },
    { // 9.288 to 20.872mV(200  to 400 degC)
        ((double)3.0000000E2),    // T0
        ((double)1.4861780E1),    // V0
        ((double)1.7214707E1),    // P1
        ((double)-9.3862713E-1),  // P2
        ((double)-7.3509066E-2),  // P3
        ((double)2.9576140E-4),   // P4
        ((double)-4.8095795E-2),  // Q1
        ((double)-4.7352054E-3),  // Q2
        ((double)0.0),            // Q3
    },
};

/*
numerator:
(v - v0)*(p1 + (v - v0)*(p2 + (v - v0)*(p3 + p4 *(v - v0))))

x4 = p4*(v - v0)

x3 = (p3 + x4) * (v - v0)

x2 = (p2 + x3) * (v - v0)

x1 = (p1 + x2) * (v - v0)

denominator:
1 + (v - v0) (q1 + (v - v0)(q2 + (q3 (v - v0))))

y3 = q3 * (v - v0)
y2 = (q2 + y3) * (v - v0)
y1 = (q1 + y2) * (v - v0)
*/

// voltage in V
double tcCalTemp(int type, double V)
{
    double t = 0.0;  // return 0 deg K on error
    double diff;
    double x4;
    double x3;
    double x2;
    double x1;
    double y3;
    double y2;
    double y1;
    int numRange = 0;
    CalCoeff *CPtr = 0;
    double *rangeVal;
    int i;
    double mV = V * 1000.0; // convert to milli-volts
    if (type == CAL_TYPE_K)
    {
        numRange = NUM_RANGE_K_TYPE;
        CPtr = &(typeKCal[0]);
        rangeVal = &(typeK_Range_Vals[0]);
    }
    else if (type == CAL_TYPE_T)
    {
        numRange = NUM_RANGE_T_TYPE;
        CPtr = &(typeTCal[0]);
        rangeVal = &(typeT_Range_Vals[0]);
    }
    else
        return (double)0.0; // 0 deg K
    //printf("%s: type %d mV %0.5f\n", __func__, type, mV);
    for (i = 0; i < numRange; i++)
    {
        if ((mV >= rangeVal[i*2]) && (mV < rangeVal[(i*2)+1]))
        {
            diff = mV - CPtr[i].V0;
            x4 = CPtr[i].P4 * diff;
            x3 = (CPtr[i].P3 + x4) * diff;
            x2 = (CPtr[i].P2 + x3) * diff;
            x1 = (CPtr[i].P1 + x2) * diff;

            y3 = CPtr[i].Q3 * diff;
            y2 = (CPtr[i].Q2 + y3) * diff;
            y1 = (CPtr[i].Q1 + y2) * diff;
            t = CPtr[i].T0 + (x1/(1+y1));
            t += (double)273.15; // convert to Kelvin
            break;
        }
    }
    return t;
}
// END Routine for Type K&T thermo-couples

// Routine for converting temperature(from cold-junction) to a voltage
// corresponding to the Type K & T thermo-couples.

double cjc4TypeKRange[] = { -20.0, 70.0 };

CalCoeff cjc4TypeK = 
{
    // -20 to 70 deg C (-0.778 to 2.851mV)
    ((double)2.5000000E1),    // T0
    ((double)1.0003453E0),    // V0
    ((double)4.0514854E-2),   // P1
    ((double)-3.8789638E-5),  // P2
    ((double)-2.8608478E-6),  // P3
    ((double)-9.5367041E-10), // P4
    ((double)-1.3948675E-3),  // Q1
    ((double)-6.7976627E-5),  // Q2
    ((double)0.0), // NO Q3.
};

double cjc4TypeTRange[] = { -20.0, 70.0 };

CalCoeff cjc4TypeT = 
{
    // -20 to 70 deg C (-0.757 to 2.909mV)
    ((double)2.5000000E1),   // T0
    ((double)9.9198279E-1),  // V0
    ((double)4.0716564E-2),  // P1
    ((double)7.1170297E-4),  // P2
    ((double)6.8782631E-7),  // P3
    ((double)4.3295061E-11), // P4
    ((double)1.6458102E-2),  // Q1
    ((double)0.0000000E+0),  // Q2
    ((double)0.0), // NO Q3.
};

// CJC Temperature is in Celsius..
double cjcTemp2tcVolt(int type, double cjcT)
{
    double cjcV = 0.0;
    double diff;
    double x4, x3, x2, x1;
    double y2, y1;
    CalCoeff *CPtr;
    double *range;

    if (type == CAL_TYPE_K)
    {
        CPtr = &cjc4TypeK;
        range = &cjc4TypeKRange[0];
    }
    else if (type == CAL_TYPE_T)
    {
        CPtr = &cjc4TypeT;
        range = &cjc4TypeTRange[0];
    }
    else
        return 0.0;

    if ((cjcT < range[0]) || (cjcT > range[1]))
        return 0.0;

    diff = cjcT - CPtr->T0;
    x4 = CPtr->P4 * diff;
    x3 = (CPtr->P3 + x4) * diff;
    x2 = (CPtr->P2 + x3) * diff;
    x1 = (CPtr->P1 + x2) * diff;

    y2 = CPtr->Q2 * diff;
    y1 = (CPtr->Q1 + y2) * diff;

    cjcV = CPtr->V0 + (x1 / (1 + y1));
    return cjcV;
}

//#define UNIT_TEST
//#define SANITY_CHECK
#define CAL_TEST
//#define CJC_TEST

#ifdef UNIT_TEST
int main()
{
#ifdef CAL_TEST
    double t;
    float mV;
    int type = CAL_TYPE_K;
    int i;
    int numRange = 0;
    CalCoeff *CPtr = 0;
    double *rangeVal;
    if (type == CAL_TYPE_K)
    {
        numRange = NUM_RANGE_K_TYPE;
        CPtr = &(typeKCal[0]);
        rangeVal = &(typeK_Range_Vals[0]);
    }
    else if (type == CAL_TYPE_T)
    {
        numRange = NUM_RANGE_T_TYPE;
        CPtr = &(typeTCal[0]);
        rangeVal = &(typeT_Range_Vals[0]);
    }
#ifdef SANITY_CHECK
    for (i = 0; i < numRange; i++)
    {
        mV = rangeVal[i*2];
        t = tcCalTemp(type, mV/1000.0);
        printf("T: %0.4f   %0.5f\n", rangeVal[i*2], t);
        mV = rangeVal[(i*2)+1];
        if  (mV >= 69.5530)
            mV = 69.550;
        t = tcCalTemp(type, mV/1000.0);
        printf("T: %0.4f   %0.5f\n", rangeVal[(i*2)+1], t);
    }
#else
    for (i = 0; i < numRange; i++)
    {
        printf("RANGE: %0.4f - %0.4f\n", rangeVal[i*2], rangeVal[(i*2)+1]);
        for (mV = rangeVal[i*2]; mV < rangeVal[(i*2)+1]; mV += 0.1)
        {
            t = tcCalTemp(type, (double)(mV/1000.0));
            printf("  %0.4f   %0.5f\n", mV, t);
        }
    }
#endif
#endif //CAL_TEST
#ifdef CJC_TEST
    {
    double v;
    float t;
    int type = CAL_TYPE_T;
    printf("\nCJC Voltages:\n");
    for (t = -20.0; t < 70.0; t += 0.01)
    {
        v = cjcTemp2tcVolt(type, t);
        printf("%0.4fC %0.5fmV\n", t, v);
    }
    }
#endif
    return 0;
}
#endif
