#ifndef __ADLER32__
#define __ADLER32__

#ifndef _ULONG_DEFINED_
typedef unsigned int  uInt;
typedef unsigned long uLong;
#endif
#define Z_NULL   0
typedef long z_off64_t;
typedef long z_off_t;
typedef unsigned char Bytef;

extern uLong adler32(uLong adler, const Bytef *buf, uInt len);
extern uLong adler32_combine(uLong adler1, uLong adler2, z_off_t len2);
extern uLong adler32_combine64(uLong adler1, uLong adler2, z_off64_t len2);
#endif
