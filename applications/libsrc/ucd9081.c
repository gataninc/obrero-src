/*************************************************
 *
 * UCD9081 functions
 * Author: Roice Joseph, based on Ciro's i2c-lib
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <i2c-dev.h>
#include "i2c-dev.h"
#include "utils.h"
#include "i2cutils.h"

/* register map */
#define VERSION_REGISTER   0x27
#define STATUS_REGISTER    0x26
#define RAILSTATUS1_REG    0x28
#define RAILSTATUS2_REG    0x29
#define FLASHLOCK_REG      0x2E
#define WADDR1_REG         0x30
#define WADDR2_REG         0x31
#define WDATA1_REG         0x32
#define WDATA2_REG         0x33
#define RAIL1_HIGH_REG     0x00 /* use auto-increment to read all eight high/low(16regs) values */
#define ERROR1_REG         0x20 /* use auto-increment to read all six values */
#define RESTART_REG        0x2F

static int read_byte_from_ucd9081(int bus, int devAddr, uint8_t *byte, int reg)
{
    int hI2C;
    __s32 val;
    /* Open the I2C device */
    if ((hI2C = open(get_i2c_bus(bus), O_RDWR)) < 0)
        return -1;

    /* Set slave address */
    if (ioctl(hI2C, I2C_SLAVE, devAddr) < 0)
    {
        close(hI2C);
        return -1;
    }
    /* Do the transaction */
    val = i2c_smbus_read_byte_data(hI2C, reg);
    if(val < 0) 
    {
        close(hI2C);
        return -1;
    }
    *byte = val & 0xFF;
    /* We are done */
    close(hI2C);
    return 0;
}

int read_ucd9081_version(int bus, int devAddr, uint8_t *ver)
{
    return read_byte_from_ucd9081(bus, devAddr, ver, VERSION_REGISTER);
}

int read_ucd9081_status(int bus, int devAddr, uint8_t *status)
{
    return read_byte_from_ucd9081(bus, devAddr, status, STATUS_REGISTER);
}

int read_ucd9081_railstatus(int bus, int devAddr, uint8_t *rail1, uint8_t *rail2)
{
    int hI2C;
    __s32 val;
    /* Open the I2C device */
    if ((hI2C = open(get_i2c_bus(bus), O_RDWR)) < 0)
        return -1;

    /* Set slave address */
    if (ioctl(hI2C, I2C_SLAVE, devAddr) < 0)
    {
        close(hI2C);
        return -1;
    }
    /* Do the transaction */
    val = i2c_smbus_read_byte_data(hI2C, RAILSTATUS1_REG);
    if(val < 0) 
    {
        close(hI2C);
        return -1;
    }
    *rail1 = val & 0xFF;
    /* no need to set the register address again, device auto-increments address */
    val = i2c_smbus_read_byte_data(hI2C, RAILSTATUS2_REG);
    if(val < 0) 
    {
        close(hI2C);
        return -1;
    }
    *rail2 = val & 0xFF;
    /* We are done */
    close(hI2C);
    return 0;
}

/*
 * Read rail voltage registers from UCD9081, compute voltages and return those
 * in the array provided. We always read all 8 rails and so the array must contain
 * space for 16 chars. 
 * Returns 0 on success, -ve values for errors
 */
int read_ucd9081_railvoltages(int bus, int devAddr, uint8_t *voltages)
{
    int hI2C;
    struct i2c_msg message[2];
    struct i2c_rdwr_ioctl_data ctrl;
    uint8_t v_reg[1];

    /* Open the I2C device */
    if ((hI2C = open(get_i2c_bus(bus), O_RDWR)) < 0)
        return -1;
    /* set-up ioctl parameters */
    ctrl.msgs  = message;
    ctrl.nmsgs = 2;

    message[0].addr   = devAddr;
    message[0].flags  = 0;
    message[0].len    = 1;
    message[0].buf    = (char *)&v_reg[0];
    v_reg[0]          = RAIL1_HIGH_REG;

    message[1].addr   = devAddr;
    message[1].flags  = I2C_M_RD;
    message[1].len    = UCD9081_NUM_RAIL_VOLTS * 2;
    message[1].buf    = (char *)&voltages[0];

    if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        close(hI2C);
        return -2;
    }
    close(hI2C);
    return 0;
}

/* 
 * Checks for existence of flash error log in ucd9081. Returns true/false
 */
#define UCD_NVERR_LOG 0x20
int ucd9081_flashlog_exists(int bus, int devAddr)
{
    int hI2C;
    __s32 val;
    uint8_t stat;

    /* Open the I2C device */
    if ((hI2C = open(get_i2c_bus(bus), O_RDWR)) < 0)
        return -1;

    /* Set slave address */
    if (ioctl(hI2C, I2C_SLAVE, devAddr) < 0)
    {
        close(hI2C);
        return -1;
    }
    /* Do the transaction */
    if ((val = i2c_smbus_read_byte_data(hI2C, STATUS_REGISTER)) < 0)
    {
        close(hI2C);
        return -2;
    }
    stat = val & 0xFF;
    return ((stat & UCD_NVERR_LOG) == UCD_NVERR_LOG);
}

/*
 * Read UCD9081 flash logs, if they are present. There are six error 
 * registers and so the err_buf must contain space for at least 6 characters.
 * Returns 1 on valid logs present, 0 for no errors and -ve values for errors
 */
int read_ucd9081_logs(int bus, int devAddr, uint8_t err_buf[])
{
    int hI2C;
    __s32 val;
    uint8_t stat;
    int ret = 0;
    struct i2c_msg message[2];
    struct i2c_rdwr_ioctl_data ctrl;
    uint8_t v_reg[1];

    /* Open the I2C device */
    if ((hI2C = open(get_i2c_bus(bus), O_RDWR)) < 0)
        return -1;

    /* Set slave address */
    if (ioctl(hI2C, I2C_SLAVE, devAddr) < 0)
    {
        close(hI2C);
        return -1;
    }
    /* Do the transaction */
    if ((val = i2c_smbus_read_byte_data(hI2C, STATUS_REGISTER)) < 0)
    {
        close(hI2C);
        return -2;
    }
    stat = val & 0xFF;
    if ((stat & UCD_NVERR_LOG) == UCD_NVERR_LOG)
    {
        /* set-up ioctl parameters */
        ctrl.msgs  = message;
        ctrl.nmsgs = 2;

        message[0].addr   = devAddr;
        message[0].flags  = 0;
        message[0].len    = 1;
        message[0].buf    = (char *)&v_reg[0];
        v_reg[0]          = ERROR1_REG;

        message[1].addr   = devAddr;
        message[1].flags  = I2C_M_RD;
        message[1].len    = 6;
        message[1].buf    = (char *)&err_buf[0];

        if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
        {
            close(hI2C);
            return -3;
        }
        ret = 1; /* error logs present in the buffer */
    }
    close(hI2C);
    return ret;
}

/*
 * Resets the flash error log of UCD9081. If parameter 'restart' is non-zero,
 * restarts the device by writing 0 to the RESTART register. This is needed
 * if the device is configured to hold itself in reset if entries are present
 * in the flash error log.
 * Returns 0 on success, -ve value for errors.
 */
int reset_ucd9081_flashlog(int bus, int devAddr, int restart)
{
    int hI2C;
    struct i2c_msg message;
    struct i2c_rdwr_ioctl_data ctrl;
    uint8_t waddr[3];
    int ret = 0;
    const int addr1 = 0x1000, addr2 = 0x107E;
    const int wrt_const = 0xBADC;
    struct timespec twait;

    /* Open the I2C device */
    if ((hI2C = open(get_i2c_bus(bus), O_RDWR)) < 0)
        return -1;

    twait.tv_sec  = 0;
    twait.tv_nsec = 15000000; /* 15 ms */

    ctrl.msgs  = &message;
    ctrl.nmsgs = 1;

    /* Steps to reset flash log according to UCD9081 */
    /* 1. write 0x02 to FLASHLOCK reg */
    message.addr  = devAddr;
    message.buf   = (char *)waddr;
    message.flags = 0;
    message.len   = 2; /* flash lock register + 0x02 */

    waddr[0]  = FLASHLOCK_REG;
    waddr[1]  = 0x02;

    if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        close(hI2C);
        return -1;
    }
    
    /* 2. write 0x1000 to WADDR reg */
    message.flags = 0;
    message.len   = 3; /* dev. register + two byte address */

    waddr[0]  = WADDR1_REG;
    waddr[1]  = addr1 & 0xFF;
    waddr[2]  = (addr1 >> 8) & 0xFF;

    if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        ret = -2;
        goto lock_flash;
    }

    /* 3. write 0xBADC to WDATA reg */
    message.flags = 0;
    message.len   = 3; /* dev. register + two byte address */

    waddr[0]  = WDATA1_REG;
    waddr[1]  = wrt_const & 0xFF;
    waddr[2]  = (wrt_const >> 8) & 0xFF;

    if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        ret = -3;
        goto lock_flash;
    }
    nanosleep(&twait, NULL); /* device needs 12 ms to erase the flash */

    /* 4. write 0x107E to WADDR reg */
    message.flags = 0;
    message.len   = 3; /* dev. register + two byte address */

    waddr[0]  = WADDR1_REG;
    waddr[1]  = addr2 & 0xFF;
    waddr[2]  = (addr2 >> 8) & 0xFF;

    if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        ret = -4;
        goto lock_flash;
    }

    /* 5. write 0xBADC to WDATA reg */
    message.flags = 0;
    message.len   = 3; /* dev. register + two byte address */

    waddr[0]  = WDATA1_REG;
    waddr[1]  = wrt_const & 0xFF;
    waddr[2]  = (wrt_const >> 8) & 0xFF;

    if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        ret = -5;
        goto lock_flash;
    }
    nanosleep(&twait, NULL); /* wait for flash erase */

lock_flash:
    /* 6. lock flash by writing 0x00 to FLASHLOCK register */
    message.flags = 0;
    message.len   = 2; /* flash lock register + 0x00 */

    waddr[0]  = FLASHLOCK_REG;
    waddr[1]  = 0x00;

    /* do not check return, we are done */
    ioctl(hI2C, I2C_RDWR, &ctrl);

    if ((ret == 0) && (restart))
    {
        /* write 0x00 to RESTART  register */
        message.flags = 0;
        message.len   = 2; 

        waddr[0]  = RESTART_REG;
        waddr[1]  = 0x00;

        if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
            ret = -6;
    }
    close(hI2C);
    return ret;
}

int ucd9081_restart(int bus, int devAddr)
{
    int hI2C;
    struct i2c_msg message;
    struct i2c_rdwr_ioctl_data ctrl;
    uint8_t waddr[2];
    int ret = 0;

    /* Open the I2C device */
    if ((hI2C = open(get_i2c_bus(bus), O_RDWR)) < 0)
        return -1;

    ctrl.msgs     = &message;
    ctrl.nmsgs    = 1;

    message.addr  = devAddr;
    message.buf   = (char *)waddr;
    message.flags = 0;
    message.len   = 2; /* register addr. + 0x00(restart code) */

    waddr[0]      = RESTART_REG;
    waddr[1]      = 0x00;
    /* write to device: ioctl return num bytes written, 
       this function returns just success not the number.. */
    if ((ret = ioctl(hI2C, I2C_RDWR, &ctrl)) > 0) 
        ret = 0;

    close(hI2C);
    return ret;
}

int ucd9081_shutdown(int bus, int devAddr)
{
    int hI2C;
    struct i2c_msg message;
    struct i2c_rdwr_ioctl_data ctrl;
    uint8_t waddr[2];
    int ret = 0;
    struct timespec twait;

    /* Open the I2C device */
    if ((hI2C = open(get_i2c_bus(bus), O_RDWR)) < 0)
        return -1;

    ctrl.msgs     = &message;
    ctrl.nmsgs    = 1;

    message.addr  = devAddr;
    message.buf   = (char *)waddr;
    message.flags = 0;
    message.len   = 2; /* register addr. + 0xC0(shutdown code) */

    waddr[0]      = RESTART_REG;
    waddr[1]      = 0xC0;
    /* write to device: ioctl return num bytes written, 
       this function returns just success not the number.. */
    if ((ret = ioctl(hI2C, I2C_RDWR, &ctrl)) > 0) 
    {
        ret = 0;
        twait.tv_sec  = 0;
        twait.tv_nsec = 25000000; /* 15 ms */
    }

    close(hI2C);
    return ret;
}

/* Read config bytes from given location. 
 * len must be multiple of 2, maximum is 32
 */
int read_ucd9081_config(int bus, int devAddr, unsigned address, int len, uint8_t *buffer)
{
    int hI2C;
    /* read needs three i2c messages: 
     * first write to set register address, second write to set the read address register
     * and third(read) to do actual read.
     */
    struct i2c_msg message[2];
    struct i2c_rdwr_ioctl_data ctrl;
    uint8_t waddr[3];

    /* verify that address is valid for device, within user/dev. config area */
    if ((address > 0xE1F0) ||
        ((address < 0xE000) && (address > 0x10F0)) ||
        (address < 0x1080))
        return -1;

    if ((len < 0) || ((len % 2) != 0) || (len > 32))
        return -2;

    /* Open the I2C device */
    if ((hI2C = open(get_i2c_bus(bus), O_RDWR)) < 0)
        return -3;

    ctrl.msgs = message;
    ctrl.nmsgs = 1;

    /* setup i2c message to update WADDR1_REG/WADDR2_REG with the requested address */
    message[0].addr  = devAddr;
    message[0].buf   = (char *)waddr;
    message[0].flags = 0;
    message[0].len   = 3; /* dev. register + two byte address */
    /* use the register auto increment feature to update read address. */
    waddr[0]  = WADDR1_REG;
    waddr[1]  = address & 0xFF;
    waddr[2]  = (address >> 8) & 0xFF;

    if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        close(hI2C);
        return -4;
    }

    /* 
     * now the i2c message to read data from the WADDR1_REG/WADDR2_REG, use auto incr/decr
     * feature to read data continously
     */
    ctrl.nmsgs = 2;
    /* write read register address */
    message[0].flags = 0;
    message[0].len   = 1;
    waddr[0]         = WDATA1_REG;

    /* i2c message for reading config data at requested address */
    message[1].addr  = devAddr;
    message[1].flags = I2C_M_RD;
    message[1].len   = len;
    message[1].buf   = (char *)buffer;

    /* This is the I2C transaction - we do it in one shot */
    if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        close(hI2C);
        return -5;
    }
    close(hI2C);
    return 0;
}

/* 
 * Update user/device configuration on a UCD9081. 
 */
int update_ucd9081_config(int bus, int devAddr, uint8_t *usrCfg, uint8_t *devCfg)
{
    int hI2C;
    struct i2c_msg message;
    struct i2c_rdwr_ioctl_data ctrl;
    uint8_t waddr[33];
    const int block_sz = 32;
    int j, idx, ret = 0;
    int start_addr, cfg_len;

    if ((!usrCfg) || (!devCfg))
        return -1;

    if ((hI2C = open(get_i2c_bus(bus), O_RDWR)) < 0)
        return -2;

    ctrl.msgs = &message;
    ctrl.nmsgs = 1;

    /* Write USR CFG first */
    start_addr = UCD9081_USRCFG_ADDR;
    cfg_len    = UCD9081_USRCFG_SIZE;

    /* Open flash by writing 0x02 to FLASHLOCK register */
fprintf(stderr, "9081: open flash\n");
    message.addr  = devAddr;
    message.buf   = (char *)waddr;
    message.flags = 0;
    message.len   = 2; /* flash lock register + 0x02 */

    waddr[0]  = FLASHLOCK_REG;
    waddr[1]  = 0x02;

    if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
        return -6;
#if 1
    /* write address of configuration section of memory, ie. start_addr */
fprintf(stderr, "9081: write start addr: 0x%X\n", start_addr);
    message.flags = 0;
    message.len   = 3; /* dev. register + two byte address */

    waddr[0]  = WADDR1_REG;
    waddr[1]  = start_addr & 0xFF;
    waddr[2]  = (start_addr >> 8) & 0xFF;
fprintf(stderr, "reg %x data %x %x\n", waddr[0], waddr[1], waddr[2]);
    if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        ret = -7;
        goto lock_flash;
    }

    /* write constant 0xBADC to WDATA register to unlock and erase flash */
fprintf(stderr, "9081: unlock & erase flash\n");
    message.flags = 0;
    message.len   = 3; /* dev. register + 0xBADC */

    waddr[0]  = WDATA1_REG;
    waddr[1]  = 0xDC;
    waddr[2]  = 0xBA;
fprintf(stderr, "reg %x data %x %x\n", waddr[0], waddr[1], waddr[2]);

    if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        ret = -8;
        goto lock_flash;
    }
    usleep(12000);

    /* 
     * write address of configuration section again, followed by at most 32 bytes of
     * data
     */
    ctrl.msgs = &message;
    idx = 0;
    for (j = start_addr; j < (start_addr + cfg_len); j += block_sz)
    {
        ctrl.nmsgs = 1;

fprintf(stderr, "9081L: write  addr section: 0x%X\n", j);
        /* write address config section */
        message.flags = 0;
        message.len   = 3; /* dev. register + two byte address */
        waddr[0]         = WADDR1_REG;
        waddr[1]         = j & 0xFF;
        waddr[2]         = (j >> 8) & 0xFF;
fprintf(stderr, "reg %x data %x %x\n", waddr[0], waddr[1], waddr[2]);
        if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
        {
            ret = -9;
            break;
        }

        /* write block_sz data */
fprintf(stderr, "9081L: write  block data @ %d: 0x%X%X\n", idx, devCfg[idx], devCfg[idx+1]);
        ctrl.nmsgs = 1;
        /* data to be written */
        message.flags = 0;
        message.len   = block_sz + 1;
        waddr[0]      = WDATA1_REG;
        memcpy(&waddr[1], &usrCfg[idx], block_sz);
        if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
        {
            ret = -10;
            break;
        }
        idx += block_sz;
    }
fprintf(stderr, "done writing USER section j %x\n", j);
#endif
    /* now the DEVCFG section */
    start_addr = UCD9081_DEVCFG_ADDR;
    cfg_len    = UCD9081_DEVCFG_SIZE;

    /* write address of configuration section of memory, ie. start_addr */
fprintf(stderr, "9081: write start addr: 0x%X\n", start_addr);
    message.flags = 0;
    message.len   = 3; /* dev. register + two byte address */

    waddr[0]  = WADDR1_REG;
    waddr[1]  = start_addr & 0xFF;
    waddr[2]  = (start_addr >> 8) & 0xFF;
fprintf(stderr, "CFG:reg %x data %x %x\n", waddr[0], waddr[1], waddr[2]);

    if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        ret = -7;
        goto lock_flash;
    }

    /* write constant 0xBADC to WDATA register to unlock and erase flash */
fprintf(stderr, "9081: unlock & erase flash\n");
    message.flags = 0;
    message.len   = 3; /* dev. register + 0xBADC */

    waddr[0]  = WDATA1_REG;
    waddr[1]  = 0xDC;
    waddr[2]  = 0xBA;
fprintf(stderr, "CFG:reg %x data %x %x\n", waddr[0], waddr[1], waddr[2]);

    if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        ret = -8;
        goto lock_flash;
    }
    usleep(24000);

    /* 
     * write address of configuration section again, followed by at most 32 bytes of
     * data
     */
    ctrl.msgs = &message;
    idx = 0;
    for (j = start_addr; j < (start_addr + cfg_len); j += block_sz)
    {
        ctrl.nmsgs = 1;

fprintf(stderr, "9081L: write  addr section: 0x%X\n", j);
        /* write address config section */
        message.flags = 0;
        message.len   = 3; /* dev. register + two byte address */
        waddr[0]         = WADDR1_REG;
        waddr[1]         = j & 0xFF;
        waddr[2]         = (j >> 8) & 0xFF;
        if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
        {
            ret = -9;
            break;
        }
fprintf(stderr, "CFG:reg %x data %x %x\n", waddr[0], waddr[1], waddr[2]);

        /* write block_sz data */
fprintf(stderr, "9081L: write  block data @ %d: %x %x\n", idx, usrCfg[idx], usrCfg[idx+1]);
        ctrl.nmsgs = 1;
        /* data to be written */
        message.flags = 0;
        message.len   = block_sz + 1;
        waddr[0]      = WDATA1_REG;
        memcpy(&waddr[1], &devCfg[idx], block_sz);
        if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
        {
            ret = -10;
            break;
        }
        idx += block_sz;
    }
fprintf(stderr, "9081: lock & close FLASH\n");
lock_flash:
    ctrl.msgs  = &message;
    ctrl.nmsgs = 1;
    /* lock flash by writing 0x00 to FLASHLOCK register */
    message.addr  = devAddr;
    message.flags = 0;
    message.len   = 2; /* flash lock register + 0x00 */
    message.buf   = (char *)waddr;

    waddr[0]  = FLASHLOCK_REG;
    waddr[1]  = 0x00;

    /* no need to check return, we are done */
    ioctl(hI2C, I2C_RDWR, &ctrl);
    //nanosleep(&twait, NULL); /* wait for device to update the flash */
    usleep(30000);
    close(hI2C);
    return ret;
}

/* START Intel Hex file parsing routines used for UCD9081 configuration */

/* structure to represent a line entry in the hex file */
/* 
 * We are not interested in any other fields(record type, checksum etc) 
 * in the file, since these are verified during parsing.
 */
#define DATA_LINE_SZ 32
typedef struct //_hex_record
{
  int addr;
  int len;
  int data[DATA_LINE_SZ];
} hex_record;

/*
 * Parse a line from the hex file and return key fields. 
 * Returns 0 on success and -ve number for errors.
 * A return value of 1 indicates an eof record.
 * Original code from Brian McGinn, modified for K2.
 */
static int parse_hex_line(char *line, hex_record *hrec)
{
    int i, sum, cksum;
    char *ptr;
    int rec_type, slen;

    if ((!line) || (!hrec))
        return -1;

    slen = strlen(line);
    if ((line[slen-1] == '\n')  || (line[slen-1] == '\r'))
    {
        line[--slen] = '\0';
        if ((line[slen-1] == '\r') || (line[slen-1] == '\n'))
            line[--slen] = '\0';
    }
    if (slen < 11) 
        return -2;
    if (line[0] != ':') 
        return(-3);
    ptr = line + 1;
    if (!sscanf(ptr, "%02x", &hrec->len)) 
        return -4;
    ptr += 2;
    if ( slen < (11 + (hrec->len * 2)) ) 
        return -5;
    if (!sscanf(ptr, "%04x", &hrec->addr)) 
        return -6;
    ptr += 4;
    if (!sscanf(ptr, "%02x", &rec_type)) 
        return -7;
    if (rec_type == 0x01) /* eof record */
        return 1;
    if (rec_type != 0x0)
        return -8;
    ptr += 2;
    sum = (hrec->len & 255) + ((hrec->addr >> 8) & 255) + (hrec->addr & 255) + (rec_type & 255);
    if (hrec->len > 64)
        return -9;
    for (i = 0; i < hrec->len; i++)
    {
        if (!sscanf(ptr, "%02x", &hrec->data[i])) 
            return -10;
        ptr += 2;
        sum += hrec->data[i] & 255;
    }
    if (!sscanf(ptr, "%02x", &cksum)) 
        return -11;
    if ( ((sum & 255) + (cksum & 255)) & 255 ) 
        return(-12); /* checksum error */
    return(0);
}

/* 
 * Parse the given hex file and return user config and device config in the buffers
 * provided. Function is specific for UCD9081. Looks for 128 bytes of user config at
 * address 0x1080 - 0x10FF and 512 bytes of device config at 0xE000 - 0xE1FF.
 * returns 0 for success, -ve values for errors.
 */
int parse_hexfile_for_ucd9081(char *file, uint8_t *usercfg, uint8_t *devcfg)
{
    FILE *fhex;
    char line[256];
    hex_record hex_line;
    int err;
    int line_no = 1;
    int ret = 0, i;
    int usr_idx = 0, dev_idx = 0;

    if ((!file) || (!usercfg) || (!devcfg))
        return -1;
    if ((fhex = fopen(file, "r")) == NULL)
    {
        fprintf(stderr, "%s, open: %s\n", __func__, strerror(errno));
        return -2;
    }
    while (1)
    {
        bzero(line, 256);
        if (fgets(line, 256, fhex) == NULL)
        {
            err = errno;
            if (!feof(fhex))
            {
                fprintf(stderr, "%s, read: %s\n", __func__, strerror(err));
                ret = -2;
            }
            break;
        }
        if ((ret = parse_hex_line(line, &hex_line)) != 0)
        {
            if (ret == 1) /* eof record */
                ret = 0;
            else
            {
                fprintf(stderr, "%s, parse_hex_line returned %d\n", __func__, ret);
                ret = -4;
            }
            break;
        }
        if ((hex_line.addr >= UCD9081_USRCFG_ADDR) && 
            (hex_line.addr < (UCD9081_USRCFG_ADDR+UCD9081_USRCFG_SIZE)))
        {
            if ((hex_line.addr - UCD9081_USRCFG_ADDR) != usr_idx)
            {
                fprintf(stderr, "%s, line %d: non-contiguous address\n", __func__, line_no);
                ret = -5;
                break;
            }
            if ((usr_idx >= UCD9081_USRCFG_SIZE) || 
                ((usr_idx + hex_line.len) > UCD9081_USRCFG_SIZE))
            {
                fprintf(stderr, "%s, line %d: data too long\n", __func__, line_no);
                ret = -5;
                break;
            }
            for (i = 0; i < hex_line.len; i++)
                usercfg[usr_idx++] = (hex_line.data[i] & 0xFF);
        }
        else if ((hex_line.addr >= UCD9081_DEVCFG_ADDR) && 
                 (hex_line.addr < (UCD9081_DEVCFG_ADDR+UCD9081_DEVCFG_SIZE)))
        {
            if ((hex_line.addr - UCD9081_DEVCFG_ADDR) != dev_idx)
            {
                fprintf(stderr, "%s, line %d: non-contiguous address\n", __func__, line_no);
                ret = -5;
                break;
            }
            if ((dev_idx >= UCD9081_DEVCFG_SIZE) || 
                ((dev_idx + hex_line.len) > UCD9081_DEVCFG_SIZE))
            {
                fprintf(stderr, "%s, line %d: data too long\n", __func__, line_no);
                ret = -5;
                break;
            }
            for (i = 0; i < hex_line.len; i++)
                devcfg[dev_idx++] = (hex_line.data[i] & 0xFF);
        }
        else
        {
            fprintf(stderr, "%s, parse: line %d: invalid address 0x%X\n", __func__, line_no, 
                                                                        hex_line.addr);
            ret = -7;
            break;
        }
        line_no++;
    }
    fclose(fhex);
    return ret;
}
/* END Intel Hex file parsing routines used for UCD9081 configuration */

/*
 * Update UCD9081 user and device config from the given file. The file should contain
 * UCD9081 configuration in Intel hex format.
 * Returns 0 on success, -ve values for errors
 */
int update_ucd9081_from_file(int bus, int devAddr, char *hexfile)
{
    uint8_t *usr_cfg;
    uint8_t *dev_cfg;
    int ret = 0;
    if ((usr_cfg = (uint8_t *)malloc(UCD9081_USRCFG_SIZE)) == NULL)
    {
        fprintf(stderr, "%s: Memory allocation(USR) failure\n", __func__);
        return -1;
    }
    if ((dev_cfg = (uint8_t *)malloc(UCD9081_DEVCFG_SIZE)) == NULL)
    {
        fprintf(stderr, "%s: Memory allocation(DEV) failure\n", __func__);
        free(usr_cfg);
        return -1;
    }
    if ((ret = parse_hexfile_for_ucd9081(hexfile, usr_cfg, dev_cfg)) != 0)
    {
        fprintf(stderr, "update_ucd9081_config USRCFG %d\n", ret);
        goto ucd9081_ret;
    }
    if ((ret = update_ucd9081_config(bus, devAddr, usr_cfg, dev_cfg)) != 0)
    {
        fprintf(stderr, "update_ucd9081_config returned %d\n", ret);
        goto ucd9081_ret;
    }
    return 0;
ucd9081_ret:
    free(usr_cfg);
    free(dev_cfg);
    return ret;
}

/*
 * read config section of a ucd9081
 */
static int read_ucd9081_config_section(int bus, int devAddr, int start, int len,
                                        uint8_t *buffer)
{
    const int block_sz = 32;
    int j, val, idx;
    /* read blocks of 32 bytes(max possible) and accumulate in buffer */
    for (j = start, idx = 0; j < (start+len); j += block_sz)
    {
        if ((val = read_ucd9081_config(bus, devAddr, j, block_sz, &(buffer[idx]))) < 0)
        {
            fprintf(stderr, "Error(%d) reading ucd9081 config\n", val);
            return -1;
        }
        idx += block_sz;
    }
    return 0;
}

/*
 * read the configuration of specified ucd9081 and compare with that in the 
 * given file. Return 0 if same, -ve if different
 */
int compare_ucd9081_config(int bus, int devAddr, char *hexfile)
{
   int ret = 0;
   uint8_t *usr_cfg;
   uint8_t *dev_cfg;
   uint8_t *dev_usr_cfg;
   uint8_t *dev_dev_cfg;
   if ((usr_cfg = (uint8_t *)malloc(UCD9081_USRCFG_SIZE)) == NULL)
   {
       fprintf(stderr, "%s: Memory allocation(USR) failure\n", __func__);
       return -1;
   }
   if ((dev_cfg = (uint8_t *)malloc(UCD9081_DEVCFG_SIZE)) == NULL)
   {
       fprintf(stderr, "%s: Memory allocation(DEV) failure\n", __func__);
       free(usr_cfg);
       return -1;
   }
   if ((dev_usr_cfg = (uint8_t *)malloc(UCD9081_USRCFG_SIZE)) == NULL)
   {
       fprintf(stderr, "%s: Memory allocation(USR_U) failure\n", __func__);
       free(dev_cfg);
       free(usr_cfg);
       return -1;
   }
   if ((dev_dev_cfg = (uint8_t *)malloc(UCD9081_DEVCFG_SIZE)) == NULL)
   {
       fprintf(stderr, "%s: Memory allocation(DEV_D) failure\n", __func__);
       free(dev_usr_cfg);
       free(dev_cfg);
       free(usr_cfg);
       return -1;
   }
   if ((ret = parse_hexfile_for_ucd9081(hexfile, usr_cfg, dev_cfg)) != 0)
        goto cmp_ucd9081_ret;
   if ((ret = read_ucd9081_config_section(bus, devAddr, 0x1080, 0x80, dev_usr_cfg)) != 0)
        goto cmp_ucd9081_ret;
   if ((ret = read_ucd9081_config_section(bus, devAddr, 0xE000, 0x200, dev_dev_cfg)) != 0)
        goto cmp_ucd9081_ret;
   if (memcmp(usr_cfg, dev_usr_cfg, 0x80) != 0)
   {
        ret = -2;
        goto cmp_ucd9081_ret;
   }
   if (memcmp(dev_cfg, dev_dev_cfg, 0x200) != 0)
   {
        ret = -3;
        goto cmp_ucd9081_ret;
   }
   return 0;
cmp_ucd9081_ret:
   free(dev_usr_cfg);
   free(dev_cfg);
   free(usr_cfg);
   return ret;
}

/*
 * Print given buffer in intel hex format. Expects 64 characters in buffer.
 */
static void printHexLine(int addr, char *buffer)
{
    int len = 0x20;
    int type = 0x00;
    int i;
    uint8_t sum;
    printf(":%02X%04X%02X", len, addr, type); 
    sum  = (len & 0xFF) + ((addr >> 8) & 0xFF) + (addr & 0xFF) + (type & 0xFF);
    for (i = 0; i < 32; i++)
    {
        printf("%02X", buffer[i]);
        sum += (buffer[i] & 0xFF);
    }
    /* now take 2's complement, see wikipedia doc on intel hex format */
    sum = 0x100 - sum;
    printf("%02X\n", sum);
}

static int dump_ucd9081_config_section(int bus, int devAddr, int start, int end)
{
    uint8_t buffer[32];
    const int block_sz = 32;
    int j, val;
    for (j = start; j < end; j += block_sz)
    {
        if ((val = read_ucd9081_config(bus, devAddr, j, block_sz, buffer)) < 0)
        {
            fprintf(stderr, "Error(%d) reading ucd9081 config\n", val);
            return -1;
        }
        printHexLine(j, (char *)buffer);
    }
    return 0;
}

int dump_ucd9081_config(int bus, int devAddr)
{
    int ret;
    /* dump user config first */
    if ((ret = dump_ucd9081_config_section(bus, devAddr, 0x1080, 0x1100)) == 0)
    {
        /* next dump device config */
        ret = dump_ucd9081_config_section(bus, devAddr, 0xE000, 0xE200);
    }
    printf(":00000001FF\n"); /* end of record indicator for hex dump */
    return ret;
}
