// Thermo-couple calibration routines.

#include <stdio.h>
#include "bing_common.h"
#include "tc_calibrate.h"

// defined in source files..
// convert voltage to temp.
extern double tcTypeRTemp(double V);
// convert cjc temp to voltage corresponding to type R TC
extern double cjcTemp2tcTypeRVolt(double cjcT);

float findTCTemp(int sType, double v)
{
    // Convert voltage to milli-volts.
    float tVal = -0.0; // 0 deg K
    switch (sType)
    {
        case Type_R:
            tVal = tcTypeRTemp(v);
            break;
        case Type_K:
            tVal = tcCalTemp(CAL_TYPE_K, v);
            break;
        case Type_T:
            tVal = tcCalTemp(CAL_TYPE_T, v);
            break;
        default:
            break;
    }
    return tVal;
}

