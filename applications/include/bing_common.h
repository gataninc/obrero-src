#ifndef __COMMON_H_
#define __COMMON_H_

#include <stdint.h>

#define LOGPRINTS
#ifdef LOGPRINTS
#define logPrint(format, ...) do { fprintf(stderr, format, ##__VA_ARGS__); fflush(stderr);} while (0)
#else
#define logPrint(format, ...)
#endif

#define EPROM_MAC_ADDR_LEN    (6)

#define MAX_I2C_BUS          (3)
#define MAX_MUX_CHAN         (8)

#define T_ADC_CHAN_TC     (1)
#define T_ADC_CHAN_DRTD   (2)
#define T_ADC_CHAN_PREAMP (3)

#define MIN_TRIM_VOLTS    (1.500)
#define MAX_TRIM_VOLTS    (3.100)

/*** ALSO defined in UI code ***/
enum
{
    UnknownSensor = 0,
    PRT,
    Type_K,
    Type_R,
    Type_T,
    DT_470,
    DT_670
};

enum
{
    SensorTypeNone,  // unknown?
    SensorTypeDiode, // PRT/diode
    SensorTypeTC,    // thermo-couple
};

enum
{
    GndModeInvalid = 0,
    GndModeFloating,
    GndModeSocket,
    GndModeMains,
    GndModeBuffered,
};
/*** END ALSO defined in UI code ***/

#define K_OFFSET   (273.15)

/*
 ****** COMMON BETWEEN UI AND SYSTEM S/W * KEEP THIS IN SYNC  *******
 */

/* 
 * IPC keys for sysV semaphores used to serialise access between
 * UI and system s/w. 
 *
 * NOTICE: These are #defined in the UI(tempsys) header file too.
 */

#define MAX_SENSORS (5)

#define SHMEM_SEMID            (0x1200300) // protect shared memory
#define CTRL_REG_SEMID         (0x2200312) // machine control regs
#define I2C_BUS1_SEMID         (0x3200323)
#define I2C_BUS2_SEMID         (0x4200334)
#define MCP3424_SEMID          (0x5200345)

// semaphore wait times (in milli-secs)
#define SEM_WAIT_TIME      (150)
#define SHMEM_SEM_WAIT     (130)
#define MCP3424_WAIT       (600)  // a read may take upto 300mS

#define DBG_LOG_TRACE      (0x1)
#define DBG_LOG_INFO       (0x2)
#define DBG_LOG_WARN       (0x4)
#define DBG_LOG_ERR        (0x8)

/*
 ***** END COMMON BETWEEN UI AND SYSTEM S/W * KEEP THIS IN SYNC  *******
 */

#endif // __COMMON_H_
