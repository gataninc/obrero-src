#ifndef __SYS_INI_H
#define __SYS_INI_H

/*
 * Entries in system.ini file. Mostly calibration values now(08/27/2015)
 */

#define SYS_INI_FILE  "/mnt/nv/system.ini"

#define CAL_SECTION   "calibration"

#define STIM_TRIM_100U_P     "DAC_stim_trim_100u_p(V)"
#define STIM_TRIM_100U_N     "DAC_stim_trim_100u_n(V)"
#define STIM_TRIM_10U_P      "DAC_stim_trim_10u_p(V)"
#define STIM_TRIM_10U_N      "DAC_stim_trim_10u_n(V)"
#define STIM_TRIM_1U_P       "DAC_stim_trim_1u_p(V)"
#define STIM_TRIM_1U_N       "DAC_stim_trim_1u_n(V)"
#define TC_GTC_GAIN          "TC_G_tc_gain"
#define CJC_ADC_GAIN         "CJC ADC Gain"
#define TC_ZERO_V            "TC_Zero_V(V)"
#define TC_BIAS_I            "I_TC_Bias(nA)"
#define DIODE_SENSOR_GAIN    "Diode Sensor Gain(G1DRTD)"
#define V_RTD_ZERO           "vRTDZero"
#define V_PREC_ZERO          "vPrecZero"
#define V_PREC_STIM          "vPrecStim"

#endif
