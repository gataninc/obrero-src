#ifndef __FUSION_DEF__
#define __FUSION_DEF__

/* Fusion touch screen information */
struct fusion_info {
	int xres; /* x resolution */
	int yres; /* y resolution */
	int xy_reverse; /* if need reverse in the x,y value x=xres-1-x, y=yres-1-y*/
};

#endif
