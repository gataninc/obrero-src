/*************************************************
 *
 * EEPROM access functions
 *
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <i2c-dev.h>
#include "bing_common.h"
#include "utils.h"

/* describe specific features of an eeprom device */
typedef struct
{
    unsigned char bus; 
    unsigned char devAddr;
    unsigned char wrCycleTm;   /* write cycle time in millisecs */
    unsigned char addrBytes;   /* no. of bytes for addr spec */
    unsigned int  devSize;     /* bytes */
    unsigned int  pageSz;      /* bytes */
} eepromDesc; 

/* Generic routines to access a single eeprom device on i2c bus */
/* ioctl(I2C_RDWR) supports max. data size of 8192. So we need a loop to read larger
   size eeproms */

static int eeprom_read_generic(eepromDesc *det, unsigned addr, unsigned size,
                               unsigned char *buffer)
{
    int hI2C;
    struct i2c_msg message[2];
    struct i2c_rdwr_ioctl_data ctrl;
    unsigned char cAddr;
    unsigned short sAddr;
    int blockSz = 8192;
    int xferSz, remain, idx;

    if (addr >= det->devSize)
        return -1;
    if ((addr + size) > det->devSize)
        return -2;
    if ((det->addrBytes <= 0) || (det->addrBytes > 2))
    {
        fprintf(stderr, "%s: invalid addrBytes %d\n", __func__, det->addrBytes);
        return -3;
    }

    if ((hI2C = open(get_i2c_bus(det->bus), O_RDWR)) < 0)
        return -4;

    ctrl.nmsgs = 2;
    ctrl.msgs  = &message[0];

    message[0].addr  = det->devAddr;
    message[0].flags = 0;

    message[1].addr  = det->devAddr;
    message[1].flags = I2C_M_RD;

    remain = size;
    idx = 0;
    while (remain > 0)
    {
        if (det->addrBytes == 1)
        {
            message[0].len = 1;
            message[0].buf = (char *)&cAddr;
            cAddr = (addr & 0xFF);
        }
        else 
        {
            message[0].len = 2;
            message[0].buf = (char *)&sAddr;
            sAddr          = (unsigned short) (addr & 0xFFFF);
        }

        xferSz = (remain > blockSz) ? blockSz : remain;
        message[1].len   = xferSz;
        message[1].buf   = (char *)&buffer[idx];

        /* do i2c transaction */
        if(ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
            break;

        idx    += xferSz;
        remain -= xferSz;
        addr   += xferSz;
    }
    close(hI2C);
    if (remain > 0)
        return -5;
    return size;
}

static int eeprom_write_generic(eepromDesc *det, unsigned addr, unsigned size,
                                unsigned char *buffer)
{
    int hI2C;
    struct i2c_msg message;
    struct i2c_rdwr_ioctl_data ctrl;
    unsigned char data[64+2]; /* max pagesize + max addr size */
    int blockSz, xferSz, remain, idx;
    struct timespec twait;
    int ret = 0;

    if (addr >= det->devSize)
        return -1;
    if ((addr + size) > det->devSize)
        return -2;
    if ((det->addrBytes != 1) && (det->addrBytes != 2))
    {
        fprintf(stderr, "%s: invalid addrBytes %d\n", __func__, det->addrBytes);
        return -3;
    }
    if (det->pageSz > 64)
        return -4;

    if ((hI2C = open(get_i2c_bus(det->bus), O_RDWR)) < 0)
        return -5;
    
    /* try to align to the page size, so next writes start at page addresses */
    blockSz = det->pageSz - (addr & (det->pageSz-1));

    ctrl.nmsgs = 1;
    ctrl.msgs  = &message;

    remain = size;
    idx    = 0;
    twait.tv_sec   = 0;
    twait.tv_nsec  = det->wrCycleTm * 1000 * 1000;
    while (remain > 0)
    {
        message.addr  = det->devAddr;
        message.flags = 0;
        xferSz = (remain > blockSz) ? blockSz : remain;
        if (det->addrBytes == 1)
        {
            message.len = xferSz + 1;
            data[0]        = (addr & 0xFF);
        }
        else if (det->addrBytes == 2)
        {
            message.len = xferSz + 2;
            data[0]        = (addr & 0xFF00) >> 8;
            data[1]        = (addr & 0xFF);
        }
        memcpy(&data[det->addrBytes], &buffer[idx], xferSz);
        message.buf = (char *)&data[0];

        /* do i2c transaction */
        if(ioctl(hI2C,I2C_RDWR,&ctrl) < 0)
        {
            ret = -6;
            break;
        }
        /* let update finish */
        nanosleep(&twait, 0);

        remain  -= xferSz;
        idx     += xferSz;
        addr    += xferSz;
        blockSz = det->pageSz;
    }
    close(hI2C);
    if (ret < 0)
        return ret;
    return size;
}
/* END Generic routines to access a single eeprom device on i2c bus */

int eeprom_read_m24c0x(int bus, int devAddr, unsigned addr, unsigned size,
                       unsigned char *buffer)
{
    eepromDesc desc;
    desc.bus       = bus;
    desc.devAddr   = devAddr;
    desc.addrBytes = 1;
    desc.devSize   = 256;
    desc.pageSz    = 16;
    desc.wrCycleTm = 10;  // from data sheet
    return (eeprom_read_generic(&desc, addr, size, buffer));
}

int eeprom_write_m24c0x(int bus, int devAddr, unsigned addr, unsigned size, 
                        unsigned char *buffer)
{
    eepromDesc desc;
    desc.bus       = bus;
    desc.devAddr   = devAddr;
    desc.addrBytes = 1;
    desc.devSize   = 256;
    desc.pageSz    = 16;
    desc.wrCycleTm = 10;  // from data sheet
    return (eeprom_write_generic(&desc, addr, size, buffer));
}
