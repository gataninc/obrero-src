/* Copyright (c) 2008 Freescale Semiconductor Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the  GNU General Public License along
 * with this program; if not, write  to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef _FSL_USB_IO_H
#define _FSL_USB_IO_H

/*
 * On some SoCs, the USB controller registers can be big or little endian,
 * depending on the version of the chip.  For these SoCs, the kernel
 * should be configured with CONFIG_USB_FSL_BIG_ENDIAN_MMIO enabled.  
 *
 * The "big-endian-regs" property should be specified in the USB node
 * of the device tree for SoCs that have BE USB registers.
 * pdata->big_endian_mmio reflects the state of that device tree property.
 *
 * In order to be able to run the same kernel binary on 2 different
 * versions of an SoC, the BE/LE decision must be made at run time.
 * _fsl_readl and fsl_writel are pointers to the BE or LE readl()
 * and writel() functions, and fsl_readl() and fsl_writel() call through
 * those pointers.
 *
 * For SoCs with the usual LE USB registers, don't enable
 * CONFIG_USB_FSL_BIG_ENDIAN_MMIO, and then fsl_readl() and fsl_writel()
 * are just macro wrappers for in_le32() and out_le32().
 * 
 * In either (LE or mixed) case, the function fsl_set_usb_accessors()
 * should be called at probe time, to either set up the readl/writel
 * function pointers (mixed case), or do nothing (LE case).
 *
 * The host USB drivers already have a mechanism to handle BE/LE
 * registers.  The functionality here is intended to be used by the
 * gadget and OTG transceiver drivers.
 *
 * This file also contains controller-to-cpu accessors for the
 * USB descriptors, since their endianess is also SoC dependant.
 * The kernel option CONFIG_USB_FSL_BIG_ENDIAN_DESC configures
 * which way to go.
 */

//#if 1 /*CONFIG_USB_FSL_BIG_ENDIAN_MMIO*/
#if 1 /*CONFIG_USB_FSL_BIG_ENDIAN_MMIO*/
static u32 __maybe_unused _fsl_readl_be(const volatile void __iomem *p)
{
	return in_be32(p);
}
static u32 __maybe_unused _fsl_readl_le(const volatile void __iomem *p)
{
	return in_le32(p);
}

static void __maybe_unused _fsl_writel_be(u32 v, volatile void __iomem *p)
{
	out_be32(p, v);
}
static void __maybe_unused _fsl_writel_le(u32 v, volatile void __iomem *p)
{
	out_le32(p, v);
}

static u32 (*_fsl_readl)(const volatile void __iomem *p);
static void (*_fsl_writel)(u32 v, volatile void __iomem *p);

#define fsl_readl(p)		(*_fsl_readl)((p))
#define fsl_writel(v, p)	(*_fsl_writel)((v), (p))

static inline void fsl_set_usb_accessors(struct fsl_usb2_platform_data *pdata)
{
	if (pdata->big_endian_mmio) {
        printk(KERN_WARNING "accessor: big-endian\n");
		_fsl_readl = _fsl_readl_be;
		_fsl_writel = _fsl_writel_be;
	} else {
        printk(KERN_WARNING "accessor: little-endian\n");
		_fsl_readl = _fsl_readl_le;
		_fsl_writel = _fsl_writel_le;
	}
}

#else /* CONFIG_USB_FSL_BIG_ENDIAN_MMIO */

#define fsl_readl(addr)		in_le32((addr))
#define fsl_writel(val32, addr) out_le32((addr), (val32))

static inline void fsl_set_usb_accessors(struct fsl_usb2_platform_data *pdata)
{
}
#endif /* CONFIG_USB_FSL_BIG_ENDIAN_MMIO */

#ifdef CONFIG_USB_FSL_BIG_ENDIAN_DESC
#define cpu_to_hc32(x)	(x)
#define hc32_to_cpu(x)	(x)
#else
#define cpu_to_hc32(x)	cpu_to_le32((x))
#define hc32_to_cpu(x)	le32_to_cpu((x))
#endif

#endif /* _FSL_USB_IO_H */
