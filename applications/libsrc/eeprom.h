#ifndef __EPROM_H
#define __EPROM_H
int eeprom_read_m24c0x(int bus, int devAddr, unsigned addr, unsigned size,
                       unsigned char *buffer);
int eeprom_write_m24c0x(int bus, int devAddr, unsigned addr, unsigned size, 
                        unsigned char *buffer);

#endif //__EPROM_H
