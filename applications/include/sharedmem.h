#ifndef __SHAREDMEM_H
#define __SHAREDMEM_H
#include "bing_common.h"

/* 
   Use a shared memory area for keeping data shared between UI/diagshell etc.
   This area is allocated and initilised at system startup time and 
   is attached/detached as needed by diagshell and UI(may be other apps too)
 */
#define IPC_KEY_SHAREDMEM  (0x132465A)

enum 
{
    HEAT_IRANGE_UNKNOWN,
    HEAT_IRANGE_2A,
    HEAT_IRANGE_200mA,
};

#define NUM_SENSORS (2)

typedef struct
{
    int   semFailed;
    int   sensorType[NUM_SENSORS];
    float vMax;
    float iMax;
    float pMax;
    int   maxVRange;
    int   grounding;
    float setV;
    int   Kp;
    int   Ki;
    int   Kd;
    int   setPt;      // in milli-kelvins
    int   delay;
    int   pidEnable;
    int   bakeOut;
    int   lcdBright;
    int   debug;
    int   pidAvg;       // true or false
    int   flowCheck;    // check coolant flow?
    // CJC related settings
    int   enableCJC;    // set when a thermocouple sensor is selected
    int   cjcIdx;       // index of TC type sensor in sensorType
    float CJCFact;      // cold junction compensation temperature.
    double CJCADCGain;  // volts/lsb, from factory calibration
    // Calibration values for PRT, refere to docs from AlitairM
    double vPrecZero; 
    double vPrecStim;
    double vRtdZero;
    // TC calibration values, refer to Alistair's doc for details
    double tcVZero;      //  zero offset, from factory calibration
    double tcGTC;        //  amp. gain, from factory calibration
    double tcVMiss;      //  offset from missing tc detection..
    //  Diode calibration values
    double vG1Zero;      // offset voltage for diode types, runtime
    double G1drtd;       // amp. gain, from factory calibration
    // Trim values for various stimulus settings, from factory calibration
    double stimTrim100u_p;
    double stimTrim100u_n;
    double stimTrim10u_p;
    double stimTrim10u_n;
    double stimTrim1u_p;
    double stimTrim1u_n;
    // 
    uint32_t errFlags;  // used by monitoring task
    int    stimI[NUM_SENSORS]; // stimulus current.
    int    heatIRange;   // HEAT_I_RANGE setting
} sysParams;

#endif // __SHAREDMEM_H
