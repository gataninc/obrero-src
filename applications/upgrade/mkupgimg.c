// Create upgrade image for Sample Prep Bing based systems.
// Input is either a kernel+ramdisk image(produced by u-boot) Or 
// a compressed ramdisk image of the all PIPS GUI components.
// The upgrade archive is targeted for PIPS(as indicated in the header).
// 05/28/2015:  Updated for Obrero(smartset2) systems
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <libgen.h>
#include <time.h>
#include <errno.h>
#include "bing_common.h"
#include "upgrade_hdr.h"
#include "adler32.h"

/* buffer for file operations */
#define BUFSIZE  8192

static uint8_t buffer[BUFSIZE];

static void Usage(char *progName)
{
    char *pName = 0;
    int j;
    pName = strrchr(progName, '/');
    if (pName)
        ++pName;
    else
        pName = progName;
    fprintf(stderr, "Usage: %s -r <rel. ver> -b <build_type> -i <img_type>"
                    " -d <mm/dd/yyyy> -t <hh:mm:ss> -o <outfile> <infile>\n", pName);
    fprintf(stderr, "  <rel. ver> is of the form majVer.minVer.patchLevel.buildNo\n");
    fprintf(stderr, "  <build_type> is one of alpha, beta, release, special\n");
    fprintf(stderr, "  <img_type> is one of APP | SYSTEM to indicate apps or "
                    "system image\n");
    fprintf(stderr, "  day and time are optional, use current time if not specified\n");
}

static char appImgStr[] = "APP";
static char sysImgStr[] = "SYSTEM";

// matches with enum, offset is used init buildtype
static char *buildTypeStr[] = { "invalid", "alpha", "beta", "release" };

// pointers to user input
static char *usrRelVer;
static char *usrBldType;
static char *usrImgType;
static char *usrInpFile;
static char *usrOutFile;
static char *usrDayStr;
static char *usrTimeStr;

// parsed values
static uint16_t pMajorNo;
static uint16_t pMinorNo;
static uint16_t pPatchLev;
static uint16_t pBuildNo;
static int    pBldType;
static uint32_t pImgType;
static uint32_t g_dayVals[3];   // 0: mon, 1: day, 2: year
static uint32_t g_timeVals[3];  // 0: hour, 1: min, 2: sec

static int parseCmdLineArgs(int argc, char *argv[])
{
    int idx, parseErr = 0;
    char commErr[] = "Error: Unexpected string after %s\n";
    idx = 1;
    while (1)
    {
        if (argv[idx][0] == '-')
        {
            switch (argv[idx][1])
            {
                case 'r':
                    if (argv[++idx][0] == '-')
                    {
                        printf(commErr, argv[idx-1]);
                        parseErr = 1;
                    }
                    else
                        usrRelVer = argv[idx++];
                    break;
                case 'b':
                    if (argv[++idx][0] == '-')
                    {
                        printf(commErr, argv[idx-1]);
                        parseErr = 1;
                    }
                    else
                        usrBldType = argv[idx++];
                    break;
                case 'i':
                    if (argv[++idx][0] == '-')
                    {
                        printf(commErr, argv[idx-1]);
                        parseErr = 1;
                    }
                    else
                        usrImgType = argv[idx++];
                    break;
                case 'o':
                    if (argv[++idx][0] == '-')
                    {
                        printf(commErr, argv[idx-1]);
                        parseErr = 1;
                    }
                    else
                        usrOutFile = argv[idx++];
                    break;
                case 't':  // time input, must be hh:mm:ss
                    if (argv[++idx][0] == '-')
                    {
                        printf(commErr, argv[idx-1]);
                        parseErr = 1;
                    }
                    else
                        usrTimeStr = argv[idx++];
                    break;
                case 'd':  // date input, must be mm/dd/yyyy
                    if (argv[++idx][0] == '-')
                    {
                        printf(commErr, argv[idx-1]);
                        parseErr = 1;
                    }
                    else
                        usrDayStr = argv[idx++];
                    break;
                default:
                    fprintf(stderr, "Unknown option <%s>\n", argv[idx]);
                    parseErr = 1;
                    break;
            }
        }
        else  // must be an input file..
        {
            usrInpFile = argv[idx++];
        }
        if (parseErr)
        {
            Usage(argv[0]);
            break;
        }
        if (idx >= argc)
            break;
    }
    return parseErr;
}

static int extractRelVer()
{
    int ret = 0, j;
    char *tmp, *pDot, *endPtr;
    ushort relVer[4];
    /* validate release version and populate global */
    tmp = usrRelVer;
    /* extract first three sections of ver. number */
    for (j = 0; j < 3; j++)
    {
        if ((pDot = strchr(tmp, '.')) == NULL)
        {
           fprintf(stderr, "Error: Invalid rel.ver string(1)\n");
           ret = 1;
           break;
        }
        *pDot = '\0';
        endPtr = 0;
        relVer[j] = strtoul(tmp, &endPtr, 10);
        if ((!endPtr) || (*endPtr != '\0'))
        {
           fprintf(stderr, "Error: Invalid rel.ver string(2)\n");
           ret = 1;
           break;
        }
        tmp = ++pDot;
    }
    /* extract the last portion, tmp already set above */
    if (ret != 0)
        return ret;
    endPtr = 0;
    relVer[3] = strtoul(tmp, &endPtr, 10);
    if ((!endPtr) || (*endPtr != '\0'))
    {
       fprintf(stderr, "Error: Invalid rel.ver string(3)\n");
       return 1;
    }
    pMajorNo  = relVer[0];
    pMinorNo  = relVer[1];
    pPatchLev = relVer[2];
    pBuildNo  = relVer[3];

    fprintf(stderr, "Rel. Ver: %hu.%02hu.%02hu.%02hu\n", relVer[0], relVer[1], relVer[2],
                                                        relVer[3]);
    return ret;
}

static int validDayStr(uint32_t *dayVals)
{
    char *tmp, *dayPtr[3]; // 0: mon, 1: day, 2: year
    char *savePtr;
    int i, err = 0;
    char invStr[] = "Error: Invalid day string";

    // parse pointers into array
    for (i = 0, tmp = usrDayStr; ; i++, tmp = 0)
    {
        if ((dayPtr[i] = strtok_r(tmp, "/", &savePtr)) == 0)
            break;
    }
    if (i < 3)
    {
        printf("%s\n", invStr);
        return 1;
    }
    // convert strings into integers
    for (i = 0; i < 3; i++)
    {
        tmp = 0;
        dayVals[i] = strtoul(dayPtr[i], &tmp, 10);
        if ((!tmp) || (*tmp != '\0'))
            break;
    }
    if (i < 3)
    {
        printf("%s\n", invStr);
        return 1;
    }
    printf("day is %u/%u/%u\n", dayVals[0], dayVals[1], dayVals[2]);
    // validate date,
    if ((dayVals[0] < 1) || (dayVals[0] > 12))
        err = 1;
    if (dayVals[2] < 2012)
        err = 1;
    // validate day of the month...
    if ((dayVals[1] < 1) || (dayVals[1] > 31))
        err = 1;
    if (err == 0)
    {
        int leap = 0;
        switch (dayVals[0]) // based on month
        {
            case 4: case 6: case 9: case 11:
                if (dayVals[1] > 30)
                    err = 1;
                break;
            case 2:
                if ((dayVals[2] % 4)  == 0) 
                {
                    leap = 1;
                    if ((dayVals[2] % 100) == 0)
                    {
                        if ((dayVals[2] % 400) != 0)
                            leap = 0;
                    }
                }
                if (leap)
                {
                    if (dayVals[1] > 29)
                        err = 1;
                }
                else
                {
                    if (dayVals[1] > 28)
                        err = 1;
                }
                break;
            default:   // rest of the months have 31 days..
                break;
        }
    }
    if (err)
    {
        printf("%s\n", invStr);
        return 1;
    }
    return 0;
}

static int validTimeStr(uint32_t *timeVals)
{
    char *tmp, *timePtr[3]; // 0: hour, 1: min, 2: sec
    char *savePtr;
    int i, err = 0;
    char invStr[] = "Error: Invalid time string";

    // parse pointers into array
    for (i = 0, tmp = usrTimeStr; ; i++, tmp = 0)
    {
        if ((timePtr[i] = strtok_r(tmp, ":", &savePtr)) == 0)
            break;
    }
    if (i < 3)
    {
        printf("%s\n", invStr);
        return 1;
    }
    // convert strings into integers
    for (i = 0; i < 3; i++)
    {
        tmp = 0;
        timeVals[i] = strtoul(timePtr[i], &tmp, 10);
        if ((!tmp) || (*tmp != '\0'))
            break;
    }
    if (i < 3)
    {
        printf("%s\n", invStr);
        return 1;
    }
    printf("time is %u:%u:%u\n", timeVals[0], timeVals[1], timeVals[2]);
    // validation 
    if ((timeVals[0] < 0) || (timeVals[0] > 23))
        err = 1;
    if ((timeVals[1] < 0) || (timeVals[1] > 59))
        err = 1;
    if ((timeVals[2] < 0) || (timeVals[2] > 59))
        err = 1;
    if (err)
    {
        printf("%s\n", invStr);
        return 1;
    }
    return 0;
}

static int validateInput()
{
    int ret = 0, i, count;
    if (usrRelVer)
        printf("RelVer:      %s\n", usrRelVer);
    if (usrBldType)
        printf("BldType:     %s\n", usrBldType);
    if (usrImgType)
        printf("Image Type:  %s\n", usrImgType);
    if (usrInpFile)
        printf("Input File:  %s\n", usrInpFile);;
    if (usrOutFile)
        printf("output File: %s\n", usrOutFile);
    if (usrDayStr)
        printf("day        : %s\n", usrDayStr);
    if (usrTimeStr)
        printf("time       : %s\n", usrTimeStr);
    if (!usrRelVer)
    {
        fprintf(stderr, "Error: Specify release version\n");
        ret = 1;
    }
    if (!usrBldType)
    {
        ret = 1;
        fprintf(stderr, "Error: Specify build type\n");
    }
    if (!usrImgType)
    {
        ret = 1;
        fprintf(stderr, "Error: Specify image type\n");
    }
    if (!usrInpFile)
    {
        ret = 1;
        fprintf(stderr, "Error: Specify an input file\n");
    }
    if (!usrOutFile)
    {
        ret = 1;
        fprintf(stderr, "Error: Specify an output file\n");
    }
    if (ret != 0)
        return ret;

    // extract release version and verify correct format
    if ((ret = extractRelVer()) != 0)
        return ret;

    // validate access permission of input file
    if (access(usrInpFile, R_OK) != 0)
    {
        fprintf(stderr, "Error: bad input file, %s\n", usrInpFile);
        return 1;
    }

    // validate image type
    if (strcmp(usrImgType, appImgStr) == 0)
        pImgType = IMAGE_APP_TYPE;
    else if (strcmp(usrImgType, sysImgStr) == 0)
        pImgType = IMAGE_SYS_TYPE;
    else
    {
        fprintf(stderr, "Error: invalid image type, %s\n", usrImgType);
        return 1;
    }

    // validate build type
    count = (sizeof(buildTypeStr) / sizeof(char *));
    for (i = 1; i < count; i++) // skip the "invalid" str
    {
        if (strcmp(buildTypeStr[i], usrBldType) == 0)
        {
            pBldType = i;
            break;
        }
    }
    if (i >= count)
    {
        fprintf(stderr, "Error: invalid build type, %s\n", usrBldType);
        return 1;
    }
    if ((usrDayStr) && (validDayStr(g_dayVals) != 0))
    {
        return 1;
    }
    if ((usrTimeStr) && (validTimeStr(g_timeVals) != 0))
    {
        return 1;
    }
    return 0;
}

/* 
 * Update checksum 32 for upgrade file. csum is kept in the first four bytes. So skip
 * these and compute checksum for the rest of the file. Update first four bytes with
 * computed checksum.
 */
static int updateFileCsum(int fd)
{
    ssize_t rdLen;
    int ret = 0;
    uLong crc32;
    /* init csum field */
    crc32 = adler32(0L, Z_NULL, 0);
    /* seek to start of data */
    if (lseek(fd, sizeof(unsigned int), SEEK_SET) == -1)
    {
        fprintf(stderr, "(1)Error seeking file: %s\n", strerror(errno));
        return -1;
    }
    while (1)
    {
        if ((rdLen = read(fd, (void *)buffer, BUFSIZE)) == -1)
        {
            fprintf(stderr, "Error reading file: %s\n", strerror(errno));
            ret = -1;
            break;
        }
        if (rdLen == 0) /* EOF */
        {
            fprintf(stderr, "csum: 0x%X\n", crc32);
            if (lseek(fd, 0, SEEK_SET) == -1)
            {
                fprintf(stderr, "(2)Error seeking file: %s\n", strerror(errno));
                ret = -1;
                break;
            }
            if (write(fd, (void *)&crc32, sizeof(uLong)) != sizeof(uLong))
            {
                fprintf(stderr, "Error update csum: %s\n", strerror(errno));
                ret = -1;
                break;
            }
            break; /* done updating csum */
        }
        /* update csum */
        crc32 = adler32(crc32, buffer, rdLen);
    }
    return ret;
}

static int appendFile(int outFD, char *inFile)
{
    ssize_t Len, Ret;
    int ret = 0;
    int inFD;

    if ((inFD = open(inFile, O_RDONLY)) == -1)
    {
        fprintf(stderr, "open(%s): %s\n", inFile, strerror(errno));
        return 1;
    }
    for (; ;)
    {
        if ((Len = read(inFD, (void *)buffer, BUFSIZE)) == -1)
        {
            ret = 1;
            fprintf(stderr, "read: %s\n", strerror(errno));
            break;
        }
        if (Len == 0)
            break;
        if ((Ret = write(outFD, (void *)buffer, Len)) < Len)
        {
            ret = 1;
            fprintf(stderr, "write: %s\n", strerror(errno));
            break;
        }
    }
    close(inFD);
    return ret;
}

int main(int argc, char *argv[])
{
    UpgradeImgHdr imgHdr;
    time_t tNow;
    struct tm *tmPtr;
    int outFd;
    if (argc < 7)
    {
        Usage(argv[0]);
        exit(1);
    }
    if (parseCmdLineArgs(argc, argv) != 0)
        exit(1);
    if (validateInput() != 0 )
        exit(1);
    printf("OK. Input validated fine.\n");
    // prepare the header now.
    memcpy(imgHdr.magicStr, MAGIC_STR_SS2, MAGIC_LEN);
    imgHdr.sysId     = IMAGE_SMARTSET2_TYPE | pImgType;
    imgHdr.majorNo    = pMajorNo;
    imgHdr.minorNo    = pMinorNo;
    imgHdr.patchLevel = pPatchLev;
    imgHdr.buildNo    = pBuildNo;
    imgHdr.bldType   = pBldType;
    if ((usrDayStr) && (usrTimeStr))
    {
        imgHdr.bldDate.year  = g_dayVals[2];
        imgHdr.bldDate.month = g_dayVals[0];
        imgHdr.bldDate.day   = g_dayVals[1];
        imgHdr.bldDate.hour  = g_timeVals[0];
        imgHdr.bldDate.min   = g_timeVals[1];
        imgHdr.bldDate.sec   = g_timeVals[2];
    }
    else
    {
        tNow = time(0);
        if ((tmPtr = localtime(&tNow)) != 0)
        {
            imgHdr.bldDate.year  = 1900 + tmPtr->tm_year;
            imgHdr.bldDate.month = (tmPtr->tm_mon+1);
            imgHdr.bldDate.day   = tmPtr->tm_mday;
            imgHdr.bldDate.hour  = tmPtr->tm_hour;
            imgHdr.bldDate.min   = tmPtr->tm_min; 
            imgHdr.bldDate.sec   = tmPtr->tm_sec; 
        }
        else
        {
            fprintf(stderr, "Error: failed to get current time\n");
            exit(1);
        }
    }
    printf("Header Info:\n");
    printf("  sysId: %x  bldVer:%hu.%hu.%hu.%hu   bldType %x\n", 
                    imgHdr.sysId, imgHdr.majorNo, imgHdr.minorNo, imgHdr.patchLevel,
                    imgHdr.buildNo,imgHdr.bldType);
    printf("  %04d:%02d:%02d:%02d:%02d:%02d\n",
                        imgHdr.bldDate.year, imgHdr.bldDate.month, imgHdr.bldDate.day,
                        imgHdr.bldDate.hour, imgHdr.bldDate.min, imgHdr.bldDate.sec); 

    // open output file and write upgrade archive header
    if ((outFd = open(usrOutFile, O_RDWR|O_CREAT|O_TRUNC, 
                                        S_IRWXU|S_IRGRP|S_IROTH)) == -1)
    {
        fprintf(stderr, "Error: open(%s): %s\n", usrOutFile, strerror(errno));
        exit(1);
    }
    if (write(outFd, (void *)&imgHdr, sizeof(imgHdr)) != sizeof(imgHdr))
    {
        fprintf(stderr, "Error: write header(%s), %s\n", usrOutFile, strerror(errno));
        close(outFd);
        exit(1);
    }
    // append input file to this one
    if (appendFile(outFd, usrInpFile) != 0)
    {
        close(outFd);
        exit(1);
    }
    // compute checksum and update csum field
    if (updateFileCsum(outFd) != 0)
    {
        close(outFd);
        exit(1);
    }
    close(outFd);
    exit(0);
}
