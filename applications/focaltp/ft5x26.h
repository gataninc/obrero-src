#ifndef _FT5X26_H_
#define _FT5X26_H_
/*
 * Jul 18, 2022: Modified to support both Old & New 1905 Displays - PP
*/

// BING specific
/* offsets to GPIO registers, needed to configure GPIO0 as interrupt */
#define GPIO0_OFFSET     0x1100

/* physical memory address of FPGA register space */
#define FPGA_REG_ADDR  0xE5000000

#define   RESET_REG    (6)     // offset to reset register
#define   FPGA_RST_TP  (0x20)  // write this to reg. to reset tp

typedef struct gpio_reg_set
{
    volatile u32 gpDIR;
    volatile u32 gpODR;
    volatile u32 gpDAT;
    volatile u32 gpIER;
    volatile u32 gpIMR;
    volatile u32 gpICR1;
    volatile u32 gpICR2;
} gpio_regs;

// Focal Tech
#define  SCREEN_MAX_X      (1024)
#define  SCREEN_MAX_Y      (600)
#define  PRESS_MAX         (255)

#define FT5X26_REG_CHIPID      (0xa3)
#define FT5X26_REG_FIRMID      (0xa6)

#define FT_NUM_TP          (1)
#define FT_META_REGS       (3)
#define FT_ONE_TCH_LEN     (6)
#define FT_TCH_LEN         (FT_META_REGS + FT_ONE_TCH_LEN * FT_NUM_TP)
#define FT_PRESS           0x7F 
#define FT_MAX_ID          0x0F 

// offsets in event register
#define FT_TD_STATUS_POS   2  
#define FT_TOUCH_FLAG_POS  3  // MSB 2 bits
#define FT_TOUCH_ID_POS    5  // MSB 4 nibbles
#define FT_TOUCH_X_H_POS   3  // LSB 4 nibbles
#define FT_TOUCH_X_L_POS   4 
#define FT_TOUCH_Y_H_POS   5  // LSB 4 nibbles
#define FT_TOUCH_Y_L_POS   6 

// status masks
#define FT_TOUCH_DOWN      0 
#define FT_TOUCH_CONTACT   2 
// number of touches mask
#define FT_STATUS_NUM_TP_MASK   0x0F 
// touch flag mask, 2 MS bits
#define FT_TOUCH_FLAG_MASK      0xC0 
// END FOCAL-TECH defines

// driver data structures
typedef struct _touch_event
{
    uint32_t x;
    uint32_t y;
    uint32_t flag;
    uint32_t id;
    uint32_t num_touches;
} touch_event;

struct ft5x26_ts_data {
	struct input_dev	    *input_dev;
    // interrupt processing
	struct work_struct 	    touch_event_work;
	struct workqueue_struct *ts_workqueue;
    // device check timer
	struct work_struct 	    dev_check_work;
	struct workqueue_struct *dev_check_q;
    struct timer_list       dev_check_timer;
    // input event
    touch_event             ts_event;
};

typedef struct 
{
    dev_t tp_dev_no;
    struct cdev char_dev;
} ft5x26_char_dev;


// ioctls: same as fusion for application compatiblity

/* unused ioctl magic number, at least now.. */
#define TPDEV_IOCTL_MAGIC 0xC5

// return number touch screen resets
#define TPDEV_GET_TPRSTCNT    _IOR(TPDEV_IOCTL_MAGIC, 1, int)
// return whether user-land need to do a reset.
#define TPDEV_GET_TPRSTSTAT   _IOR(TPDEV_IOCTL_MAGIC, 2, int)
// set tp reset dbg flag
#define TPDEV_SET_DBGFLAG     _IOW(TPDEV_IOCTL_MAGIC, 3, int)
// set tp reset trace flag
#define TPDEV_SET_TRACEFLAG   _IOW(TPDEV_IOCTL_MAGIC, 4, int)
// set tp reset reset delay
#define TPDEV_SET_RESET_DELAY _IOW(TPDEV_IOCTL_MAGIC, 5, int)

#define TPDEV_IOCTL_MAX  5

#endif // _FT5X26_H_
