/*
 * System monitoring task.
 * Read register 0 of status register and check following bits:
 *   BING_OVERHEATING: b07 
 *      Report over-heating error when bit goes high
 *   BING_HEAT_OUT_ENABLED: b09 
 *      Report over-voltage error when bit goes low
 * When CHILLER_IP is enabled in personality:
 *   BING_AUX_DIG_IN_2: b01 
 *      Report flow error when bit goes low
 *
 * Also monitor following:
 *  Read voltage and current from system. Compute power. Check limits 
 *  against vMax, iMax and pMax values. If any exceeds and stays in this state
 *  for  a duration, report over-X error.
 *
 *  Some of the operations need to be done ONLY when output is enabled, ie.
 *  when h-adc reads back > 0 OR when PID loop is active.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <syslog.h>
#include <time.h>
#include <sys/time.h>
#include "bing_common.h"
#include "sharedmem.h"
#include "regs.h"
#include "utils.h"
#include "i2cutils.h"
#include "sysutils.h"

volatile sysParams *pCache;

#define BING_AUX_DIG_IN2      (0x2)
#define BING_HEAT_ILIMIT      (0x40)  // is this HEAT_ISO_HI_???
#define BING_OVERHEATING      (0x80)
#define BING_HEAT_OUT_ENABLED (0x200)

// Error flags, keeping it global to reduce stack space used by function..
static int errOverV = 0;
static int errHeaterOP = 0;
static int errOverTemp = 0;
static int errCoolantFlow = 0;
static int errVMax = 0;
static int errIMax = 0;
static int errPMax = 0;
struct timeval t0, t1;

#define TADC_HALF_VAL (0x7FFFFF)
#define TADC_DIV      (8388607-1) // 2^23-1
#define TADC_VREF     (2.50)
double tADCToV(int32_t adcVal)
{
    int neg = 0;
    uint32_t tRegVal;
    double v;
 
    tRegVal = (adcVal & 0xFFFFFF);
    if (tRegVal > TADC_HALF_VAL)
    {
        neg = 1;
        tRegVal -= TADC_HALF_VAL;
    }
    v = (TADC_VREF / TADC_DIV) * tRegVal;
    if (neg)
        v = (TADC_VREF - v) * -1.0;
    return v;
}

static int doSysMonitor()
{
    uint16_t statReg;
    int sRet = 0;
    int vRet = 0;
    float V, I;
    uint32_t rawI, rawV;
    int32_t tADCVal;
    double tADCV, unScaledV;
    int heatingEnabled = 0;
    int sType, chan;
    double heaterR;
    double sensorR;
    for (; ;)
    {
        mySleep(241);
        if (pCache->sensorType[0] != 0) // a valid sensor is set
        {
            if (lockCtrlReg() != 0)
                continue;  // retry in a short time..
            gettimeofday(&t0, 0);
            // get all info that is needed for monitoring..
            // read status registers
            sRet = readStatRegFull(0, 1, &statReg);
            // read voltage & current
            vRet = getHeaterValues(&V, &rawV, &I, &rawI);
            sType = findSensorType(pCache->sensorType[0]);
            if (sType == SensorTypeTC)
                chan = T_ADC_CHAN_TC;
            else
                chan = T_ADC_CHAN_DRTD;
            readT_ADCValues(chan, 5, &tADCVal);
            tADCV = scaleTADCVal(chan, tADCVal);
            unScaledV = tADCToV(tADCVal);
            gettimeofday(&t1, 0);
            unlockCtrlReg();
            if (vRet == 0)
                heatingEnabled = ((pCache->pidEnable) || (V > 0.10));
            else
                heatingEnabled = pCache->pidEnable;
            // check heater open, heater short, sensor open and sensor short..
            // v=ir
            if (heatingEnabled)
            {
                heaterR = V / I;
                sensorR = tADCV / getStimulusI(pCache->stimI[0]);
                if (I < (100.0/1000000.0)) 
                    printf("HEATER OPEN\n");
                if (fabs(heaterR)  < 0.20) // 0.20Ohms
                    printf("HEATER SHORT\n");
                if (fabs(unScaledV) > 2.250) 
                    printf("SENSOR OPEN CIRCUIT\n");
                if (fabs(sensorR) < (0.2))  // 0.2Ohms
                    printf("DIODE/PRT: SENSOR SHORT\n");
            }
            {
               uint32_t d = diffTimeVal(&t1, &t0);
               float  f = d/1000.0;
               logPrint("Elapsed: %0.6f, hEnable %d vRet %d\n", f, heatingEnabled, vRet);
               logPrint("  V: %0.6f I: %0.6f tV: %0.6f unScV: %0.6f hR: %0.3f  sR: %0.3f\n" , V, I, tADCV, unScaledV, heaterR, sensorR);
            }
#if 0
            if (sRet == 0)
            {
                //printf("%s: R 0x%X\n", __func__, statReg);
                if ((statReg & BING_HEAT_OUT_ENABLED) == 0)
                {
                    errOverV++;
                    if (errOverV >= 2)
                    {
                        printf("ERROR: Over-voltage [0x%X/0x%X]\n", statReg,
                                        BING_HEAT_OUT_ENABLED);
                        errOverV = 0;
                    }
                }
                else
                    errOverV = 0;
                if (statReg & BING_HEAT_ILIMIT)
                {
                    errHeaterOP++;
                    if (errHeaterOP >= 2)
                    {
                        printf("ERROR: HEATER O/P\n");
                        errHeaterOP = 0;
                    }
                }
                else
                    errHeaterOP = 0;
                if (statReg & BING_OVERHEATING)
                {
                    errOverTemp++;
                    if (errOverTemp >= 2)
                    {
                        printf("ERROR: UNIT OVER-TEMP\n");
                        errOverTemp = 0;
                    }
                }
                else
                    errOverTemp = 0;
                if ((pCache->flowCheck) && (!(statReg & BING_AUX_DIG_IN2)))
                {
                    errCoolantFlow++;
                    if (errCoolantFlow >= 2)
                    {
                        printf("ERROR: COOLANT FLOW\n");
                        errCoolantFlow = 0;
                    }
                }
                else
                    errCoolantFlow = 0;
            }
            else
            {
                errOverV = errHeaterOP = errOverTemp = errCoolantFlow = 0;
            }
            if (!heatingEnabled)
            {
                //errOverV = errHeaterOP = errOverTemp = errCoolantFlow = 0;
                errVMax = errIMax = errPMax = 0;
                continue;
            }
            //printf("%s: V %0.5f I %0.5f\n", __func__, V, I);
            if ((pCache->vMax > 0.01) && (V > pCache->vMax))
            {
                errVMax++;
                if (errVMax >= 2)
                {
                    printf("ERROR: voltage > vMax(%0.5f > %0.5f)\n",
                                                        V, pCache->vMax);
                    errVMax = 0;
                }
            }
            else
                errVMax = 0;
            if ((pCache->iMax > 0.01) && (I > pCache->iMax))
            {
                errIMax++;
                if (errIMax >= 2)
                {
                    printf("ERROR: current > iMax(%0.5f > %0.5f)\n",
                                                        I, pCache->iMax);
                    errIMax = 0;
                }
            }
            else
                errIMax = 0;
            if ((pCache->pMax > 0.01) && ((I*V) > pCache->pMax))
            {
                errPMax++;
                if (errPMax >= 2)
                {
                    printf("ERROR: power > pMax(%0.5f > %0.5f)\n",
                                                        (V*I), pCache->pMax);
                    errPMax = 0;
                }
            }
            else
                errPMax = 0;
#endif
        }
    }
    return 0;
}

int main()
{
    if (initLib() != 0)
    {
        syslog(LOG_ERR, "Failed to init library!");
        logPrint("Error: failed to init library!!\n");
        exit(1);
    }
    if ((pCache = getSharedMemory(IPC_KEY_SHAREDMEM, sizeof(sysParams))) == 0)
    {
        syslog(LOG_ERR, "Error attaching shared memory");
        logPrint("Error attaching shared memory\n");
        exit(1);
    }
    syslog(LOG_NOTICE, "Monitoring task started");
    doSysMonitor();
    syslog(LOG_ERR, "END Monitoring task task");
    return 0;
}
