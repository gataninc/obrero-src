#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>
#include <math.h>
#include <byteswap.h>
#include "bing_common.h"
#include "sharedmem.h"
#include "fpga.h"
#include "regs.h"
#include "utils.h"
#include "i2cutils.h"
#include "sysutils.h"

extern volatile sysParams *pCache;

// NOICE: MUST BE holding register lock when calling this function.
int setVoltageRange(int vMax)
{
    uint8_t bitVal = 0x0;
    uint32_t offset;
    uint16_t regSpace[sizeof(bingMachineCtrl)];
    bingMachineCtrl *machCtrl = (bingMachineCtrl *)regSpace;
    switch (vMax)
    {
        case 40:
            bitVal = 0x0;
            break;
        case 20:
            bitVal = 0x40;
            break;
        case 10:
            bitVal = 0x80;
            break;
        case 0:
            bitVal = 0xC0;
            break;
        default:
        logPrint("Error: invalid range value\n");
        return -1;
    }
    offset = 1;  // control reg 1
    readMachCtrlRegs(machCtrl, ((offset+1)*2));
    // clear the bits first
    regSpace[offset] &= ~(0xC0 << 8); // lower byte
    regSpace[offset] |= (bitVal << 8); // lower byte
    updateMachCtrlRegs(machCtrl, ((offset+1)*2));
    updateSync(SYNC_REGS);
    return 0;
}

int setIRange(int range)
{
    uint8_t bitVal = 0x0;
    uint32_t offset;
    uint16_t regSpace[sizeof(bingMachineCtrl)];
    bingMachineCtrl *machCtrl = (bingMachineCtrl *)regSpace;
    switch (range)
    {
        case HEAT_IRANGE_2A:
            bitVal = 0;
            break;
        case HEAT_IRANGE_200mA:
            bitVal = 8;
            break;
        default:
            logPrint("Error: invalid range for I\n");
            return -1;
    }
    offset = 1;
    readMachCtrlRegs(machCtrl, ((offset+1)*2));
    // clear bit
    regSpace[offset] &= ~(0x8 << 8); // lower byte
    regSpace[offset] |= (bitVal << 8);
    updateMachCtrlRegs(machCtrl, ((offset+1)*2));
    updateSync(SYNC_REGS);
    return 0;
}

// NOICE: MUST BE holding register lock when calling this function.
int setGroundingMode(int gndMode)
{
    uint8_t bitVal = 0x0;
    uint32_t offset;
    uint16_t regSpace[sizeof(bingMachineCtrl)];
    bingMachineCtrl *machCtrl = (bingMachineCtrl *)regSpace;
    if (gndMode ==  GndModeFloating)
        bitVal = 0x0;
    else if (gndMode == GndModeSocket)
        bitVal = 0x2;
    else if (gndMode == GndModeMains)
        bitVal = 0x4;
    else if (gndMode == GndModeBuffered)
        bitVal = 0x6;
    else
    {
        logPrint("Error: invalid option\n");
        return -1;
    }
    offset = 1;  // control reg 1
    readMachCtrlRegs(machCtrl, ((offset+1)*2));
    // clear the bits first
    regSpace[offset] &= ~(0x6 << 8); // lower byte
    regSpace[offset] |= (bitVal << 8); // lower byte
    updateMachCtrlRegs(machCtrl, ((offset+1)*2));
    updateSync(SYNC_REGS);
    return 0;
}

// do not read register, but use the value passed in
float getMaxVRangeQuick(uint8_t regVal)
{
    float val = 0.0;
    switch (regVal & 0xC0)
    {
        case 0:
            val  = 40.0;
            break;
        case 0x40:
            val  = 20.0;
            break;
        case 0x80:
            val  = 10.0;
            break;
        case 0xC0:
            val  = 0.0;
            break;
    }
    return val;
}

// NOICE: MUST BE holding register lock when calling this function.
float getMaxVRange()
{
    float val = 0;
    uint8_t bitVal;
    int offset = 1;
    if (readCtrlRegL(offset, 1, &bitVal) != 0)
    {
        printf("Error: read machine ctrl register\n");
        return -1.0;
    }
    switch (bitVal & 0xC0)
    {
        case 0:
            val  = 40.0;
            break;
        case 0x40:
            val  = 20.0;
            break;
        case 0x80:
            val  = 10.0;
            break;
        case 0xC0:
            val  = 0.0;
            break;
    }
    return val;
}

float getHeaterADCRange()
{
    return (40.8 + 0.413); // range -0.413...40.8V
}

float getHeaterDACRangeQuick(uint8_t regVal)
{
    float val = 0;
    switch (regVal & 0xC0)
    {
        case 0:
            val  = 41.93; // range -0.1..41.83V
            break;
        case 0x40:
            val  = 20.93; // range -0.1..20.83V
            break;
        case 0x80:
            val  = 10.43; // range -0.1..10.33V
            break;
        case 0xC0:
            val  = 0;
            break;
    }
    return val;
}

// return DAC range for  current voltage range setting
// NOICE: MUST BE holding register lock when calling this function.
float getHeaterDACRange()
{
    float val = 0;
    uint8_t bitVal;
    int offset = 1;
    if (readCtrlRegL(offset, 1, &bitVal) != 0)
    {
        printf("Error: read machine ctrl register\n");
        return -1.0;
    }
    switch (bitVal & 0xC0)
    {
        case 0:
            val  = 41.93; // range -0.1..41.83V
            break;
        case 0x40:
            val  = 20.93; // range -0.1..20.83V
            break;
        case 0x80:
            val  = 10.43; // range -0.1..10.33V
            break;
        case 0xC0:
            val  = 0;
            break;
    }
    return val;
}

// MUST BE holding control register lock..
int setHeaterDAC(float v)
{
    uint16_t regVal;
    float vMax;
    float rangeMax;
    rangeMax = getHeaterDACRange();
    vMax = getMaxVRange();
    if ((v < 0.0) || (v > vMax))
    {
        printf("Error: invalid setting, cur. range: 0 - %0.3f V\n", vMax);
        return -1;
    }
    regVal = (uint16_t)((0xFFFF/rangeMax) * v);
    regVal = bswap_16(regVal);
    writeCtrlRegStrobe(8, 2, (void *)&regVal, 0, 4);
    return 0;
}

int setHeaterDACRegVal(uint16_t regVal)
{
    regVal = bswap_16(regVal);
    writeCtrlRegStrobe(8, 2, (void *)&regVal, 0, 4);
    return 0;
}

#define MAX_I_RANGE  (2.2+0.00219)
int getHeaterValues(float *V, uint32_t *rawV, float *I, uint32_t *rawI)
{
    float vMax;
    uint8_t adcRegs[4]; // voltage and current together
    uint16_t rawVal;

    readStatReg(15, 4, &adcRegs[0]);
    // Voltage
    rawVal = (adcRegs[3] << 8) | adcRegs[2];
    vMax = getHeaterADCRange();
    *V = ((rawVal/65535.0)*vMax);
    *rawV = rawVal;
    // Current
    rawVal = (adcRegs[1] << 8) | adcRegs[0];
    *I = ((rawVal/65535.0)*MAX_I_RANGE);
    *rawI = rawVal;
    return 0;
}

// get average heater value for calibration
#define HEAT_AVG_COUNT (1000)
int getHeaterAvgRaw(int count, uint16_t *avgR)
{
    uint8_t adcRegs[4]; // voltage and current together
    uint32_t sumVal = 0;
    uint16_t rawVal;
    int i;

    if ((count < 0) || (count > HEAT_AVG_COUNT))
        return -1;
    for (i = 0; i < count; i++)
    {
        readStatReg(15, 4, &adcRegs[0]);
        // raw Voltage
        rawVal = (adcRegs[3] << 8) | adcRegs[2];
        sumVal += rawVal;
        usleep(50);
    }
    rawVal = sumVal / count;
    *avgR = rawVal;
    return 0;
}

int getHeaterAvg(int count, double *avgV)
{
    float vMax;
    uint8_t adcRegs[4]; // voltage and current together
    uint32_t sumVal = 0;
    uint16_t rawVal;
    int i;

    if ((count < 0) || (count > HEAT_AVG_COUNT))
        return -1;
    for (i = 0; i < count; i++)
    {
        readStatReg(15, 4, &adcRegs[0]);
        // raw Voltage
        rawVal = (adcRegs[3] << 8) | adcRegs[2];
        sumVal += rawVal;
        usleep(50);
    }
    rawVal = sumVal / count;
    // compute voltage now..
    vMax = getHeaterADCRange();
    *avgV = ((rawVal/65535.0)*vMax);
    return 0;
}


/*
 * Read T-ADC count number of times and return values in regVals.
 */
int readT_ADCValues(int chan, int count, int32_t *regVals)
{
    uint8_t statBuf[sizeof(bingMachStatus)];
    bingMachStatus *machStat;
    machStat = (bingMachStatus *)(&statBuf[0]);
    int offset = 15 * 2; // T_ADC_OUT3 + 1
    int i;
    if ((chan < 1) || (chan > 3))
        return -1;
    if ((count < 0) || (count > 100000))
        return -1;
    for (i = 0; i < count; i++)
    {
        //setCtrlRegBits(0, 0x4, 0x4);
        updateSync(SYNC_REGS);
        readMachStatRegs(machStat, offset);
        if (chan == 1)
            regVals[i] = (statBuf[16] << 16)|(statBuf[14] << 8)|statBuf[12];
        else if (chan == 2)
            regVals[i] = (statBuf[22] << 16)|(statBuf[20] << 8)|statBuf[18];
        else
            regVals[i] = (statBuf[28] << 16)|(statBuf[26] << 8)|statBuf[24];
        //setCtrlRegBits(0, 0x4, 0x0);
        usleep(20);
    }
    return 0;
}

extern int mcp4728Write(int bus, int devAddr, int chan, int gain, float v);
static int setTrimVoltages(int sel)
{
    int ret = 0;
    double trimV;
    switch (sel)
    {
        case STIM_1uA:
            trimV = pCache->stimTrim1u_p;
            break;
        case STIM_1uA_N:
            trimV = pCache->stimTrim1u_n;
            break;
        case STIM_10uA:
            trimV = pCache->stimTrim10u_p;
            break;
        case STIM_10uA_N:
            trimV = pCache->stimTrim10u_n;
            break;
        case STIM_100uA:
            trimV = pCache->stimTrim100u_p;
            break;
        case STIM_100uA_N:
            trimV = pCache->stimTrim100u_n;
            break;
        case STIM_0uA:
        default:
            return 0;
            break;
    }
    if ((trimV >= MIN_TRIM_VOLTS) && (trimV <= MAX_TRIM_VOLTS))
    {
        int bus = 2, devAddr = 0x61;
        int muxChan = 1;
        int ret = 0, i;
        for (i = 0; i < 3; i++)
        {
            if (lockI2CBus(bus) == 0)
                break;
            usleep(20000);
        }
        if (i >= 3)
        {
            logPrint("Error: failed to acquire I2C lock\n");
            return -1;
        }
        if ((ret = i2cmux_select_chan(muxChan)) != 0)
        {
            logPrint("Error: failed to set mux channel\n");
            unlockI2CBus(bus);
            return -1;
        }
        logPrint("Trim V: %0.8f\n", trimV);
        ret = mcp4728Write(bus, devAddr, 1, 2, trimV);
        usleep(5000);
        ret = mcp4728Write(bus, devAddr, 2, 2, trimV);
        unlockI2CBus(bus);
    }
    return ret;
}

int setStimulusCurrent(int sel, int setV)
{
    uint8_t rBits = 0;
    switch (sel)
    {
        case STIM_0uA:
            rBits = 0x0;
            break;
        case STIM_1uA:
            rBits = 0xC0;
            break;
        case STIM_10uA:
            rBits = 0x80;
            break;
        case STIM_100uA:
            rBits = 0x40;
            break;
        case STIM_1uA_N:
            rBits = 0xE0;
            break;
        case STIM_10uA_N:
            rBits = 0xA0;
            break;
        case STIM_100uA_N:
            rBits = 0x60;
            break;
        default:
            return -1;
    }
    setCtrlRegBits(2, 0xE0, rBits); // isel1:0:pol
    if (setV)
        return setTrimVoltages(sel);
    return 0;
}

double getStimulusI(int stimI)
{
    double I = 0.0;
    switch (stimI)
    {
        case STIM_0uA:
            I = 0.0;
            break;
        case STIM_1uA:
            I = (1.0 / 1e6);
            break;
        case STIM_10uA:
            I = (10.0 / 1e6);
            break;
        case STIM_100uA:
            I = (100.0 / 1e6);
            break;
    }
    return I;
}

int setDRTDGain(int sel)
{
    uint8_t rBits = 0;
    switch (sel)
    {
        case GAIN_X1:
            rBits = 0x0;
            break;
        case GAIN_X21:
            rBits = 0x8;
            break;
        case GAIN_X10:
            rBits = 0x10;
            break;
        case GAIN_X210:
            rBits = 0x18;
            break;
        default:
            return -1;
    }
    setCtrlRegBits(2, 0x18, rBits);
    return 0;
}

int setDRTDCalSel1(int sel)
{
    uint8_t rBits = 0;
    switch (sel)
    {
        case DRTD_CAL_NORM:
            rBits = 0;
            break;
        case DRTD_CAL_R100:
            rBits = 0x1;
            break;
        case DRTD_CAL_R1K:
            rBits = 0x2;
            break;
        case DRTD_CAL_ZERO:
            rBits = 0x3;
            break;
        default:
            return -1;
    }
    setCtrlRegBits(2, 0x3, rBits);
    return 0;
}

int setDRTDCalSel2(int sel)
{
    uint8_t rBits = 0;
    switch (sel)
    {
        case DRTD_CAL2_NORM:
            rBits = 0x0;
            break;
        case DRTD_CAL2_CAL:
            rBits = 0x4;
            break;
        default:
            return -1;
    }
    setCtrlRegBits(2, 0x4, rBits);
    return 0;
}

int findSensorType(int sensor)
{
    if ((sensor == PRT) || (sensor == DT_470) || (sensor == DT_670))
        return SensorTypeDiode;
    if ((sensor == Type_K) || (sensor == Type_R) || (sensor == Type_T))
        return SensorTypeTC;
    return SensorTypeNone;
}

#define TADC_HALF_VAL (0x7FFFFF)
#define TADC_DIV      (8388607-1) // 2^23-1
#define TADC_VREF     (2.50)
double scaleTADCVal(int chan, int32_t adcVal)
{
    int neg = 0;
    uint32_t tRegVal;
    double v;
 
    tRegVal = (adcVal & 0xFFFFFF);
    if (tRegVal > TADC_HALF_VAL)
    {
        neg = 1;
        tRegVal -= TADC_HALF_VAL;
    }
    v = (TADC_VREF / TADC_DIV) * tRegVal;
    if (neg)
        v = (TADC_VREF - v) * -1.0;
    if (chan == T_ADC_CHAN_TC)
    {
        v -= (pCache->tcVZero + pCache->tcVMiss);
        v /= pCache->tcGTC; // thermocouple: value is scaled
    }
    else if (chan == T_ADC_CHAN_DRTD)
    {
        v /= pCache->G1drtd;          // diode/prt: account for 100/101 divider
        v -= pCache->vG1Zero;
    }
    return v;
}

// scale to 2.5V, do not apply sensor scaling.
double scaleTADCVal2p5V(int32_t adcVal)
{
    int neg = 0;
    uint32_t tRegVal;
    double v;
 
    tRegVal = (adcVal & 0xFFFFFF);
    if (tRegVal > TADC_HALF_VAL)
    {
        neg = 1;
        tRegVal -= TADC_HALF_VAL;
    }
    v = (TADC_VREF / TADC_DIV) * tRegVal;
    if (neg)
        v = (TADC_VREF - v) * -1.0;
    return v;
}

// Read Accumulated (AC main synced) ADC readings from FPGA and compute voltage
#define ADC_SUM_SIGN_BIT   ((int64_t)(0x1) << 35) // bit 36 is the sign bit
#define ADC_SUM_OVFL_BIT   ((int64_t)(0x1) << 34) // bit 35 is the overflow bit
#define ADC_SUM_BITS       ((int64_t)(0x7FFFFFFFF)) // 35bits

// !!! MUST be holding register lock !!!
// accIdx: Accumulation reg index. Main acc. reg: 0, alt0 acc. reg: 1
// default 0, AccIdx: works only with (fpga r.0x50 + mcode r.0xb) and above.
int getAdcFpgaAvg(int accIdx, int32_t *avg)
{
    int rdSz = 6;
    uint16_t adcVals[rdSz];
    int offset = 56;
    int64_t sumADC;
    int64_t tmpSum, finSum;
    short numSamples;
    int i;
    *avg = 0;
    if (accIdx == 1)
        offset = 64;
    for (i = 0; i < 3; i++)
    {
        // READ register first
        readStatRegFull(offset, rdSz, adcVals);
        sumADC = ((((int64_t)(adcVals[4]) & 0x0f) << 32) | 
                  (((int64_t)(adcVals[3]) & 0xff) << 24) | 
                  (((int64_t)(adcVals[2]) & 0xff) << 16) | 
                  ((adcVals[1] & 0xff) << 8) | (adcVals[0] & 0xff));
        numSamples = (int16_t)adcVals[5];
        if (((numSamples >= MIN_SAMPLES_60HZ) && (numSamples <= MAX_SAMPLES_60HZ)) ||// US A/C @60Hz
            ((numSamples >= MIN_SAMPLES_50HZ) && (numSamples <= MAX_SAMPLES_50HZ)))  // UK A/C @50Hz
            break;
        usleep(21000);
    }
    if (i >= 3)
    {
        printf("NumSamples ERR [%d]\n", numSamples);
        printf("== Mains (power line) synchronisation failed\n ==");fflush(stderr);
        return -1;
    }
    //printf("sumADC %llx ", sumADC, ADC_SUM_SIGN_BIT);
    if (sumADC & ADC_SUM_SIGN_BIT)
    {
        //printf("-ve, ");
        tmpSum = ((~(sumADC & ADC_SUM_BITS)) + 1);
        finSum = tmpSum & ADC_SUM_BITS;
        //printf("finSum %llX -> ", finSum);
        if (finSum)
            finSum *= -1;
        else
            finSum = 0xFFFFFFF800000000; //make it largest -ve number
    }
    else
        finSum = sumADC;
    (*avg) = (int32_t) (finSum / (int)numSamples);
    (*avg) &= 0xFFFFFF;
    //printf("finSum %lld[%llX], samples: %hu avg: 0x%X\n", 
    //                            finSum, finSum, numSamples, *avg);
    return (int)numSamples;
}

// compute T-ADC average over 'numMainsCycles' and return voltage
int getAvgTADCFpgaVoltage(int chan, int numMainsCycles, int scale2p5V, 
                                    double *V)
{
    int64_t sum = 0;
    int32_t val, avg;
    int i;
    if ((numMainsCycles < 1) || (numMainsCycles > 250))
        return -1;
    for (i = 0; i < numMainsCycles; i++)
    {
        if (getAdcFpgaAvg(0, &val) > 0) // always main acc. reg
            sum += val;
        else
            break;
    }
    if (i < numMainsCycles)
        return -1;
    if (numMainsCycles > 1)
        avg = sum / numMainsCycles;
    else
        avg = sum;
    if (scale2p5V)
        *V = scaleTADCVal2p5V(avg);
    else
        *V = scaleTADCVal(chan, avg);
//logPrint("chan: %d, avg %d[%X], v: %0.6f\n", chan, avg, avg, *V);
    return 0;
}

// do sign bit manipulation etc.
int32_t computeAccADCAvg(int64_t sumADC, int numSamples)
{
    int64_t tmpSum, finSum;
    int32_t avg;
    if (sumADC & ADC_SUM_SIGN_BIT)
    {
        //printf("-ve, ");
        tmpSum = ((~(sumADC & ADC_SUM_BITS)) + 1);
        finSum = tmpSum & ADC_SUM_BITS;
        //printf("finSum %llX -> ", finSum);
        if (finSum)
            finSum *= -1;
        else
            finSum = 0xFFFFFFF800000000; //make it largest 36 bit -ve number
    }
    else
        finSum = sumADC;
    avg = (int32_t) (finSum / (int)numSamples);
    avg = (avg & 0xFFFFFF);
    //printf("finSum %lld[%llX], samples: %hu avg: 0x%X\n", 
    //                            finSum, finSum, numSamples, *avg);
    return avg;
}

#define TADC_HALF_VAL (0x7FFFFF)
#define TADC_DIV      (8388607-1) // 2^23-1
#define TADC_VREF     (2.50)
void computeTADCVoltage(uint32_t rVal, double *fVal)
{
    int neg = 0;
    uint32_t tRegVal;
    tRegVal = rVal & 0xFFFFFF;
    if (tRegVal > TADC_HALF_VAL)
    {
        neg = 1;
        tRegVal -= TADC_HALF_VAL;
    }
    *fVal = (TADC_VREF / TADC_DIV) * tRegVal;
    if (neg)
        *fVal = (TADC_VREF - (*fVal)) * -1.0;
}

#define AVG_MAX_SAMPLES (100000)
static int32_t avgAdcVals[AVG_MAX_SAMPLES];
int getAvgTADCVolt(int chan, int count, int scale2p5V, int filter, double *V)
{
    int64_t sum;
    uint32_t rVals;
    double fVals;
    int skips = 0;
    int i;
    if (count > AVG_MAX_SAMPLES)
        return -1;
    readT_ADCValues(chan, count, avgAdcVals);
    // sign extend the 24-bit signed values to 4byte signed integer.
    for (i = 0; i < count; i++)
    {
        if ((uint32_t)avgAdcVals[i] > 0x7FFFFF)
            avgAdcVals[i] |= 0xFF000000;
    }
    sum = 0;
    if (count == 1)
        rVals = avgAdcVals[0];
    else
    {
    // find average of avgAdcVals[] and compute voltage based on the avg.
    // filter outliers..
    {
        int j, k, netCnt = 0;
        for (i = 0; i < count; i++)
        {
            //printf("ADC rd %d: 0x%X\n", i, avgAdcVals[i]);
            if (filter)
            {
                if (i == 0)
                    { j = 1; k = 2; }
                else if (i == (count -1))
                    { j = (count - 2); k = (count - 3); }
                else
                    { j = i-1; k = i+1; }
                if ((abs(avgAdcVals[i] - avgAdcVals[j]) > 0x4FFF) &&
                    (abs(avgAdcVals[i] - avgAdcVals[k]) > 0x4FFF))
                {
                    skips++;
                    continue;
                }
            }
            sum += avgAdcVals[i];
            netCnt++;
        }
        if (skips > 0)
            printf("   SKIPPED %d\n", skips);
        //printf("Sum %d, Count: %d\n", sum, netCnt);
        rVals  = (uint32_t)(sum / (int64_t)netCnt);
        if ((uint32_t)rVals > 0x7FFFFF)
            rVals &= 0xFFFFFF;
        //printf("Sum %lld, Count: %d rVals %X[%d]\n", sum, netCnt, 
        //                rVals[chan-1], rVals[chan-1]);
    }
    }
    if ((uint32_t)rVals > 0x7FFFFF)
        rVals &= 0xFFFFFF;
    if (scale2p5V)
        fVals = scaleTADCVal2p5V(rVals);
    else
        fVals = scaleTADCVal(chan, rVals);
    *V = fVals;
    return 0;
}

int tryLockI2C(int bus)
{
    int i;
    for (i = 0; i < 2; i++)
    {
        if (lockI2CBus(bus) == 0)
            break;
        usleep(10000);
    }
    if (i >= 2)
        return -1;
    return 0;
}

// defined in mcp3424.c
int mcp3424ReadInit(int bus, int devAddr, int chan, int gain);
int mcp3424ReadValue(int bus, int devAddr, int chan, int gain, float *val);
int mcp3424Read(int bus, int addr, int chan, int gain, float *val)
{
    int muxChan = 2;
    unsigned long long tStart, tEnd;
    int ret, i, convTm, loopCnt = 25;
//unsigned long long t1, t2, t3, t4;
//t1=getTimeInMs();
    if (tryLockI2C(bus) != 0)
    {
        logPrint("Error: failed to acquire I2C lock\n");
        return -1;
    }
    if ((ret = i2cmux_select_chan(muxChan)) != 0)
    {
        logPrint("Error: failed to set mux channel\n");
        unlockI2CBus(bus);
        return -1;
    }
    convTm = mcp3424ReadInit(bus, addr, chan, gain); 
    tStart = getTimeInMs();
    unlockI2CBus(bus);
//t2=getTimeInMs();
    if (convTm <= 0) // returns wait time  in mS
        return -1;
    if (convTm > 50)
        usleep((convTm-50)*1000);
    else
        usleep(10000);
//t3=getTimeInMs();
    if (tryLockI2C(bus) != 0)
    {
        logPrint("Error: failed to acquire I2C lock\n");
        return -1;
    }
    if ((ret = i2cmux_select_chan(muxChan)) != 0)
    {
        logPrint("Error: failed to set mux channel\n");
        unlockI2CBus(bus);
        return -1;
    }
    // Should not have to wait for more than max. 50ms...
    loopCnt = 25;
    for (i = 0; i < loopCnt; i++) 
    {
        tEnd = getTimeInMs();
        if ((tEnd - tStart) >= convTm)
            break;
        usleep(2000);
    }
    if (i >= loopCnt)
        logPrint("%s: loop reached limit..\n", __func__);
    ret = mcp3424ReadValue(bus, addr, chan, gain, val);
    unlockI2CBus(bus);
//t4=getTimeInMs();
//logPrint("%s: convT: %d, noLock: %lld mS, loopWait: %d\n",
//__func__, convTm, (tEnd-tStart), i);
//logPrint("%s: init: %lld mS, read: %lld mS\n",__func__, (t2-t1), (t4-t3));
    return ret;
}

// defined in mcp3424.c
int mcp3424ReadRawVal(int bus, int devAddr, int chan, int gain, int *val);
int mcp3424ReadRaw(int bus, int devAddr, int chan, int gain, int *val)
{
    int muxChan = 2;
    unsigned long long tStart, tEnd;
    int ret, i, convTm, loopCnt = 25;
    if (tryLockI2C(bus) != 0)
    {
        logPrint("Error: failed to acquire I2C lock\n");
        return -1;
    }
    if ((ret = i2cmux_select_chan(muxChan)) != 0)
    {
        logPrint("Error: failed to set mux channel\n");
        unlockI2CBus(bus);
        return -1;
    }
    convTm = mcp3424ReadInit(bus, devAddr, chan, gain); 
    tStart = getTimeInMs();
    unlockI2CBus(bus);
    if (convTm <= 0) // returns wait time  in mS
        return -1;
    if (convTm > 50)
        usleep((convTm-50)*1000);
    else
        usleep(10000);
    if (tryLockI2C(bus) != 0)
    {
        logPrint("Error: failed to acquire I2C lock\n");
        return -1;
    }
    if ((ret = i2cmux_select_chan(muxChan)) != 0)
    {
        logPrint("Error: failed to set mux channel\n");
        unlockI2CBus(bus);
        return -1;
    }
    // Should not have to wait for more than max. 50ms...
    loopCnt = 25;
    for (i = 0; i < loopCnt; i++) 
    {
        tEnd = getTimeInMs();
        if ((tEnd - tStart) >= convTm)
            break;
        usleep(2000);
    }
    if (i >= loopCnt)
        logPrint("%s: loop reached limit..\n", __func__);
//logPrint("%s: convT: %d, noLock: %lld, loopWait: %d\n",
//__func__, convTm, (tEnd-tStart), i);
    ret = mcp3424ReadRawVal(bus, devAddr, chan, gain, val);
    unlockI2CBus(bus);
    return ret;
}

// Code below is from Alistair, modified for Gatan.
// Calculate temperature from resistance using Steinhart-Hart equation
double pa_thermistor_r2c(double r) 
{
    // Constants from http://www.thinksrs.com/downloads/programs/Therm%20Calc/NTCCalibrator/NTCcalculator.htm
    // using datasheet resistance values for NTCLE305E4202SB thermisor at 0C, 50C, 100C
    const double A = 1.143078811E-3;
    const double B = 2.318521710E-4;
    const double C = 0.9649561035E-7;
    double lnr;
    lnr = log(r);
    return (1 / (A + (B * lnr) + (C * lnr * lnr * lnr))) - 273.15;
}

// Calculate temperature from resistance using Steinhart-Hart equation
double pcb_thermistor_r2c(double r) 
{
    // Constants from http://www.thinksrs.com/downloads/programs/Therm%20Calc/NTCCalibrator/NTCcalculator.htm
    // using datasheet resistance values for ERTJ1VG103FA thermisor at 5C, 30C, 60C
    const double A = 0.7933754388E-3;
    const double B = 2.677088632E-4;
    const double C = 1.213739024E-7;
    double lnr;
    lnr = log(r);
    return (1 / (A + (B * lnr) + (C * lnr * lnr * lnr))) - 273.15;
}

double pcb_thermistor_v2r(double v) 
{
    const double F5V = 5.0;       // Supply voltage
    const double R791 = 330.0;    // Thermistor pull-down resistor
    const double R789 = 1.0E6;    // Hysteresis feedback resistor
    const double R787 = 47.0E3;   // Hysteresis series resistor
    double rl; // Total thermistor pull-down resistance (including hysteresis resistors)
    double rt; // Calculated thermistor resistance
    
    rl = 1/((1/(R789+R787))+(1/R791));
    rt = ((F5V*rl)/v)-rl;
    return rt;
}

// Code from Alistair: split into two calls.
// compute Power amplifier temperature
int computePCBTemp(float *pcbV, float *pcbR, float *pcbT)
{
    int bus = 2, devAddr = 0x68;
    int gain = 1;

    // Calculate PCB thermistor resistance from voltage
    if (mcp3424Read(bus, devAddr, 4, gain, pcbV) != 0)
        return -1;
    (*pcbV) /= 1000.0; // convert to V
    *pcbR = (float)pcb_thermistor_v2r((double)(*pcbV));
    *pcbT = (float)pcb_thermistor_r2c((double)(*pcbR));
    return 0;
}

// compute Power amplifier temperature
int computePATemp(float *paV, float *paR, float *paT)
{
    int bus  = 2, devAddr = 0x68;
    int gain = 1;
    // Calculate PA thermistor resistance from voltage
    // TODO: this could be a slightly more accurate calculation if it took into 
    // account R788 properly, rather than just correcting by around 1%

    if (mcp3424Read(bus, devAddr, 3, gain, paV) != 0)
        return -1;
    (*paV) /= 1000.0; // convert to V
    *paR = (5000.0/(*paV))-2000.0; // for R782=1kOhm R790=1kOhm F5V=5V
    // correction factor measured in lab, due to hysterises resistor R788
    (*paR) *= 1.0116; 
    *paT = (float)pa_thermistor_r2c((double)(*paR));
    return 0;
}
// END Code below is from Alistair, modified for Gatan.

/* UART routines */

/*
 * There is only one serial device and it is read only by lower library at
 * startup time. So no locking is needed at this time(02/01/2016).
 */

int checkRxUARTEmpty(int uart)
{
    return 1;
}

int checkTxUARTFull(int uart)
{
    return 0;
}

int writeToUART(int uartNo, uint8_t *str, int len, uint32_t delay)
{
    return -1;
}

int readFromUART(int uartNo, uint32_t delay, char *rdStr)
{
    if (uartNo != UART4FIFO) // only holder EEPROM now
        return -1;
    return libReadFromUART(uartNo, delay, rdStr, RX_FIFO_SZ);
}
/* END UART routines */
