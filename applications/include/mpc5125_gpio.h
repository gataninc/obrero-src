/*************************************************
 *
 * MPC5125 gpio device driver header
 *
 */

#ifndef _GPIO_DEV_H
#define _GPIO_DEV_H

/* Operation codes */
#define GPIO_CLEAR      0x10    /* arg is bit to operate */
#define GPIO_SET        0x20    /* arg is bit to operate */
#define GPIO_FLIP       0x30    /* arg is bit to operate */
#define GPIO_GET        0x40    /* arg is pointer to unsigned int */
#define FPGA_LOAD       0x50

/* Register codes */
#define GPDIR    0x0
#define GPODR    0x1
#define GPDAT    0x2
#define GPIER    0x3
#define GPIMR    0x4
#define GPICR    0x5

/* IOCTL code */
#define GPIO_IOCTL(op,reg)    (op | reg)

/* Test a bit index */
#define GPIO_TEST(val,bit)    ((val >> (31-bit))&0x1)

#endif /* GPIO_DEV_H */
