#! /bin/sh

if [ -z "$1" ]; then
   echo "Specify FPGA file as parameter"
   exit 1
fi

if [ ! -f "$1" ]; then
   echo "Input file not found"
   exit 1
fi

dsh reset stopmcode
fpgaload "$1"
if [ $? -ne 0 ]; then
   echo "Failed to load FPGA file"
   dsh reset startmcode
   exit 1
fi
dsh loadfile /fpga/obrero_mcode.txt
usleep 100000
dsh reset startmcode
