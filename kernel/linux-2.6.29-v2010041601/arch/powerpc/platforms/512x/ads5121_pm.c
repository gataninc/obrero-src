/*
 * Copyright (C) 2008 Freescale Semiconductor, Inc. All rights reserved.
 *
 * Description:
 * This file implements power management for the ADS5121
 *
 * This file is part of the Linux kernel
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 */

#include <linux/init.h>
#include <linux/suspend.h>
#include <linux/of_platform.h>
#include <asm/time.h>
#include <asm/mpc512x.h>
#include <asm/ipic.h>
#include <asm/reg.h>
#include <sysdev/fsl_soc.h>
#include "mpc512x_pm.h"

static struct mpc512x_pm ads5121_pm_data;
static struct ads5121_hib_regs *ads5121_save_ptr;

/* Array to store the SRAM contents */
static char saved_sram[256 * 1024];
static u32 ads5121_targeted_state = MPC512x_PM_NONE;
static u32 ads5121_set_rtc_alarm(void);
#ifdef CONFIG_MPC5121_ADS_HIB
int fsl_deep_sleep(void)
{
	return ads5121_targeted_state;
}
#endif
/*
 * Name       : ads5121_save_regs
 * Desc       : This function is called to store the Peripheral registers
 *		which dont have drivers associated with it to store it back.
 *
 * Parameters : sram - Pointer of a Mapped Memory Location.
 * Return     : void
 */
static int ads5121_save_regs(u32 *sram)
{
	u32 *reg_ptr;
	ads5121_save_ptr = kmalloc(sizeof(struct ads5121_hib_regs), GFP_KERNEL);

	if (!ads5121_save_ptr)
		return -1;

	reg_ptr = (u32 *)((u32)ads5121_pm_data.mbar +
						MPC512x_IMMRBAR_IPIC_OFFSET);
	_memcpy_fromio(ads5121_save_ptr->ipic_regs, reg_ptr,
					sizeof(ads5121_save_ptr->ipic_regs));

	reg_ptr = (u32 *)((u32)ads5121_pm_data.mbar +
						MPC512x_IMMRBAR_CLK_OFFSET);
	_memcpy_fromio(ads5121_save_ptr->clk_regs, reg_ptr,
					sizeof(ads5121_save_ptr->clk_regs));

	reg_ptr = (u32 *)((u32)ads5121_pm_data.mbar +
						MPC512x_IMMRBAR_GPT_OFFSET);
	_memcpy_fromio(ads5121_save_ptr->gpt_regs, reg_ptr,
					sizeof(ads5121_save_ptr->gpt_regs));

	reg_ptr = (u32 *)((u32)ads5121_pm_data.mbar +
						MPC512x_IMMRBAR_GPIO_OFFSET);
	_memcpy_fromio(ads5121_save_ptr->gpio_regs, reg_ptr,
					sizeof(ads5121_save_ptr->gpio_regs));

	memcpy(saved_sram, sram, sizeof(saved_sram));
	return 0;
}

/*
 * Name       : ads5121_restore_regs
 * Desc       : This function is called to restore the Peripheral registers
 *		which dont have drivers associated with it to restore it back.
 *
 * Parameters : sram - Pointer of a Mapped Memory Location.
 * Return     : void
 */
static void ads5121_restore_regs(u32 *sram)
{
	u32 *reg_ptr;

	/* Disable here explicitly, needs not enable since the interrrups
	 * would be enabled latter on the suspend_enter function
	 * in the kernel/power/main.c
	 */
	local_irq_disable();

	memcpy(sram, saved_sram, sizeof(saved_sram));

	reg_ptr = (u32 *)((u32)ads5121_pm_data.mbar +
					MPC512x_IMMRBAR_IPIC_OFFSET);
	_memcpy_toio(reg_ptr, ads5121_save_ptr->ipic_regs,
					sizeof(ads5121_save_ptr->ipic_regs));

	reg_ptr = (u32 *)((u32)ads5121_pm_data.mbar +
					MPC512x_IMMRBAR_CLK_OFFSET);
	_memcpy_toio(reg_ptr, ads5121_save_ptr->clk_regs,
					sizeof(ads5121_save_ptr->clk_regs));

	reg_ptr = (u32 *)((u32)ads5121_pm_data.mbar +
					MPC512x_IMMRBAR_GPT_OFFSET);
	_memcpy_toio(reg_ptr, ads5121_save_ptr->gpt_regs,
					sizeof(ads5121_save_ptr->gpt_regs));

	reg_ptr = (u32 *)((u32)ads5121_pm_data.mbar +
					MPC512x_IMMRBAR_GPIO_OFFSET);
	_memcpy_toio(reg_ptr, ads5121_save_ptr->gpio_regs,
					sizeof(ads5121_save_ptr->gpio_regs));

	kfree(ads5121_save_ptr);
}

/*
 * Name       : ads5121_hibernate
 * Desc       : This function is called to hibernate.
 *
 * Parameters : void
 * Return     : void
 */
/* Set the Target Time Register to a Future Value */
static int ads5121_hibernate(void)
{
	 void ads5121_low_power(u32 *, u32 *, u32);
	/*
	 * 1. Save SRAM data to DDR
	 * 2. Copy code to SRAM
	 * 3. Configure RTC to hibernate with specified timeout.
	 * 3. Jump to SRAM and put DDR in self refresh.
	 */
	u32 reg, ret;
	u32 offset_minutes;

	u32 *rtc = (u32 *)((u32)ads5121_pm_data.mbar +
						MPC512x_IMMRBAR_RTC_OFFSET);
	u32 *sram = (u32 *) in_be32((u32 *)((u32)ads5121_pm_data.mbar +
						MPC512x_IMMRBAR_SRAM_OFFSET));

	/* IOREMAP the SRAM address obtained from MBAR */
	sram = ioremap((u32)sram, (256 * 1024));

	if (!sram) {
		printk(KERN_ERR "Error mapping SRAM\n");
		return -1;
	}

	ret = ads5121_save_regs(sram);
	if (ret < 0)
		return -1;

	/* Set the BC6 bit in the Keep Alive Register to indicate Hibernate.
	 * The Bit Value would retained across the power cycles.
	 */
	reg = in_be32(&rtc[MPC512x_RTC_KEEPALIVE >> 2]);
	reg |= (1 << 8);
	out_be32(&rtc[MPC512x_RTC_KEEPALIVE >> 2], reg);


	/* Set the DIS_HIB_MODE to 0 to enable the Hibernate mode
	   out of MPC5121e.
	 */
	reg = in_be32(&rtc[MPC512x_RTC_KEEPALIVE >> 2]);
	reg &= ~(1 << 7);
	out_be32(&rtc[MPC512x_RTC_KEEPALIVE >> 2], reg);


	/* Store the Value of the TTR Register in RTC so as to restore */
	ads5121_pm_data.rtc_targettime = in_be32(&rtc[MPC512x_RTC_TTR >> 2]);

	offset_minutes = ads5121_set_rtc_alarm();

	ads5121_low_power(sram, ads5121_pm_data.mbar, offset_minutes);

	/* We are out of hibernate.. Lets restart jiffies */
	wakeup_decrementer();

	out_be32(&rtc[MPC512x_RTC_TTR >> 2], ads5121_pm_data.rtc_targettime);

	/* Reset the BC6 bit after coming out of Hibernate */
	reg = in_be32(&rtc[MPC512x_RTC_KEEPALIVE >> 2]);
	reg &= ~(1 << 8);
	out_be32(&rtc[MPC512x_RTC_KEEPALIVE >> 2], reg);

	/* Restore the Registers */
	ads5121_restore_regs(sram);

	/* IOUMAP this location only after the restore funcion. The restore
	 * function would copy data back and then only release this memory.
	 */
	iounmap(sram);
	return 0;
}

/*
 * Name       : ads5121_set_rtc_wakeup
 * Desc       : This function is called to enable the wakeup sources.
 *
 * Parameters : void
 * Return     : void
 */
static void ads5121_set_rtc_wakeup(struct mpc512x_pm *p_pmdata)
{
	u32 rtc_reg;
	u32 *rtc;

	if (!p_pmdata->mbar)
		return;

	rtc = (u32 *)((u32)p_pmdata->mbar + MPC512x_IMMRBAR_RTC_OFFSET);
	rtc_reg = in_be32(&rtc[MPC512x_RTC_KEEPALIVE >> 2]);

	/* Set the Active LVL values for the Wake-up Sources[1-5] */
	rtc_reg |= MPC512x_RTCKAR_WKUP_SRCLVL;
	out_be32(&rtc[MPC512x_RTC_KEEPALIVE >> 2], rtc_reg);

	rtc_reg = in_be32(&rtc[MPC512x_RTC_KEEPALIVE >> 2]);
	/* Enable the Wake-Up sources */
	rtc_reg |= (MPC512x_RTCKAR_WKUP_SRCEN);
	out_be32(&rtc[MPC512x_RTC_KEEPALIVE >> 2], rtc_reg);
}

static void ads5121_prepare_hibernate(struct mpc512x_pm *p_pmdata)
{
	/*
	*  Enable the wakeup sources
	*/
	ads5121_set_rtc_wakeup(p_pmdata);
}
/*
 * Name       : ads5121_pm_valid
 * Desc       : Checks whether the PM state is valid
 *
 * Parameters : void
 * Return     : 1 - Valid , 0 - Invalid
 */
static int ads5121_pm_valid(suspend_state_t state)
{
	switch (state) {
	case PM_SUSPEND_STANDBY:
	case PM_SUSPEND_MEM:
		return 1;
	default:
		return 0;
	}
}

/*
 * Name       : ads5121_pm_settarget
 * Desc       : Set the state to which the system is to enter.
 *
 * Parameters : void
 * Return     : 0 - Success
 */
static int ads5121_pm_settarget(suspend_state_t state)
{
	switch (state) {
	case PM_SUSPEND_STANDBY:
		ads5121_targeted_state = MPC512x_PM_STANDBY;
		break;
	case PM_SUSPEND_MEM:
		ads5121_targeted_state = MPC512x_PM_SUSP_MEM;
		break;
	default:
		ads5121_targeted_state = MPC512x_PM_NONE;
	}
	return 0;
}
/*
 * Name       : ads5121_pm_prepare
 * Desc       : This function would map the IO regions. Also sets the DDRC and
 * 		RTC regs for Deep Sleep Mode.
 *
 * Parameters : void
 * Return     : int
 *		ENOSYS
 *
 */
static int ads5121_pm_prepare(void)
{
	mpc512x_pm_setup(&ads5121_pm_data);

	switch (ads5121_targeted_state) {
	case MPC512x_PM_STANDBY:
		mpc512x_prepare_deepsleep(&ads5121_pm_data);
		break;
	case MPC512x_PM_SUSP_MEM:
		ads5121_prepare_hibernate(&ads5121_pm_data);
		break;
	}
	return 0;
}

/*
 * Name       : ads5121_pm_enter
 * Desc       : This function is exported to the Power Management Core. This
 *	`	function is called with the state which the system should enter.
 *
 * Parameters : state 	- PM_SUSPEND_STANDBY
			- PM_SUSPEND_MEM
 * Return     : int
 * 		-1 : FAILED
 *		0  : SUCCESS
 */
static int ads5121_pm_enter(suspend_state_t state)
{
	if (!ads5121_pm_data.mbar) {
		printk(KERN_ERR "Failed to enter PM mode as IO not mapped.\n");
		return -1;
	}
	switch (ads5121_targeted_state) {

	case MPC512x_PM_STANDBY:
		mpc512x_enter_deepsleep(&ads5121_pm_data);
		break;
	case MPC512x_PM_SUSP_MEM:
		ads5121_hibernate();
		break;
	default:
		break;
	}
	return 0;
}

/*
 * Name       : ads5121_pm_finish
 * Desc       : This routine is called by the kernel on exit from
 * 		power down modes. Restores the DDRC and RTC regs
 * 		suspend to memory. Also releases allocated resources.
 *
 * Parameters : void
 * Return     : void
 */
static void ads5121_pm_finish(void)
{
	switch (ads5121_targeted_state) {

	case MPC512x_PM_STANDBY:
		mpc512x_finish_deepsleep(&ads5121_pm_data);
		break;
	case MPC512x_PM_SUSP_MEM:
		break;
	}
	ads5121_targeted_state = MPC512x_PM_NONE;

	mpc512x_pm_release(&ads5121_pm_data);
}

static struct platform_suspend_ops ads5121_pm_ops = {
	.valid		= ads5121_pm_valid,
	.begin	= ads5121_pm_settarget,
	.prepare	= ads5121_pm_prepare,
	.enter		= ads5121_pm_enter,
	.finish		= ads5121_pm_finish,
};

/*
 * Name       : ads5121_pm_init
 * Desc       : This function registers the platform_suspend_ops
 * 		structure with the kernel.
 *
 * Parameters : void
 * Return     : int
 */
int __init ads5121_pm_init(void)
{
	suspend_set_ops(&ads5121_pm_ops);
	return 0;
}

/*
 * Name       : ads5121_set_rtc_alarm
 * Desc       : This function woul dbe called from the ads5121_hibernate funct-
 *		which return the alarm offset in case it is set. If not then
 *		it would retun 0xFFFFFFFF, which would be the MAX value for TTR.
 *
 * Parameters :
 * Return     : u32 - Offset of Alarm Value with the Current Time.
 */
static u32 ads5121_set_rtc_alarm()
{
	u32 rtc_reg;
	u32 *rtc;
	u32 alm_hr, alm_min, cur_hr, cur_min;
	u32 offset_minutes;

	rtc = (u32 *)((u32)ads5121_pm_data.mbar + MPC512x_IMMRBAR_RTC_OFFSET);

	rtc_reg = in_be32(&rtc[MPC512x_RTC_AIER >> 2]);

	if (rtc_reg & MPC512x_RTCAIER_ALMEN_MASK) {
		/*Alarm was set.. Let us wakeup in that time..*/
		alm_hr = (rtc_reg >> MPC512x_RTC_HR_OFFSET)
				& MPC512x_RTC_HR_MASK;
		alm_min = (rtc_reg >> MPC512x_RTC_MIN_OFFSET)
				& MPC512x_RTC_MIN_MASK;
		rtc_reg = in_be32(&rtc[MPC512x_RTC_CTR >> 2]);
		cur_min = (rtc_reg >> MPC512x_RTC_MIN_OFFSET)
				& MPC512x_RTC_MIN_MASK;

		if (in_be32(&rtc[MPC512x_RTC_TSR >> 2])
				& MPC512x_RTCTSR_SLCHR_MASK) {
			/* 12 Hour Format*/
			cur_hr = (rtc_reg >> MPC512x_RTC_HR_OFFSET) & 0xF;
			if (rtc_reg & MPC512x_RTC_CTR_PM)
				cur_hr += 12;
		} else
			cur_hr = (rtc_reg >> MPC512x_RTC_HR_OFFSET)
					& MPC512x_RTC_HR_MASK;
		offset_minutes = (alm_hr * MPC512x_RTC_MINS_PER_HR + alm_min) -
					(cur_hr * MPC512x_RTC_MINS_PER_HR + cur_min);

		if (offset_minutes > 0)
			offset_minutes = offset_minutes * MPC512x_RTC_MINS_PER_HR;
		else
			offset_minutes = MPC512x_RTCTTR_MAXTIMEOUT;
	} else
		offset_minutes = MPC512x_RTCTTR_MAXTIMEOUT;

	return(offset_minutes);
}


