#ifndef __DIAGSHELL_H__
#define __DIAGSHELL_H__

typedef int (*pCmdFunc)(int args, char *argv[]);

#define CMD_NORMAL 0x00
#define CMD_HIDDEN 0x01
#define CMD_ADMIN  0x02
typedef struct 
{
    char     *name;          /* name of the function */
    pCmdFunc func;           /* function to do the job */
    char     *doc;           /* help for this command */
    uint32_t flags;
} shellCmd;

#endif /* __DIAGSHELL_H__ */
