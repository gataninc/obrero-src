#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <bits/socket.h>
#include <string.h>
#include <sys/select.h>
#include <sys/time.h>
#include <errno.h>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "dmmfuncs.h"

// Demonstrates basic instrument control through TCP
// Copyright (C) 2007 Agilent Technologies
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// The GNU General Public License is available at
// http://www.gnu.org/copyleft/gpl.html.

// Modified for Gatan(RJ, 08-12-2015)

#define SOCKETS_PORT				5025
#define SOCKETS_BUFFER_SIZE			1024
#define SOCKETS_TIMEOUT				5

int writeDMMString(int mySocket,char string[])
{
	int retval;
	
	if ((retval = send(mySocket,string,strlen(string),0)) == -1) 
    {
		printf("Error: Unable to send message (%i)...\n", errno);
		perror("sockets"); // Print error message based on errno
	}
	return retval;
}

static int waitForDMMData(int mySocket)
{
	fd_set MyFDSet;
	struct timeval tv;
	int retval;
	
	// Wait for data to become available
	FD_ZERO(&MyFDSet); // Initialize fd_set structure
	FD_SET(mySocket, &MyFDSet); // Add socket to "watch list"
	tv.tv_sec = SOCKETS_TIMEOUT; 
    tv.tv_usec=0; // Set timeout
	retval = select(mySocket+1, &MyFDSet, NULL, NULL, &tv); // Wait for change
	
	// Interpret return value
	if (retval == -1) 
    {
		printf("Error: Problem with select (%i)...\n", errno);
		perror("sockets"); // Print error message based on errno
	}
	return retval; // 0 = timeout, 1 = socket status has changed
}

int readDMMString(int mySocket,char *buffer)
{
	int actual;
	
	// Wait for data to become available
	if (waitForDMMData(mySocket) == 0)
	{
		// Timeout
		printf("Timeout waiting for data from DMM\n");
		return -1;
	}
	
	// Read data
	if ((actual = recv(mySocket, buffer, 200, 0)) == -1) 
    {
		printf("Error: Unable to receive data (%i)...\n", errno);
		perror("sockets"); // Print error message based on errno
	}
	else 
    {
      	buffer[actual]=0;
	}
	return actual;
}

int getDMMString(int dmmSock, char *cmd, char *reply, int repLen)
{
	int actual;
    if (writeDMMString(dmmSock, cmd) > 0)
    {
        // Wait for data to become available
        if (waitForDMMData(dmmSock) == 0)
        {
            // Timeout
            printf("Timeout waiting for data from DMM\n");
            return -1;
        }
        // Read data
        if ((actual = recv(dmmSock, reply, repLen, 0)) <= 0) 
        {
            printf("Error: Unable to receive data (%i)...\n", errno);
            perror("sockets"); // Print error message based on errno
        }
        else 
        {
            int i;
            reply[actual]=0;
            // strip CR at end
            for (i = (actual-1); i > 0; i++)
            {
                if ((reply[i] == '\r') || (reply[i] == '\n'))
                    reply[i] = '\0';
                break;
            }
        }
        return actual;
    }
    return -1;
}

static int SetNODELAY(int mySocket)
{
	int StateNODELAY = 1; // Turn NODELAY on
	int ret;
	
	ret = setsockopt(mySocket, // Handle to socket connection
		IPPROTO_TCP,           // Protocol level (TCP)
		TCP_NODELAY,           // Option on this level (NODELAY)
		(void *)&StateNODELAY, // Pointer to option variable
		sizeof StateNODELAY);  // Size of option variable
	
	if (ret==-1) 
    {
		printf("Error: Unable to set NODELAY option (%i)...\n", errno);
		perror("sockets"); // Print error message based on errno
	}
	return ret;
}

// End code from Agilent(KeySight): Modified for Gatan

// open connection to DMM at address IPStr and return socket handle to caller.
int openDMM(char *IPStr)
{
	int mySocket;
	struct sockaddr_in MyAddress;

	// Create socket (allocate resources)
	if ((mySocket = socket(PF_INET,      // IPv4
                            SOCK_STREAM, // TCP
                            0)) == -1) 
    {
		perror("sockets"); // Print error message based on errno
 		printf("Error: Unable to create socket (%i)...\n",errno);
		return -1;
	}
	
	// Establish TCP connection
	memset(&MyAddress, 0, sizeof(struct sockaddr_in)); // Set structure to zero
	MyAddress.sin_family = PF_INET; // IPv4
	MyAddress.sin_port = htons(SOCKETS_PORT); // Port number (in network order)
	MyAddress.sin_addr.s_addr= 
                        inet_addr(IPStr); // IP address (in network order)
	if (connect(mySocket, (struct sockaddr *)&MyAddress,
		sizeof(struct sockaddr_in)) == -1) 
    {
		perror("sockets"); // Print error message based on errno
		printf("Error: Unable to establish connection to socket (%i)...\n",
			errno);
        return -1;
	}
	
	// Minimize latency by setting TCP_NODELAY option
	if (SetNODELAY(mySocket) != 0)
        return -1;
    return mySocket;
}

int closeDMM(int desc)
{
    if (close(desc) == -1)
    {
        perror("close socket");
        printf("Error closing socket\n");
        return -1;
    }
    return 0;
}

//#define UNIT_TEST
#ifdef UNIT_TEST
int printUsage(char *cmd)
{
    fprintf(stderr, "%s <dev. IP Addr> <cmd string>\n", cmd);
    exit(1);
}

int main(int argc,char **argv)
{
    char *devIP;
    char *cmdPtr, cmdStr[256];
    char respStr[1024];
    int isQuery = 0;
    int devHndl, retVal = 0;

    if (argc < 3)
        printUsage(argv[0]); // exits..
    devIP = argv[1];
    cmdPtr = argv[2];
    if (cmdPtr[strlen(cmdPtr)-1] == '?')
        isQuery = 1;
    if (argc == 4)
        isQuery = 1;

    if ((devHndl = openDMM(devIP)) < 0)
        exit(1);
    snprintf(cmdStr, 255, "%s\n", cmdPtr);
    cmdStr[255] = '\0';
    writeDMMString(devHndl, cmdStr);
    if (isQuery)
    {
        if (readDMMString(devHndl, respStr) == 0)
        {
            retVal = 1;
            goto doExit;
        }
        printf("\nRESPONSE: %s\n", respStr);
    }
doExit:
    close(devHndl);
    exit(retVal);
}
#endif
