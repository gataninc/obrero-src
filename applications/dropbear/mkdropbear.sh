#! /bin/sh
DROPBEARSRC=dropbear-0.53.1.tar.gz
DROPBEARDIR=dropbear-0.53.1
DROPBEARBIN=dropbearmulti
PPC_STRIP=powerpc-e300c3-linux-gnu-strip

if [ ! -d ${DROPBEARDIR} -o ! -x ${DROPBEARDIR}/${DROPBEARBIN} ]; then
# unarchive source tar ball
    if [ ! -f ${DROPBEARSRC} ]; then
        echo; echo "ERROR: dropbear archive not found!"; echo
        exit 1
    fi
    echo; echo " === Un-archiving dropbear sources ==="; echo
    tar xvfz ${DROPBEARSRC}
    if [ $? -ne 0 ]; then
        echo; echo "ERROR: Un-archiving dropbear source"; echo
        exit 1
    fi
    cd ${DROPBEARDIR}
    echo; echo " === Setting up build env. ==="; echo
# Build dropbear binaries, set some options first..
    perl -pi -e 's,^(#define ENABLE_X11FWD),//\1,; ' options.h
    perl -pi -e 's,^(#define DO_HOST_LOOKUP),//\1,; ' options.h
    perl -pi -e 's,^#define DROPBEAR_RANDOM_DEV.*,#define DROPBEAR_RANDOM_DEV "/dev/urandom",; ' options.h

# set necesary env. variables
    export CC=powerpc-e300c3-linux-gnu-gcc
    export LDFLAGS="-L../../../rootfs/usr/lib/ -L../../../rootfs/lib"
    export CFLAGS="-I ../../../rootfs/usr/include"

# configure and build steps.
    echo; echo " === Running configure ==="; echo
    ./configure --prefix=/usr --host=powerpc-linux --build=i686-pc-linux-gnu

    make clean

    echo; echo " === Building binaries ==="; echo
    make PROGRAMS="dropbear dbclient dropbearkey dropbearconvert scp" MULTI=1 SCPROGRESS=1

    ${PPC_STRIP} -s ${DROPBEARBIN}

    echo "=== Dropbear build done === "; echo
else
    echo; echo "Dropbear already built.. Skipping build"; echo
fi

