/******************
*******************/
#ifndef _PIEBOX_STRUCT_H_
#define	_PIEBOX_STRUCT_H_

#define	CFG_IMMR	0x80000000


#define	SYS_CONFIGRATION_BASE	0x0000
#define	SOFTWARE_WATCHDOG_BASE	0x0900
#define	REAL_TIME_BASE			0x0a00
#define	GENERAL_TIMER_BASE		0x0b00
#define	INTERRUPT_CONTROLER_BASE	0x0c00
#define	CSB_BASE					0x0d00
#define	RESET_BASE					0x0e00
#define	CLOCK_BASE					0x0f00
#define	POWER_MANAGEMENT_BASE	0x1000
#define	GENERAL_GPIO_BASE			0x1100
#define	MSCAN_BASE				0x1300
#define	BYTE_CONTROLLER_BASE		0x1400
#define	SECURE_DIGITAL_BASE		0x1500
#define	SONY_DIGITAL_BASE			0x1600
#define	I2C1_BASE					0x1700
#define	I2C2_BASE					0x1720
#define	I2C3_BASE					0x1740
#define	AXE_BASE					0x2000
#define	DISPLAY_BASE				0x2100
#define	CLOCK_FREQUENCY_BASE		0x2200
#define	FAST_ETHERNET_BASE		0x2800
#define	USB_ULPI_BASE				0x3000
#define	USB_UTMI_BASE				0x4000
#define	PCI_DMA_BASE				0x8000
#define	PCI_CONFIG_BASE			0x8300
#define	PCI_IOS_BASE				0x8400
#define	PCI_CONTROLLER_BASE		0x8500
#define	DRAM_CONTROLLER_BASE		0x9000
#define	IO_CONTROL_BASE			0xa000
#define	IIM_BASE					0xb000
#define	LOCALPLUS_BASE			0x10000
#define	PATA_BASE					0x10200
#define	PSC_CONTROL_BASE			0x11000
#define	PSC_N_CONTROL_BASE(n)		(n*0x100+PSC_CONTROL_BASE)
#define	SFIFO_PSC_BASE			0x11f00
#define	IO_CONTROL_MEM_REG		(CFG_IMMR+IO_CONTROL_BASE+0x000)
#define	IO_CONTROL_GP_REG			(CFG_IMMR+IO_CONTROL_BASE+0x004)




#define PIEBOX_LED_SET_REG(addr,value)	 *(volatile unsigned int *)(addr)=(value)


#define	GPIO_BIT_OFFSET(x)		(1<<(31-x))
#define	GPT_BIT_OFFSET(x)		(1<<(7-x))
#define	GPIO_PIEBOX_BIT_OFFSET(x)		GPIO_BIT_OFFSET(x)

#define	GPIO_RUN_LED		1
#define	GPIO_P_LED			15
#define	GPIO_YNET_LED		14
#define	GPIO_PWROFF_MUTE	13

#define COLOR_BIT_MASK	(GPIO_BIT_OFFSET(GPIO_RUN_LED)|GPIO_BIT_OFFSET(GPIO_P_LED)|GPIO_BIT_OFFSET(GPIO_YNET_LED))
/*
#define	GPIO_WIFI_P		13
*/
/*
#define	GPIO_PDN			12
*/
#define	GPIO_LED_PWM		0
#define	GPIO_PENABLE_LED	11
#define	GPIO_PIEKEY_LED	10

#define	GPIO_KEY_VOL		20
#define	GPIO_KEY_MENU		22
#define	GPIO_KEY_LCD		3


#define	GPIO_SYSTEM_START_NORFLASH		12

#define	GPIO_HDR_GPIO6_LCD_ENABLE	6
#define	GPIO_LCD_GPT2					2

#define	LCD_BRIGHT_GPT_MAX		100		
#define	LED_BRIGHT_GPT_MAX		100	

/**************
gpio int mode
******************/
#define	GPIO_INTERRUPT_MODE_ANY		0
#define	GPIO_INTERRUPT_MODE_LOW_TO_HEIGHT	1
#define	GPIO_INTERRUPT_MODE_HIGHT_TO_LOW	2
#define	GPIO_INTERRUPT_MODE_PLUSE	3


enum{
	PIEBOX_LED_GREEN=0,
	PIEBOX_LED_RED,
	PIEBOX_LED_YELLOW	,
	PIEBOX_LED_PIEKEY
};

/************
mtc systemcall defined
**************/
#define	MTC_SYSTEM_CALL_NUMBER		319

#define	MTC_SYSTEM_CALL_SET_LCD_VALUE	0xf000
#define	MTC_SYSTEM_CALL_GET_LCD_VALUE	0xf001
#define	MTC_SYSTEM_CALL_SET_LED_VALUE	0xf002
#define	MTC_SYSTEM_CALL_GET_LED_VALUE	0xf003

#define	MTC_SYSTEM_CALL_GET_LED_STATUS	0xf004
#define	MTC_SYSTEM_CALL_SET_LED_STATUS	0xf005

#define	MTC_SYSTEM_CALL_GET_LCD_MAX		0xf006
#define	MTC_SYSTEM_CALL_GET_LED_MAX		0xf007

#define	MTC_SYSTEM_CALL_GET_FB0_ADDR	0xf008





struct io_control_struct {
	 unsigned int io_control;
	unsigned int 	value;
};
struct mpc5121_gpio_struct{
	volatile unsigned int 	gpdir;
	volatile unsigned int 	gpodr;
	volatile unsigned int 	gpdat;
	volatile unsigned int 	gpier;	
	volatile unsigned int 	gpimr;
	volatile unsigned int 	gpicr1;
	volatile unsigned int 	gpicr2;
};
struct mpc5121_gpt_struct{
	volatile unsigned int 	enable_mode;
	volatile unsigned int	counter;
	volatile unsigned int	pwm_config;
	volatile unsigned int	status;	
};

struct mpc5121_gpt_control_struct{
	struct mpc5121_gpt_struct	gpt[7];
};
 void mpc5121_usb_ide_dma_lock(void);
 void mpc5121_usb_ide_dma_unlock(void);
#endif
