GCC = powerpc-e300c3-linux-gnu-gcc
GPP = powerpc-e300c3-linux-gnu-g++
AR  = powerpc-e300c3-linux-gnu-ar
#CFLAGS = -Wall -I../include -O3
CFLAGS =  -O3 -Wall -I../include 
#LDFLAGS = -rdynamic -L../lib -lBaltoro 
LDFLAGS = -L../lib -lBaltoro 
ARFLAGS = crv

%.o:	%.c
	$(GCC) -c  $(CFLAGS) -c -o $@ $<

%.o:	%.cpp
	$(GPP) -c  $(CFLAGS) -c -o $@ $<
