#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include "i2c-dev.h"
#include "bing_common.h"
#include "utils.h"

// MCP3424 4 channel ADC
/*
   Resolutions vs LSB
   12bits   1mV
   14bits   250uV
   16bits   62.5uV
   18bits   15.62uV
 */
/*
 [From document: Obrero Analogue Interface Description, 11/25/2014]
 Ch 1 - CJC(cold junction compensation) measurement. (18bit mode)
     If CJC temp is between 0 - 50 degC, set gain to 4
     If CJC temp is between 0 - 100 degC, set gain to 2
 Ch 3 - PA(power amplifier) temperature 
 Ch 4 - PCB temperature.
 */

enum { R12BITS = 0, R14BITS, R16BITS, R18BITS, MAX_RESOL } mcp2434Resolutions;

float LSBVal[] = { 1000.0, 250.0, 62.5, 15.625 }; // all in micro-volts

static int g_curResol = R18BITS;

/*
 if MSB == 0(+ve output code), input voltage = outputcode * LSB / PGA(gain)
 if MSB == 1(-ve output code), 
    input voltage = (2's complement of outputcode) * LSB / PGA(gain)
 */
static float computeVoltage(int32_t adcVal, int gainBit)
{
    int isNeg = 0;
    int temp;
    float result;
    int gainVal;
    if (gainBit == 1)
        gainVal = 2;
    else if (gainBit == 2)
        gainVal = 4;
    else if (gainBit == 3)
        gainVal = 8;
    else
        gainVal = 1;
    //logPrint("%s: adcVal 0x%X, resol %d\n", __func__, adcVal, g_curResol);
    // negative read-outs are already sign extended
    if ((adcVal & (0x1 << 23)) != 0)
    {
        adcVal |= 0xFF000000; // sign extend uint32_t
        isNeg = 1;
    }
    //logPrint("isNeg %d adcVal %d(0x%X) resol %d gain %d\n", 
    //                isNeg, adcVal, adcVal, g_curResol, gainVal);
    temp = adcVal;
    result = (temp * (LSBVal[g_curResol]/gainVal))/1000.0; // milli-volts
    //logPrint("%s: result %0.3f\n", __func__, result);
    return result;
}

// resol is the number of bits(12,14,16 or 18bits)
int mcp3424SetResol(int resol)
{
    int ret = 0;
    switch (resol)
    {
        case 12:
            g_curResol = R12BITS;
            break;
        case 14:
            g_curResol = R14BITS;
            break;
        case 16:
            g_curResol = R16BITS;
            break;
        case 18:
            g_curResol = R18BITS;
            break;
        default:
            logPrint("%s: incorrect resolution(%d)\n", __func__, resol);
            ret = -1;
            break;
    }
    return ret;
}

/*
  Configuration Register bits:
    b7: Ready bit(active low). 
        Read : 1 o/p register not updated, 0 o/p register updated with conversion result.
        Write:(only effective for one-shot conversion)
            1 initiate a new conversion, 0 no effect
    b6-5: Channel selection. 00-chan 1(default), 01 chan 2, 10 chan 3, 11 chan 4.
    b4: Conversion mode bit. 1 - continous conversion, 0 - one shot conversion
    b3-2: Sample rate selection. 
        00: 240SPS(12b), 01: 60SPS(14b), 10: 15SPS(16b), 11: 3.75SPS(18b)
    b1-0: gain selection bits.
        00: x1(default), 01: x2, 10: 0x4, 11: 0x8

  Writes to device: Device expects one configuration byte. Ignores any more bytes.

  Read conversion result: On a read, device outputs conversion data followed by 
    configuration byte.

    18 bit mode: 3 data bytes + 1 config byte
        D17 is MSB(=sign bit), first 6 bits are repeated MSB of conv. result.
            conversion result

    16 bit mode: 2 data bytes + 1 config byte
        D15 is MSB(=sign bit)

    14 bit mode: 2 data bytes + 1 config byte
        D13 is MSB(=sign bit), first 2 bits are repeated MSB of conv. result.

    12 bit mode: 2 data bytes + 1 config byte
        D11 is MSB(=sign bit), first 4 bits are repeated MSB of conv. result.

 */

// Split the function into two, do not want to hold i2c lock for 300mS.
// mcp3424ReadInit() and mcp3424ReadValue()

// Initiate a read from MCP3424 device. Return -ve number on error, number of 
// milli-seconds to wait for result on success.
int mcp3424ReadInit(int bus, int devAddr, int chan, int gain)
{
    int hI2C;
    struct i2c_msg message[1];
    struct i2c_rdwr_ioctl_data ctrl;
    uint8_t cfgByte;
    int err = 0;
    int convTm = 300000; // in micro-secs
    uint8_t gBits = 0;

    if ((chan < 1) ||  (chan > 4))
        return -1;
//chan=2;
    {
        uint8_t chSel = 0, sampleSel = 0;
        // channel selection bits
        if (chan == 1)
            chSel = 0x0;
        else if (chan == 2)
            chSel = 0x20;
        else if (chan == 3)
            chSel = 0x40;
        else if (chan == 4)
            chSel = 0x60;
        // fix gain selection
        if (chan == 1)
        {
            if (gain == 2)
                gBits = 0x1;
            else if (gain == 4)
                gBits = 0x2;
            else if (gain == 8)
                gBits = 0x3;
            else
                gBits = 0;
        }
        else
            gBits = 0;
        // sample rate selection bits
        if (g_curResol == R12BITS)
        {
            convTm = 10000;
            sampleSel = 0x0;
        }
        else if (g_curResol == R14BITS)
        {
            convTm = 30000;
            sampleSel = 0x4;
        }
        else if (g_curResol == R16BITS)
        {
            convTm = 100000;
            sampleSel = 0x8;
        }
        else if (g_curResol == R18BITS)
        {
            convTm = 300000;
            sampleSel = 0xC;
        }
        // assemble config byte
        cfgByte = 0x80 | chSel | sampleSel | gBits; // one-shot conversion
    }
    //logPrint("%s: cfgByte 0x%X convTm %d\n", __func__, cfgByte, convTm);

    // Write configuration byte to start conversion
    if ((hI2C = open(get_i2c_bus(bus), O_RDWR)) < 0)
        return -1;

    ctrl.nmsgs = 1;
    ctrl.msgs  = &message[0];

    message[0].addr  = devAddr;
    message[0].flags = 0;
    message[0].len = 1;
    message[0].buf = (char *)&cfgByte;

    /* do i2c transaction */
    if(ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        err = errno;
        logPrint("Error: failed to write to mcp3424(%d)\n", err);
        close(hI2C);
        return -2;
    }
    close(hI2C);
    return (convTm/1000); // in mS
}

int mcp3424ReadValue(int bus, int devAddr, int chan, int gain, float *val)
{
    int hI2C;
    struct i2c_msg message[1];
    struct i2c_rdwr_ioctl_data ctrl;
    uint8_t dataBytes[4];
    int err = 0;
    int32_t adcVal;
    uint8_t gBits = 0;

    if ((chan < 1) ||  (chan > 4))
        return -1;
    // fix gain selection
    if (chan == 1)
    {
        if (gain == 2)
            gBits = 0x1;
        else if (gain == 4)
            gBits = 0x2;
        else if (gain == 8)
            gBits = 0x3;
        else
            gBits = 0;
    }
    else
        gBits = 0;

    if ((hI2C = open(get_i2c_bus(bus), O_RDWR)) < 0)
        return -1;

    ctrl.nmsgs = 1;
    ctrl.msgs  = &message[0];

    message[0].addr  = devAddr;
    message[0].flags = I2C_M_RD;
    message[0].len   = 4;
    switch (g_curResol)
    {
        case R12BITS:
        case R14BITS:
        case R16BITS:
            message[0].len   = 3;
            break;
        case R18BITS:
            message[0].len   = 4;
            break;
    }
    message[0].buf   = (char *)&dataBytes[0];
    if(ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        err = errno;
        logPrint("Error: failed to read mcp3424(%d)\n", err);
        close(hI2C);
        return -3;
    }
    close(hI2C);

    //printf("%s: adcVal %x %x %x %x\n", __func__, dataBytes[0], dataBytes[1],
    //        dataBytes[2], dataBytes[3]);
#if 0
{ uint8_t t = dataBytes[0]; dataBytes[0] = dataBytes[3]; dataBytes[3] = t;
    t = dataBytes[2]; dataBytes[2] = dataBytes[1]; dataBytes[1] = t; }
printf("%s: adcVal %x %x %x %x\n", __func__, dataBytes[0], dataBytes[1],
            dataBytes[2], dataBytes[3]);
#endif
    if (message[0].len == 3) // use 2 bytes
        adcVal = (int)(*(int16_t *)dataBytes);
    else
        adcVal = (dataBytes[0] << 16) | dataBytes[1] << 8 | dataBytes[2];
    //printf("adcVal %d(0x%X)\n", adcVal, adcVal);
    *val = computeVoltage(adcVal, gBits);
    return 0;
}

// return raw register value
// MUST call mcp3424ReadInit() and wait (conv.time) before calling this function
int mcp3424ReadRawVal(int bus, int devAddr, int chan, int gain, int *val)
{
    int hI2C;
    struct i2c_msg message[1];
    struct i2c_rdwr_ioctl_data ctrl;
    uint8_t dataBytes[4];
    int err = 0;
    int32_t adcVal;
    uint8_t gBits = 0;

    if ((chan < 1) ||  (chan > 4))
        return -1;
    // fix gain selection
    if (chan == 1)
    {
        if (gain == 2)
            gBits = 0x1;
        else if (gain == 4)
            gBits = 0x2;
        else
            gBits = 0;
    }
    else
        gBits = 0;
    //logPrint("%s: cfgByte 0x%X convTm %d\n", __func__, cfgByte, convTm);

    if ((hI2C = open(get_i2c_bus(bus), O_RDWR)) < 0)
        return -1;

    ctrl.nmsgs = 1;
    ctrl.msgs  = &message[0];

    message[0].addr  = devAddr;
    message[0].flags = I2C_M_RD;
    message[0].len   = 4;
    switch (g_curResol)
    {
        case R12BITS:
        case R14BITS:
        case R16BITS:
            message[0].len   = 3;
            break;
        case R18BITS:
            message[0].len   = 4;
            break;
    }
    message[0].buf   = (char *)&dataBytes[0];
    if(ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        err = errno;
        logPrint("Error: failed to read mcp3424(%d)\n", err);
        close(hI2C);
        return -3;
    }
    close(hI2C);

    //printf("%s: adcVal %x %x %x %x\n", __func__, dataBytes[0], dataBytes[1],
    //        dataBytes[2], dataBytes[3]);
    if (message[0].len == 3) // use 2 bytes
        adcVal = (int)(*(int16_t *)dataBytes);
    else
        adcVal = (dataBytes[0] << 16) | dataBytes[1] << 8 | dataBytes[2];
    //printf("adcVal %d(0x%X)\n", adcVal, adcVal);
    *val = adcVal;
    return 0;
}


/*
 * Code from Alistair May
 */
#if 0
// Read from MCP3424 CJC ADC (U654)
// Channel number is 1..4
float get_cjc_adc(uint16_t channel, uint16_t gain) 
{
    char response[0x4000];
    char cmd[256];
    uint8_t config, address;
    char *endptr;
    long int ret;
    char swabbed[7];
        
    // Set I2C multiplexer IC (address 0x77) on Bing to divert traffic to MCP3424 ADC
    ssh_command("i2cset 1 0x77 0 2 2>&1", response, sizeof(response));
    
    // Configure ADC and select channel
    // Mode: continuous conversion, 16 bit resolution (15SPS)
    // ADC FS is 0x7FFF as we are running single-ended
    // See page 18 of MCP3424 datasheet
    channel--;
    channel &= 0b11;
    config = 0b00011000 | (channel << 5);
    switch (gain) {
        case 1:
            config |= 00;
            break;
        case 2:
            config |= 01;
            break;
        case 4:
            config |= 10;
            break;
        case 8:
            config |= 11;
            break;
        default:
            fatal("Invalid MCP3424 gain. Must be 1, 2, 4, or 8");
    }
    
    // Configure ADC
    address = 0x68;
    sprintf(cmd, "i2cset 1 0x%02x 0x%02x 0x%02x 2>&1", address, config, config);
    ssh_command(cmd, response, sizeof(response));
    
    usleep(66666); // wait 66ms for ADC to get valid sample
    
    // Read back ADC result
    // This is a real hack. There is no i2cget on Bing, so I have used i2cset with a command the ADC
    // takes as a "nop". i2cset reads back the written value - but instead reads back the ADC
    // result: the data we want!
    // This won't work in 18 bit mode, as we can only read back 2 bytes here (1 word), and 18 bit
    // requires 3 bytes to be read from the ADC. We run in 16 bit mode.
    sprintf(cmd, "i2cset 1 0x%02x 0x%02x 0x%02x%02x w 2>&1 | grep mismatch | cut -d ' ' -f 10", address, config, config, config);
    ssh_command(cmd, response, sizeof(response));
    //printf("response: %s\n", response);
    if (response[0] != '0') fatal("failed to convert value returned by CJC/EEPROM ADC. Missing '0'");
    if (response[1] != 'x') fatal("failed to convert value returned by CJC/EEPROM ADC. Missing 'x'");
    if (response[6] != 10)  fatal("failed to convert value returned by CJC/EEPROM ADC. Response too long.");
    swabbed[0] = '0';
    swabbed[1] = 'x';
    swabbed[2] = response[4];
    swabbed[3] = response[5];
    swabbed[4] = response[2];
    swabbed[5] = response[3];
    swabbed[6] = 0;
    ret = strtol(swabbed, &endptr, 16);
    return (((float)ret / 32767.0) * 2.048) / (float)gain; // VREF=2.048V, ADC full scale = (2^15)-1
}
#endif

