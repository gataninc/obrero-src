/*************************************************
 *
 * Temperature sensor functions
 *
 * Author: Ciro Noronha
 * Modified for Gatan: Roice
 *
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <bing_common.h>
#include <i2c-dev.h>
#include <utils.h>
#include <i2cutils.h>

#define TEMP_REGISTER_HIGH	0x00
#define TEMP_REGISTER_LOW	0x10
#define CORRECTION_REGISTER	0x20
#define CONFIG1_REGISTER	0x09
#define CONFIG2_REGISTER	0x0A
#define RESET_REGISTER      0xFC
#define CONVERSION_RATE_REG 0x0B
#define CONFIG1_VALUE		0x00
#define CONFIG2_VALUE		0x08 /* do not use remote channel on TMP421s */
#define I2C_VENDOR_ID		0xFE
#define I2C_DEVICE_ID		0xFF
#define TEMP_CONV_RATE      0x04 /* 1 conv. / sec */
#define SENSOR_ERROR		0x02 /* PVLD bit */

/*************************************************
 *
 * Initializes the temperature sensor TMP421. We do not initialise TMP422 since
 * power on defaults are fine for our use. Power on defaults must be fine for
 * even TMP421s...
 * Returns 0 if OK, -1 if error
 *
 *************************************************/

int init_temp_sensor(int bus, int devAddr)
{
    int hI2C;

    /* Open the I2C device */
    hI2C = open(get_i2c_bus(bus), O_RDWR);
    if(hI2C < 0)
	return -1;

    /* Set slave address */
    if(ioctl(hI2C, I2C_SLAVE, devAddr) < 0) {
	close(hI2C);
	return -1;
    }

    /* set reset reg to 0x0 */
    if(i2c_smbus_write_byte_data(hI2C, RESET_REGISTER, 0x0) < 0) {
	close(hI2C);
	return -1;
    }

    /* Issue first configuration command */
    if(i2c_smbus_write_byte_data(hI2C, CONFIG1_REGISTER, CONFIG1_VALUE) < 0) {
	close(hI2C);
	return -1;
    }

    /* Issue the second configuration command */
    if(i2c_smbus_write_byte_data(hI2C, CONFIG2_REGISTER, CONFIG2_VALUE) < 0) {
	close(hI2C);
	return -1;
    }

    /* set conversion rate register */
    if(i2c_smbus_write_byte_data(hI2C, CONVERSION_RATE_REG, TEMP_CONV_RATE) < 0) {
	close(hI2C);
	return -1;
    }

    /* Done */
    close(hI2C);
    return 0;
}

/*************************************************
 *
 * Reads temperature from "channel" into "pTemp".
 * Returns 0 if OK, -1 if error
 *
 *************************************************/
 
int read_temperature(int bus, int devAddr, double *pTemp, temp_channel channel)
{
  int hI2C;
  __s32 val;
  char high = 0xFF,low = 0xFF;

  /* Validation */
  if(channel > REMOTE2)
    return -1;

  /* Open the I2C device */
  hI2C = open(get_i2c_bus(bus), O_RDWR);
  if(hI2C < 0)
    return -1;

  /* Set slave address */
  if(ioctl(hI2C, I2C_SLAVE, devAddr) < 0) {
    close(hI2C);
    return -1;
  }

  /* Do the transaction */
  val = i2c_smbus_read_byte_data(hI2C, channel + TEMP_REGISTER_HIGH);
  if(val < 0) {
    close(hI2C);
    return -1;
  }
  high = val & 0xFF;
  val  = i2c_smbus_read_byte_data(hI2C, channel + TEMP_REGISTER_LOW);
  if(val < 0) {
    close(hI2C);
    return -1;
  }
  low = val & 0xFF;

  /* Check for error conditions */
  if((low & SENSOR_ERROR) != 0) {
    close(hI2C);
    return -1;
  }
  //fprintf(stderr, "TEMP: high %x low %x\n", high, low);
  /* Convert the data into an actual temperature */
  if(high <= 0x7F)
      *pTemp = (double)high + (double)(low>>4)*0.0625;
  else
      *pTemp = (double)(low>>4)*0.0625 - (double)((high ^ 0xFF)+1);

  /* We are done */
  close(hI2C);
  return 0;
}

/*
 * Return raw temperature(without conversion) for monitoring purposes.
 * The hi byte contains the integral part of the temperature in Celsius.
 * This is valid for temp. >= 0 upto 127 C.
 * When displaying the temp. to user, use the above function which does 
 * the conversion and reports more accurate temperature.
 */
int read_raw_temperature(int bus, int devAddr, temp_channel chan, unsigned char *hiByte)
{
    int hI2C;
    struct i2c_msg message[2];
    struct i2c_rdwr_ioctl_data ctrl;
    uint8_t data[2];
    uint8_t reg;

    /* Validation */
    if(chan > REMOTE2)
       return -1;

    /* Open the I2C device */
    if ((hI2C = open(get_i2c_bus(bus), O_RDWR)) < 0)
        return -1;

    ctrl.nmsgs = 2;
    ctrl.msgs  = message;

    /* set pointer register  to high byte */
    reg              = chan + TEMP_REGISTER_HIGH;
    message[0].addr  = devAddr;
    message[0].flags = 0;
    message[0].len   = 1;
    message[0].buf   = (char *)&reg;

    /* Read two bytes hi & low bytes of temperatures */
    message[1].addr  = devAddr;
    message[1].flags = I2C_M_RD;
    message[1].len   = 2; 
    message[1].buf   = (char *)data;

    if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        close(hI2C);
        return -2;
    }
    close(hI2C);
    if (data[1] & SENSOR_ERROR)
        return -3;
    *hiByte = data[0];
    return 0;
}

/*************************************************
 *
 * Sets the correction factor for "channel" to "factor"
 * Returns 0 if OK, -1 if error
 *
 *************************************************/

int set_correction_factor(int bus, int devAddr, unsigned char factor, temp_channel channel)
{
  int hI2C;
  __u8 reg;

  /* Correction factor cannot be set for the internal sensor */
  if(channel == INTERNAL)
    return -1;

  /* Open the I2C device */
  hI2C = open(get_i2c_bus(bus), O_RDWR);
  if(hI2C < 0)
    return -1;

  /* Set slave address */
  if(ioctl(hI2C, I2C_SLAVE, devAddr) < 0) {
    close(hI2C);
    return -1;
  }

  /* This is a simple "write" operation" */
  reg = CORRECTION_REGISTER + channel;
  if(i2c_smbus_write_byte_data(hI2C, reg, factor) < 0) {
    close(hI2C);
    return -1;
  }

  /* Done */
  close(hI2C);
  return 0;
}

/*************************************************
 *
 * Reads the vendor and device ID into "pvendor", "pdevice"
 * Returns 0 if OK, -1 if error
 * Expected values: 0x55 for vendor, 0x21 for device
 *
 *************************************************/

int get_sensor_id(int bus, int devAddr, unsigned char *pvendor, unsigned char *pdevice)
{
  int hI2C;
  __s32 val;

  /* Open the I2C device */
  hI2C = open(get_i2c_bus(bus), O_RDWR);
  if(hI2C < 0)
    return -1;

  /* Set slave address */
  if(ioctl(hI2C, I2C_SLAVE, devAddr) < 0) {
    close(hI2C);
    return -1;
  }

  /* Read vendor ID */
  val = i2c_smbus_read_byte_data(hI2C,I2C_VENDOR_ID);
  if(val < 0) {
    close(hI2C);
    return -1;
  }
  *pvendor = val & 0xFF;

  /* Read device ID */
  val = i2c_smbus_read_byte_data(hI2C,I2C_DEVICE_ID);
  if(val < 0) {
    close(hI2C);
    return -1;
  }
  *pdevice = val & 0xFF;

  /* read conversion rate register */
  val = i2c_smbus_read_byte_data(hI2C, 0x0B);
  if(val < 0) {
    close(hI2C);
    return -1;
  }
  //fprintf(stderr, "ConvRateReg 0x%X\n", val);
  /* Done */
  close(hI2C);
  return 0;
}
