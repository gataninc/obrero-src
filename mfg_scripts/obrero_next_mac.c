/*
 * Obrero MAC address generation
 * Program to increment the specified MAC address and print the next MAC 
 * address  in the series.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

static int parseByte(char *pStr, unsigned char *pByte)
{
    unsigned char val;
    int ix, len;
    int ret = 0;
    if ((len = strlen(pStr)) > 2)
        return -1;
    for (ix = 0, val = 0; ix < len; ix++)
    {
        val <<= 4;
        if ((pStr[ix] >= '0') && (pStr[ix] <= '9'))
            val = val | (pStr[ix] - '0');
        else if ((pStr[ix] >= 'A') && (pStr[ix] <= 'F'))
            val = val | (pStr[ix] - 'A' + 0xA);
        else if ((pStr[ix] >= 'a') && (pStr[ix] <= 'f'))
            val = val | (pStr[ix] - 'a' + 0xA);
        else
        {
            ret = -1;
            break;
        }
    }
    if (ret == 0)
        *pByte = val;
    return ret;
}

/*
 * since sscanf of glibc does not appear to work, use our own function to parse
 * a mac address.
 */
int parseMacStr(char *macStr, unsigned char macAddr[])
{
    char *ptr;
    int idx;
    int ret = -1;
    if ((macStr) && (macAddr))
    {
        for (idx = 0; idx < 5; idx++)
        {
            ptr = strchr(macStr, ':');
            if (!ptr)
                break;
            *ptr = '\0';
            if (parseByte(macStr, &macAddr[idx]) != 0)
            {
                fprintf(stderr, "Invalid numeric value <%s>\n", macStr);
                break;
            }
            //fprintf(stderr, "%02x:", macAddr[idx]);
            macStr = ++ptr;
            if (!macStr)
                break;
        }
        if (idx == 5)
        {
            if (parseByte(macStr, &macAddr[idx]) != 0)
                fprintf(stderr, "Invalid numeric value <%s>\n", macStr);
            else
            {
                //fprintf(stderr, "%02x <--\n", macAddr[idx]);
                ret = 0;
            }
        }
    }
    return ret;
}

int main(int nargs, char *argv[])
{
    unsigned char macAddr[6];
    int i;
    if (nargs != 2)
    {
        printf("%s MAC address\n   Prints the next MAX addr.\n", argv[0]);
        exit(1);
    }
    // split MAC address into octets
    if (parseMacStr(argv[1], macAddr) != 0)
    {
        printf("Invalid MAC address\n");
        exit(1);
    }
    //for (i = 0; i < 6; i++)
    //    printf("%02X ", macAddr[i]);
    //printf("\n");
    // sanity check..
    if (macAddr[3] != 5)
    {
        printf("!! Error: Invalid MAC address for Obrero !!\n");
        exit(1);
    }
    if ((macAddr[4] == 0xFF) && (macAddr[5] == 0xFF))
        goto  MACLimit;
    // simple case
    if (macAddr[5] < 0xFF)
    {
        macAddr[5]++;
        goto DoneNextMAC;
    }
    if (macAddr[4] < 0xFF)
    {
        macAddr[4]++;
        macAddr[5] = 0;
        goto DoneNextMAC;
    }
MACLimit:
    printf("!! ERROR: MAC address LIMIT reached !!\n");
    exit(1);
DoneNextMAC:
    for (i = 0; i < 6; i++)
        printf("%02X%s", macAddr[i], ((i < 5) ? ":" : ""));
    printf("\n");
    exit(0);
}
