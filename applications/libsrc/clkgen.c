#include <stdio.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include "i2c-dev.h"
#include "utils.h"

// read specified register from the clkgen chip
int readClkGenReg(int bus, int devAddr, int reg, uint8_t *value)
{
    int hI2C;
    struct i2c_msg message[2];
    struct i2c_rdwr_ioctl_data ctrl;
    unsigned char dataReg[1];

    /* Open the I2C device */
    if ((hI2C = open(get_i2c_bus(bus), O_RDWR)) < 0)
        return -1;
    /* set-up ioctl parameters */
    ctrl.msgs  = message;
    ctrl.nmsgs = 2;

    message[0].addr   = devAddr;
    message[0].flags  = 0;
    message[0].len    = 1;
    message[0].buf    = (char *)&dataReg[0];
    dataReg[0]        = reg;

    message[1].addr   = devAddr;
    message[1].flags  = I2C_M_RD;
    message[1].len    = 1; /* read one byte */
    message[1].buf    = (char *)value;

    if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        close(hI2C);
        return -2;
    }
    close(hI2C);
    return 0;
}

// write specified register in clkgen chip with specified value
int writeClkGenReg(int bus, int devAddr, int reg, uint8_t value)
{
    int hI2C;
    struct i2c_msg message[2];
    struct i2c_rdwr_ioctl_data ctrl;
    unsigned char dataReg[2];

    /* Open the I2C device */
    if ((hI2C = open(get_i2c_bus(bus), O_RDWR)) < 0)
        return -1;
    /* set-up ioctl parameters */
    ctrl.msgs  = message;
    ctrl.nmsgs = 1;

    dataReg[0]        = reg;
    dataReg[1]        = value;

    message[0].addr   = devAddr;
    message[0].flags  = 0;
    message[0].len    = 2;
    message[0].buf    = (char *)&dataReg[0];

    if (ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        close(hI2C);
        return -2;
    }
    close(hI2C);
    return 0;
}
