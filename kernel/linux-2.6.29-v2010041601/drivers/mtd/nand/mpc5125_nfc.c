/*
 * Copyright (C) 2009 Freescale Semiconductor, Inc. All rights reserved.
 *
 * Author: Shaohui Xie <b21989@freescale.com>
 *
 * Description:
 * MPC5125 Nand driver.
 *
 * Based on original driver mpc5121_nfc.c.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
 /*
 modyfied by Cloudy Chen <chen_yunsong@mtcera.com>
 */

#include <linux/module.h>
#include <linux/clk.h>
#include <linux/delay.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/nand.h>
#include <linux/mtd/partitions.h>
#include <linux/of_device.h>
#include <linux/of_platform.h>

#include <asm/mpc512x.h>
#include <asm/mpc5125_nfc.h>
#include <linux/dma-mapping.h>

#define	DRV_NAME		"mpc5125_nfc"
#define	DRV_VERSION		"0.5"
#define	SPARE_BUFFER_MAX_SIZE	0x400
#define	DATA_BUFFER_MAX_SIZE		0x2000
/* Timeouts */
#define NFC_RESET_TIMEOUT	1000		/* 1 ms */
#define NFC_TIMEOUT		(HZ / 10)	/* 1/10 s */

#ifdef CONFIG_NFC_DMA_ENABLE
#define NFC_DMA_ENABLE		1
#else
#define NFC_DMA_ENABLE		0
#endif
struct mpc5125_nfc_prv {
	struct mtd_info		mtd;
	struct device *dev;
	struct nand_chip	chip;
#ifdef CONFIG_MTD_PARTITIONS
	struct mtd_partition *parts;
	int nr_parts;
#endif
	int			irq;
	void __iomem		*regs;
	struct clk		*clk;
	wait_queue_head_t	irq_waitq;
	uint			column;
	int			spareonly;
	u32		irq_stat;
	u32		wait_timeout;
	void __iomem		*csreg;
	void  *data_buffers;
       dma_addr_t      data_buffers_phyaddr;
       void  *ops_buffer;
       dma_addr_t      ops_buffer_phyaddr;
       void *tmp_buf;
	struct semaphore  int_sem;
	 unsigned int sync_flags;
};

static int get_status;
static int get_id;

#define	NFC_IRQ_ENABLE	(IDLE_EN_MASK|WERR_EN_MASK)
#define	NFC_IRQ_MASK		(IDLE_IRQ_MASK|WERR_IRQ_MASK)

#ifdef CONFIG_MTD_NAND_MPC5125_HARDWARE_ECC_CORRECTION
static int hardware_ecc = 1;
#else
static int hardware_ecc = 0;
#endif
#define	MPC5125_NFC_ECC_STATUS_ADD	(NFC_SPARE_AREA(0)+0xf0)
#if 1
/*for ecc_MODE=0x6 45bytes*2*/
static struct nand_ecclayout nand_hw_eccoob_4k = {
	.eccbytes = 90,	/* actually 72 but only room for 64 */
	.eccpos = {
		/* 9 bytes of ecc for each 512 bytes of data */
		19,20,21,22,23,24,25,26,27,28,29,30,
		31,32,33,34,35,36,37,38,39,40,
		41, 42, 43, 44, 45, 46, 47,48,49,50,
		51,52,53,54,55, 56, 57, 58, 59, 60,
		61, 62, 63,
		83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,
		98,99,100,
		101,102,103,104,105,106,107,108,109,110,
		111,112,113,114,115,116,117,118,119,120,
		121,122,123,124,125,126,127/* 120, 121, 122, 123, 124, 125, 126, 127, */
	},
	.oobavail = 30,
	.oobfree = { {4, 15}, {68, 15}}
};
#else
/*for ecc_MODE=0x5 30bytes*2*/
static struct nand_ecclayout nand_hw_eccoob_4k = {
	.eccbytes = 60,	/* actually 72 but only room for 64 */
	.eccpos = {
		/* 9 bytes of ecc for each 512 bytes of data */
		34,35,36,37,38,39,40,
		41, 42, 43, 44, 45, 46, 47,48,49,50,
		51,52,53,54,55, 56, 57, 58, 59, 60,
		61, 62, 63,
		98,99,100,
		101,102,103,104,105,106,107,108,109,110,
		111,112,113,114,115,116,117,118,119,120,
		121,122,123,124,125,126,127/* 120, 121, 122, 123, 124, 125, 126, 127, */
	},
	.oobavail = 48,
	.oobfree = { {8, 24}, {68, 24}}
};
#endif

#if NFC_DMA_ENABLE
static void mpc5125_dma_config(struct mtd_info *mtd, struct nand_chip *chip,unsigned isRead);
static void mpc5125_nand_dma_wait(struct mtd_info *mtd, struct nand_chip *chip);
#endif

static struct nand_ecclayout nand_hw_eccoob_4k_218_spare = {
	.eccbytes = 64,	/* actually 144 but only room for 64 */
	.eccpos = {
		/* 18 bytes of ecc for each 512 bytes of data */
		7, 8, 9, 10, 11, 12, 13, 14, 15,
		    16, 17, 18, 19, 20, 21, 22, 23, 24,
		33, 34, 35, 36, 37, 38, 39, 40, 41,
		    42, 43, 44, 45, 46, 47, 48, 49, 50,
		59, 60, 61, 62, 63, 64, 65, 66, 67,
		    68, 69, 70, 71, 72, 73, 74, 75, 76,
		85, 86, 87, 88, 89, 90, 91, 92, 93,
		    94, /* 95, 96, 97, 98, 99, 100, 101, 102,
		111, 112, 113, 114, 115, 116, 117, 118, 119,
		    120, 121, 122, 123, 124, 125, 126, 127, 128,
		137, 138, 139, 140, 141, 142, 143, 144, 145,
		    146, 147, 148, 149, 150, 151, 152, 153, 154,
		163, 164, 165, 166, 167, 168, 169, 170, 171,
		    172, 173, 174, 175, 176, 177, 178, 179, 180,
		189, 190, 191, 192, 193, 194, 195, 196, 197,
		    198, 199, 200, 201, 202, 203, 204, 205, 206, */
	},
	.oobavail = 4,
	.oobfree = {{0, 5}, {26, 8}, {52, 8}, {78, 8},
		    {104, 8}, {130, 8}, {156, 8}, {182, 8}}
};

#ifdef CONFIG_MTD_PARTITIONS
static const char *mpc5125_nfc_pprobes[] = { "cmdlinepart", NULL };
#endif

static inline u32 nfc_read(struct mtd_info *mtd, uint reg)
{
	struct nand_chip *chip = mtd->priv;
	struct mpc5125_nfc_prv *prv = chip->priv;

	return in_be32(prv->regs + reg);
}

/* Write NFC register */
static inline void nfc_write(struct mtd_info *mtd, uint reg, u32 val)
{
	struct nand_chip *chip = mtd->priv;
	struct mpc5125_nfc_prv *prv = chip->priv;
	out_be32(prv->regs + reg, val);
}

/* Set bits in NFC register */
static inline void nfc_set(struct mtd_info *mtd, uint reg, u32 bits)
{
	nfc_write(mtd, reg, nfc_read(mtd, reg) | bits);
}

/* Clear bits in NFC register */
static inline void nfc_clear(struct mtd_info *mtd, uint reg, u32 bits)
{
	nfc_write(mtd, reg, nfc_read(mtd, reg) & ~bits);
}


static inline void
nfc_set_field(struct mtd_info *mtd, u32 reg, u32 mask, u32 shift, u32 val)
{
	struct nand_chip *chip = mtd->priv;
	struct mpc5125_nfc_prv *prv = chip->priv;

	out_be32(prv->regs + reg,
			(in_be32(prv->regs + reg) & (~mask))
			| val << shift);
}

static inline int
nfc_get_field(struct mtd_info *mtd, u32 reg, u32 field_mask)
{
	struct nand_chip *chip = mtd->priv;
	struct mpc5125_nfc_prv *prv = chip->priv;

	return in_be32(prv->regs + reg) & field_mask;
}

static inline u8 nfc_check_status(struct mtd_info *mtd)
{
	u8 fls_status = 0;
	fls_status = nfc_get_field(mtd, NFC_FLASH_STATUS2, STATUS_BYTE1_MASK);
	return fls_status;
}

/* clear cmd_done and cmd_idle falg for the coming command */
static void mpc5125_nfc_clear(struct mtd_info *mtd)
{
	nfc_write(mtd, NFC_IRQ_STATUS, 1 << CMD_DONE_CLEAR_SHIFT);
	nfc_write(mtd, NFC_IRQ_STATUS, 1 << IDLE_CLEAR_SHIFT);
	nfc_write(mtd, NFC_IRQ_STATUS, 1 << WERR_CLEAR_SHIFT);
}

/* Wait for operation complete */
static void mpc5125_nfc_done(struct mtd_info *mtd)
{
	struct nand_chip *chip = mtd->priv;
	struct mpc5125_nfc_prv *prv = chip->priv;
	int rv;
	unsigned int wait_time=NFC_TIMEOUT;
	mpc5125_nfc_clear(mtd);
	nfc_set(mtd, NFC_IRQ_STATUS, NFC_IRQ_ENABLE);
	prv->wait_timeout=0;
	prv->sync_flags=0;
	nfc_set_field(mtd, NFC_FLASH_CMD2, START_MASK,
			START_SHIFT, 1);
	{
	if (!(nfc_read(mtd,NFC_IRQ_STATUS)&(NFC_IRQ_MASK))){
		rv = wait_event_timeout(prv->irq_waitq,
			(nfc_read(mtd,NFC_IRQ_STATUS)&NFC_IRQ_MASK),
			wait_time);
		if ((!rv))
		{
			prv->irq_stat=nfc_read(mtd,NFC_IRQ_STATUS);

			if(!(prv->sync_flags))
				printk(KERN_WARNING DRV_NAME
				":Lost irq :%08x.\n", prv->sync_flags);

			printk(KERN_WARNING DRV_NAME
				": Timeout while waiting for BUSY :%08x.\n",prv->irq_stat);
			prv->wait_timeout=1;

		}
	}
	}
	mpc5125_nfc_clear(mtd);
}

static inline u8 mpc5125_nfc_get_id(struct mtd_info *mtd, int col)
{
	u32 flash_id1 = 0;
	u8 *pid;

	flash_id1 = nfc_read(mtd, NFC_FLASH_STATUS1);
	pid = (u8 *)&flash_id1;

	return *(pid + col);
}

static inline u8 mpc5125_nfc_get_status(struct mtd_info *mtd)
{
	u32 flash_status = 0;
	u8 *pstatus;

	flash_status = nfc_read(mtd, NFC_FLASH_STATUS2);
	pstatus = (u8 *)&flash_status;

	return *(pstatus + 3);
}

/* Invoke command cycle */
static inline void
mpc5125_nfc_send_cmd(struct mtd_info *mtd, u32 cmd_byte1,
		u32 cmd_byte2, u32 cmd_code)
{
	mpc5125_nfc_clear(mtd);
	nfc_set_field(mtd, NFC_FLASH_CMD2, CMD_BYTE1_MASK,
			CMD_BYTE1_SHIFT, cmd_byte1);

	nfc_set_field(mtd, NFC_FLASH_CMD1, CMD_BYTE2_MASK,
			CMD_BYTE2_SHIFT, cmd_byte2);

	nfc_set_field(mtd, NFC_FLASH_CMD2, BUFNO_MASK,
			BUFNO_SHIFT, 0);

	nfc_set_field(mtd, NFC_FLASH_CMD2, CMD_CODE_MASK,
			CMD_CODE_SHIFT, cmd_code);

	if (cmd_code == RANDOM_OUT_CMD_CODE)
		nfc_set_field(mtd, NFC_FLASH_CMD2, BUFNO_MASK,
			BUFNO_SHIFT, 1);
}

/* Receive ID and status from NAND flash */
static inline void
mpc5125_nfc_send_one_byte(struct mtd_info *mtd, u32 cmd_byte1, u32 cmd_code)
{
	mpc5125_nfc_clear(mtd);
	nfc_set_field(mtd, NFC_FLASH_CMD2, CMD_BYTE1_MASK,
			CMD_BYTE1_SHIFT, cmd_byte1);

	nfc_set_field(mtd, NFC_FLASH_CMD2, BUFNO_MASK,
			BUFNO_SHIFT, 0);

	nfc_set_field(mtd, NFC_FLASH_CMD2, CMD_CODE_MASK,
			CMD_CODE_SHIFT, cmd_code);
}

/* NFC interrupt handler */
static irqreturn_t mpc5125_nfc_irq(int irq, void *data)
{
	struct mtd_info *mtd = data;
	struct nand_chip *chip = mtd->priv;
	struct mpc5125_nfc_prv *prv = chip->priv;
	prv->irq_stat=nfc_read(mtd,NFC_IRQ_STATUS);
	nfc_clear(mtd, NFC_IRQ_STATUS, NFC_IRQ_ENABLE);
	wake_up(&prv->irq_waitq);
	/*mpc5125_nfc_clear(mtd);*/
	prv->sync_flags|=1;
	return IRQ_HANDLED;
}

/* Do address cycle(s) */
static void mpc5125_nfc_addr_cycle(struct mtd_info *mtd, int column, int page)
{

	if (column != -1) {
		nfc_set_field(mtd, NFC_COL_ADDR,
				COL_ADDR_MASK,
				COL_ADDR_SHIFT, column);
	}

	if (page != -1) {
		nfc_set_field(mtd, NFC_ROW_ADDR,
				ROW_ADDR_MASK,
				ROW_ADDR_SHIFT, page);
	}
	/* DMA Disable */
#if (NFC_DMA_ENABLE<1)
	nfc_clear(mtd, NFC_FLASH_CONFIG, CONFIG_DMA_REQ_MASK);
#endif
	/* PAGE_CNT = 1 */
	nfc_set_field(mtd, NFC_FLASH_CONFIG, CONFIG_PAGE_CNT_MASK,
			CONFIG_PAGE_CNT_SHIFT, 0x2);
}


/* Control chips select signal on ADS5125 board */
static void ads5125_select_chip(struct mtd_info *mtd, int chip)
{

	if ((chip < 0)||(chip>3)) {
			nfc_set_field(mtd, NFC_ROW_ADDR,
			ROW_ADDR_CHIP_SEL_RB_MASK,
			ROW_ADDR_CHIP_SEL_RB_SHIFT, 0);

			nfc_set_field(mtd, NFC_ROW_ADDR,
			ROW_ADDR_CHIP_SEL_MASK,
			ROW_ADDR_CHIP_SEL_SHIFT, 0);
		return;
	}

			nfc_set_field(mtd, NFC_ROW_ADDR,
			ROW_ADDR_CHIP_SEL_RB_MASK,
			ROW_ADDR_CHIP_SEL_RB_SHIFT, (1<<chip));

			nfc_set_field(mtd, NFC_ROW_ADDR,
			ROW_ADDR_CHIP_SEL_MASK,
			ROW_ADDR_CHIP_SEL_SHIFT, (1<<chip));

}

/* Read NAND Ready/Busy signal */
static int mpc5125_nfc_dev_ready(struct mtd_info *mtd)
{
	/*
	 * NFC handles ready/busy signal internally. Therefore, this function
	 * always returns status as ready.
	 */
	return 1;
}

/* Write command to NAND flash */
static void mpc5125_nfc_command(struct mtd_info *mtd, unsigned command,
						int column, int page)
{
	struct nand_chip *chip = mtd->priv;
	struct mpc5125_nfc_prv *prv = chip->priv;

	prv->column = (column >= 0) ? column : 0;
	prv->spareonly = 0;
	get_id = 0;
	get_status = 0;


	switch (command) {
	case NAND_CMD_PAGEPROG:
		mpc5125_nfc_send_cmd(mtd,
				PROGRAM_PAGE_CMD_BYTE1,
				PROGRAM_PAGE_CMD_BYTE2,
#if (NFC_DMA_ENABLE)
				DMA_PROGRAM_PAGE_CMD_CODE);
		/*
		nfc_set_field(mtd, NFC_FLASH_CMD2, CMD_BYTE1_MASK,
			CMD_BYTE1_SHIFT, READ_STATUS_CMD_BYTE);
			*/
		mpc5125_dma_config(mtd,chip,0);
#else
				PROGRAM_PAGE_CMD_CODE);
#endif
		break;
	/*
	 * NFC does not support sub-page reads and writes,
	 * so emulate them using full page transfers.
	 */
	case NAND_CMD_READ0:
		column = 0;
		goto read0;
		break;

	case NAND_CMD_READ1:
		prv->column += 256;
		command = NAND_CMD_READ0;
		column = 0;
		goto read0;
		break;

	case NAND_CMD_READOOB:
		prv->spareonly = 1;
		command = NAND_CMD_READ0;
		column = 0;
read0:

		mpc5125_nfc_send_cmd(mtd,
				PAGE_READ_CMD_BYTE1,
				PAGE_READ_CMD_BYTE2,
				READ_PAGE_CMD_CODE);
#if NFC_DMA_ENABLE
		mpc5125_dma_config( mtd,  chip,1);
#endif
		break;

	case NAND_CMD_SEQIN:
		mpc5125_nfc_command(mtd, NAND_CMD_READ0, column, page);
		column = 0;
		break;

	case NAND_CMD_ERASE1:
		mpc5125_nfc_send_cmd(mtd,
				ERASE_CMD_BYTE1,
				ERASE_CMD_BYTE2,
				ERASE_CMD_CODE);
		break;
	case NAND_CMD_ERASE2:
		return;
	case NAND_CMD_READID:
		get_id = 1;
		mpc5125_nfc_send_one_byte(mtd, command, READ_ID_CMD_CODE);
		break;
	case NAND_CMD_STATUS:
		get_status = 1;
		mpc5125_nfc_send_one_byte(mtd, command, STATUS_READ_CMD_CODE);
		break;
	case NAND_CMD_RNDOUT:
		mpc5125_nfc_send_cmd(mtd,
				RANDOM_OUT_CMD_BYTE1,
				RANDOM_OUT_CMD_BYTE2,
				RANDOM_OUT_CMD_CODE);
		break;
	default:
		return;
	}

	mpc5125_nfc_addr_cycle(mtd, column, page);
	mpc5125_nfc_done(mtd);

#if (NFC_DMA_ENABLE)
	/*mpc5125_nand_dma_wait(mtd,chip);*/
	nfc_clear(mtd, NFC_FLASH_CONFIG, CONFIG_DMA_REQ_MASK);
#endif


}

/* Copy data from/to NFC spare buffers. */
static void mpc5125_nfc_copy_spare(struct mtd_info *mtd, uint offset,
						u8 *buffer, uint size, int wr)
{
	struct nand_chip *nand = mtd->priv;
	struct mpc5125_nfc_prv *prv = nand->priv;
	u16 ooblen = mtd->oobsize;
	u8 i, count;
	uint  sbsize, blksize;

	/*
	 * NAND spare area is available through NFC spare buffers.
	 * The NFC divides spare area into (page_size / 512) chunks.
	 * Each chunk is placed into separate spare memory area, using
	 * first (spare_size / num_of_chunks) bytes of the buffer.
	 *
	 * For NAND device in which the spare area is not divided fully
	 * by the number of chunks, number of used bytes in each spare
	 * buffer is rounded down to the nearest even number of bytes,
	 * and all remaining bytes are added to the last used spare area.
	 *
	 * For more information read section 26.6.10 of MPC5121e
	 * Microcontroller Reference Manual, Rev. 3.
	 */

	/* Calculate number of valid bytes in each spare buffer */
	count = mtd->writesize >> 11;
	count=(count>0)?count:1;
	sbsize = (ooblen / count >> 1) << 1;
	/*printk("%s line:%d  %s len:%d\n",__FUNCTION__,__LINE__,wr?"write":"read",size);*/
	for(i=0;(i<count)&&size;i++)
	{
		blksize = min(sbsize, size);
		if(wr)
		{
			memcpy_toio(prv->regs + NFC_SPARE_AREA(i) ,
							buffer, blksize);
		}
		else
		{
			memcpy_fromio(buffer,
				prv->regs + NFC_SPARE_AREA(i), blksize);
		}
		/*mpc5125_spare_debug(buffer,blksize);*/
		buffer += blksize;
		offset += blksize;
		size -= blksize;
	}
}

/* Copy data from/to NFC main and spare buffers */
static void mpc5125_nfc_buf_copy(struct mtd_info *mtd, u_char *buf, int len,
									int wr)
{
	struct nand_chip *chip = mtd->priv;
	struct mpc5125_nfc_prv *prv = chip->priv;
	uint c = prv->column;
	uint l;
	/* Handle spare area access */
	if (prv->spareonly || c >= mtd->writesize) {
		/* Calculate offset from beginning of spare area */
		if (c >= mtd->writesize)
			c -= mtd->writesize;

		prv->column += len;
#if NFC_DMA_ENABLE
		if(wr)
		{
			memcpy(prv->ops_buffer,buf,len);
		}
		else
		{
			memcpy(buf,prv->ops_buffer,len);
		}
#else
		mpc5125_nfc_copy_spare(mtd, c, buf, len, wr);
#endif
		return;
	}

	/*
	 * Handle main area access - limit copy length to prevent
	 * crossing main/spare boundary.
	 */
	l = min((uint)len, mtd->writesize - c);
	/*
	printk("%s line:%d %s l=0x%x,mtd->writesize=0x%x,c=0x%x \n",__FUNCTION__,__LINE__,wr?"write":"read",l,mtd->writesize,c);
	*/
	prv->column += l;

	if (wr)
	{
		unsigned int size,i;
#if NFC_DMA_ENABLE
		memcpy(prv->data_buffers+c,buf,len);
#else
		for(i=(c/PAGE_2K);i<4;i++)
		{
			size=min(len,PAGE_2K);
			memcpy_toio(prv->regs + NFC_MAIN_AREA(i) + c, buf, size);
			buf+=size;
			len-=size;
			if(!len)break;
		}
#endif
	}
	else {
		if (get_status) {
			get_status = 0;
			*buf = mpc5125_nfc_get_status(mtd);
		} else if (l == 1 && c <= 3 && get_id) {
			*buf = mpc5125_nfc_get_id(mtd, c);
		} else
		{
			unsigned int size,i;
#if NFC_DMA_ENABLE
			if(len==mtd->writesize)
				{
			memcpy(buf,prv->data_buffers+c,len);
				}
			else
#endif
			for(i=(c/PAGE_2K);i<4;i++)
			{
				size=min(len,PAGE_2K);
				memcpy_fromio(buf,prv->regs + NFC_MAIN_AREA(i) + c, size);
				buf+=size;
				len-=size;
				if(!len)break;
			}
		}
	}
}

/* Read data from NFC buffers */
static void mpc5125_nfc_read_buf(struct mtd_info *mtd, u_char *buf, int len)
{
	mpc5125_nfc_buf_copy(mtd, buf, len, 0);
}

/* Write data to NFC buffers */
static void mpc5125_nfc_write_buf(struct mtd_info *mtd,
						const u_char *buf, int len)
{
	mpc5125_nfc_buf_copy(mtd, (u_char *)buf, len, 1);
}

/* Compare buffer with NAND flash */
static int mpc5125_nfc_verify_buf(struct mtd_info *mtd,
						const u_char *buf, int len)
{
	u_char tmp[256];
	uint bsize;
	while (len) {
		bsize = min(len, 256);
		mpc5125_nfc_read_buf(mtd, tmp, bsize);
		if (memcmp(buf, tmp, bsize))
			return 1;
		buf += bsize;
		len -= bsize;
	}
	return 0;
}

/* Read byte from NFC buffers */
static u8 mpc5125_nfc_read_byte(struct mtd_info *mtd)
{
	u8 tmp;
	mpc5125_nfc_read_buf(mtd, &tmp, sizeof(tmp));
	return tmp;
}

/* Read word from NFC buffers */
static u16 mpc5125_nfc_read_word(struct mtd_info *mtd)
{
	u16 tmp;
	mpc5125_nfc_read_buf(mtd, (u_char *)&tmp, sizeof(tmp));
	return tmp;
}

/*
 * Read NFC configuration from Reset Config Word
 *
 */
static int mpc5125_nfc_read_hw_config(struct mtd_info *mtd)
{
	uint rcw_pagesize = 0;
	uint rcw_sparesize = 0;

	rcw_pagesize = 4096;
	rcw_sparesize = 128;
	mtd->writesize = rcw_pagesize;
	mtd->oobsize = rcw_sparesize;
	return 0;
}

/* Free driver resources */
static void mpc5125_nfc_free(struct device *dev, struct mtd_info *mtd)
{
	struct nand_chip *chip = mtd->priv;
	struct mpc5125_nfc_prv *prv = chip->priv;

	if (prv->clk) {
		clk_disable(prv->clk);
		clk_put(prv->clk);
	}
	if (prv->csreg)
		iounmap(prv->csreg);
}

#ifdef CONFIG_MTD_PARTITIONS
static int parse_flash_partitions(struct mtd_info *mtd,struct device_node *dp)
{
	int i;
	const  char *name;
	struct device_node *pp;
	struct nand_chip *chip = mtd->priv;
	struct mpc5125_nfc_prv *prv = chip->priv;
	int nr_parts = 0;

	for (pp = dp->child; pp; pp = pp->sibling)
		nr_parts++;

	if (!nr_parts)
		return 0;

	prv->parts = devm_kzalloc(prv->dev,
			nr_parts * sizeof(struct mtd_partition),
			GFP_KERNEL);
	if (!prv->parts)
		return -ENOMEM;
	for (pp = dp->child, i =0; pp; pp = pp->sibling, i++) {
		const u32 *reg;
		int len;

		reg = of_get_property(pp, "reg", &len);
		if (!reg || (len != 2*sizeof(u32))) {
			printk(KERN_ERR DRV_NAME ": "
				"Invalid 'reg' on %s\n", dp->full_name);
			kfree(prv->parts);
			prv->parts = NULL;
			return -EINVAL;
		}
		prv->parts[i].offset = reg[0];
		prv->parts[i].size = reg[1];

		name = of_get_property(pp, "label", &len);
		if (!name)
			name = of_get_property(pp, "name", &len);
		prv->parts[i].name = (char *)name;

		if (of_get_property(pp, "read-only", &len))
			prv->parts[i].mask_flags = MTD_WRITEABLE;
	}
	return nr_parts;
}
#endif
static void mpc5125_nand_enable_hwecc(struct mtd_info *mtd, int mode)
{
	nfc_set_field(mtd, NFC_FLASH_CONFIG,
			CONFIG_ECC_MODE_MASK,
			CONFIG_ECC_MODE_SHIFT, ECC_30_BYTE);
	return;
}
/*
 * Function to correct the detected errors. This NFC corrects all the errors
 * detected. So this function is not required.
 */
static int mpc5125_nand_correct_data(struct mtd_info *mtd, u_char * dat,
				 u_char * read_ecc, u_char * calc_ecc)
{
	panic("Shouldn't be called here: %d\n", __LINE__);
	return 0;		/* FIXME */
}

/*
 * Function to calculate the ECC for the data to be stored in the Nand device.
 * This NFC has a hardware RS(511,503) ECC engine together with the RS ECC
 * CONTROL blocks are responsible for detection  and correction of up to
 * 4 symbols of 9 bits each in 528 byte page.
 * So this function is not required.
 */

static int mpc5125_nand_calculate_ecc(struct mtd_info *mtd, const u_char * dat,
				  u_char * ecc_code)
{
	panic(KERN_ERR "Shouldn't be called here %d \n", __LINE__);
	return 0;		/* FIXME */
}
static int mpc5125_nand_read_oob(struct mtd_info *mtd, struct nand_chip *chip,
			     int page, int sndcmd)
{

	if(sndcmd)
	{
		mpc5125_nfc_command(mtd,NAND_CMD_READ0,0,page);
		sndcmd=0;
	}
#if NFC_DMA_ENABLE
{
	struct mpc5125_nfc_prv *prv = chip->priv;
	memcpy(chip->oob_poi,prv->ops_buffer, mtd->oobsize);
}
#else
	mpc5125_nfc_copy_spare(mtd,0,chip->oob_poi, mtd->oobsize,0);
#endif
	return sndcmd;
}

static int mpc5125_nand_write_oob(struct mtd_info *mtd, struct nand_chip *chip,
			      int page)
{

	unsigned int stat;
	struct mpc5125_nfc_prv *prv = chip->priv;
	mpc5125_nfc_command(mtd,NAND_CMD_READ0,0,page);
#if NFC_DMA_ENABLE
	memcpy(prv->ops_buffer,chip->oob_poi, mtd->oobsize);
#else
	mpc5125_nfc_copy_spare(mtd,0,chip->oob_poi, mtd->oobsize,1);
#endif
	mpc5125_nfc_command(mtd,NAND_CMD_PAGEPROG,0,page);
	if(prv->wait_timeout)
	{
		printk(KERN_ERR"%s line:%d wait timeout.\n",__FUNCTION__,__LINE__);
		return -EIO;
	}
	if(prv->irq_stat&WERR_IRQ_MASK)
	{
		printk(KERN_ERR"%s line:%d faield.\n",__FUNCTION__,__LINE__);
		return -EIO;
	}
	return 0;
}

static int mpc5125_nand_read_page(struct mtd_info *mtd, struct nand_chip *chip,
			      uint8_t * buf)
{
	unsigned int stat;
	struct mpc5125_nfc_prv *prv = chip->priv;
	u8 *erase_page_check,ecc_bytes=0,i;
	u8 ecc_bytes_map[]={0,8,12,15,23,30,45,60};
	stat=nfc_read(mtd, NFC_FLASH_CONFIG);
	stat>>=17;
	stat&=0x7;
	ecc_bytes=ecc_bytes_map[stat];
	erase_page_check=(u8 *)(PAGE_virtual_2K-ecc_bytes+prv->regs);
	stat=nfc_read(mtd, MPC5125_NFC_ECC_STATUS_ADD+4);
	DEBUG(MTD_DEBUEVEL3,"%s line:%d stat:%08x\n",__FUNCTION__,__LINE__,stat);
	if(stat&0x80)
	{
		/*check the page is erased*/
		if(stat&0x3f)
		{
			mtd->ecc_stats.failed++;
			printk(KERN_WARNING "UnCorrectable RS-ECC Error\n");
		}

	}
	else if(stat&0x3f)
	{
		/*printk(KERN_WARNING "Correctable ECC %d\n",stat&0x3f);*/
		mtd->ecc_stats.corrected+=stat&0x3f;
	}
#if NFC_DMA_ENABLE
	memcpy(buf,prv->data_buffers,mtd->writesize);
	memcpy(chip->oob_poi,prv->ops_buffer, mtd->oobsize);
#else
	mpc5125_nfc_buf_copy(mtd, buf, mtd->writesize, 0);
	mpc5125_nfc_copy_spare(mtd,0,chip->oob_poi, mtd->oobsize,0);
#endif
	return 0;
}

static void mpc5125_nand_write_page(struct mtd_info *mtd, struct nand_chip *chip,
				const uint8_t * buf)
{
#if NFC_DMA_ENABLE
	struct mpc5125_nfc_prv *prv = chip->priv;
	memcpy(prv->data_buffers,buf,mtd->writesize);
	memcpy(prv->ops_buffer,chip->oob_poi, mtd->oobsize);
#else
	mpc5125_nfc_buf_copy(mtd, buf, mtd->writesize, 1);
	mpc5125_nfc_copy_spare(mtd,0,chip->oob_poi, mtd->oobsize,1);
#endif

}
static int chip_nand_write_page(struct mtd_info *mtd, struct nand_chip *chip,
			   const uint8_t *buf, int page, int cached, int raw)
{
	int status;
#if (NFC_DMA_ENABLE<1)
	chip->cmdfunc(mtd, NAND_CMD_SEQIN, 0x00, page);
#endif
	if (unlikely(raw))
		chip->ecc.write_page_raw(mtd, chip, buf);
	else
		chip->ecc.write_page(mtd, chip, buf);

	/*
	 * Cached progamming disabled for now, Not sure if its worth the
	 * trouble. The speed gain is not very impressive. (2.3->2.6Mib/s)
	 */
	cached = 0;

	if (!cached || !(chip->options & NAND_CACHEPRG)) {
#if NFC_DMA_ENABLE
		chip->cmdfunc(mtd, NAND_CMD_PAGEPROG, 0x00, page);
#else
		chip->cmdfunc(mtd, NAND_CMD_PAGEPROG, -1, -1);
#endif

		status = chip->waitfunc(mtd, chip);
		/*
		 * See if operation failed and additional status checks are
		 * available
		 */
		if ((status & NAND_STATUS_FAIL) && (chip->errstat))
			status = chip->errstat(mtd, chip, FL_WRITING, status,
					       page);

		if (status & NAND_STATUS_FAIL)
			return -EIO;
	} else {
#if NFC_DMA_ENABLE
		chip->cmdfunc(mtd, NAND_CMD_PAGEPROG, 0x00, page);
#else
		chip->cmdfunc(mtd, NAND_CMD_PAGEPROG, -1, -1);
#endif
		status = chip->waitfunc(mtd, chip);
	}
	if(nfc_get_field(mtd, NFC_IRQ_STATUS,WERR_IRQ_MASK|WERR_STATUS_MASK))
	{
		printk(KERN_ERR"%s line:%d write page %d failed\n",__FUNCTION__,__LINE__,page);
		nfc_set_field(mtd, NFC_IRQ_STATUS,
			WERR_CLEAR_MASK,
			WERR_CLEAR_SHIFT, 1);
		return -EIO;
	}
	return 0;
}
#if NFC_DMA_ENABLE
static void mpc5125_dma_config(struct mtd_info *mtd, struct nand_chip *chip,unsigned isRead)
{
	struct mpc5125_nfc_prv *prv = chip->priv;
	nfc_write(mtd, NFC_DMA1_ADDR,prv->data_buffers_phyaddr);
	nfc_write(mtd, NFC_DMA2_ADDR,prv->ops_buffer_phyaddr);
	if(isRead)
		nfc_set_field(mtd, NFC_FLASH_CONFIG,
                       CONFIG_DMA_REQ_MASK,
                       CONFIG_DMA_REQ_SHIFT, 1);
	else
		nfc_set_field(mtd, NFC_FLASH_CONFIG,
                       CONFIG_DMA_REQ_MASK,
                       CONFIG_DMA_REQ_SHIFT, 0);
         nfc_set_field(mtd, NFC_FLASH_COMMAND_REPEAT,
                       COMMAND_REPEAT_MASK,
                       COMMAND_REPEAT_SHIFT, 0);
	nfc_set_field(mtd, NFC_FLASH_CMD2, BUFNO_MASK,
		BUFNO_SHIFT, 0);
}
static void mpc5125_nand_dma_wait(struct mtd_info *mtd, struct nand_chip *chip)
{
	struct mpc5125_nfc_prv *prv = chip->priv;
	if(((DMA_BUSY_MASK|ECC_BUSY_MASK|RESIDUE_BUSY_MASK)&nfc_read(mtd,NFC_IRQ_STATUS)))
	{
		int rv;
		 rv=wait_event_timeout(prv->irq_waitq,
			(nfc_read(mtd,NFC_IRQ_STATUS)&(CMD_DONE_IRQ_MASK|IDLE_IRQ_MASK))==(CMD_DONE_IRQ_MASK|IDLE_IRQ_MASK), NFC_TIMEOUT*4);
		 if (!rv)
		{

			prv->irq_stat=nfc_read(mtd,NFC_IRQ_STATUS);
			printk(KERN_ERR DRV_NAME"%s timeour status:%08x\n",__FUNCTION__,prv->irq_stat);
			prv->wait_timeout=1;

		}
	}
}
/**
 * nand_read_page_raw - [Intern] read raw page data without ecc
 * @mtd:	mtd info structure
 * @chip:	nand chip info structure
 * @buf:	buffer to store read data
 */
static int nand_read_page_raw(struct mtd_info *mtd, struct nand_chip *chip,
			      uint8_t *buf)
{
	struct mpc5125_nfc_prv *prv = chip->priv;
	memcpy(buf,prv->data_buffers,mtd->writesize);
	memcpy(chip->oob_poi,prv->ops_buffer, mtd->oobsize);
	return 0;
}
/**
 * nand_write_page_raw - [Intern] raw page write function
 * @mtd:	mtd info structure
 * @chip:	nand chip info structure
 * @buf:	data buffer
 */
static void nand_write_page_raw(struct mtd_info *mtd, struct nand_chip *chip,
				const uint8_t *buf)
{
	struct mpc5125_nfc_prv *prv = chip->priv;
	memcpy(prv->data_buffers,buf,mtd->writesize);
	memcpy(prv->ops_buffer,chip->oob_poi, mtd->oobsize);
	mpc5125_dma_config(mtd,chip,0);
}
#endif

static int __init mpc5125_nfc_probe(struct of_device *op,
					const struct of_device_id *match)
{
	struct device_node *rootnode, *dn = op->node;
	struct device *dev = &op->dev;
	struct mpc5125_nfc_prv *prv;
	struct resource res;
	struct mtd_info *mtd;
	struct nand_chip *chip;
	unsigned long regs_paddr, regs_size;
	const uint *chips_no;
	int retval = 0;
	int len;
	prv = devm_kzalloc(dev, sizeof(*prv), GFP_KERNEL);
	if (!prv) {
		printk(KERN_ERR DRV_NAME ": Memory exhausted!\n");
		return -ENOMEM;
	}
	mtd = &prv->mtd;
	chip = &prv->chip;

	mtd->priv = chip;
	chip->priv = prv;
	prv->dev = dev;

	/* Read NFC configuration from Reset Config Word */
	retval = mpc5125_nfc_read_hw_config(mtd);
	if (retval) {
		printk(KERN_ERR DRV_NAME ": Unable to read NFC config!\n");
		return retval;
	}
	/*speed up nand flash r/w add by cloudy*/
	{
        /* set NFC clock divider */
		volatile u32 *nfc_div=ioremap(0x80000f80,sizeof(u32));
		if(!nfc_div)
		{
			printk(KERN_ERR DRV_NAME ": Unable to speed up nfc !\n");
		}
		else
		{
#ifdef CONFIG_MTD_NAND_MPC5125_HARDWARE_ECC_CORRECTION
			*nfc_div=(0x1430<<16);
#else
			*nfc_div=(0x2860<<16);
#endif
			iounmap(nfc_div);
		}
	}
	prv->irq = irq_of_parse_and_map(dn, 0);
	if (prv->irq == NO_IRQ) {
		printk(KERN_ERR DRV_NAME ": Error mapping IRQ!\n");
		return -EINVAL;
	}
	retval = of_address_to_resource(dn, 0, &res);
	if (retval) {
		printk(KERN_ERR DRV_NAME ": Error parsing memory region!\n");
		return retval;
	}
	chips_no = of_get_property(dn, "chips", &len);
	if (!chips_no || len != sizeof(*chips_no)) {
		printk(KERN_ERR DRV_NAME ": Invalid/missing 'chips' "
								"property!\n");
		return -EINVAL;
	}
	regs_paddr = res.start;
	regs_size = res.end - res.start + 1;
	if (!devm_request_mem_region(dev, regs_paddr, regs_size, DRV_NAME)) {
		printk(KERN_ERR DRV_NAME ": Error requesting memory region!\n");
		return -EBUSY;
	}
	prv->regs = devm_ioremap(dev, regs_paddr, regs_size);
	if (!prv->regs) {
		printk(KERN_ERR DRV_NAME ": Error mapping memory region!\n");
		return -ENOMEM;
	}
    printk("NFC: reg %X len %X, map %X\n", (u32)regs_paddr, (u32)regs_size, (u32)prv->regs);
	prv->data_buffers=dma_alloc_coherent(NULL, DATA_BUFFER_MAX_SIZE,
                       &prv->data_buffers_phyaddr, GFP_KERNEL);
	if(!prv->data_buffers)return -ENOMEM;
	prv->ops_buffer=dma_alloc_coherent(NULL, SPARE_BUFFER_MAX_SIZE,
                       &prv->ops_buffer_phyaddr, GFP_KERNEL);
	if(!prv->ops_buffer)
	{
		dma_free_coherent(NULL,DATA_BUFFER_MAX_SIZE,prv->data_buffers,prv->data_buffers_phyaddr);
		return -ENOMEM;
	}
	/* Enable NFC clock */
	prv->clk = clk_get(dev, "nfc_clk");
	if (!prv->clk) {
		printk(KERN_ERR DRV_NAME ": Unable to acquire NFC clock!\n");
		retval = -ENODEV;
		goto error;
	}
	clk_enable(prv->clk);
	init_waitqueue_head(&prv->irq_waitq);
	retval = devm_request_irq(dev, prv->irq, &mpc5125_nfc_irq,
			0, DRV_NAME, mtd);
	if (retval) {
		printk(KERN_ERR DRV_NAME ": Error requesting IRQ!\n");
		goto error;
	}

	mtd->name = "MPC5125 NAND";
	chip->write_page=chip_nand_write_page;
	chip->dev_ready = mpc5125_nfc_dev_ready;
	chip->cmdfunc = mpc5125_nfc_command;
	chip->read_byte = mpc5125_nfc_read_byte;
	chip->read_word = mpc5125_nfc_read_word;
	chip->read_buf = mpc5125_nfc_read_buf;
	chip->write_buf = mpc5125_nfc_write_buf;

	chip->verify_buf = mpc5125_nfc_verify_buf;
	chip->options = NAND_NO_AUTOINCR|NAND_USE_FLASH_BBT|NAND_SKIP_BBTSCAN;
	chip->select_chip = ads5125_select_chip;
	if(hardware_ecc)
	{
		chip->ecc.read_page = mpc5125_nand_read_page;
		chip->ecc.write_page = mpc5125_nand_write_page;
		chip->ecc.read_oob = mpc5125_nand_read_oob;
		chip->ecc.write_oob = mpc5125_nand_write_oob;
		chip->ecc.calculate = mpc5125_nand_calculate_ecc;
		chip->ecc.hwctl = mpc5125_nand_enable_hwecc;
		chip->ecc.correct = mpc5125_nand_correct_data;
		chip->ecc.mode = NAND_ECC_HW;
		chip->ecc.size = 512;	/* RS-ECC is applied for both MAIN+SPARE not MAIN alone */
		chip->ecc.bytes = 9;	/* used for both main and spare area */
		chip->ecc.layout=&nand_hw_eccoob_4k;
		nfc_set_field(mtd, NFC_FLASH_CONFIG,
			CONFIG_ECC_SRAM_ADDR_MASK,
			CONFIG_ECC_SRAM_ADDR_SHIFT, (MPC5125_NFC_ECC_STATUS_ADD>>3)&0x00001ff);

		nfc_set_field(mtd, NFC_FLASH_CONFIG,
			CONFIG_ECC_MODE_MASK,
			CONFIG_ECC_MODE_SHIFT, ECC_45_BYTE);

		nfc_set_field(mtd, NFC_FLASH_CONFIG,
			CONFIG_CMD_TIMEOUT_MASK,
			CONFIG_CMD_TIMEOUT_SHIFT, 0xf);

		nfc_set_field(mtd, NFC_FLASH_CONFIG,
			CONFIG_ECC_SRAM_REQ_MASK,
			CONFIG_ECC_SRAM_REQ_SHIFT, 1);

	}
	else
	{
#if NFC_DMA_ENABLE
		chip->ecc.read_page_raw= nand_read_page_raw;
		chip->ecc.write_page_raw= nand_write_page_raw;
#endif
		chip->ecc.mode = NAND_ECC_SOFT;
		chip->ecc.layout=&nand_hw_eccoob_4k;
		nfc_set_field(mtd, NFC_FLASH_CONFIG,
			CONFIG_ECC_MODE_MASK,
			CONFIG_ECC_MODE_SHIFT, ECC_BYPASS);
		nfc_set_field(mtd, NFC_FLASH_CONFIG,
			CONFIG_ECC_SRAM_REQ_MASK,
			CONFIG_ECC_SRAM_REQ_SHIFT, 0);
	}
	memset( &prv->int_sem, 0, sizeof(struct semaphore));
	sema_init( &prv->int_sem, 0 );

	/* SET SECTOR SIZE */
	nfc_write(mtd, NFC_SECTOR_SIZE,PAGE_virtual_2K);
	nfc_set_field(mtd, NFC_FLASH_CONFIG,
			CONFIG_PAGE_CNT_MASK,
			CONFIG_PAGE_CNT_SHIFT, 2);
	nfc_set_field(mtd, NFC_FLASH_CONFIG,
			CONFIG_ADDR_AUTO_INCR_MASK,
			CONFIG_ADDR_AUTO_INCR_SHIFT, 0);
	nfc_set_field(mtd, NFC_FLASH_CONFIG,
			CONFIG_BUFNO_AUTO_INCR_MASK,
			CONFIG_BUFNO_AUTO_INCR_SHIFT, 1);
	nfc_set_field(mtd, NFC_FLASH_CONFIG,
			CONFIG_16BIT_MASK,
			CONFIG_16BIT_SHIFT, 0);
#if NFC_DMA_ENABLE
	nfc_set_field(mtd,NFC_DMA_CONFIG,DMA_CONFIG_DMA1_CNT_MASK,
                       DMA_CONFIG_DMA1_CNT_SHIFT,PAGE_2K);
        nfc_set_field(mtd,NFC_DMA_CONFIG,DMA_CONFIG_DMA2_CNT_MASK,
                       DMA_CONFIG_DMA2_CNT_SHIFT,0x40);
        nfc_set_field(mtd,NFC_DMA_CONFIG,DMA_CONFIG_DMA1_ACT_MASK,
                       DMA_CONFIG_DMA1_ACT_SHIFT,1);
         nfc_set_field(mtd,NFC_DMA_CONFIG,DMA_CONFIG_DMA2_OFFSET_MASK,
                       DMA_CONFIG_DMA2_OFFSET_SHIFT,(PAGE_2K>>1));
         nfc_set_field(mtd,NFC_DMA_CONFIG,DMA_CONFIG_DMA2_ACT_MASK,
                       DMA_CONFIG_DMA2_ACT_SHIFT,1);
#endif
	/* SET FAST_FLASH = 1 */
	nfc_set_field(mtd, NFC_FLASH_CONFIG,
			CONFIG_FAST_FLASH_MASK,
			CONFIG_FAST_FLASH_SHIFT, 1);
	nfc_set_field(mtd, NFC_FLASH_CONFIG,
			CONFIG_BOOT_MODE_MASK,
			CONFIG_BOOT_MODE_SHIFT, 0);
	/* Detect NAND chips */
	if (nand_scan(mtd, *chips_no)) {
		printk(KERN_ERR DRV_NAME ": NAND Flash not found !\n");
		devm_free_irq(dev, prv->irq, mtd);
		retval = -ENXIO;
		goto error;
	}
	dev_set_drvdata(dev, mtd);


	/* Register the partitions */
#ifdef CONFIG_MTD_PARTITIONS
	prv->nr_parts =
	    parse_mtd_partitions(mtd, mpc5125_nfc_pprobes, &prv->parts, 0);
	if (prv->nr_parts > 0)
		add_mtd_partitions(mtd, prv->parts, prv->nr_parts);
	else if ((prv->nr_parts = parse_flash_partitions(mtd, dn)) > 0) {
		dev_info(dev, "Using OF partition info\n");
		add_mtd_partitions(mtd, prv->parts, prv->nr_parts);
	} else
#endif
	{
		pr_info("Registering %s as whole device\n", mtd->name);
		add_mtd_device(mtd);
	}
	if (retval) {
		printk(KERN_ERR DRV_NAME ": Error adding MTD device!\n");
		devm_free_irq(dev, prv->irq, mtd);
		goto error;
	}
	return 0;
error:
	dma_free_coherent(NULL,DATA_BUFFER_MAX_SIZE,prv->data_buffers,prv->data_buffers_phyaddr);
	dma_free_coherent(NULL,SPARE_BUFFER_MAX_SIZE,prv->ops_buffer,prv->ops_buffer_phyaddr);
	mpc5125_nfc_free(dev, mtd);
	return retval;
}

static int __exit mpc5125_nfc_remove(struct of_device *op)
{
	struct device *dev = &op->dev;
	struct mtd_info *mtd = dev_get_drvdata(dev);
	struct nand_chip *chip = mtd->priv;
	struct mpc5125_nfc_prv *prv = chip->priv;

	nand_release(mtd);
	devm_free_irq(dev, prv->irq, mtd);
	dma_free_coherent(NULL,DATA_BUFFER_MAX_SIZE,prv->data_buffers,prv->data_buffers_phyaddr);
	dma_free_coherent(NULL,SPARE_BUFFER_MAX_SIZE,prv->ops_buffer,prv->ops_buffer_phyaddr);
	mpc5125_nfc_free(dev, mtd);

	return 0;
}

static struct of_device_id mpc5125_nfc_match[] = {
	{ .compatible = "fsl,mpc5125-nfc", },
	{},
};

static struct of_platform_driver mpc5125_nfc_driver = {
	.owner		= THIS_MODULE,
	.name		= DRV_NAME,
	.match_table	= mpc5125_nfc_match,
	.probe		= mpc5125_nfc_probe,
	.remove		= __exit_p(mpc5125_nfc_remove),
	.suspend	= NULL,
	.resume		= NULL,
	.driver		= {
		.name	= DRV_NAME,
		.owner	= THIS_MODULE,
	},
};

static int __init mpc5125_nfc_init(void)
{
	pr_info("MPC5125 MTD nand Driver %s\n", DRV_VERSION);
	if (of_register_platform_driver(&mpc5125_nfc_driver) != 0) {
		printk(KERN_ERR DRV_NAME ": Driver register failed!\n");
		return -ENODEV;
	}
	return 0;
}

static void __exit mpc5125_nfc_cleanup(void)
{
	of_unregister_platform_driver(&mpc5125_nfc_driver);
}

module_init(mpc5125_nfc_init);
module_exit(mpc5125_nfc_cleanup);

MODULE_AUTHOR("Freescale Semiconductor, Inc.");
MODULE_DESCRIPTION("MPC5125 NAND MTD driver");
MODULE_LICENSE("GPL");
MODULE_VERSION(DRV_VERSION);
