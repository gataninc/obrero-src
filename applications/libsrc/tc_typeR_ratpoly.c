/*
   Refer to the document:
   http://www.mosaic-industries.com/embedded-systems/microcontroller-projects/temperature-measurement/thermocouple/calibration-table
   for more details on the implementation.
 */
#include <stdio.h>
#include <math.h>

// Routine for converting voltage to temperature for 
// Type R thermo-couples using rational polynomials

// constants for range 1: -0.226 to 1.469mV (-50 to 200 degC)
#define R1_T0   ((double)1.3054315E2)
#define R1_V0   ((double)8.8333090E-1)
#define R1_P1   ((double)1.2557377E2)
#define R1_P2   ((double)1.3900275E2)
#define R1_P3   ((double)3.3035469E1)
#define R1_P4   ((double)-8.5195924E-1)
#define R1_Q1   ((double)1.2232896E0)
#define R1_Q2   ((double)3.5603023E-1)
#define R1_Q3   ((double)0.0)

// constants for range 2: 1.469 to 7.461mV (200 to 760 degC)
#define R2_T0   ((double)5.4188181E2)
#define R2_V0   ((double)4.9312886E0)
#define R2_P1   ((double)9.0208190E1)
#define R2_P2   ((double)6.1762254E0)
#define R2_P3   ((double)-1.2279323E0)
#define R2_P4   ((double)1.4873153E-2)
#define R2_Q1   ((double)8.7670455E-2)
#define R2_Q2   ((double)-1.2906694E-2)
#define R2_Q3   ((double)0.0)

// constants for range 3: 7.461 to 14.277mV (760 to 1275 degC)
#define R3_T0   ((double)1.0382132E3)
#define R3_V0   ((double)1.1014763E1)
#define R3_P1   ((double)7.4669343E1)
#define R3_P2   ((double)3.4090711E0)
#define R3_P3   ((double)-1.4511205E-1)
#define R3_P4   ((double)6.3077387E-3)
#define R3_Q1   ((double)5.6880253E-2)
#define R3_Q2   ((double)-2.0512736E-3)
#define R3_Q3   ((double)0.0)

// constants for range 4: 14.277 to 21.101mV (1275 to 1768 degC)
#define R4_T0   ((double)1.5676133E3)
#define R4_V0   ((double)1.8397910E1)
#define R4_P1   ((double)7.1646299E1)
#define R4_P2   ((double)-1.0866763E0)
#define R4_P3   ((double)-2.0968371E0)
#define R4_P4   ((double)-7.6741168E-1)
#define R4_Q1   ((double)-1.9712341E-2)
#define R4_Q2   ((double)-2.9903595E-2)
#define R4_Q3   ((double)-1.0766878E-2)

/*
numerator:
(v - v0)*(p1 + (v - v0)*(p2 + (v - v0)*(p3 + p4 *(v - v0))))

x4 = p4*(v - v0)

x3 = (p3 + x4) * (v - v0)

x2 = (p2 + x3) * (v - v0)

x1 = (p1 + x2) * (v - v0)

denominator:
1 + (v - v0) (q1 + (v - v0)(q2 + (q3 (v - v0))))

y3 = q3 * (v - v0)
y2 = (q2 + y3) * (v - v0)
y1 = (q1 + y2) * (v - v0)
*/

// voltage in V
double tcTypeRTemp(double V)
{
    double t = 0.0;
    double diff;
    double x4;
    double x3;
    double x2;
    double x1;
    double y3;
    double y2;
    double y1;
    double mV = V * 1000.0; // convert to milli-volts
//printf("%s: mV %0.5f\n", __func__, mV);
    if ((mV >= -0.226) && (mV < 1.469))
    {
        diff = mV - R1_V0;
        x4 = R1_P4 * diff;
        x3 = (R1_P3 + x4) * diff;
        x2 = (R1_P2 + x3) * diff;
        x1 = (R1_P1 + x2) * diff;

        y3 = R1_Q3 * diff;
        y2 = (R1_Q2 + y3) * diff;
        y1 = (R1_Q1 + y2) * diff;
        t = R1_T0 + (x1/(1+y1));
        t += (double)273.15; // convert to Kelvin
    }
    else if ((mV >= 1.469) && (mV < 7.461))
    {
        diff = mV - R2_V0;
        x4 = R2_P4 * diff;
        x3 = (R2_P3 + x4) * diff;
        x2 = (R2_P2 + x3) * diff;
        x1 = (R2_P1 + x2) * diff;

        y3 = R2_Q3 * diff;
        y2 = (R2_Q2 + y3) * diff;
        y1 = (R2_Q1 + y2) * diff;
        t = R2_T0 + (x1/(1+y1));
        t += (double)273.15; // convert to Kelvin
    }
    else if ((mV >= 7.461) && (mV < 14.277))
    {
        diff = mV - R3_V0;
        x4 = R3_P4 * diff;
        x3 = (R3_P3 + x4) * diff;
        x2 = (R3_P2 + x3) * diff;
        x1 = (R3_P1 + x2) * diff;

        y3 = R3_Q3 * diff;
        y2 = (R3_Q2 + y3) * diff;
        y1 = (R3_Q1 + y2) * diff;
        t = R3_T0 + (x1/(1+y1));
        t += (double)273.15; // convert to Kelvin
    }
    else if ((mV >= 14.277) && (mV < 21.101))
    {
        diff = mV - R4_V0;
        x4 = R4_P4 * diff;
        x3 = (R4_P3 + x4) * diff;
        x2 = (R4_P2 + x3) * diff;
        x1 = (R4_P1 + x2) * diff;

        y3 = R4_Q3 * diff;
        y2 = (R4_Q2 + y3) * diff;
        y1 = (R4_Q1 + y2) * diff;
        t = R4_T0 + (x1/(1+y1));
        t += (double)273.15; // convert to Kelvin
    }
    return t;
}
// END Routine for Type R thermo-couple

// Routine for converting temperature(from cold-junction) to a voltage
// corresponding to the Type R thermo-couple.

// CJC Temperature is in Celsius..
double cjcTemp2tcTypeRVolt(double cjcT)
{
    double cjcV;
    double t0;
    double v0;
    double p1, p2, p3, p4;
    double q1, q2;
    double diff;
    double x4, x3, x2, x1;
    double y2, y1;

    if ((cjcT < -20.0) || (cjcT > 70.0))
        return 0.0;

    t0 = 2.5E1;
    v0 = 1.4067016E-1;
    p1 = 5.9330356E-3;
    p2 = 2.7736904E-5;
    p3 = -1.0819644E-6;
    p4 = -2.3098349E-9;
    q1 = 2.6146871E-3;
    q2 = -1.8621487E-4;

    diff = cjcT - t0;
    x4 = p4 * diff;
    x3 = (p3 + x4) * diff;
    x2 = (p2 + x3) * diff;
    x1 = (p1 + x2) * diff;

    y2 = q2 * diff;
    y1 = (q1 + y2) * diff;

    cjcV = v0 + (x1 / (1 + y1));
    return cjcV;
}

//#define UNIT_TEST
#ifdef UNIT_TEST
int main()
{
    double t;
    float mV;
    printf("RANGE1:\n");
    for (mV = -0.230; mV < 1.460; mV += 0.001) 
    {
        t = tcTypeRTemp((double)(mV/1000.0));
        printf("%0.3f   %0.5f\n", mV, t);
    }
    printf("RANGE2:\n");
    for (mV = 1.460; mV < 7.460; mV += 0.001) 
    {
        t = tcTypeRTemp((double)(mV/1000.0));
        printf("%0.3f   %0.5f\n", mV, t);
    }
    printf("RANGE3:\n");
    for (mV = 7.460; mV < 14.270; mV += 0.001) 
    {
        t = tcTypeRTemp((double)(mV/1000.0));
        printf("%0.3f   %0.5f\n", mV, t);
    }
    printf("RANGE4:\n");
    for (mV = 14.270; mV < 21.120; mV += 0.001) 
    {
        t = tcTypeRTemp((double)(mV/1000.0));
        printf("%0.3f   %0.5f\n", mV, t);
    }
    /*
    {
    double v;
    float t;
    printf("\nCJC Voltages:\n");
    for (t = -20.0; t < 70.0; t += 0.05)
    {
        v = cjcTemp2tcTypeRVolt(t);
        printf("%0.3fC %0.5fmV\n", t, v);
    }
    }
    */
    return 0;
}
#endif
