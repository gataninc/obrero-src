#! /bin/sh

# enable BING_IO_DRIVE_EN
dsh poke 0 0x200

# config clockgen
dsh clkgen config /fpga/register_map_clkgen.txt
#enable output
#dsh clkgen output 2

# The bits below are set to 0 by default
# BING_DRTD_CAL_SEL[1:0] 0xb00
# BING_DRTD_GAIN[1:0] 0b00
# BING_DRTD_CAL_SEL_2 0
# BING_DRTD_POL 0

# BING_DRTD_ISEL 0b10
dsh poke 2 0x80

# BING_DRTD_SAMPLE 1
dsh poke 3 1

# set Voltage range to 0-10 for safety
dsh set vrange 10
echo; echo "Voltage range set to: 0 - 10V"; echo

