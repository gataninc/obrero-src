/*
 * drivers/input/touchscreen/ma17p0x.c
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <linux/hrtimer.h>
#include <linux/slab.h>
#include <linux/input.h>
#include <linux/interrupt.h>
#include <linux/i2c.h>
#include <linux/i2c/ma17p0x.h>
#include <linux/delay.h>

__u8 ma17p0x[19];

// buffer map
#define REG_TOTAL_DATA_LEN 	0x0
#define REG_RESERVED_0		0x1
#define REG_NUM_OF_FINGER	0x2
#define REG_X1_HIGH_BYTE		0x3
#define REG_X1_LOW_BYTE		0x4
#define REG_Y1_HIGH_BYTE		0x5
#define REG_Y1_LOW_BYTE		0x6
#define REG_X2_HIGH_BYTE		0x7
#define REG_X2_LOW_BYTE		0x8
#define REG_Y2_HIGH_BYTE		0x9
#define REG_Y2_LOW_BYTE		0xa
#define REG_X3_HIGH_BYTE		0xb
#define REG_X3_LOW_BYTE		0xc
#define REG_Y3_HIGH_BYTE		0xd
#define REG_Y3_LOW_BYTE		0xe
#define REG_X4_HIGH_BYTE		0xf
#define REG_X4_LOW_BYTE		0x10
#define REG_Y4_HIGH_BYTE		0x11
#define REG_Y4_LOW_BYTE		0x12

enum ma17p0x_mode {
	watch_mode = 0,
	watch_mode_e = 0x0e,
	active_mode = 0x10,
	fast_scan_mode = 0x20,
	freeze_mode = 0x90
};

struct mt4 {
	struct input_dev	*input;
	char			phys[32];
	struct i2c_client	*client;

	spinlock_t		lock;

	u16			model;

	unsigned		pendown;
	int			irq;

	int			(*get_pendown_state)(void);
	void			(*clear_penirq)(void);
	int			(*if_penirq)(void);
	void			(*enable_irq)(int);

	struct delayed_work work;
};

struct ts_point {
	int x;
	int y;
};

#define EVENT_PENDOWN 1
#define EVENT_REPEAT  2
#define EVENT_PENUP   3

#define get_val(_high, _low) (_high << 4 | _low >> 4)

static void ma17p0x_ts_poscheck(struct work_struct *work)
{
	struct mt4 *ts = container_of(work, struct mt4, work.work);
	struct i2c_adapter *adapter = to_i2c_adapter(ts->client->dev.parent);
	struct ts_point pt[4];
	int i, fingers = 0;
	u8 data_length;
	int event = EVENT_PENUP;

	struct i2c_msg msg[] = {
		{
			.addr = ts->client->addr,
			.flags = I2C_M_RD,
			.buf = &data_length,
			.len = 1
		},
		{
			.addr = ts->client->addr,
			.flags = I2C_M_RD,
			.buf = ma17p0x,
			.len = sizeof(ma17p0x)
		}
	};

	if ((i2c_transfer(adapter, &msg[0], 1) != 1)) {
		if (ts->get_pendown_state() == 0)
			goto pen_up;
		goto out;
	}

	msg[1].len = data_length + 1;
	if (i2c_transfer(adapter, &msg[1], 1) != 1) {
		if (ts->get_pendown_state() == 0)
			goto pen_up;
		goto out;
	}

	fingers = ma17p0x[REG_NUM_OF_FINGER];
	if (fingers <= 0 || fingers > 4)
		goto out;

	event = EVENT_PENDOWN;
	for (i = 0; i < fingers; i++) {
		pt[i].x = get_val(ma17p0x[REG_X1_HIGH_BYTE + 4 * i],
				ma17p0x[REG_X1_LOW_BYTE + 4 * i]);
		pt[i].y = get_val(ma17p0x[REG_Y1_HIGH_BYTE + 4 * i],
				ma17p0x[REG_Y1_LOW_BYTE + 4 * i]);
	}

pen_up:

#if 0
	if (event == EVENT_PENUP)
		printk("%s\n", "PENUP");
	if (event == EVENT_PENDOWN) {
		for (i = 0; i < fingers; i++)
			printk("%04d,%04d ", pt[i].x, pt[i].y);
		printk("\n");
	}
#endif

	if (event == EVENT_PENDOWN) {

		input_report_key(ts->input, BTN_TOUCH, 1);

		for (i = 0; i < fingers; i++) {
			input_report_abs(ts->input, ABS_X, pt[i].x);
			input_report_abs(ts->input, ABS_Y, pt[i].y);
		}

		input_report_abs(ts->input, ABS_PRESSURE, 1);
		input_sync(ts->input);

	} else if (event == EVENT_PENUP) {
		input_report_key(ts->input, BTN_TOUCH, 0);
		input_report_abs(ts->input, ABS_PRESSURE, 0);
		input_sync(ts->input);
	}
out:
	ts->enable_irq(1);
}

static irqreturn_t ma17p0x_irq(int irq, void *handle)
{
	struct mt4 *ts = handle;

	if (ts->if_penirq && !ts->if_penirq())
		return IRQ_NONE;

	/* the touch screen controller chip is hooked up to the cpu
	 * using i2c and a single interrupt line. the interrupt line
	 * is pulled low whenever someone taps the screen. to deassert
	 * the interrupt line we need to acknowledge the interrupt by
	 * communicating with the controller over the slow i2c bus.
	 *
	 * we can't acknowledge from interrupt context since the i2c
	 * bus controller may sleep, so we just disable the interrupt
	 * here and handle the acknowledge using delayed work.
	 */

	if (ts->enable_irq)
		ts->enable_irq(0);
	if (ts->clear_penirq)
		ts->clear_penirq();

	schedule_delayed_work(&ts->work, 0);

	return IRQ_HANDLED;
}

#define MODE_SELECT_BYTE 0x14

static inline int ma17p0x_mode_select(struct mt4 *tsc, enum ma17p0x_mode mode)
{
	return i2c_smbus_write_byte_data(tsc->client, MODE_SELECT_BYTE, (u8)mode);
}

static int ma17p0x_ts_open(struct input_dev *dev)
{
	return 0;
}

static void ma17p0x_ts_close(struct input_dev *dev)
{
}

extern struct ma17p0x_platform_data ts_ma17p0x_data;

static int ma17p0x_probe(struct i2c_client *client,
			const struct i2c_device_id *id)
{
	struct input_dev *input_dev;
	struct mt4 *ts;
	int err;

	if (!i2c_check_functionality(client->adapter,
				     I2C_FUNC_SMBUS_READ_WORD_DATA))
		return -EIO;

	client->dev.platform_data = &ts_ma17p0x_data;

	ts = kzalloc(sizeof(struct mt4), GFP_KERNEL);
	input_dev = input_allocate_device();
	if (!ts || !input_dev) {
		err = -ENOMEM;
		goto err_free_mem;
	}

	i2c_set_clientdata(client, ts);

	ts->client 			= client;
	ts->input			= input_dev;
	ts->get_pendown_state 	= ts_ma17p0x_data.get_pendown_state;
	ts->clear_penirq      	= ts_ma17p0x_data.clear_penirq;
	ts->if_penirq	   	= ts_ma17p0x_data.if_penirq;
	ts->enable_irq	   	= ts_ma17p0x_data.enable_irq;
	ts->irq 			= client->irq;
	INIT_DELAYED_WORK(&ts->work, ma17p0x_ts_poscheck);

	snprintf(ts->phys, sizeof(ts->phys), "%s/input0",
		dev_name(&client->dev));

	input_dev->name = "MA17P0X Touchscreen";
	input_dev->phys = ts->phys;
	input_dev->id.bustype = BUS_I2C;
	input_dev->open = ma17p0x_ts_open;
	input_dev->close = ma17p0x_ts_close;

	input_dev->evbit[0] = BIT(EV_SYN) | BIT(EV_KEY) | BIT(EV_ABS);
	input_dev->keybit[BIT_WORD(BTN_TOUCH)] = BIT_MASK(BTN_TOUCH);

	set_bit(EV_ABS, input_dev->evbit);
	set_bit(ABS_X, input_dev->absbit);
	set_bit(ABS_Y, input_dev->absbit);
	set_bit(ABS_PRESSURE, input_dev->absbit);

	input_set_abs_params(input_dev, ABS_X, 0, 4096, 0, 0);
	input_set_abs_params(input_dev, ABS_Y, 0, 4096, 0, 0);
	input_set_abs_params(input_dev, ABS_PRESSURE, 0, 1, 0, 0);

	ts_ma17p0x_data.init_platform_hw();

	err = request_irq(ts->irq, ma17p0x_irq, IRQF_SHARED,
			client->dev.driver->name, ts);
	if (err < 0) {
		dev_err(&client->dev, "Unable to request touchscreen IRQ.\n");
		goto err_free_mem;
	}

	err = input_register_device(input_dev);
	if (err)
		goto err_free_irq;

	dev_info(&client->dev, "registered with irq (%d)\n", ts->irq);
	return 0;

 err_free_irq:
	free_irq(ts->irq, ts);
 err_free_mem:
	input_free_device(input_dev);
	kfree(ts);

	return err;
}

static int ma17p0x_remove(struct i2c_client *client)
{
	struct mt4	*ts = i2c_get_clientdata(client);
	struct ma17p0x_platform_data *pdata;

	pdata = client->dev.platform_data;
	pdata->exit_platform_hw();

	free_irq(ts->irq, ts);
	input_unregister_device(ts->input);
	kfree(ts);

	return 0;
}

static struct i2c_device_id ma17p0x_idtable[] = {
	{ "ma17p0x", 0 },
	{ }
};

MODULE_DEVICE_TABLE(i2c, ma17p0x_idtable);

static struct i2c_driver ma17p0x_driver = {
	.driver = {
		.owner	= THIS_MODULE,
		.name	= "ma17p0x"
	},
	.id_table	= ma17p0x_idtable,
	.probe		= ma17p0x_probe,
	.remove	= ma17p0x_remove,
};

static int __init ma17p0x_init(void)
{
	return i2c_add_driver(&ma17p0x_driver);
}

static void __exit ma17p0x_exit(void)
{
	i2c_del_driver(&ma17p0x_driver);
}

module_init(ma17p0x_init);
module_exit(ma17p0x_exit);

MODULE_AUTHOR("Wang Yang <wang_yang@mtcera.com>");
MODULE_DESCRIPTION("MA17P0X TouchScreen Driver");
MODULE_LICENSE("GPL");
