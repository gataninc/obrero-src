#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <syslog.h>
#include <sys/time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <semaphore.h>
#include <sys/sysinfo.h>
#include "bing_common.h"

static char dev_null_str[] = "/dev/null";

static char *i2c_bus_str[MAX_I2C_BUS] = 
{ 
    "/dev/i2c-0",
    "/dev/i2c-1",
    "/dev/i2c-2"
};

char *get_i2c_bus(int idx)
{
    switch (idx)
    {
        case 1:
            return i2c_bus_str[0];
            break;
        case 2:
            return i2c_bus_str[1];
            break;
        case 3:
            return i2c_bus_str[2];
            break;
        default:
            return dev_null_str;
            break;
    }
    /* UNREACHABLE */
    return 0;
}

/*
 * from Ciro's code..
 */
static int parseByte(char *pStr, unsigned char *pByte)
{
    unsigned char val;
    int ix, len;
    int ret = 0;
    if ((len = strlen(pStr)) > 2)
        return -1;
    for (ix = 0, val = 0; ix < len; ix++)
    {
        val <<= 4;
        if ((pStr[ix] >= '0') && (pStr[ix] <= '9'))
            val = val | (pStr[ix] - '0');
        else if ((pStr[ix] >= 'A') && (pStr[ix] <= 'F'))
            val = val | (pStr[ix] - 'A' + 0xA);
        else if ((pStr[ix] >= 'a') && (pStr[ix] <= 'f'))
            val = val | (pStr[ix] - 'a' + 0xA);
        else
        {
            ret = -1;
            break;
        }
    }
    if (ret == 0)
        *pByte = val;
    return ret;
}

/*
 * since sscanf of glibc does not appear to work, use our own function to parse
 * a mac address.
 */
int parseMacStr(char *macStr, unsigned char macAddr[])
{
    char *ptr;
    int idx;
    int ret = -1;
    if ((macStr) && (macAddr))
    {
        for (idx = 0; idx < 5; idx++)
        {
            ptr = strchr(macStr, ':');
            if (!ptr)
                break;
            *ptr = '\0';
            if (parseByte(macStr, &macAddr[idx]) != 0)
            {
                fprintf(stderr, "Invalid numeric value <%s>\n", macStr);
                break;
            }
            //fprintf(stderr, "%02x:", macAddr[idx]);
            macStr = ++ptr;
            if (!macStr)
                break;
        }
        if (idx == 5)
        {
            if (parseByte(macStr, &macAddr[idx]) != 0)
                fprintf(stderr, "Invalid numeric value <%s>\n", macStr);
            else
            {
                //fprintf(stderr, "%02x <--\n", macAddr[idx]);
                ret = 0;
            }
        }
    }
    return ret;
}

/*
 * return pointer to static string containing IP addres
 */
char *cvtToIPStr(unsigned int ipVal)
{
    static char ipAddr[16];
    unsigned char *pOct = (unsigned char *)&ipVal;
    sprintf(ipAddr, "%d.%d.%d.%d", pOct[0] & 0xFF, pOct[1] & 0xFF,
            pOct[2] & 0xFF, pOct[3] & 0xFF);
    return ipAddr;
}

int cvtToUInt(char *word, uint32_t *value)
{
    char *endPtr;
    ulong longVal;
    if (!word || !(*word))
        return -1;
    longVal = strtoul(word, &endPtr, 0);
    if (*endPtr != '\0')
    {
        fprintf(stderr, "Invalid numeric value <%s>\n", word);
        return -2;
    }
    *value = (uint32_t)longVal;
    return 0;
}

int convToInt(char *str, int *val)
{
    char *endptr;

    if (!str)
        return -1;
    errno = 0;
    *val = strtol(str, &endptr, 0);
    if ((errno != 0) || (endptr == str))
    {
       fprintf(stderr, "Invalid numeric value <%s>\n", str);
       return -1;
    }
    return 0;
}

int cvtToFloat(char *fltStr, float *fltVal)
{
    char *endPtr;
    double dbl;

    errno = 0;
    if (!fltStr || !(*fltStr))
        return -1;
    dbl = strtod(fltStr, &endPtr);
    if ((*endPtr != '\0') || (errno != 0))
    {
        fprintf(stderr, "Invalid float value <%s>\n", fltStr);
        return -2;
    }
    *fltVal = (float)dbl;
    return 0;
}

int cvtToDouble(char *fStr, double *dblVal)
{
    char *endPtr;
    double dbl;

    errno = 0;
    if (!fStr || !(*fStr))
        return -1;
    dbl = strtod(fStr, &endPtr);
    if ((*endPtr != '\0') || (errno != 0))
    {
        fprintf(stderr, "Invalid float value <%s>\n", fStr);
        return -2;
    }
    *dblVal = dbl;
    return 0;
}

void mySleep(uint32_t ms)
{
    struct timespec tv, rem;
    int i = 0;
    tv.tv_sec = ms / 1000;
    tv.tv_nsec = (ms - (tv.tv_sec * 1000)) * 1000 * 1000; /* nanosecs */
startOver:
    errno = 0;
    if (nanosleep(&tv, &rem) != 0)
    {
        if (errno == EINTR)
        {
            tv = rem;
            i++;
            if (i < 2) /* avoid inf. loop, just in case.. */
                goto startOver;
            else
            {
                syslog(LOG_ERR, "%s Error: nanosleep returned EINTR", __func__);
                fprintf(stderr, "%s Error: nanosleep returned EINTR", __func__);
            }
        }
        else if (errno == EFAULT)
        {
            syslog(LOG_ERR, "%s Error: nanosleep returned EFAULT", __func__);
            fprintf(stderr, "%s Error: nanosleep returned EFAULT", __func__);
        }
        else
            fprintf(stderr, "%s Error: nanosleep returned %d", __func__, errno);
    }
}

/* Find difference between values in two timeval structs, in micro secs.
 * THIS DOES NOT HANDLE WRAP AROUND OF TIME since we do not expect clock time 
 * to wrap around till 2038...
 * Can only be used to measure differences of about an hour(71+ mins).
 */
unsigned int diffTimeVal(struct timeval *last, struct timeval *first)
{
    return (((last->tv_sec - first->tv_sec) * 1000000) + 
        (last->tv_usec - first->tv_usec));
}

// get current time
unsigned long long getTimeInMs()
{
    struct timeval tNow;
    gettimeofday(&tNow, 0);
    return (unsigned long long)((tNow.tv_sec * 1000) + (tNow.tv_usec / 1000));
}

// return elapsed time (from a time returned by call above)
unsigned long long getTimeElapsed(unsigned long long last)
{
    struct timeval tNow;
    unsigned long long now;
    gettimeofday(&tNow, 0);
    now = (unsigned long long)((tNow.tv_sec * 1000) + (tNow.tv_usec / 1000));
    return (now - last);
}

int getStringFromFile(char *inFile, char *string, int sLen)
{
    char *line = 0;
    FILE *fp;
    ssize_t ret;
    ssize_t len;
    int i;
    if ((fp = fopen(inFile, "r")) == NULL)
        return -1;
    ret = getline(&line, &len, fp);
    fclose(fp);
    if (ret == -1)
        return -2;
    for (i = (len-1); i > 0; i--)
    {
        if ((line[i] == '\n') || (line[i] == '\r'))
            line[i] = '\0';
    }
    strncpy(string, line, sLen);
    string[sLen-1] = '\0';
    free(line);
    return 0;
}

// return system uptime in seconds
int getSysUpTime()
{
    struct sysinfo sInfo;
    int err = sysinfo(&sInfo);
    if (err != 0)
    {
        perror("sysinfo");
        return -1;
    }
    return sInfo.uptime;
}

#ifndef whitespace
#define whitespace(c) (((c) == ' ') || ((c) == '\t'))
#endif
char *stripWhiteSpace(char *input)
{
    register char *s, *t;
    /* strip start of string */
    for (s = input; whitespace(*s); s++)
        ;
    if (*s == 0)
        return s;
    /* strip end of string */
    t  = s + strlen(s) - 1;
    while (t > s && whitespace(*t))
        t--;
    *(++t) = '\0';
    return s;
}


/******************************************************************************/
/*  THE CODE below is shared between the UI and system software. Build systems 
    for UI and system s/w are different and so these are kept in two seperate
    repositories.. KEEP THIS IN SYNC 
 */
/******************************************************************************/
/*
 * Use linux system calls to print information about all existing shared memory
 * segments in the system.
 */
int printSharedMemInfo(key_t key)
{
    int maxIdx, i;
    struct shmid_ds shmBuf;
    struct shm_info shmInf;
    if ((maxIdx = shmctl(0, SHM_INFO, (struct shmid_ds *)&shmInf)) == -1)
    {
        perror("SHM_INFO");
        return -1;
    }
    printf("Num Segs %d Total Shared Pg: %lu\n", shmInf.used_ids,
            shmInf.shm_tot);
    for (i = 0; i < maxIdx; i++)
    {
        if (shmctl(i, SHM_STAT, &shmBuf) == -1)
            continue;
        printf("Key 0x%X\n", (int)shmBuf.shm_perm.__key);
        printf("  Sz: %d  Pid %d LastAtPid %d NumAtt %d\n", shmBuf.shm_segsz,
                shmBuf.shm_cpid, shmBuf.shm_lpid, (int)shmBuf.shm_nattch);
        printf("  euid: %d  egid: %d  uid: %d  gid: %d\n", shmBuf.shm_perm.uid,
                shmBuf.shm_perm.gid, shmBuf.shm_perm.cuid, shmBuf.shm_perm.cgid);
        printf("  mode: 0%o  seq: 0x%X\n", shmBuf.shm_perm.mode, shmBuf.shm_perm.__seq);
    }
    return 0;
}

/*
 * Allocate shared memory of given size with specified key. Attach the allocated
 * memory and return pointer to it.
 */
void *allocSharedMemory(key_t key, int memSize)
{
    int shmId, err;
    void *semPtr;
    errno = 0;
    if ((shmId = shmget(key, memSize, IPC_CREAT|IPC_EXCL|0666)) == -1)
    {
        err = errno;
        syslog(LOG_ERR, "shmget: %s\n", strerror(err));
        return 0;
    }
    if ((semPtr = shmat(shmId, 0, 0)) == (void *)-1)
    {
        err = errno;
        syslog(LOG_ERR, "shmat: %s\n", strerror(err));
        return 0;
    }
    return semPtr;
}

/*
 * Attach an already allocated shared memory area and return the address.
 */
void *getSharedMemory(key_t key, int memSize)
{
    void *semPtr;
    int shmId, err;
    errno = 0;
    if ((shmId = shmget(key, memSize, 0)) == -1)
    {
        err = errno;
        syslog(LOG_ERR, "shmget: %s\n", strerror(err));
        return 0;
    }
    if ((semPtr = shmat(shmId, 0, 0)) == (void *)-1)
    {
        err = errno;
        syslog(LOG_ERR, "shmat: %s\n", strerror(err));
        return 0;
    }
    return semPtr;
}

/*
 * Release shared memory acquired by either allocSharedMemory() or getSharedMemory()
 */
int relSharedMemory(void *shmMem)
{
    int err = 0;
    if (shmdt(shmMem) != 0)
    {
        err = errno;
        syslog(LOG_ERR, "shmdt: %s\n", strerror(err));
        return -1;
    }
    return 0;
}

/*
 * Remove the shared memory object if no process has this segment attached.
 * Must be called by the same process that created the shared object.
 */
int delSharedMemory(key_t key)
{
    int shmId;
    struct shmid_ds shmBuf;
    int err;
    if ((shmId = shmget(key, 0, 0)) == -1)
    {
        err = errno;
        syslog(LOG_ERR, "shmget: %s\n", strerror(err));
        return -1;
    }
    if (shmctl(shmId, IPC_STAT, &shmBuf) == -1)
    {
        err = errno;
        syslog(LOG_ERR, "shmctl(stat): %s\n", strerror(err));
        return -2;
    }
    if (shmBuf.shm_nattch != 0)
    {
        syslog(LOG_ERR, "%s: IPC key: 0x%X Pid %d last Pid %d NumAttach %d\n",
                                __func__, (int)key, shmBuf.shm_cpid, 
                                shmBuf.shm_lpid, (uint32_t)shmBuf.shm_nattch);
        return -3;
    }
    if (shmctl(shmId, IPC_RMID, 0) == -1)
    {
        err = errno;
        syslog(LOG_ERR, "shmctl(rm): %s\n", strerror(err));
        return -4;
    }
    return 0;
}

/* SYSTEM V  SEMAPHORE PRIMITIVES */
/*
 * Create a semaphore that can be used as a mutex(single member).
 */
int sysVsemGet(key_t key, int create)
{
    int sem;
    union semun {
        int val;
        struct semid_ds *buf;
        unsigned short  *array;
    } arg;
    errno = 0;
    if (create)
    {
        if ((sem = semget(key, 1, IPC_CREAT|0666)) == -1)
            return sem;
        /* initialise to 1, so that first lock attempt succeeds */
        arg.val = 1;
        if (semctl(sem, 0, SETVAL, arg) != 0)
            return -1;
    }
    else
        if ((sem = semget(key, 1, 0)) == -1)
            return sem;
    return sem;
}

/*
 * Implement a 'wait' method for the above mutex.
 * if parameter noWait is true, returns immediately if the mutex cannot
 * be obtained.
 */
int sysVsemWait(int semId, int noWait)
{
    struct sembuf wait[2];
    wait[1].sem_num  = 0;
    wait[1].sem_op   = -1;
    wait[1].sem_flg  = SEM_UNDO;
    if (noWait)
        wait[0].sem_flg  |= IPC_NOWAIT;
    errno = 0;
    if (semop(semId, &wait[0], 2) != 0)
        return -1;
    return 0;
}

/*
 * Implement a timed out 'wait' method for the above mutex.
 * Caller can specify number of ms to wait for the lock.
 */
int sysVsemTimedWait(int semId, unsigned int ms)
{
    struct sembuf wait;
    struct timespec tm;

    /* decrement semaphore to 'not available' */
    wait.sem_num  = 0;
    wait.sem_op   = -1;
    wait.sem_flg  = SEM_UNDO;

    tm.tv_sec = ms/1000;
    tm.tv_nsec = (ms - (tm.tv_sec*1000)) * 1000 * 1000;

    errno = 0;
    if (semtimedop(semId, &wait, 1, &tm) != 0)
        return -1;
    return 0;
}

/*
 * Implement a 'post' method for the above mutex.
 */
int sysVsemPost(int semId)
{
    struct sembuf post[1];
    /* set value back to 1 to indicate availability */
    post[0].sem_num  = 0;
    post[0].sem_op   = 1;
    post[0].sem_flg  = SEM_UNDO; 
    errno = 0;
    if (semop(semId, &post[0], 1) != 0)
        return -1;
    return 0;
}

int sysVsemDel(int semId)
{
    errno = 0;
    if (semctl(semId, 0, IPC_RMID) != 0)
        return -1;
    return 0;
}

#define DO_LOCKING

#ifdef DO_LOCKING
static int g_semShMem;
static int g_semCtrlReg;
static int g_semI2CB1;
static int g_semI2CB2;
static int g_semMCP3424;
#endif

int lockMCP3424()
{
#ifdef DO_LOCKING
    if (! g_semMCP3424)
    {
        if ((g_semMCP3424 = sysVsemGet(MCP3424_SEMID, 0)) < 0)
        {
            g_semMCP3424 = 0;
            return -1;
        }
    }
    if (sysVsemTimedWait(g_semMCP3424, MCP3424_WAIT) == 0)
        return 0;
    return -1;
#else
    return 0;
#endif
}

int unlockMCP3424()
{
#ifdef DO_LOCKING
    return sysVsemPost(g_semMCP3424);
#else
    return 0;
#endif
}

int lockSharedMem()
{
#ifdef DO_LOCKING
    if (! g_semShMem)
    {
        if ((g_semShMem = sysVsemGet(SHMEM_SEMID, 0)) < 0)
        {
            g_semShMem = 0;
            return -1;
        }
    }
    if (sysVsemTimedWait(g_semShMem, (2 * SHMEM_SEM_WAIT)) == 0)
        return 0;
    return -1;
#else
    return 0;
#endif
}

int unlockSharedMem()
{
#ifdef DO_LOCKING
    return sysVsemPost(g_semShMem);
#else
    return 0;
#endif
}

int lockCtrlReg()
{
#ifdef DO_LOCKING
    if (! g_semCtrlReg)
    {
        if ((g_semCtrlReg = sysVsemGet(CTRL_REG_SEMID, 0)) < 0)
        {
            g_semCtrlReg = 0;
            return -1;
        }
    }
    if (sysVsemTimedWait(g_semCtrlReg, SEM_WAIT_TIME) == 0)
        return 0;
    return -1;
#else
    return 0;
#endif
}

int unlockCtrlReg()
{
#ifdef DO_LOCKING
    return sysVsemPost(g_semCtrlReg);
#else
    return 0;
#endif
}

int lockI2CBus(int busNo)
{
#ifdef DO_LOCKING
    if (busNo == 1)
    {
        if (! g_semI2CB1)
        {
            if ((g_semI2CB1 = sysVsemGet(I2C_BUS1_SEMID, 0)) < 0)
            {
                g_semI2CB1 = 0;
                return -1;
            }
        }
        if (sysVsemTimedWait(g_semI2CB1, SEM_WAIT_TIME) == 0)
            return 0;
        return -1;
    }
    else
    {
        if (! g_semI2CB2)
        {
            if ((g_semI2CB2 = sysVsemGet(I2C_BUS2_SEMID, 0)) < 0)
            {
                g_semI2CB2 = 0;
                return -1;
            }
        }
        if (sysVsemTimedWait(g_semI2CB2, SEM_WAIT_TIME) == 0)
            return 0;
        return -1;
    }
#else
    return 0;
#endif
}

int unlockI2CBus(int busNo)
{
#ifdef DO_LOCKING
    if (busNo == 1)
        return sysVsemPost(g_semI2CB1);
    else
        return sysVsemPost(g_semI2CB2);
#else
    return 0;
#endif
}

/******************************************************************************/
/*  THE CODE ABOVE is shared between the UI and system s/w. KEEP IN SYNC      */
/******************************************************************************/
