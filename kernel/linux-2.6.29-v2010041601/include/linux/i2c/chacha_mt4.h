#ifndef __LINUX_I2C_CHACHA_MT4_H
#define __LINUX_I2C_CHACHA_MT4_H

/* linux/i2c/chacha_mt4.h */

struct chacha_mt4_platform_data {
	u16	model;				/* 2007. */
	int	gpio;

	int	(*get_pendown_state)(void);
	void	(*clear_penirq)(void);		/* If needed, clear 2nd level
						   interrupt source */
   	int	(*if_penirq)(void);
	void 	(*enable_irq)(int);
	int	(*init_platform_hw)(void);
	void	(*exit_platform_hw)(void);
	void 	(*gpio_attb)(int);
};

#endif
