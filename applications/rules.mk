.depend: $(SRCS)
	@rm -f $@
	@for f in $(SRCS); do \
		$(GCC) $(CFLAGS) -M $$f >> $@ ; \
	done
