/*
 * drivers/input/touchscreen/chacha_mt4.c
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <linux/hrtimer.h>
#include <linux/slab.h>
#include <linux/input.h>
#include <linux/interrupt.h>
#include <linux/i2c.h>
#include <linux/i2c/chacha_mt4.h>
#include <linux/delay.h>

#define REG_LENGTH 11

__u8 mt4_data[REG_LENGTH];

// buffer map
#define REG_TOUCH_STATUS 	0x0
#define REG_X1_LOW 		0x1
#define REG_X1_HIGH 		0x2
#define REG_Y1_LOW 		0x3
#define REG_Y1_HIGH 		0x4
#define REG_X2_LOW		0x5
#define REG_X2_HIGH 		0x6
#define REG_Y2_LOW 		0x7
#define REG_Y2_HIGH 		0x8
#define REG_STRENGTH_LOW 	0x9
#define REG_STRENGTH_HIGH	0xa
#define REG_RESERVE_1		0xb
#define REG_FIRMWARE_ID		0xc
#define REG_X_SENSITIVITY 	0xd
#define REG_Y_SENSITIVITY 	0xe
#define REG_RESERVE_2		0xf
#define REG_RESERVE_3		0x10
#define REG_RESERVE_4		0x11
#define REG_RESERVE_5		0x12
#define REG_OFFSET 		0x13
#define REG_POWER_MODE 		0x14
#define REG_EEPROM_WRITE 	0x15

enum chacha_mt4_mode {
	watch_mode = 0,
	watch_mode_e = 0x0e,
	active_mode = 0x10,
	fast_scan_mode = 0x20,
	freeze_mode = 0x90
};

struct mt4 {
	struct input_dev	*input;
	char			phys[32];
	struct i2c_client	*client;

	spinlock_t		lock;

	u16			model;

	unsigned		pendown;
	int			irq;

	int			(*get_pendown_state)(void);
	void			(*clear_penirq)(void);
	int			(*if_penirq)(void);
	void			(*enable_irq)(int);

	struct delayed_work work;
};

#define EVENT_PENDOWN 1
#define EVENT_REPEAT  2
#define EVENT_PENUP   3

static void chacha_mt4_ts_poscheck(struct work_struct *work)
{
	int event, fingers, x1, y1, x2, y2;
	struct mt4 *ts = container_of(work, struct mt4, work.work);
	struct i2c_adapter *adapter = to_i2c_adapter(ts->client->dev.parent);

	struct i2c_msg msg = {
		.addr = ts->client->addr,
		.flags = I2C_M_RD,
		.buf = mt4_data,
		.len = sizeof(mt4_data)
	};

	memset(&mt4_data, 0, sizeof(mt4_data));

	if (i2c_transfer(adapter, &msg, 1) != 1) {
		dev_err(&ts->client->dev, "Unable to transfer i2c request.\n");
		goto out;
	}

	fingers = mt4_data[REG_TOUCH_STATUS] & 0x03;
	if (fingers == 3)
		fingers--;

	x1 = mt4_data[REG_X1_LOW] | mt4_data[REG_X1_HIGH] << 8;
	y1 = mt4_data[REG_Y1_LOW] | mt4_data[REG_Y1_HIGH] << 8;
	x2 = mt4_data[REG_X2_LOW] | mt4_data[REG_X2_HIGH] << 8;
	y2 = mt4_data[REG_Y2_LOW] | mt4_data[REG_Y2_HIGH] << 8;

	event = (fingers == 0) ? EVENT_PENUP : EVENT_PENDOWN;

#if 0
	printk("mode: %02d, ", (mt4_data[REG_TOUCH_STATUS] & 0x30) >> 4);

	if (fingers == 0)
		printk("%s ", "PENUP");
	if (fingers > 0)
		printk("%04d,%04d ", x1, y1);
	if (fingers >= 2)
		printk("%04d,%04d ", x2, y2);
	printk("\n");
#endif

	if (event == EVENT_PENDOWN) {
//		input_report_key(ts->input, BTN_TOOL_FINGER, fingers == 1);
//		input_report_key(ts->input, BTN_TOOL_DOUBLETAP, fingers == 2);
//		input_report_key(ts->input, BTN_TOOL_TRIPLETAP, fingers > 2);

		input_report_key(ts->input, BTN_TOUCH, fingers > 0);
		input_report_abs(ts->input, ABS_X, x1);
		input_report_abs(ts->input, ABS_Y, y1);
		input_report_abs(ts->input, ABS_PRESSURE, 1);

		if (fingers == 2) {
			input_report_abs(ts->input, ABS_X, x2);
			input_report_abs(ts->input, ABS_Y, y2);
		}

		input_sync(ts->input);

	} else if (event == EVENT_PENUP) {
		input_report_key(ts->input, BTN_TOUCH, 0);
		input_report_abs(ts->input, ABS_PRESSURE, 0);
		input_sync(ts->input);
	}
 out:
	ts->enable_irq(1);
}

static irqreturn_t chacha_mt4_irq(int irq, void *handle)
{
	struct mt4 *ts = handle;

	if (ts->if_penirq && !ts->if_penirq())
		return IRQ_NONE;

	/* the touch screen controller chip is hooked up to the cpu
	 * using i2c and a single interrupt line. the interrupt line
	 * is pulled low whenever someone taps the screen. to deassert
	 * the interrupt line we need to acknowledge the interrupt by
	 * communicating with the controller over the slow i2c bus.
	 *
	 * we can't acknowledge from interrupt context since the i2c
	 * bus controller may sleep, so we just disable the interrupt
	 * here and handle the acknowledge using delayed work.
	 */

	if (ts->enable_irq)
		ts->enable_irq(0);
	if (ts->clear_penirq)
		ts->clear_penirq();

	schedule_delayed_work(&ts->work, 0);

	return IRQ_HANDLED;
}

static int chacha_mt4_calibration(struct mt4 *ts)
{
	struct i2c_adapter *adapter = to_i2c_adapter(ts->client->dev.parent);

	u8 cmd[]= {0x14, 0x40, 0xf3};

	struct i2c_msg msg = {
		.addr = ts->client->addr,
		.flags = 0,
		.buf = cmd,
		.len = sizeof(cmd)
	};

	if (i2c_transfer(adapter, &msg, 1) != 1) {
		dev_err(&ts->client->dev, "Unable to transfer i2c request.\n");
		return -EIO;
	}

	return 0;
}

#define MODE_SELECT_BYTE 0x14

static inline int chacha_mt4_mode_select(struct mt4 *tsc, enum chacha_mt4_mode mode)
{
	return i2c_smbus_write_byte_data(tsc->client, MODE_SELECT_BYTE, (u8)mode);
}

static int chacha_mt4_ts_open(struct input_dev *dev)
{
	return 0;
}

static void chacha_mt4_ts_close(struct input_dev *dev)
{
}

extern struct chacha_mt4_platform_data ts_chacha_mt4_data;

static int chacha_mt4_probe(struct i2c_client *client,
			const struct i2c_device_id *id)
{
	struct input_dev *input_dev;
	struct mt4 *ts;
	int err;

	if (!i2c_check_functionality(client->adapter,
				     I2C_FUNC_SMBUS_READ_WORD_DATA))
		return -EIO;

	client->dev.platform_data = &ts_chacha_mt4_data;

	ts = kzalloc(sizeof(struct mt4), GFP_KERNEL);
	input_dev = input_allocate_device();
	if (!ts || !input_dev) {
		err = -ENOMEM;
		goto err_free_mem;
	}

	i2c_set_clientdata(client, ts);

	ts->client 			= client;
	ts->input			= input_dev;
	ts->get_pendown_state 	= ts_chacha_mt4_data.get_pendown_state;
	ts->clear_penirq      	= ts_chacha_mt4_data.clear_penirq;
	ts->if_penirq	   	= ts_chacha_mt4_data.if_penirq;
	ts->enable_irq	   	= ts_chacha_mt4_data.enable_irq;
	ts->irq 			= client->irq;
	INIT_DELAYED_WORK(&ts->work, chacha_mt4_ts_poscheck);

	snprintf(ts->phys, sizeof(ts->phys), "%s/input0",
		dev_name(&client->dev));

	input_dev->name = "TOPSTD ChaCha M-T4 Touchscreen";
	input_dev->phys = ts->phys;
	input_dev->id.bustype = BUS_I2C;
	input_dev->open = chacha_mt4_ts_open;
	input_dev->close = chacha_mt4_ts_close;

	input_dev->evbit[0] = BIT(EV_SYN) | BIT(EV_KEY) | BIT(EV_ABS);
	input_dev->keybit[BIT_WORD(BTN_TOUCH)] = BIT_MASK(BTN_TOUCH);

	set_bit(EV_ABS, input_dev->evbit);
	set_bit(ABS_X, input_dev->absbit);
	set_bit(ABS_Y, input_dev->absbit);
	set_bit(ABS_PRESSURE, input_dev->absbit);

	input_set_abs_params(input_dev, ABS_X, 0, 10240, 0, 0);
	input_set_abs_params(input_dev, ABS_Y, 0, 5632, 0, 0);
	input_set_abs_params(input_dev, ABS_PRESSURE, 0, 1, 0, 0);

#if 1
	ts_chacha_mt4_data.gpio_attb(0);
	udelay(30);
	chacha_mt4_calibration(ts);
	msleep_interruptible(500);
	udelay(50);
	chacha_mt4_mode_select(ts, fast_scan_mode);
	ts_chacha_mt4_data.gpio_attb(1);
#endif

	ts_chacha_mt4_data.init_platform_hw();

	err = request_irq(ts->irq, chacha_mt4_irq, IRQF_SHARED,
			client->dev.driver->name, ts);
	if (err < 0) {
		dev_err(&client->dev, "Unable to request touchscreen IRQ.\n");
		goto err_free_mem;
	}

	err = input_register_device(input_dev);
	if (err)
		goto err_free_irq;

	dev_info(&client->dev, "registered with irq (%d)\n", ts->irq);
	return 0;

 err_free_irq:
	free_irq(ts->irq, ts);
 err_free_mem:
	input_free_device(input_dev);
	kfree(ts);

	return err;
}

static int chacha_mt4_remove(struct i2c_client *client)
{
	struct mt4	*ts = i2c_get_clientdata(client);
	struct chacha_mt4_platform_data *pdata;

	pdata = client->dev.platform_data;
	pdata->exit_platform_hw();

	free_irq(ts->irq, ts);
	input_unregister_device(ts->input);
	kfree(ts);

	return 0;
}

static struct i2c_device_id chacha_mt4_idtable[] = {
	{ "chacha_mt4", 0 },
	{ }
};

MODULE_DEVICE_TABLE(i2c, chacha_mt4_idtable);

static struct i2c_driver mt4_driver = {
	.driver = {
		.owner	= THIS_MODULE,
		.name	= "chacha_mt4"
	},
	.id_table	= chacha_mt4_idtable,
	.probe		= chacha_mt4_probe,
	.remove	= chacha_mt4_remove,
};

static int __init chacha_mt4_init(void)
{
	return i2c_add_driver(&mt4_driver);
}

static void __exit chacha_mt4_exit(void)
{
	i2c_del_driver(&mt4_driver);
}

module_init(chacha_mt4_init);
module_exit(chacha_mt4_exit);

MODULE_AUTHOR("Wang Yang <wang_yang@mtcera.com>");
MODULE_DESCRIPTION("ChaCha M-T4 TouchScreen Driver");
MODULE_LICENSE("GPL");
