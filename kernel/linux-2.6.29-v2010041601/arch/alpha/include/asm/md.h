/* $Id: md.h,v 1.1.1.1 2015/04/01 01:16:04 rjoseph Exp $
 * md.h: High speed xor_block operation for RAID4/5 
 *
 */
 
#ifndef __ASM_MD_H
#define __ASM_MD_H

/* #define HAVE_ARCH_XORBLOCK */

#define MD_XORBLOCK_ALIGNMENT	sizeof(long)

#endif /* __ASM_MD_H */
