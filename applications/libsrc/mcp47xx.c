#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include "i2c-dev.h"
#include "bing_common.h"
#include "utils.h"

/*
 Access routines for mcp4726 and mcp4728 DACs
 */

// MCP4726 single channel 12 bit DAC
/*
 MCP4726 devices are used to provide power to aux1 and aux2 auxillary
 channels of Obrero. These channels can provide 0-12V @ 100mA.
 */

// Output voltage calculation:(this is not used any more.. 05/12/2015)
// Vout = (VRef * DAC register value)/(no. of resistors in ladder) * Gain
// Assume gain is 1 for now.
// DAC reg Value = (Vout * no. of resistors) / Vref
// no. of resistors = 4096(for model 4726)

// Use only volatile settings now.

// Based on code from Alistair May.
#define MAX_V         (13.0)
// Write Volatile DAC register
int mcp47x6Write(int bus, int devAddr, float v)
{
    int hI2C;
    struct i2c_msg message[1];
    struct i2c_rdwr_ioctl_data ctrl;
    uint8_t dataBytes[4];
    float gain = 5.22;
    float vref = 2.50;
    uint16_t dacReg;
    int err = 0;

    if (v < 0.00010)
        v = 0.0;
    if (v > MAX_V)
        v = MAX_V;

    // offset and gain correction, based on Alistair's 1905 controller
    v = (v + 0.181) / 1.013; 
    //printf("V:(offset & gain corrected): %0.5f\n", v);

    dacReg = (uint16_t)((v / (gain * vref)) * 65535.0);
    //printf("dacReg: 0x%X\n", dacReg);

    //logPrint("%s: dac 0x%X dacReg %d(0x%x)\n", __func__, devAddr, dacReg, dacReg);

    if ((hI2C = open(get_i2c_bus(bus), O_RDWR)) < 0)
        return -1;

    ctrl.nmsgs = 1;
    ctrl.msgs  = &message[0];

    message[0].addr  = devAddr;
    message[0].flags = 0;
    message[0].len = 3;
    message[0].buf = (char *)&dataBytes[0];

    // Write volatile, ext. buffered ref, not power down, gain=1
    dataBytes[0] = 0x58; //0b01011000
    dataBytes[1] = (dacReg >> 8) & 0xff;
    dataBytes[2] = (dacReg & 0xFF);
    //printf("DataBytes [%x : %x : %x]\n",dataBytes[0],dataBytes[1],dataBytes[2]);

    /* do i2c transaction */
    if(ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        err = errno;
        logPrint("Error: failed to write to mcp47x6(%d)\n", err);
        close(hI2C);
        return -2;
    }
    close(hI2C);
    return 0;
}

/*
 Byte 0 - Volatile status bits and configuration bits
 Byte 3 - Volatile status bits and non-volatile configuration bits
 Bytes 1 and 2 contains volatile dac setting
 Bytes 4 and 5 contains non-volatile dac setting
 --------------------------------------------------------------------
 Bit definitions for Byte 0 & 3
 7 - ready/busy bits. 1 - ready, 0 - in EEPROM programming cycle 
 6 - Power on reset status. 1 - device is powered with Vdd > vpor, 0 - pwr off
 4-3 - Vref select. 0x - Vdd(un-buf), 10 - Vref pin(un-buf), 11 - Vref pin(buf)
 2-1 - power down select. 00 - normal, 01/10/11 - powered down
 0 - Gain select. 0 - 1x, 1 - 2x
 */
static void printmcp47x6StatusBits(uint8_t sByte)
{
    if (sByte & 0x80)
        printf("  Ready, ");
    else 
        printf("  Busy, ");
    if (sByte & 0x40)
        printf("  Power On, ");
    else
        printf("  Power Off, ");
    if ((sByte & 0x18) == 0x18)
        printf("  Vref buffered, ");
    else if ((sByte & 0x18) != 0)
        printf("  Vref un-buffered, ");
    else 
        printf("  Vdd un-buffered, ");
    if ((sByte & 0x6) == 0)
        printf("  Normal Op, ");
    else
        printf("  Powered down, ");
    if (sByte & 1)
        printf("  Gain x2\n");
    else
        printf("  Gain x1\n");
}

static void printmcp47x6Data(uint8_t *dataBuf)
{
    printf("Vol. Status and Config:\n");
    printmcp47x6StatusBits(dataBuf[0]);
    printf("Volatile Reg. 0x%X\n", (((dataBuf[1] << 8) | dataBuf[2]) >> 4));
    printf("Vol. Status and non-vol. Config:\n");
    printmcp47x6StatusBits(dataBuf[3]);
    printf("Non-Volatile Reg. 0x%X\n", (((dataBuf[4] << 8) | dataBuf[5]) >> 4));
}

// Device returns 6 bytes of data
// [vol status bits(2b), vol cfg bits(6b)], data bits1, data bits2, 
// nv cfg bits, nv data bits1, nv data bits2
int mcp47x6Read(int bus, int devAddr, float *v)
{
    int hI2C;
    float gain = 5.22;
    float vref = 2.50;
    struct i2c_msg message[1];
    struct i2c_rdwr_ioctl_data ctrl;
    uint8_t dataBuf[6];
    uint16_t rVal;
    float value;
    int err = 0;

    if ((hI2C = open(get_i2c_bus(bus), O_RDWR)) < 0)
        return -1;
    //printf("%s: dac 0x%X\n", __func__, devAddr);
    ctrl.nmsgs = 1;
    ctrl.msgs  = &message[0];

    message[0].addr  = devAddr;
    message[0].flags = I2C_M_RD;
    message[0].len = 6;
    message[0].buf = (char *)&dataBuf[0];

    /* do i2c transaction */
    if(ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        err = errno;
        logPrint("Error: failed to read mcp47x6(%d)\n", err);
        close(hI2C);
        return -2;
    }
    close(hI2C);
    //printmcp47x6Data(dataBuf);
    //rVal = dataBuf[1] << 4 | ((dataBuf[2] & 0xf0) >> 4);
    rVal = (dataBuf[1] << 8) | (dataBuf[2] & 0xff);
    //printf("rVal 0x%x \n", rVal);
    value = (rVal * gain * vref) / 65535.0;
    //printf("Value: %0.3f\n", value);
    *v = (value *1.013) - 0.181;
    return 0;
}

// Access routines for mcp4728 4 channel DAC
/*
 This is a 4 channel device, only 2 channels are used as of now(12/2/2014)
 [From document: Obrero Analogue Interface Description, 11/25/2014]
 Channel A trims the DRTD negative current level(in conjunction with
     BING_DRTD_POL signal).
 Channel B trims the DRTD positive current level(in conjunction with
     BING_DRTD_POL signal).
 */

// Write single register: update volatile DAC register and EEPROM
// Assumes internal reference
// Byte 1 : device address byte
// Byte 2 Bits:
//    Cmd bits(7-5: 010, write bytes)
//    Write Function(4-3: 11: single write for DAC register and Eeprom)
//    Channel No(2-1: channel 00:1, 01:2,10:3,11:4)
//    UDAC bit(0: don't care)
// Byte 3 bits:
//    Vref bit(7: 0 - internal REF, 1 - external REF)
//    PwrDown bits(6-5: 00 - normal mode, 01/10/11 - powered down)
//    Gain Select(4: 0 - x1, 1 - x2)
//    LSB 4bits: 4 MSB bits of DAC setting
// Byte 4: LSB 8 bits of DAC setting
#define VREF_4728 (2.049)
int mcp4728Write(int bus, int devAddr, int chan, int gain, float v)
{
    int hI2C;
    struct i2c_msg message[1];
    struct i2c_rdwr_ioctl_data ctrl;
    uint8_t dataBytes[4];
    int err = 0, gBits = 0;
    int dacReg;

    if ((chan < 1) || (chan > 4))
        return -1;
    if (gain == 2)
    {
        if ((v < 0.0) || (v > (2*VREF_4728)))
            return -1;
        dacReg = (int)((v * 4096.0) / (2 * VREF_4728));
        gBits = 0x10;
    }
    else
    {
        if ((v < 0.0) || (v > VREF_4728))
            return -1;
        dacReg = (int)((v * 4096.0) / VREF_4728);
        gBits = 0x00;
    }

    //logPrint("%s: v: %0.3f dacReg %d(0x%x)\n", __func__, v, dacReg, dacReg);

    if ((hI2C = open(get_i2c_bus(bus), O_RDWR)) < 0)
        return -1;

    ctrl.nmsgs = 1;
    ctrl.msgs  = &message[0];

    message[0].addr  = devAddr;
    message[0].flags = 0;
    message[0].len = 3;
    message[0].buf = (char *)&dataBytes[0];

    // Command bits are 11 
    //dataBytes[0] = 0x58 | (((chan-1) & 0x3) << 1);
    // C[2:0] 0b010, W[1:0] 0b00 : Multiwrite DAC, reg. in DAC1,DAC0
    dataBytes[0] = 0x40 | (((chan-1) & 0x3) << 1);
    // Power down bits are 00, vref: 1
    dataBytes[1] = 0x80 | gBits | ((dacReg & 0xF00) >> 8);
    dataBytes[2] = dacReg & 0xFF;
//logPrint("%s: g:%d v:%0.3f dBytes 0x%x 0x%x 0x%x\n", __func__, gain, v, dataBytes[0], dataBytes[1], dataBytes[2]);
    /* do i2c transaction */
    if(ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        err = errno;
        logPrint("Error: failed to read to mcp47x6(%d)\n", err);
        close(hI2C);
        return -2;
    }
    close(hI2C);
    return 0;
}

static void printmcp4728Chan(uint8_t *dataBuf)
{
    printf("Chan: %d Reg: 0x%X\n\n", ((dataBuf[0] & 0x30) >> 4),
                        (((dataBuf[1] & 0xF) << 8) | (dataBuf[2])));
}

static void printmcp4728Data(uint8_t *dataBuf)
{
    printf("Chan A:(volatile)[0x%x 0x%x 0x%X]\n", dataBuf[0], dataBuf[1],
            dataBuf[2]);
    printmcp4728Chan(&(dataBuf[0]));
    printf("Chan A:(EEPROM)[0x%x 0x%x 0x%x]\n", dataBuf[3], dataBuf[4],
            dataBuf[5]);
    printmcp4728Chan(&(dataBuf[3]));
    printf("Chan B:(volatile)[0x%x 0x%x 0x%x]\n", dataBuf[6], dataBuf[7],
            dataBuf[8]);
    printmcp4728Chan(&(dataBuf[6]));
    printf("Chan B:(EEPROM)[0x%x 0x%x 0x%x]\n", dataBuf[9], dataBuf[10],
            dataBuf[11]);
    printmcp4728Chan(&(dataBuf[9]));
    printf("Chan C:(volatile)[0x%x 0x%x 0x%x]\n", dataBuf[12], dataBuf[13],
            dataBuf[14]);
    printmcp4728Chan(&(dataBuf[12]));
    printf("Chan C:(EEPROM)[0x%x 0x%x 0x%x]\n", dataBuf[15], dataBuf[16],
            dataBuf[17]);
    printmcp4728Chan(&(dataBuf[15]));
    printf("Chan D:(volatile)[0x%x 0x%x 0x%x]\n", dataBuf[18], dataBuf[19],
            dataBuf[20]);
    printmcp4728Chan(&(dataBuf[18]));
    printf("Chan D:(EEPROM)[0x%x 0x%x 0x%x]\n", dataBuf[21], dataBuf[22],
            dataBuf[23]);
    printmcp4728Chan(&(dataBuf[21]));
}

static void inline printReg(int ch, int regVal)
{
    int gain = 1;
    if ((ch == 1) || (ch == 2))
        gain = 2;
    printf("%d: %0.3f\n", ch, ((gain * 2.048 * regVal) / 4096));
}

static void printVolatileRegs(uint8_t *dataBuf)
{
    printReg(1, (((dataBuf[1] & 0xF) << 8) | (dataBuf[2])));
    printReg(2, (((dataBuf[7] & 0xF) << 8) | (dataBuf[8])));
    printReg(3, (((dataBuf[13] & 0xF) << 8) | (dataBuf[14])));
    printReg(4, (((dataBuf[19] & 0xF) << 8) | (dataBuf[20])));
}
// Read from device returns 24 Bytes.
int mcp4728Read(int bus, int devAddr, uint8_t *dataBuf)
{
    int hI2C;
    struct i2c_msg message[1];
    struct i2c_rdwr_ioctl_data ctrl;
    int err = 0;

    if ((hI2C = open(get_i2c_bus(bus), O_RDWR)) < 0)
        return -1;

    memset(dataBuf, 0, 24);
    ctrl.nmsgs = 1;
    ctrl.msgs  = &message[0];

    message[0].addr  = devAddr;
    message[0].flags = I2C_M_RD;
    message[0].len = 24;
    message[0].buf = (char *)&dataBuf[0];

    /* do i2c transaction */
    if(ioctl(hI2C, I2C_RDWR, &ctrl) < 0)
    {
        err = errno;
        logPrint("Error: failed to read to mcp4728(%d)\n", err);
        close(hI2C);
        return -2;
    }
    close(hI2C);
    //printmcp4728Data(dataBuf);
    printVolatileRegs(dataBuf);
    return 0;
}
