#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <errno.h>
#include <string.h>
#include <syslog.h>
#include "bing_common.h"
#include "sharedmem.h"
#include "sysutils.h"
#include "utils.h"

// allocate shared memory segment, semaphore, initialise.
// report any errors to syslog for now..
int main()
{
    int err;
    int semId;
    int semFail = 0;
    sysParams *pCache;

    if ((pCache = allocSharedMemory(IPC_KEY_SHAREDMEM, sizeof(sysParams))) == 0)
    {
        err = errno;
        syslog(LOG_ERR, "Failed to allocate shared memory: %s\n", strerror(err));
        printf("\n  ERROR: shared memory - %s\n", strerror(err));
        exit(1);
    }
    // memory is set to 0 by allocation routine.

    // allocate semaphores now.
    if ((semId = sysVsemGet(SHMEM_SEMID, 1)) < 0)
    {
        err = errno;
        syslog(LOG_ERR, "Shared memory sem: %s\n", strerror(err));
        printf("\n  ERROR: Shared memory sem - %s\n", strerror(err));
        semFail++;
    }
    if ((semId = sysVsemGet(CTRL_REG_SEMID, 1)) < 0)
    {
        err = errno;
        syslog(LOG_ERR, "Control reg sem: %s\n", strerror(err));
        printf("\n  ERROR: Control reg sem - %s\n", strerror(err));
        semFail++;
    }

    if ((semId = sysVsemGet(I2C_BUS1_SEMID, 1)) < 0)
    {
        err = errno;
        syslog(LOG_ERR, "I2C bus1 sem: %s\n", strerror(err));
        printf("\n  ERROR: I2C bus1 sem - %s\n", strerror(err));
        semFail++;
    }

    if ((semId = sysVsemGet(I2C_BUS2_SEMID, 1)) < 0)
    {
        err = errno;
        syslog(LOG_ERR, "I2C bus2 sem: %s\n", strerror(err));
        printf("\n  ERROR: I2C bus2 sem - %s\n", strerror(err));
        semFail++;
    }

    if (semFail)
        pCache->semFailed++;

    // init shared memory params
    pCache->sensorType[0] = DT_470;
    pCache->lcdBright     = 5; // half bright; set at startup
    pCache->pidAvg        = 0;
    pCache->tcVMiss       = (211.0 / 1000000.0);  // use approx vMiss:211uV
    pCache->tcGTC         = 42.15526;          // nominal value
    pCache->stimI[0]      = STIM_10uA; //(10.0/1000000.0);
    pCache->stimI[1]      = STIM_10uA; //(10.0/1000000.0);
    pCache->heatIRange    = HEAT_IRANGE_2A;

    // factory calibration defaults.
    pCache->tcVZero       = 1.0;    // invalid value
    pCache->CJCADCGain    = 10.00;  // invalid value
    pCache->G1drtd        = 0.9900990099; // default 1/1.01
    pCache->stimTrim100u_p = 
    pCache->stimTrim100u_n = 
    pCache->stimTrim10u_p = 
    pCache->stimTrim10u_n = 
    pCache->stimTrim1u_p = 
    pCache->stimTrim1u_n =  1.0;  // invalid value

    exit(0);
}
