/* 
 * drivers/input/touchscreen/ft5x26.c
 *
 * FocalTech ft5x26 TouchScreen driver. 
 *
 * Copyright (c) 2010  Focal tech Ltd.
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * VERSION      	DATE			AUTHOR
 *    1.0		  2010-01-05			WenFS
 *
 * note: only support mulititouch	Wenfs 2010-10-01
 * Nov 12, 2018: Modified extensively for Gatan - RJ
 * Jul 18, 2022: Modified to support both Old & New 1905 Displays - PP
 */
#include <linux/i2c.h>
#include <linux/input.h>
//#include <linux/earlysuspend.h>
#include <linux/interrupt.h>
#include <linux/workqueue.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/semaphore.h>
#include <asm/bitops.h>
#include <asm/io.h>
#include <asm/irq.h>
#include <asm/atomic.h>
#include <asm/uaccess.h>
#include <linux/gpio.h>
#include <linux/module.h>

#include "ft5x26.h"

/* taking short-cuts.. using an arch specific call to get immr */
extern phys_addr_t get_immrbase(void);

#ifdef ORIG_CODE
static unsigned char *reset_ptr;    // pointer to fpga regs
#endif
static gpio_regs *gpio;

// FOCAL-TECH defines

#define FT5X26_NAME        "focaltp"
#define FT5X26_DRV         "ft5x26"

#undef CONFIG_HAS_EARLYSUSPEND

// serialise access to device in interrupt/non-interrupt threads
static struct semaphore sem_dev;

static struct i2c_client *this_client;
//static struct ft5x26_ts_platform_data *pdata;

//#define CONFIG_FT5X26_MULTITOUCH 1

ft5x26_char_dev tp_char_dev;

#ifdef ORIG_CODE
static atomic_t touch_count = ATOMIC_INIT(0);
static atomic_t tp_reset_count = ATOMIC_INIT(0);
#endif

#ifdef ORIG_CODE
static unsigned long next_reset_jiffy;
static unsigned long dev_tmr_reset_cnt = 0;
// ignore first interrupt after reset, protected by sem_dev
static int reset_flag = 0; 
#endif
static unsigned long last_intr_jiffy;
static unsigned long dev_intr_cnt = 0;

static int ft5x26_i2c_rxdata(char *rxdata, int length)
{
	int ret;

	struct i2c_msg msgs[] = {
		{
			.addr	= this_client->addr,
			.flags	= 0,
			.len	= 1,
			.buf	= rxdata,
		},
		{
			.addr	= this_client->addr,
			.flags	= I2C_M_RD,
			.len	= length,
			.buf	= rxdata,
		},
	};

    //msleep(1);
	ret = i2c_transfer(this_client->adapter, msgs, 2);
	if (ret < 0)
		pr_err("msg %s i2c read error: %d\n", __func__, ret);
	
	return ret;
}

static int ft5x26_i2c_txdata(char *txdata, int length)
{
	int ret;

	struct i2c_msg msg[] = {
		{
			.addr	= this_client->addr,
			.flags	= 0,
			.len	= length,
			.buf	= txdata,
		},
	};

   	//msleep(1);
	ret = i2c_transfer(this_client->adapter, msg, 1);
	if (ret < 0)
		pr_err("%s i2c write error: %d\n", __func__, ret);

	return ret;
}

static int ft5x26_write_reg(u8 addr, u8 para)
{
    u8 buf[3];
    int ret = -1;

    buf[0] = addr;
    buf[1] = para;
    ret = ft5x26_i2c_txdata(buf, 2);
    if (ret < 0) {
        pr_err("write reg failed! %#x ret: %d", buf[0], ret);
        return -1;
    }
    
    return 0;
}

static int ft5x26_read_reg(u8 addr, u8 *pdata)
{
	int ret;
	u8 buf[2] = {0};
	struct i2c_msg msgs[] = {
		{
			.addr	= this_client->addr,
			.flags	= 0,
			.len	= 1,
			.buf	= buf,
		},
		{
			.addr	= this_client->addr,
			.flags	= I2C_M_RD,
			.len	= 1,
			.buf	= buf,
		},
	};

	buf[0] = addr;

    //msleep(1);
	ret = i2c_transfer(this_client->adapter, msgs, 2);
	if (ret < 0)
		pr_err("msg %s i2c read error: %d\n", __func__, ret);

	*pdata = buf[0];
	return ret;
  
}

#ifdef ORIG_CODE
static void ft5x26_reset(void)
{
    setbits8((reset_ptr+RESET_REG), FPGA_RST_TP);
    msleep(50);
    clrbits8((reset_ptr+RESET_REG), FPGA_RST_TP);
    msleep(200);
    reset_flag = 1;
}
#endif

#define DELAY_REG_READ (20) //ms
#define TMOUT_REG_READ (300) //ms
#define TP_CHIP_ID     (0x54)
static int ft5x26_wait_tp_valid(void)
{
	unsigned char uc_reg_value; 
    int i = 0;
    int ret = 0;
    do
    {
        ret = ft5x26_read_reg(FT5X26_REG_CHIPID, &uc_reg_value);
        if ((ret < 0) || (uc_reg_value != TP_CHIP_ID))
            ;
        else if (uc_reg_value == TP_CHIP_ID)
            return 0;
        i++;
    } while ((i * DELAY_REG_READ) < TMOUT_REG_READ);
    printk("%s: Chip NOT ready!\n", __func__);
    return -1;
}

static void ft5x26_ts_release(void)
{
	struct ft5x26_ts_data *data = i2c_get_clientdata(this_client);
#ifdef CONFIG_FT5X26_MULTITOUCH	
	input_report_abs(data->input_dev, ABS_MT_TOUCH_MAJOR, 0);
#else
	input_report_abs(data->input_dev, ABS_PRESSURE, 0);
	input_report_key(data->input_dev, BTN_TOUCH, 0);
//printk("%s: 0\n", __func__);
#endif
	input_sync(data->input_dev);
}

static int ft5x26_read_touch_data(void)
{
	struct ft5x26_ts_data *data = i2c_get_clientdata(this_client);
    touch_event *event = &data->ts_event;
    static int errCnt = 0;
    static int tflag = 0;
	u8 buf[FT_TCH_LEN] = {0};
	int ret = -1;

#ifdef CONFIG_FT5X26_MULTITOUCH
	ret = ft5x26_i2c_rxdata(buf, 31);
#else
    ret = ft5x26_i2c_rxdata(buf, FT_TCH_LEN);
#endif
    if (ret < 0) 
    {
		printk("%s read_data i2c_rxdata failed: %d\n", __func__, ret);
#ifdef ORIG_CODE
        errCnt++;
        if (errCnt > 1)
        {
            atomic_inc(&tp_reset_count);
            ft5x26_reset();
            ft5x26_wait_tp_valid();
            errCnt = 0;
        }
#endif
		return ret;
	}
    else
        errCnt = 0;

	memset(event, 0, sizeof(touch_event));
	event->num_touches = buf[FT_TD_STATUS_POS] & FT_STATUS_NUM_TP_MASK;
    event->flag = (buf[FT_TOUCH_FLAG_POS] & FT_TOUCH_FLAG_MASK) >> 6;
    //printk("%s: n: %d, f: %d, P: %dx%d \n", __func__, event->num_touches, 
    //                                    event->flag, event->x, event->y);
    if (event->num_touches == 0) {
        if (tflag)
        {
            ft5x26_ts_release();
            tflag = 0;
        }
        return 1; 
    }

#ifdef CONFIG_FT5X26_MULTITOUCH
    switch (event->touch_point) {
		case 5:
			event->x5 = (s16)(buf[0x1b] & 0x0F)<<8 | (s16)buf[0x1c];
			event->y5 = (s16)(buf[0x1d] & 0x0F)<<8 | (s16)buf[0x1e];
		case 4:
			event->x4 = (s16)(buf[0x15] & 0x0F)<<8 | (s16)buf[0x16];
			event->y4 = (s16)(buf[0x17] & 0x0F)<<8 | (s16)buf[0x18];
		case 3:
			event->x3 = (s16)(buf[0x0f] & 0x0F)<<8 | (s16)buf[0x10];
			event->y3 = (s16)(buf[0x11] & 0x0F)<<8 | (s16)buf[0x12];
		case 2:
			event->x2 = (s16)(buf[9] & 0x0F)<<8 | (s16)buf[10];
			event->y2 = (s16)(buf[11] & 0x0F)<<8 | (s16)buf[12];
		case 1:
			event->x1 = (s16)(buf[3] & 0x0F)<<8 | (s16)buf[4];
			event->y1 = (s16)(buf[5] & 0x0F)<<8 | (s16)buf[6];
            break;
		default:
		    return -1;
	}
#else
    if (event->num_touches == 1) 
    {
    	event->x = ((buf[FT_TOUCH_X_H_POS] & 0x0F) << 8)|buf[FT_TOUCH_X_L_POS];
		event->y = ((buf[FT_TOUCH_Y_H_POS] & 0x0F) << 8)|buf[FT_TOUCH_Y_L_POS];
        tflag = 1;
    }
#endif

    return 0;
}

#define val_cut_max(x, max, reverse) \
do                                   \
{                                    \
    if(x > max)                      \
        x = max;                     \
    if(reverse)                      \
        x = (max) - (x);             \
}                                    \
while(0)

static void ft5x26_report_value(void)
{
	struct ft5x26_ts_data *data = i2c_get_clientdata(this_client);
	touch_event *event = &data->ts_event;
#ifdef ORIG_CODE
    int reverse=1;
#else
    int reverse=0;
#endif

#ifdef CONFIG_FT5X26_MULTITOUCH
	switch(event->touch_point) {
		case 5:
			input_report_abs(data->input_dev, ABS_MT_TOUCH_MAJOR, event->pressure);
			input_report_abs(data->input_dev, ABS_MT_POSITION_X, event->x5);
			input_report_abs(data->input_dev, ABS_MT_POSITION_Y, event->y5);
			input_report_abs(data->input_dev, ABS_MT_WIDTH_MAJOR, 1);
			input_mt_sync(data->input_dev);
		case 4:
			input_report_abs(data->input_dev, ABS_MT_TOUCH_MAJOR, event->pressure);
			input_report_abs(data->input_dev, ABS_MT_POSITION_X, event->x4);
			input_report_abs(data->input_dev, ABS_MT_POSITION_Y, event->y4);
			input_report_abs(data->input_dev, ABS_MT_WIDTH_MAJOR, 1);
			input_mt_sync(data->input_dev);
		case 3:
			input_report_abs(data->input_dev, ABS_MT_TOUCH_MAJOR, event->pressure);
			input_report_abs(data->input_dev, ABS_MT_POSITION_X, event->x3);
			input_report_abs(data->input_dev, ABS_MT_POSITION_Y, event->y3);
			input_report_abs(data->input_dev, ABS_MT_WIDTH_MAJOR, 1);
			input_mt_sync(data->input_dev);
		case 2:
			input_report_abs(data->input_dev, ABS_MT_TOUCH_MAJOR, event->pressure);
			input_report_abs(data->input_dev, ABS_MT_POSITION_X, event->x2);
			input_report_abs(data->input_dev, ABS_MT_POSITION_Y, event->y2);
			input_report_abs(data->input_dev, ABS_MT_WIDTH_MAJOR, 1);
			input_mt_sync(data->input_dev);
		case 1:
			input_report_abs(data->input_dev, ABS_MT_TOUCH_MAJOR, event->pressure);
			input_report_abs(data->input_dev, ABS_MT_POSITION_X, event->x1);
			input_report_abs(data->input_dev, ABS_MT_POSITION_Y, event->y1);
			input_report_abs(data->input_dev, ABS_MT_WIDTH_MAJOR, 1);
			input_mt_sync(data->input_dev);
	        input_report_key(data->input_dev, BTN_TOUCH, 1);
	        input_sync(data->input_dev);

		default:
			break;
	}
#else	/* CONFIG_FT5X26_MULTITOUCH*/
	if (event->num_touches == 1) 
    {
        val_cut_max(event->x, SCREEN_MAX_X-1, reverse);
        val_cut_max(event->y, SCREEN_MAX_Y-1, reverse);
		input_report_abs(data->input_dev, ABS_X, event->x);
		input_report_abs(data->input_dev, ABS_Y, event->y);
#ifdef ORIG_CODE
		input_report_abs(data->input_dev, ABS_PRESSURE, FT_PRESS);
#endif
		input_report_key(data->input_dev, BTN_TOUCH, 1);
	    input_sync(data->input_dev);
//printk("%s: %d %d \n", __func__, event->x, event->y);
	}
#endif	/* CONFIG_FT5X26_MULTITOUCH*/
}	/*end ft5x26_report_value*/

static int n_int = 0;
static void ft5x26_ts_irq_work(struct work_struct *work)
{
	int ret = -1;
	//printk("==work 1=\n");
    dev_intr_cnt++;
	if ((ret = ft5x26_read_touch_data()) == 0)
    {
		ft5x26_report_value();
#ifdef ORIG_CODE
        // Do not count the first interrupt after a reset of the controller
        // otherwise, this breaks our LCD dimming logic
        // Touch controller could be reset every few seconds because of 
        // inactivity. Touch controller emits an interrupt after every reset
        // The touchcount goes up every few seconds. LCD dim thread watches this
        // count to dim/bright LCD
        if (! reset_flag)
            atomic_inc(&touch_count);
        else
            reset_flag = 0;
#endif
    }
	//printk("==work 2=\n");
    up(&sem_dev);
    enable_irq(this_client->irq);
}

#define TS_INT_BIT (0x40000000) // bit 30
static irqreturn_t ft5x26_ts_interrupt(int irq, void *dev_id)
{
	struct ft5x26_ts_data *ft5x26_ts = dev_id;
    uint32_t bits = in_be32(&gpio->gpIER);

    // make sure it is a touch event
    if ((bits & TS_INT_BIT) == TS_INT_BIT)
    {
        //printk("TS  INT(0x%X) ", bits);
    }
    else
    {
        //printk("NOT TS  INT(0x%X)\n", bits);
	    return IRQ_HANDLED;
    }
    n_int++;
    // try locking device. unlock is done in worker thread
    if (down_trylock(&sem_dev) != 0)
        return IRQ_HANDLED;
    last_intr_jiffy = jiffies;
    disable_irq_nosync(this_client->irq);		
    //printk("INT -- %d\n", n_int);
    //clear  interrupt
    out_be32(&gpio->gpIER, bits);
	if (!work_pending(&ft5x26_ts->touch_event_work))
		queue_work(ft5x26_ts->ts_workqueue, &ft5x26_ts->touch_event_work);
	return IRQ_HANDLED;
}

#ifdef ORIG_CODE
// check for touch panel hangs and reset if needed
static void ft5x26_ts_dev_check(struct work_struct *work)
{
	struct ft5x26_ts_data *data = i2c_get_clientdata(this_client);

    if (time_before(jiffies, next_reset_jiffy))
        goto out_dev_check;

    if (down_trylock(&sem_dev) != 0)
        goto out_dev_check;
     
    disable_irq_nosync(this_client->irq);		
    out_be32(&gpio->gpIER, TS_INT_BIT); // clear any pending interrupts
    
    ft5x26_reset();
    ft5x26_wait_tp_valid();

    //out_be32(&gpio->gpIER, TS_INT_BIT); // clear any pending interrupts
    next_reset_jiffy = jiffies + (6 * HZ);
    up(&sem_dev);
    enable_irq(this_client->irq);
    dev_tmr_reset_cnt++;
    if ((dev_tmr_reset_cnt % 10) == 0)
        printk("%s: int: %lu tmr_reset: %lu tp_reset: %d\n", __func__, 
                dev_intr_cnt, dev_tmr_reset_cnt, atomic_read(&tp_reset_count));

out_dev_check:
    data->dev_check_timer.expires = jiffies + msecs_to_jiffies(1000);
    add_timer(&data->dev_check_timer);
}

static void dev_check_timer_func(unsigned long data)
{
    struct ft5x26_ts_data *ft5x26_ts = (struct ft5x26_ts_data *)data;
    if (time_before(jiffies, (last_intr_jiffy+HZ)))
    {
        ft5x26_ts->dev_check_timer.expires = jiffies + msecs_to_jiffies(1000);
        add_timer(&ft5x26_ts->dev_check_timer);
    }
    else
        queue_work(ft5x26_ts->dev_check_q,  &ft5x26_ts->dev_check_work);
}

// START character device routines
ssize_t ft5x26_read(struct file *filp, char __user *buf, size_t count, 
                                                                loff_t *f_pos)
{
    // XXX: Assuming only ONE task will read at a time. Even without a 
    // serialising lock, we should be OK since we use atomic_read..
    // Irrespective of number of bytes asked, just return the count always..
    int val = atomic_read(&touch_count);
    int dataSz = sizeof(int);
    if (count < dataSz)
        return -EFAULT;
    return ((put_user(val, (int __user *)buf)) ? -EFAULT : dataSz);
}

int ft5x26_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, 
                                                            unsigned long arg)
{
    int ret = 0;
    int intVal;
    //printk("%s: cmd %x\n", __func__, cmd);
    // validate cmd..
    if (_IOC_TYPE(cmd) != TPDEV_IOCTL_MAGIC)
    {
        printk("%s: !magic %d\n", __func__, _IOC_TYPE(cmd));
        return -ENOTTY;
    }
    if (_IOC_NR(cmd) > TPDEV_IOCTL_MAX)
    {
        printk("%s: >max %d(%d)\n", __func__, _IOC_NR(cmd), TPDEV_IOCTL_MAX);
        return -ENOTTY;
    }
    // we will use put_user/get_user so access_ok is not needed
    switch (cmd)
    {
        case TPDEV_GET_TPRSTCNT:
            intVal = atomic_read(&tp_reset_count);
            ret = put_user(intVal, (int __user *)arg);
            break;
        case TPDEV_GET_TPRSTSTAT:
            intVal = atomic_read(&tp_reset_count);
            ret = put_user(intVal, (int __user *)arg);
            break;
        case TPDEV_SET_DBGFLAG:
            ret = 0; //get_user(tp_reset_dbg, (int __user *)arg);
            break;
        case TPDEV_SET_TRACEFLAG:
            ret = 0; //get_user(tp_reset_trace, (int __user *)arg);
            break;
        case TPDEV_SET_RESET_DELAY:
#if 0
            ret = get_user(intVal, (int __user *)arg);
            if (ret == 0)
            {
                if ((intVal >= 100) && (intVal <= 5000))
                    tp_reset_delay = intVal;
                else
                    ret = -ERANGE;
            }
#else
            ret = 0;
#endif
            break;
        default:
            ret = -ENOTTY;
            break;
    }
    return ret;
}

static struct file_operations ft5x26_fops = 
{
    .owner = THIS_MODULE,
    .read  = ft5x26_read,
    .ioctl = ft5x26_ioctl,
};
// END character device routines
#endif

static int ft_drv_setup(struct ft5x26_ts_data *ft5x26_ts)
{
    int err = 0;
    // interrupt work
	INIT_WORK(&ft5x26_ts->touch_event_work, ft5x26_ts_irq_work);
	ft5x26_ts->ts_workqueue = 
                    create_singlethread_workqueue("ft_ts_work");
	if (!ft5x26_ts->ts_workqueue) 
    {
		err = -ESRCH;
        printk(KERN_ERR "%s: work queue create failed\n", FT5X26_NAME);
        return err;
	}
#ifdef ORIG_CODE
    // hang check timer
	INIT_WORK(&ft5x26_ts->dev_check_work, ft5x26_ts_dev_check);
	ft5x26_ts->dev_check_q = 
                    create_singlethread_workqueue("ft_ts_dev_check");
	if (!ft5x26_ts->dev_check_q) 
    {
		err = -ESRCH;
        printk(KERN_ERR "%s: dev_check queue create failed\n", FT5X26_NAME);
	    destroy_workqueue(ft5x26_ts->ts_workqueue);
        return err;
	}
    // character device: make this same as fusion tp device to keep 
    // user level code unchanged irrespective whether the system has
    // fusion touch controller or the focal tech one.

    // create a char device for keeping track of touch events from userland
    if (alloc_chrdev_region(&tp_char_dev.tp_dev_no, 0, 1, "fusion_tpdev") < 0)
    {
        err = -ENODEV;
        goto out_drv_setup2;
    }
    /* register with kernel */
    cdev_init(&tp_char_dev.char_dev, &ft5x26_fops);
    if (cdev_add(&tp_char_dev.char_dev, tp_char_dev.tp_dev_no, 1) < 0)
    {
        err = -ENODEV;
        goto out_drv_setup1;
    }

    // initialize timers
    init_timer(&ft5x26_ts->dev_check_timer);
    ft5x26_ts->dev_check_timer.data = (unsigned long) (ft5x26_ts);
    ft5x26_ts->dev_check_timer.function = dev_check_timer_func;
#endif
    return err;

#ifdef ORIG_CODE
out_drv_setup1:
    unregister_chrdev_region(tp_char_dev.tp_dev_no, 1);

out_drv_setup2:
    destroy_workqueue(ft5x26_ts->ts_workqueue);
    destroy_workqueue(ft5x26_ts->dev_check_q);
    return err;
#endif
}

static int ft_register_input(struct ft5x26_ts_data *ft5x26_ts)
{
	struct input_dev *input_dev;
    int err = 0;
    // input device allocation and setup
	input_dev = input_allocate_device();
	if (!input_dev) 
    {
		err = -ENOMEM;
		printk("%s: failed to allocate input device\n", __func__);
		return err;
	}
	
	ft5x26_ts->input_dev = input_dev;

#ifdef CONFIG_FT5X26_MULTITOUCH
	set_bit(ABS_MT_TOUCH_MAJOR, input_dev->absbit);
	set_bit(ABS_MT_POSITION_X, input_dev->absbit);
	set_bit(ABS_MT_POSITION_Y, input_dev->absbit);
	set_bit(ABS_MT_WIDTH_MAJOR, input_dev->absbit);

	input_set_abs_params(input_dev,
			     ABS_MT_POSITION_X, 0, SCREEN_MAX_X, 0, 0);
	input_set_abs_params(input_dev,
			     ABS_MT_POSITION_Y, 0, SCREEN_MAX_Y, 0, 0);
	input_set_abs_params(input_dev,
			     ABS_MT_TOUCH_MAJOR, 0, PRESS_MAX, 0, 0);
	input_set_abs_params(input_dev,
			     ABS_MT_WIDTH_MAJOR, 0, 200, 0, 0);
#else
	set_bit(ABS_X, input_dev->absbit);
	set_bit(ABS_Y, input_dev->absbit);
	//set_bit(ABS_PRESSURE, input_dev->absbit);
	input_dev->keybit[BIT_WORD(BTN_TOUCH)] = BIT_MASK(BTN_TOUCH);

	input_set_abs_params(input_dev, ABS_X, 0, SCREEN_MAX_X, 0, 0);
	input_set_abs_params(input_dev, ABS_Y, 0, SCREEN_MAX_Y, 0, 0);
	//input_set_abs_params(input_dev, ABS_PRESSURE, 0, PRESS_MAX, 0 , 0);
#endif

	set_bit(EV_ABS, input_dev->evbit);
	set_bit(EV_KEY, input_dev->evbit);

	input_dev->name		= FT5X26_NAME;
	err = input_register_device(input_dev);
	if (err) 
    {
	    input_free_device(input_dev);
		printk("%s: failed to register input device: %s\n", 
                                            __func__, FT5X26_NAME);
	}
    return err;
}

static int 
ft5x26_ts_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct ft5x26_ts_data *ft5x26_ts;
	int err = 0;
    unsigned int immr_base;
	unsigned char uc_reg_value; 
	
	printk("== %s addr: 0x%X IRQ: %d\n", __func__, client->addr, client->irq);
	
	if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C)) 
    {
		err = -ENODEV;
        printk(KERN_ERR "%s: i2c check failed\n", FT5X26_NAME);
		goto exit_check_functionality_failed;
	}

	this_client = client;

    // make this call ONLY after setting this_client.
    // see if the device is present
	if (ft5x26_read_reg(FT5X26_REG_CHIPID, &uc_reg_value) < 0)
    {
        err = -ENODEV;
        goto exit_check_functionality_failed;
    }

	ft5x26_ts = kzalloc(sizeof(*ft5x26_ts), GFP_KERNEL);
	if (!ft5x26_ts)	
    {
		err = -ENOMEM;
        printk(KERN_ERR "%s: mem-alloc failed\n", FT5X26_NAME);
		goto exit_alloc_data_failed;
	}

	i2c_set_clientdata(client, ft5x26_ts);

    if ((err = ft_register_input(ft5x26_ts)) < 0)
        goto exit_error_devfunc;

    if ((err = ft_drv_setup(ft5x26_ts)) != 0)
        goto exit_error_devfunc;

    // map gpio registers
    immr_base = get_immrbase();
    gpio = (gpio_regs *)ioremap((immr_base + GPIO0_OFFSET), 0x80);
    printk("IMMR 0x%X gpio 0x%X\n", immr_base, (u32)gpio);
#ifdef ORIG_CODE
    // map FPGA registers
    reset_ptr = (unsigned char *)ioremap(FPGA_REG_ADDR, 0x10);
    printk("FPGA Regs: 0x%X\n", (u32)reset_ptr);
#endif
    /* make sure gpio1 is set to input */
    clrbits32(&gpio->gpDIR, 0x40000000);

    /* set bit 30 to enable interrupts on GPIO1 */
    setbits32(&gpio->gpIMR, 0x40000000);

    /* set bits 29-28 to 01 to enable interrupt gen. on 
       high to low change on GPIO1 */
#ifdef ORIG_CODE
    setbits32(&gpio->gpICR1, 0x20000000);
#else
    setbits32(&gpio->gpICR1, 0x10000000);
#endif
#ifdef ORIG_CODE
    // reset touch controller
    ft5x26_reset();
    ft5x26_wait_tp_valid();
#endif
    //get some register information
	ft5x26_read_reg(FT5X26_REG_CHIPID, &uc_reg_value);
    printk("[FST] Chip Id = 0x%x\n", uc_reg_value);
	ft5x26_read_reg(FT5X26_REG_FIRMID, &uc_reg_value);
    printk("[FST] Firmware version = 0x%x\n", uc_reg_value);

    init_MUTEX(&sem_dev);

    // interrrupt stuff
    err = request_irq(this_client->irq, ft5x26_ts_interrupt, IRQF_SHARED,
                                ft5x26_ts->input_dev->name, ft5x26_ts);
    if (err < 0)
    {
        printk(KERN_ERR "Error request irq %d: %d\n", this_client->irq, err);
        err = -ENODEV;
        goto exit_irq_request_failed;
    }
#ifdef ORIG_CODE
    next_reset_jiffy = last_intr_jiffy = jiffies;
    // get hang timer going after 15 secs
    ft5x26_ts->dev_check_timer.expires = jiffies + msecs_to_jiffies(15000);
    add_timer(&ft5x26_ts->dev_check_timer);
#endif
	printk("== probe done =\n");
    return 0;

exit_irq_request_failed:
	cancel_work_sync(&ft5x26_ts->touch_event_work);
	destroy_workqueue(ft5x26_ts->ts_workqueue);
exit_error_devfunc:
	i2c_set_clientdata(client, NULL);
	kfree(ft5x26_ts);
exit_alloc_data_failed:
exit_check_functionality_failed:
	return err;
}

static int __devexit ft5x26_ts_remove(struct i2c_client *client)
{
	struct ft5x26_ts_data *ft5x26_ts = i2c_get_clientdata(client);
	printk("==ft5x26_ts_remove=\n");
#ifdef CONFIG_HAS_EARLYSUSPEND
	unregister_early_suspend(&ft5x26_ts->early_suspend);
#endif
	free_irq(client->irq, ft5x26_ts);
	input_unregister_device(ft5x26_ts->input_dev);
	kfree(ft5x26_ts);
	cancel_work_sync(&ft5x26_ts->touch_event_work);
	destroy_workqueue(ft5x26_ts->ts_workqueue);
	//i2c_set_clientdata(client, NULL);
	return 0;
}

#define FT_DEV_ADDR    (0x38)

static const struct i2c_device_id ft5x26_ts_id[] = {
	{ FT5X26_NAME, FT_DEV_ADDR },
    {},
};

MODULE_DEVICE_TABLE(i2c, ft5x26_ts_id);

static unsigned short addr_list[] = { FT_DEV_ADDR, I2C_CLIENT_END };

static const struct i2c_client_address_data addr_data = { \
    .normal_i2c = addr_list,    \
    .probe      = 0,            \
    .ignore     = 0,            \
    .forces     = 0,            \
};

static struct i2c_driver ft5x26_ts_driver = {
	.probe		  = ft5x26_ts_probe,
	.remove		  = __devexit_p(ft5x26_ts_remove),
	.id_table	  = ft5x26_ts_id,
    .address_data = &addr_data,
	.driver	= {
		.name	= FT5X26_DRV,
		.owner	= THIS_MODULE,
	},
};

static int __init ft5x26_ts_init(void)
{
    int ret;
	printk("==ft5x26_ts_init==\n");

    ret = i2c_add_driver(&ft5x26_ts_driver);
    if (ret < 0)
    {
        printk(KERN_ERR "driver %s, can't add i2c driver:%d\n",FT5X26_NAME,ret);
        return -ENODEV;
    }
    return 0;
}

static void __exit ft5x26_ts_exit(void)
{
	printk("==ft5x26_ts_exit==\n");
	i2c_unregister_device(this_client);
}

module_init(ft5x26_ts_init);
module_exit(ft5x26_ts_exit);

//MODULE_AUTHOR("<wenfs@Focaltech-systems.com>");
MODULE_DESCRIPTION("FocalTech ft5x26 TouchScreen driver");
MODULE_LICENSE("GPL");
