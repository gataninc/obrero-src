#ifndef __UPGRADE_HDR_H
#define __UPGRADE_HDR_H

// personality version info
#define PDEFS_VER_FILE "/mnt/nv/pdefsver.txt"

/*
 * Upgrade header:
 * unsigned int chkSum        - checksum of all bytes excluding checksum bytes
 * char[]  magicStr           - uniq string to identify upgrade image
 * unsigned int systemId      - indicates image for pips/pecs/ilian etc
 * unsigned short majorNo     - build version
 * unsigned short minorNo     - build version
 * unsigned short patchLevel  - build version
 * unsigned short buildNo      - build version
#endif
 * unsigned int bldType       - alpha, beta, release, special etc??
 * gatanDate bldDate          - build date
 */
#include <stdint.h>

#define MAGIC_STR         "S@mplePrep_Upgr@de_Im@ge"
#define MAGIC_STR_SS2     "Gatan_Sm@rtSet2_Up_Im@ge"
#define MAGIC_LEN     24

typedef struct
{
    uint16_t year;     // actual year, not an offset
    uint8_t  month;    // month: range 0 - 11
    uint8_t  day;      // day of month: range 1 - 31
    uint8_t  hour;     // hour: range 0 - 23
    uint8_t  min;      // minute: range 0 - 59
    uint8_t sec;       // second: range 0 - 59
    uint8_t pad;       // padding to make size multiple of 4
} gatanDate;

enum
{
    BuildInvalid = 0,
    BuildAlpha,
    BuildBeta,
    BuildRelease,
    MAX_BUILD_TYPES
    // new build types go above MAX_BUILD_TYPES
} enumBuildTypes;

#define IMAGE_SYS_TYPE    0xAA // system/kernel image
#define IMAGE_APP_TYPE    0x55 // applications image(GUI)

// The define below is ORed with the above #defines to make complete system ID
// Byte 0 defines image type and remaining bytes define the target - smartset2
#define IMAGE_PIPS_TYPE        0x00001F00 // used for PIPS/PECS
#define IMAGE_SMARTSET2_TYPE   0x00002D00 // Obrero

typedef struct 
{
    uint32_t     ckSum;
    char       magicStr[MAGIC_LEN];
    uint32_t     sysId;     // indicates system + type of image(system or apps)
    uint16_t     majorNo;
    uint16_t     minorNo;
    uint16_t     patchLevel;
    uint16_t     buildNo;
    uint32_t     bldType;
    gatanDate  bldDate;
} UpgradeImgHdr;
#endif // __UPGRADE_HDR_H
