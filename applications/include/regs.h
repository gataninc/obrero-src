#ifndef __REGS_H
#define __REGS_H

#ifdef __FPGA_H
int readMachStatRegs(bingMachStatus *regs, int offset);
int readMachCtrlRegs(bingMachineCtrl *regs, int offset);
int updateMachCtrlRegs(bingMachineCtrl *regs, int offset);
#endif
void updateSync(int syncType);
int setCtrlRegBits(int offset, uint16_t mask, uint16_t value);
int writeCtrlReg(int offset, int size, void *value);
int writeCtrlRegFull(int offset, int size, uint16_t *value);
int writeCtrlRegStrobe(int offset, int size, void *value, int sReg, int sBit);
// read low bytes of control register(uint8_t)
int readCtrlRegL(int offset, int size, void *value);
// read whole control register(uint16_t)
int readCtrlReg(int offset, int size, uint16_t *value);
int readStatReg(int offset, int size, void *value);
int readStatRegFull(int offset, int size, uint16_t *bytes);
int readStatRegNoSwap(int offset, int size, uint16_t *bytes);
int libCheckRxUARTEmpty(int uart);
int libCheckTxUARTFull(int uart);
int libWriteToUART(int uartNo, uint8_t *str, int len, uint32_t delay);
int libReadFromUART(int uartNo, uint32_t delay, char *rdStr, int len);
int initLib();

#endif // __REGS_H
