/*
   fpga configuration for Bing Controller.  Based on Ciro's fpgaload for K2 series..
 */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <time.h>

#include <mpc5125_gpio.h>

#define INITIAL_PULSE  4000               /* nano-seconds */
#define SLEEP_TIME     20000000           /* nano-seconds, 20ms */
#define WAIT_CYCLES    25                 /* 25 * SLEEP_TIME ~ 500 ms */

#define GPIO_OUT_FPGA_NCFG    14   
#define GPIO_IN_FPGA_NSTAT    11
#define GPIO_IN_FPGA_CFGDONE  10

#define FPGA_DOWNLOAD_BASE    0xE4000000
#define FPGA_DOWNLOAD_SIZE    (64*1024)

static inline void set_bit(int hDev, int reg, int bit)
{
    if (ioctl(hDev, GPIO_IOCTL(GPIO_SET, reg), bit) < 0)
        perror("gpio SET failed");
}

static inline void clear_bit(int hDev, int reg, int bit)
{
    if (ioctl(hDev, GPIO_IOCTL(GPIO_CLEAR, reg), bit) < 0)
        perror("gpio CLEAR failed");
}


static inline int test_bit(int hDev, int reg, int bit)
{
    uint32_t val;
    if (ioctl(hDev, GPIO_IOCTL(GPIO_GET, reg), &val) < 0)
        perror("gpio GET failed");
    return GPIO_TEST(val, bit);
}

static uint8_t reverseBits(uint8_t byte)
{
    uint8_t v = byte;
    uint8_t r = v;
    uint32_t s = sizeof(v) * CHAR_BIT - 1; /* extra shift needed at end */
    for (v >>= 1; v; v >>= 1)
    {
        r <<= 1;
        r |= v & 1;
        s--;
    }
    r <<= s;
    return r;
}

int main(int argc, char *argv[])
{
    int hDev, hFile, hMem;
    uint32_t dummyArg = 0;
    int ret = 0;
    struct timespec tSleep;
    int nCycles;
    off_t fSz;
    uint8_t *fpgaBuf;
    volatile void *pFPGA;

    /* verify arguments first */
    if (argc != 2)
    {
        fprintf(stderr, "Usage: %s <FPGA bitstream file>\n", argv[0]);
        exit(1);
    }

    if ((hFile = open(argv[1], O_RDONLY)) == -1)
    {
        perror("Error, open fpga bitstream");
        exit(1);
    }

    fSz = lseek(hFile, 0, SEEK_END);
    if (fSz == (off_t)-1)
    {
        perror("Error, seek fpga bitstream");
        close(hFile);
        exit(1);
    }
    lseek(hFile, 0, SEEK_SET);

    if ((fpgaBuf = (uint8_t *)malloc(fSz)) == 0)
    {
        fprintf(stderr, "Memory allocation failure\n");
        close(hFile);
        exit(1);
    }
    if (read(hFile, fpgaBuf, fSz) != fSz)
    {
        perror("Error, read fpga bitstream");
        close(hFile);
        exit(1);
    }
    close(hFile);

    /* get access to memory */
    if ((hMem = open("/dev/mem", O_RDWR)) == -1)
    {
        perror("Error, open dev mem");
        free(fpgaBuf);
        exit(1);
    }
    if ((pFPGA = mmap(0, FPGA_DOWNLOAD_SIZE, PROT_READ|PROT_WRITE,
                            MAP_SHARED, hMem, FPGA_DOWNLOAD_BASE)) == MAP_FAILED)
    {
        perror("Error, memmap");
        close(hMem);
        free(fpgaBuf);
        exit(1);
    }
    if ((hDev = open("/dev/fpga_ctrl0", O_RDWR)) == -1)
    {
        perror("Error, open fpga device");
        munmap((void *)pFPGA, FPGA_DOWNLOAD_SIZE);
        close(hMem);
        free(fpgaBuf);
        exit(1);
    }
#if 0
/* DEBUG */
    ioctl(hDev, FPGA_LOAD, dummyArg);
    sleep(1);
#endif
    /* prepare for FPGA loading */
    /* setup nConfig(gpio 14) */
    clear_bit(hDev, GPDAT, GPIO_OUT_FPGA_NCFG); /* initial value */
    clear_bit(hDev, GPODR, GPIO_OUT_FPGA_NCFG); /* not open drain, actively driven */
    set_bit(hDev, GPDIR, GPIO_OUT_FPGA_NCFG);   /* configure as o/p */

    /* configure nStatus(gpio11) as input */
    clear_bit(hDev, GPDIR, GPIO_IN_FPGA_NSTAT);   /* configure as i/p */

    /* configure cfgDone(gpio10) as input */
    clear_bit(hDev, GPDIR, GPIO_IN_FPGA_CFGDONE);

    /* make sure we pulse nConfig low, long enough */
    tSleep.tv_sec = 0;
    tSleep.tv_nsec = INITIAL_PULSE;
    nanosleep(&tSleep, 0);
/* DEBUG */
    if (test_bit(hDev, GPDAT, GPIO_IN_FPGA_NSTAT))
        fprintf(stderr,"Error: nStat is already high???\n");

    /*  raise nConfig */
    set_bit(hDev, GPDAT, GPIO_OUT_FPGA_NCFG); /* initial value */

    /* now FPGA has to respond with nSTAT going high */
    for (nCycles = 0, tSleep.tv_sec = 0, tSleep.tv_nsec = SLEEP_TIME;
                (nCycles < WAIT_CYCLES) && (!test_bit(hDev,GPDAT, GPIO_IN_FPGA_NSTAT));
                ++nCycles)
        nanosleep(&tSleep, 0);
    if (!test_bit(hDev, GPDAT, GPIO_IN_FPGA_NSTAT))
    {
        fprintf(stderr,"Error: FPGA not responding (nStat == 0)\n");
        goto errOut;
    }
    printf("Downloading bitstream..\n"); 
    /* copy fpga bit stream */
    for (nCycles = 0; nCycles < fSz; nCycles++)
    {
        /**(volatile unsigned char *)pFPGA = fpgaBuf[nCycles];*/
        *(volatile unsigned char *)pFPGA = reverseBits(fpgaBuf[nCycles]);
    }
    /* write two dummy bytes to generate two falling DCLK edges after CONF_DONE */
    *(volatile unsigned char *)pFPGA = 0x1;
    *(volatile unsigned char *)pFPGA = 0x2;

    /* clean-up */
    free(fpgaBuf);
    munmap((void *)pFPGA, FPGA_DOWNLOAD_SIZE);
    close(hMem);
    /* now FPGA has to respond with CFG_DONE */
    for (nCycles = 0, tSleep.tv_sec = 0, tSleep.tv_nsec = SLEEP_TIME;
                (nCycles < WAIT_CYCLES) && (!test_bit(hDev,GPDAT, GPIO_IN_FPGA_CFGDONE));
                ++nCycles)
        nanosleep(&tSleep, 0); 
    if (!test_bit(hDev, GPDAT, GPIO_IN_FPGA_CFGDONE))
    {
        fprintf(stderr, "Error: FPGA did not acknowledge bitstream(CFGDONE = 0)\n");
        ret = 1;
    }
    close(hDev);
    exit(ret);
errOut:
    free(fpgaBuf);
    munmap((void *)pFPGA, FPGA_DOWNLOAD_SIZE);
    close(hMem);
    close(hDev);
    exit(1);
}
