#!/bin/bash

swVerFile=local/etc/sw_version.txt
if [ $# -eq 1 ]; then
   echo "Setting sw_version to $1"
   echo "$1" >${swVerFile}
   sw_version="$1"
else
   sw_version=$(cat ${swVerFile})
fi

tStampFile=local/etc/bld_date.txt
#echo -n "Build Time: " >${tStampFile};
date >${tStampFile}

echo "SWVersion $sw_version"
KERN_VER=2.6.29

#JFFS_VERSION=0
#JFFS_ERASE=0x4000
#
#export GZIP="-9"
#export PATH=$PATH:/opt/freescale/ltib/usr/bin

# build kernel
cd kernel/linux
make ARCH=powerpc 
cd -

if [ -d applications ]; then
# create some directories
    if [ ! -d applications/lib ]; then
        mkdir applications/lib
    fi
    if [ ! -d applications/bin ]; then
        mkdir applications/bin
    fi
    if [ ! -d applications/modules ]; then
        mkdir applications/modules
    fi

# ltib processes files found in the top level directory named 'merge'
# so prepare 'merge' directory which holds our modules and apps
    if [ -d merge ] ; then
        rm -rf merge
    fi
    mkdir -p merge/bin
    mkdir -p merge/lib/modules/${KERN_VER}/kernel/
    mkdir -p merge/mnt/nv
    mkdir -p merge/usr/sbin merge/usr/bin

    (cd local; cp -R * ../merge/;)
    find merge -name CVS -exec rm -rf {} \; > /dev/null 2>&1

cp applications/dropbear/dropbear-0.53.1/dropbearmulti merge/usr/sbin
(cd merge/usr/sbin; ln -sf dropbearmulti dropbear)
(cd merge/usr/bin; for f in dbclient dropbearkey dropbearconvert scp; do ln -sf ../sbin/dropbearmulti $f; done)

    cp kernel/linux/drivers/rtc/rtc-m41t80.ko merge/lib/modules/${KERN_VER}/kernel
    abort=0
    curdir=$(pwd)
    cd applications; 
    make clean; make 
    cp bin/* ../merge/bin ; 
    cp modules/* ../merge/lib/modules/${KERN_VER}/kernel; 
    cd ../merge/bin; 
    ln -sf fw_printenv fw_setenv; 
    ln -sf diagshell dsh
    cd ${curdir}

    if [ $abort -eq 1 ]; then
        echo; echo "Failed to build applications. Abort"; echo
        exit 1
    fi
fi

./ltib --deploy $*

#tstamp=$(/bin/date  "+%m%d%y_%k%M")
tstamp=$(/bin/date  "+%m%d%y")

# update dts image
dtc -f -b 0 -I dts -O dtb -R 8 -S 0x3000 -o dts/Obrero.dtb dts/mpc5125-obrero.dts

# build final image
imageFile=Obrero.${sw_version}.img
rm -f ${imageFile}; sync;sync

mkimage -A ppc -O Linux -T multi -C gzip -n "Gatan Bing Controller RT Image" -d kernel/linux/vmlinux.bin.gz:rootfs.ext2.gz:dts/Obrero.dtb ${imageFile} ; sync;sync

echo; echo "==== Building Upgrade image.."
imgPgm=applications/upgrade/mkupgimg
imgDir=$(pwd)
# create upgrade dir if it does not exist
upgDir=./upgrade
upgFile=${upgDir}/1905_sys_v${sw_version}.fwimg
if [ ! -d $upgDir ]; then
    mkdir $upgDir
    if [ $? -ne 0 ]; then
        echo "Error: Failed to create upgrade directory!"
        exit
    fi
fi
# build image
${imgPgm} -r ${sw_version}.0 -b beta -i SYSTEM -o ${upgFile} ${imageFile}
if [ ! -f ${upgFile} ]; then
    echo "Error: Failed to create upgrade archive"
    exit
fi
echo "==== Done. Upgrade image is ${upgFile}"

exit 0
