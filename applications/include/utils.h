#ifndef __UTILS_H
#define __UTILS_H

#include <stdint.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>

char *get_i2c_bus(int idx);
char *cvtToIPStr(unsigned int ipVal);
int cvtToUInt(char *word, uint32_t *value);
int cvtToFloat(char *fltStr, float *fltVal);
int cvtToDouble(char *fStr, double *dblVal);
int convToInt(char *str, int *val);
void mySleep(uint32_t ms);
#ifdef _SYS_TIME_H
unsigned int diffTimeVal(struct timeval *last, struct timeval *first);
#endif
int getStringFromFile(char *inFile, char *string, int sLen);
long long getTimeInMs();
long long getTimeElapsed(long long last);
/* system V semaphore primitives */
int sysVsemGet(key_t key, int create);
int sysVsemWait(int semId, int noWait);
int sysVsemTimedWait(int semId, unsigned int ms);
int sysVsemPost(int semId);
int sysVsemDel(int semId);
/* shared memory primitives */
int printSharedMemInfo(key_t key);
void *allocSharedMemory(key_t key, int memSize);
void *getSharedMemory(key_t key, int memSize);
int delSharedMemory(key_t key);
#ifdef __pid_t_defined
pid_t *sysPIDOf(char *cmdName);
#endif
// locking stuff
int lockSharedMem();
int unlockSharedMem();
int lockCtrlReg();
int unlockCtrlReg();
int lockI2CBus(int busNo);
int unlockI2CBus(int busNo);
int lockMCP3424();
int unlockMCP3424();
int getSysUpTime();
char *stripWhiteSpace(char *input);

#endif //__UTILS_H
