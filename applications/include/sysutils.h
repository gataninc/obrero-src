#ifndef __SYSUTILS_H_
#define __SYSUTILS_H_
#include <stdint.h>

enum 
{ 
    STIM_0uA = 0, 
    // +ve stimulus
    STIM_1uA, 
    STIM_10uA, 
    STIM_100uA, 
    // -ve stimulus
    STIM_1uA_N, 
    STIM_10uA_N, 
    STIM_100uA_N,
};

enum { GAIN_X1, GAIN_X21, GAIN_X10, GAIN_X210 };

enum 
{ 
    DRTD_CAL_NORM,  // Normal, external diode/RTD
    DRTD_CAL_R100,  // 100 Ohm(0.01%) precision Resistor
    DRTD_CAL_R1K,   // 1 KOhm(0.01%) precision Resistor
    DRTD_CAL_ZERO   // input zeroed
};

enum 
{ 
    DRTD_CAL2_NORM,    // select normal signal
    DRTD_CAL2_CAL,     // select precision 0-2.5V square wave
};

// AC mains sync
#define SAMPLES_50HZ       (2000)
#define SAMPLES_60HZ       (1666)
#define HZ50_10PCT         (200)
#define HZ60_10PCT         (167)
#define MIN_SAMPLES_60HZ   (SAMPLES_60HZ - HZ60_10PCT)
#define MAX_SAMPLES_60HZ   (SAMPLES_60HZ + HZ60_10PCT)
#define MIN_SAMPLES_50HZ   (SAMPLES_50HZ - HZ50_10PCT)
#define MAX_SAMPLES_50HZ   (SAMPLES_50HZ + HZ50_10PCT)

double scaleTADCVal(int chan, int32_t adcVal);
double scaleTADCVal2p5V(int32_t adcVal);
void computeTADCVoltage(uint32_t rVal, double *fVal);
int findSensorType(int sensor);
int setVoltageRange(int vMax);
int setIRange(int range);
int setGroundingMode(int gndMode);
float getMaxVRange();
float getMaxVRangeQuick(uint8_t regVal);
float getHeaterADCRange();
float getHeaterDACRangeQuick(uint8_t regVal);
float getHeaterDACRange();
int getHeaterVoltage(double *V, uint32_t *rawV);
int getHeaterCurrent(double *I, uint32_t *rawI);
int readT_ADCValues(int chan, int count, int32_t *regVals);
int setDRTDCalSel2(int sel);
int setDRTDCalSel1(int sel);
int setDRTDGain(int sel);
int setStimulusCurrent(int sel, int setV);
double getStimulusI(int stimI);
int getHeaterValues(float *V, uint32_t *rawV, float *I, uint32_t *rawI);
int setHeaterDAC(float v);
int setHeaterDACRegVal(uint16_t regVal);
int getAdcFpgaAvg(int sIdx, int32_t *avg);
int getAvgTADCVolt(int chan, int count, int scale2p5V, int filter, double *V);
//int getAvgTADCVolt(int chan, int count, int scale2p5V, double *V);
int getHeaterAvg(int count, double *avgV);
int getHeaterAvgRaw(int count, uint16_t *avgR);
int32_t computeAccADCAvg(int64_t sumADC, int numSamples);
int getAvgTADCFpgaVoltage(int chan, int numMainsCycles, int scale2p5V, double *V);
int readFromUART(int uartNo, uint32_t delay, char *rdStr);

#endif // __SYSUTILS_H_
