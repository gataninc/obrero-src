#define _XOPEN_SOURCE 500
#define _GNU_SOURCE
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <math.h>
#include <byteswap.h>
#include <syslog.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/statvfs.h>
#include <ftw.h>
#include "readline.h"
#include "history.h"
#include "diagshell.h"
#include "bing_common.h"
#include "sharedmem.h"
#include "fpga.h"
#include "utils.h"
#include "i2cutils.h"
#include "diode_calibrate.h"
#include "tc_calibrate.h"
#include "sysutils.h"
#include "caldata.h"
#include "sys_ini.h"

// from calfuncs.c
int doCalFunc(int dongleId, char *IPStr, int skips, int pause);

static char errLockMsg[] = "Error: failed to acquire %s lock\n";
volatile sysParams *pCache;

#define dbgPrint(format, ...) do { if (pCache->debug) {fprintf(stderr, format, ##__VA_ARGS__);fflush(stderr);} } while (0)

static char helpShow[] = \
"Show details of various sub-systems. Available options are:\n"\
"show logs                 - show system logs\n"\
"show interface            - show network interfaces\n"\
"show tasks                - show all processes in the system\n"\
"show top                  - show/monitor cpu usage of all tasks\n"\
"show memory               - display various memory statistics(host)\n";

static char helpFPGAConfig[]  = \
"Configure FPGA\n"\
"Usage: fpgaload <filename>\n";

static char helpMacAddr[] = \
"Update mac address in eeprom for 1G and 10G ethernet ports of control baord\n"\
"Usage: macaddr <MAC_addr>\n"\
"   mac addr is of the format xx:xx:xx:xx:xx:xx\n"\
"   if <MAC_addr> not specified, displays current mac address\n";

static char helpPrintMac[] = \
"Print machine status registers\n";

static char helpPrintMacCtrl[] = \
"Print machine control registers\n"\
"Usage: pcntl\n";

static char helpPrintSyncBits[] = \
"Print sync bit register\n"\
"Usage: psync\n";

static char helpWriteSyncBits[] = \
"Update sync bit register\n"\
"Usage: wsync [reg|lookup]\n";

static char helpResetCmds[] = \
"reset sections of the system\n"\
"Usage: reset [ startmcode | stopmcode | mcode | chaddr | uart ]\n"\
"startmcode  - start micro code execution\n"\
"stopmcode   - stop micro code execution\n"\
"mcode       - reset micro code pgm counter\n"\
"chaddr      - reset channel address counter\n"\
"uart        - reset UARTs\n";

static char helpLoadFile[] = \
"Load machine control, lookup table, FPGA micro-code etc \n"\
"Usage: loadfile <input file>\n";

static char helpTempCmd[] = \
"Print board temperatures\n"\
"Usage: temp\n";

static char helpMfgData[] = \
"read/udpate manufacturing data\n"\
"Usage:\n"\
"  mfgdata write-pca  <part no> <rev> <eng. rev> <serial>\n"\
"  mfgdata write-assm <part no> <rev> <eng. rev> <serial>\n"\
"  mfgdata write-prod <name> <model> <serial no> [company name]\n"\
"      company name is optional, uses \"Gatan Inc\" if not specified\n"\
"  mfgdata read\n";

static char helpDbgPrint[] = \
"print contents of debug channel\n"\
"Usage: pdbg <size in bytes>\n";

static char helpPeekCmd[] = \
"Print content of a machine control register\n"\
"Usage: peek <reg offset> [ num regs ]\n";

static char helpPokeCmd[] = \
"Write value to a machine control register\n"\
"Usage: poke <reg offset> <value>\n";

static char helpClkGen[] = \
"clkgen config function\n"\
"Usage: clkgen config [filename]\n"\
"       clkgen output [reg_val]\n"\
"\tclkgen config : program clkgen with default config\n"\
"\tclkgen config <filename> : program clkgen with timing specified in file\n";

static char helpSetCmd[] = \
"set various parameters, device registers etc\n"\
"Usage: set <h-dac|trimdac> <chan|reg> <value>\n"\
"       set vrange <40|20|10|0>\n"\
"       set ground <floating|socket|mains|buffered>\n"\
"       set diode  <dt470 | dt670>\n"\
"       set sensor <dt470 | dt670 | type_r | type_k | type_s>\n"\
"       set vmax <vMax voltage>\n"\
"       set imax <iMax current>\n"\
"       set pmax <pMax power>\n"\
"       set auxdac <1|2> <voltage>, range 0-12V\n"\
"       set relay <1|2|both> <on|close|off|open>\n"\
"       set stimulus < 0 | 1 | 10 | 100 | -1 | -10 | -100 >\n"\
"  h-dac: Heater DAC, range: 0-40, depending on range setting\n"\
"  trimdac: Diode/RTD current trim DAC, range: 0 - 2.0\n"\
"  vrange: set heating voltage range -\n"\
"          40: 0-40, 20: 0-20, 10: 0-10, 0: hi impedance\n";

static char helpGetCmd[] = \
"Read device registers, print current settings etc\n" \
"Usage: get <h-adc|trimdac> [chan|reg]\n"\
"       get vrange\n"\
"       get ground\n"\
"       get diode\n"\
"       get sensor\n"\
"       get stats\n"\
"       get cjcdata\n"\
"       get preamp\n"\
"       get maxvals\n"\
"       get auxdac <1|2>\n"\
"       get relay\n"\
"        get t-adc [ 1 | 2 | 3 ]\n"\
"  t-adc: ADS1274\n"\
"       With no argument, prints temperature info for current sensor channel\n"\
"       1 - TC channel, 2 - DRTD channel, 3 - pre-amp channel\n"\
"  h-adc: Heater ADC voltage and current\n"\
"  trimdac: Diode/RTD current trim DAC\n"\
"    stats: Print temperature, voltage and current values\n"\
"  cjcdata: Print CJC voltage\n"\
"   preamp: Print pre-amp input voltage\n";

// The delay parameter is 60nS / count, range 1 - 0xFFFFFF
// 0xFFFFFF: results in 1 update per 1.023 seconds.
static char helpPIDCmd[] = \
"Set PID parameters\n"\
"Usage: pid < setpt | kp | ki | kd > < value >\n"\
"       pid < enable | disable > or < on | off >\n"\
"       pid < dbg >, print PID run time information\n"\
"  Just the command without arguments prints the current settings\n";

static char helpBright[] = \
"Set brightness of LCD display\n"\
"Usage: bright <0 - 10>\n";

static char helpCJCRAMTest[] = \
"Write different bit patterns to CJC RAM, read back and verify\n"\
"Usage: cjcramtest\n";

static char helpBakeCmd[] = \
"Enable/disable bake feature\n"\
"Usage: bake [ on | off ]\n"\
"   Just the command prints the current bake status\n";

static char helpChillerCmd[] = \
"Enable/disable chiller\n"\
"Usage: chiller [ on | off ]\n"\
"   Just the command prints the current status\n";

static char helpHelp[] = "Display command summary\n";

static char helpCalData[] = \
"Load/print calibration data table for FPGA\n"\
"Usage: caldata load [ dt470 | dt670 ]\n"\
"       caldata print [ loc ] [ count ], loc range 0-2047.\n";

static char helpCalTemp[] = \
"Print calibrated temperature, test routine for PID loop\n"\
"Usage: caltemp <adc_val>  <count>  <incr>\n"\
"         prints 'count' entries starting at 'adc_val' in interval of  'incr'"\
"       caltemp <adc_val>, just prints one value at adc_val"\
"       just the command prints the corrected temperature bytes from FPGA\n";

static char helpTADCAvg[] = \
"Print average of a number of readings from t-ADC\n"\
"Usage: adcavg [count:1-3000] [channel:1-2]\n"\
"   defaults: count - 100, channel - 2[diode/RTD]\n";

static char helpTblPrt[] = \
"tblprt: Print temperature look up for the specified type and range\n"\
"Usage: tblprt <sensor type> <start val> <end endVal> <incr>\n\n"\
"   dt470   : values in mV, valid range: 90.62   - 1698.12\n"\
"   dt670   : values in mV, valid range: 90.68   - 1644.29\n"\
"   type_k  : values in mV, valid range: -3.554  - 69.553\n"\
"   type_t  : values in mV, valid range: -4.68   - 20.872\n"\
"   type_r  : values in mV, valid range: -0.226  - 21.101\n"\
"   prt     : values in Ohms, valid range: 18.52 - 390.48\n";

static char helpRelayCmd[] = \
"Control relay 1 and relay 2\n"\
"Usage: relay <1|2|both> <on|off>\n"\
"   default state off\n";

static char helpCalibrate[] = \
"Run calibration routines\n"\
"Usage: calibrate <dongleId IP_Addr> [skips] [-p]\n"\
"       -p : pause on error, wait for input before proceeding\n";


static char helpReadUART[] = \
"Read from rx fifo\n"\
"Usage: rfifo <4|5> [nByte]\n"\
"       rfifo <cjc|rs485> [nByte]\n"\
"Optional parameter nBytes is number of characters to read from UART\n";

static char helpHolderId[] = \
"Usage: holderid\n"\
"    Read holder identifier from cable eeprom\n";

static char helpHidden[] = "";

/* forward decl. */
shellCmd *findCommand(char *cmd);
static int doHelpCmd(int argc, char **argv);
static int doShowCmd(int, char **);
static int doMacAddr(int nargs, char **arg);
static int doFPGALoad(int nargs, char **arg);
static int doPrintMacStat(int nargs, char **arg);
static int doPrintMacCtrl(int nargs, char **arg);
static int doPrintSyncBits(int nargs, char **arg);
static int doWriteSyncBits(int nargs, char **arg);
static int doResetCmds(int nargs, char **arg);
static int doLoadFile(int nargs, char **arg);
static int doDumpRegs(int nargs, char **arg);
static int doTempCmd(int nargs, char **arg);
static int doMfgDataOps(int nargs, char **arg);
static int doPrintDbgCh(int nargs, char **arg);
static int doPeekReg(int nargs, char **arg);
static int doPokeReg(int nargs, char **arg);
static int doDbgCmd(int nargs, char **arg);
static int doMCP3424Cmd(int nargs, char **arg);
static int doMCP4726Cmd(int nargs, char **arg);
static int doClkGenCmd(int nargs, char **arg);
static int doSetCmd(int nargs, char **arg);
static int doGetCmd(int nargs, char **arg);
static int doLCDBrightCmd(int nargs, char **arg);
static int doPIDCmd(int nargs, char **arg);
static int doCJCRAMTest(int nargs, char **arg);
static int doCJCDataCmd(int nargs, char **arg);
static int doBakeCmd(int nargs, char **arg);
static int doDebugCmd(int nargs, char **arg);
static int doDiodeTable(int nargs, char **arg);
static int doCalDataCmd(int nargs, char **arg);
static int doCalTempCmd(int nargs, char **arg);
static int doChillerCmd(int nargs, char **arg);
static int doThermoCoupleTbl(int nargs, char **arg);
static int doRTDTable(int nargs, char **arg);
static int doTADCAvg(int nargs, char **arg);
static int doCJCTask(int nargs, char **arg);
static int doPrtLookup(int nargs, char **arg);
static int doFPGATempLookup(int nargs, char **arg);
static int doRelayCmd(int nargs, char **arg);
static int doCalibrateFunc(int nargs, char **arg);
static int doUpgradeCmd(int nargs, char **arg);
static int doPwrSeqCmd(int nargs, char **arg);
static int doLoadFactoryCal(int nargs, char **arg);
static int doProfileStatRegs(int nargs, char **arg);
static int doProfileTADC(int nargs, char **arg);
static int doProfileTADCNew(int nargs, char **arg);
static int doReadUART(int nargs, char *arg[]);
static int doHolderIdCmd(int nargs, char *arg[]);

static int doStatsCmd();
static void scaleTADC(uint32_t *regVals, double *fVals);
static int genPrtTblForFPGA(fpgaCalData *calData, int count);
static int loadCalData(int dType);
static int doMCP4726GetCmd(int nargs, char **arg);
static int doMCP4726SetCmd(int nargs, char **arg);
shellCmd commands[] = 
{
    { "help",          doHelpCmd,           helpHelp,           CMD_NORMAL },
    { "fpgaload",      doFPGALoad,          helpFPGAConfig,     CMD_NORMAL },
    { "pcntl",         doPrintMacCtrl,      helpPrintMacCtrl,   CMD_NORMAL },
    { "pstat",         doPrintMacStat,      helpPrintMac,       CMD_NORMAL },
    { "psync",         doPrintSyncBits,     helpPrintSyncBits,  CMD_NORMAL },
    { "wsync",         doWriteSyncBits,     helpWriteSyncBits,  CMD_NORMAL },
    { "reset",         doResetCmds,         helpResetCmds,      CMD_NORMAL },
    { "loadfile",      doLoadFile,          helpLoadFile,       CMD_NORMAL },
    { "show",          doShowCmd,           helpShow,           CMD_NORMAL },
    { "temp",          doTempCmd,           helpTempCmd,        CMD_NORMAL },
    { "macaddr",       doMacAddr,           helpMacAddr,        CMD_NORMAL },
    { "bright",        doLCDBrightCmd,      helpBright,         CMD_NORMAL },
    { "pid",           doPIDCmd,            helpPIDCmd,         CMD_NORMAL },
    { "cjcramtest",    doCJCRAMTest,        helpHidden,         CMD_NORMAL },
    { "bake",          doBakeCmd,           helpBakeCmd,        CMD_NORMAL },
    { "chiller",       doChillerCmd,        helpChillerCmd,     CMD_NORMAL },
    { "debug",         doDebugCmd,          helpHidden,         CMD_HIDDEN },
    { "mfgdata",       doMfgDataOps,        helpMfgData,        CMD_NORMAL },
    { "loadfactory",   doLoadFactoryCal,    helpHidden,         CMD_NORMAL },
    { "rfifo",         doReadUART,          helpReadUART,       CMD_NORMAL },
#if 0
    { "relay",         doRelayCmd,          helpRelayCmd,       CMD_NORMAL },
    { "pdbg",          doPrintDbgCh,        helpDbgPrint,       CMD_NORMAL },
    { "cjadc",         doMCP3424Cmd,        helpHidden,         CMD_NORMAL },
    { "auxdac",        doMCP4726Cmd,        helpHidden,         CMD_NORMAL },
#endif
    { "set",           doSetCmd,            helpSetCmd,         CMD_NORMAL },
    { "get",           doGetCmd,            helpGetCmd,         CMD_NORMAL },
    { "clkgen",        doClkGenCmd,         helpClkGen,         CMD_NORMAL },
    { "peek",          doPeekReg,           helpPeekCmd,        CMD_NORMAL },
    { "poke",          doPokeReg,           helpPokeCmd,        CMD_NORMAL },
    { "diodetbl",      doDiodeTable,        helpHidden,         CMD_HIDDEN },
    { "tctbl",         doThermoCoupleTbl,   helpHidden,         CMD_HIDDEN },
    { "rtdtbl",        doRTDTable,          helpHidden,         CMD_HIDDEN },
    { "caldata",       doCalDataCmd,        helpCalData,        CMD_NORMAL },
    { "caltemp",       doCalTempCmd,        helpCalTemp,        CMD_NORMAL },
    { "adcavg",        doTADCAvg,           helpTADCAvg,        CMD_NORMAL },
    { "cjctask",       doCJCTask,           helpHidden,         CMD_HIDDEN },
    { "tblprt",        doPrtLookup,         helpTblPrt,         CMD_NORMAL },
    { "fpgatemp",      doFPGATempLookup,    helpHidden,         CMD_HIDDEN },
    { "calibrate",     doCalibrateFunc,     helpCalibrate,      CMD_NORMAL },
    { "upgrade",       doUpgradeCmd,        helpHidden,         CMD_HIDDEN },
    { "pwrseq",        doPwrSeqCmd,         helpHidden,         CMD_HIDDEN },
    { "profsregs",     doProfileStatRegs,   helpHidden,         CMD_HIDDEN },
    { "proftadc",      doProfileTADCNew,    helpHidden,         CMD_HIDDEN },
    { "holderid",      doHolderIdCmd,       helpHolderId,       CMD_NORMAL },
    { "?",             doHelpCmd,           helpHelp,           CMD_HIDDEN },
    { (char *)0,       (pCmdFunc)0,         (char *)0,          CMD_NORMAL }
};

#define CLK_GEN_BUS  (2)
static char sysCmd[1024];
static char logfile1[] = "/var/log/messages";
static char logfile2[] = "/var/log/sys.log";
static char fpgaFile[] = "/dev/fpga0";
static int gInteractive = 1;
static volatile void *pFpgaMem = 0;
static volatile void *pFpgaUartBase = 0;
static int g_sigInt = 0;

void sigHandler(int sig)
{
    g_sigInt++;
}

static int doHelpCmd(int argc, char *argv[])
{
    int i, cmdCnt = 0;
    shellCmd *cmdPtr;
    if (argc == 0)
    {
        printf("Available commands are:\n  ");
        for (i = 0; commands[i].name; i++)
        {
            if (commands[i].flags == CMD_NORMAL)
            {
                printf("%s ", commands[i].name);
                cmdCnt++;
                if (cmdCnt == 8)
                {
                    printf("\n  ");
                    cmdCnt = 0;
                }
            }
        }
        printf("\n\nType help followed by command for more details on each command\n");
        printf("eg. \"help show\" displays usage of the command show\n\n");
        return 0;
    }
    if (argc == 1)
    {
        if ((cmdPtr = findCommand(argv[0])) != 0)
        {
            printf("%s: %s\n", argv[0], cmdPtr->doc);
        }
        else
            fprintf(stdout, "help: Unknown command\n");
    }
    else
        fprintf(stdout, "help: Unknown command\n");
    return 0;

}

static void printShMem(int v)
{
    int i;
    printf("Shared memory:\n");
    printf("  sensorType:\n");
    for (i = 0; i < NUM_SENSORS; i++)
        if (pCache->sensorType[i])
            printf("    %d. %d\n", (i+1), pCache->sensorType[i]);
    printf("  Vmax: %0.3f Imax: %0.3f Pmax: %0.3f\n",
                    pCache->vMax, pCache->iMax, pCache->pMax);
    printf("  MaxVRange  : %dV\n", pCache->maxVRange);
    printf("  Grounding  : %d\n", pCache->grounding);
    printf("  V          : %0.3f\n", pCache->setV);
    printf("\n");
    printf("  PID Enable : %d\n", pCache->pidEnable);
    printf("  Averaging  : %d\n", pCache->pidAvg);
    printf("  P / I / D  : %d / %d / %d\n", pCache->Kp, pCache->Ki, pCache->Kd);
    printf("  Set Pt     : %0.3f\n", (float)(pCache->setPt/1000.0));
    printf("\n");
    printf("  Bake Out   : %d\n", pCache->bakeOut);
    printf("  LCD Bright : %d\n", pCache->lcdBright);
    printf("  debug      : %d\n", pCache->debug);
    printf("\n");
    printf("  CJC Enable   : %d\n", pCache->enableCJC);
    printf("  CJC Idx      : %d\n", pCache->cjcIdx);
    printf("  CJC Factor   : %0.6f\n", pCache->CJCFact);
    printf("  CJC ADC Gain : %0.6f\n", pCache->CJCADCGain);
    printf("\n");
    printf("  PRT: vZero : %0.6f vStim: %0.6f rZero: %0.6f\n\n",
            pCache->vPrecZero, pCache->vPrecStim, pCache->vRtdZero);
    printf("  TC: vZero  : %0.6f vMiss: %0.6f   gTC: %0.6f\n\n",
            pCache->tcVZero, pCache->tcVMiss, pCache->tcGTC);
    printf("  DIODE: G1DRTD: %0.6f  vG1Zero : %0.6f\n\n",
            pCache->G1drtd, pCache->vG1Zero);
    printf("  Trim 100uA : %0.6f(p), %0.6f(n)\n",
            pCache->stimTrim100u_p, pCache->stimTrim100u_n);
    printf("  Trim 10uA : %0.6f(p), %0.6f(n)\n",
            pCache->stimTrim10u_p, pCache->stimTrim10u_n);
    printf("  Trim 1uA : %0.6f(p), %0.6f(n)\n\n",
            pCache->stimTrim1u_p, pCache->stimTrim1u_n);
    printf("  I Range    : %s(%d)\n", 
            ((pCache->heatIRange == HEAT_IRANGE_UNKNOWN) ? "Unknown" : 
                (pCache->heatIRange == HEAT_IRANGE_2A) ? "2A" : "200mA"),
            pCache->heatIRange);
    printf("  Stimuls I  : %0.6f : %0.6f\n", getStimulusI(pCache->stimI[0]),
                                             getStimulusI(pCache->stimI[1]));
    printf("  errFlags   : 0x%X\n", pCache->errFlags);
    if (v)
    {
        ;
    }
    printf("\n");
}

static int doShowCmd(int argc, char *argv[])
{
    struct stat sbuf;
    int doSysCmd = 1;
    int ret = 0;
    if (argc < 1)
        return 1;
    if (strcasecmp(argv[0], "logs") == 0)
    {
        if (stat(logfile1, &sbuf) == 0)
        {
            if (gInteractive)
                sprintf(sysCmd, "/usr/bin/less %s", logfile1);
            else
                sprintf(sysCmd, "/bin/cat %s", logfile1);
        }
        else if (stat(logfile2, &sbuf) == 0)
        {
            if (gInteractive)
                sprintf(sysCmd, "/usr/bin/less %s", logfile2);
            else
                sprintf(sysCmd, "/bin/cat %s", logfile2);
        }
        else
            fprintf(stdout, "Logfile not found\n");
    }
    else if (strcasecmp(argv[0], "interface") == 0)
        sprintf(sysCmd, "/sbin/ifconfig eth0");
    else if (strcasecmp(argv[0], "tasks") == 0)
    {
        sprintf(sysCmd, "/bin/ps"); /* list processes */
    }
    else if (strcasecmp(argv[0], "top") == 0)
    {
        if (gInteractive)
            sprintf(sysCmd, "/usr/bin/top -d 2"); 
        else
            sprintf(sysCmd, "/bin/ps"); 
    }
    else if (strcasecmp(argv[0], "memory") == 0)
    {
        sprintf(sysCmd, "PATH=/bin echo \"== Memory:\"; cat /proc/meminfo; "\
                "echo \"== IO Mem:\"; cat /proc/iomem; echo \"== IO Ports:\"; "\
                "cat /proc/ioports");
    }
    else if (strcmp(argv[0], "shmem") == 0)
    {
        doSysCmd = 0;
        if (argc > 1)
            printShMem(1);
        else
            printShMem(0);
    }
    else
    {
        fprintf(stdout, "Unknown command\n");
        return -1;
    }
    if (doSysCmd)
        return(system(sysCmd));
    return ret;
}

static int doFPGALoad(int nargs, char *arg[])
{
    char *fileName;
    struct stat sbuf;
    if (nargs != 1)
    {
        printf("%s", helpFPGAConfig);
        return -1;
    }
    //logPrint("FPGA File: %s\n", arg[0]);
    sprintf(sysCmd, "/bin/fpgaload %s", arg[0]);
    //logPrint("sysCmd : %s\n", sysCmd);
    return system(sysCmd);
}

static int mapFpgaMem(void)
{
    int hMem;
    int err1, err2;
    if ((hMem = open("/dev/mem", O_RDWR)) == -1)
    {
        perror("open /dev/mem");
        return -1;
    }
    errno = 0;
    pFpgaMem = mmap(0, FPGA_MEMORY_SIZE, PROT_READ|PROT_WRITE,
                    MAP_SHARED, hMem, FPGA_MEMORY_BASE);
    err1 = errno;
    errno = 0;
    pFpgaUartBase = mmap(0, FPGA_MEMORY_SIZE, PROT_READ|PROT_WRITE,
                    MAP_SHARED, hMem, FPGA_UART_BASE);
    err2 = errno;
    close(hMem);
    if (pFpgaMem == MAP_FAILED)
    {
        fprintf(stderr, "Error memmap regs, %s\n", strerror(err1));
        pFpgaMem = 0;
        return -1;
    }
    if (pFpgaUartBase == MAP_FAILED)
    {
        fprintf(stderr, "Error memmap uart base, %s\n", strerror(err2));
        pFpgaUartBase = 0;
        return -1;
    }
    //printf("Mapped FPGA registers\n");fflush(stdout);
    return 0;
}

#define PRT_BITS(adcReg, bitMask)  ((int)(adcReg.loByte & bitMask))
static void printBingMachCtrlRegs(bingMachineCtrl *machCtrlRegs)
{
    printf("Registers:\n");
}

#define MACH_CTRL_WORDS (sizeof(bingMachineCtrl)/sizeof(uint16_t))
#define MACH_CTRL_SIZE  (sizeof(bingMachineCtrl))
static int doPrintMacCtrl(int nargs, char **arg)
{
    volatile uint8_t *pMem;
    uint8_t machCtrlBuf[MACH_CTRL_SIZE];
    uint16_t *machCtrlPtr;
    bingMachineCtrl *machCtrlRegs;
    int i;
    pMem = (volatile uint8_t *)pFpgaMem;
    if (lockCtrlReg() != 0)
    {
        printf(errLockMsg, "ctrl reg");
        return -1;
    }
    RESET_CH_CTR(pMem);
    for (i = 0; i < MACH_CTRL_SIZE; i++)
        machCtrlBuf[i] = pMem[MACH_CTRL_CH];
    unlockCtrlReg();
    machCtrlPtr = (uint16_t *)machCtrlBuf;
    printf("  --> values displayed in big endian byte order <--\n");
    printf("%04d: ", 0);
    for (i = 0; i < MACH_CTRL_WORDS; i++)
    {
        printf("%04x ", machCtrlPtr[i]);
        if ((i) && (((i+1) % 8) == 0) && ((i+1) < MACH_CTRL_WORDS))
            printf("\n%04d: ", (i+1));
    }
    printf("\n");
    printf("  --> END values displayed in big endian byte order <--\n");
    machCtrlRegs = (bingMachineCtrl *)&machCtrlBuf[0];
    printBingMachCtrlRegs(machCtrlRegs);
    return 0;
}

/*
   Read and print machine control status bytes..
   These are 29 16-bit values at register bank 0
 */
#define MACH_STAT_WORDS  (sizeof(bingMachStatus)/sizeof(uint16_t))
#define MACH_STAT_BUF_SZ (sizeof(bingMachStatus))
static int doPrintMacStat(int nargs, char **arg)
{
    volatile uint8_t *pMem;
    uint8_t machStatBuf[MACH_STAT_BUF_SZ];
    bingMachStatus *machStat;
    int i, j;
    int offs, numBytes;
    pMem = (volatile uint8_t *)pFpgaMem;
    if (nargs > 0)
    {
        if (convToInt(arg[0], &offs) < 0)
            return -1;
        numBytes = (offs+1)*2;
        if ((numBytes < 2) || (numBytes > sizeof(bingMachStatus)))
        {
            logPrint("Error: Invalid register offset\n");
            return -1;
        }
    }
    else numBytes = sizeof(bingMachStatus);
    if (lockCtrlReg() != 0)
    {
        printf(errLockMsg, "ctrl reg");
        return -1;
    }
    updateSync(SYNC_REGS);
    RESET_CH_CTR(pMem);
    // read necessary number of bytes..
    for (i = 0; i < numBytes; i++)
         machStatBuf[i] = pMem[MACH_STAT_CH];
    if (nargs > 0)
    {
        printf("%d: 0x%x\n", offs, 
                ((machStatBuf[numBytes-1]<<8)|machStatBuf[numBytes-2]));
    }
    else
    {
        printf("%04d: ", 0);
        for (i = 0, j = 0; i < MACH_STAT_WORDS; i++, j+=2)
        {
            // display hiByte first and lowByte next to print 16bits in order
            printf("%02x%02x ", machStatBuf[j+1], machStatBuf[j]);
            if ((i) && (((i+1) % 8) == 0) && ((i+1) < MACH_STAT_WORDS))
                printf("\n%04d: ", (i+1));
        }
        printf("\n\n");
        machStat = (bingMachStatus *)machStatBuf;
        printf("FPGA Rev: 0x%X\n", machStat->fpgaRev.loByte);
    }
    unlockCtrlReg();
    return 0;
}

static int doPrintSyncBits(int nargs, char **arg)
{
    volatile uint8_t *pMem;
    uint8_t syncReg;
    pMem = (volatile uint8_t *)pFpgaMem;
    if (lockCtrlReg() != 0)
    {
        printf(errLockMsg, "ctrl reg");
        return -1;
    }
    RESET_CH_CTR(pMem);
    syncReg = pMem[CPU_FPGA_SYNC_CH];
    unlockCtrlReg();
    printf("Control register(0x%X) setting ", syncReg);
    if (syncReg & 0x01)
        printf("POSTED\n");
    else
        printf("PROCESSED\n");
    printf("Lookup up table ");
    if (syncReg & 0x02)
        printf("POSTED\n");
    else
        printf("PROCESSED\n");
    return 0;
}

static int doWriteSyncBits(int nargs, char **arg)
{
    volatile uint8_t *pMem;
    if (nargs != 1)
    {
        printf("%s", helpWriteSyncBits);
        return -1;
    }
    pMem = (volatile uint8_t *)pFpgaMem;
    if (lockCtrlReg() != 0)
    {
        printf(errLockMsg, "ctrl reg");
        return -1;
    }
    if (strcmp(arg[0], "reg") == 0)
        updateSync(SYNC_REGS);
    else if (strcmp(arg[0], "lookup") == 0)
        updateSync(LOOKUP_TBL_A_CH);
    else
        printf("Error: unknown parameter\n");
    unlockCtrlReg();
    return 0;
}

static int doResetCmds(int nargs, char *arg[])
{
    volatile uint8_t *pMem;
    if (nargs != 1)
    {
        printf(helpResetCmds);
        return -1;
    }
    pMem = (volatile uint8_t *)pFpgaMem;
    if (lockCtrlReg() != 0)
    {
        printf(errLockMsg, "ctrl reg");
        return -1;
    }
    if (strcmp(arg[0], "startmcode") == 0)
    {
        RESET_CH_CTR(pMem);
        START_UCODE_EXEC(pMem);
    }
    else if (strcmp(arg[0], "stopmcode") == 0)
    {
        RESET_CH_CTR(pMem);
        STOP_UCODE_EXEC(pMem);
    }
    else if (strcmp(arg[0], "mcode") == 0)
    {
        RESET_CH_CTR(pMem);
        RESET_UCODE_PC(pMem);
    }
    else if (strcmp(arg[0], "chaddr") == 0)
    {
        RESET_CH_CTR(pMem);
    }
    else if (strcmp(arg[0], "uart") == 0)
    {
        volatile uint8_t *pUart;
        printf("reset UARTS\n");
        pUart = (volatile uint8_t *)pFpgaUartBase;
        pUart[6] = 0x30;  // reset UARTS - CJC & RS485
        usleep(20);
        pUart[6] = 0x0;  
    }
    else
    {
        unlockCtrlReg();
        printf("Error: unknown parameter\n");
        return -1;
    }
    updateSync(SYNC_REGS);
    unlockCtrlReg();
    return 0;
}

#define SKIP_LINE(buf, offs) \
    { while ((buf[offs]) && (buf[offs] != 0x0a)) offs++; offs++; lineNo++; }
#define COMMENT_CHAR    ';'
#define MAX_FPGA_CMD_BYTES  (1024*8) /* 2K words -> 8kbytes */

/* 
   parse address line and following data/cmd bytes from the buffer.
   Size of cmdBuf is assumed to be MAX_FPGA_CMD_BYTES
   returns actual number of bytes read
 */
static int doParse(char *inBuf, unsigned int size, unsigned char *cmdBuf, 
                    unsigned int *addr)
{
    int offset = 0;
    int idx = 0;
    int ret = 0;
    unsigned int val;
    int lineNo = 1;

    while ((inBuf[offset]) && (inBuf[offset] == COMMENT_CHAR))
        SKIP_LINE(inBuf,offset);
    if (offset >= size)
    {
        fprintf(stderr, "Error incomplete file\n");
        return -1;
    }
    //printf("SKIP1 bytes: [%d] %c %c\n", offset, inBuf[offset], inBuf[offset+1]);
    if (sscanf(&inBuf[offset], "%x", addr) != 1)
    {
        fprintf(stderr, "Error in address line %d\n", lineNo);
        return -2;
    }
    SKIP_LINE(inBuf, offset);
    if (offset >= size)
    {
        fprintf(stderr, "Error incomplete file\n");
        return -1;
    }
    //printf("SKIP2 bytes: [%d] %c %c\n", offset, inBuf[offset], inBuf[offset+1]);
    while (offset < size)
    {
        while ((inBuf[offset]) && (inBuf[offset] == COMMENT_CHAR))
            SKIP_LINE(inBuf, offset);
        while (1) /* skip empty or lines starting with non-digits*/
        {
            if (offset >= size)
                break;
            if (isxdigit(inBuf[offset]))
                break;
            else
            {
                SKIP_LINE(inBuf, offset);
            }
        }
        if (offset >= size)
            break;
        //printf("SKIP bytes: [%d] %c %c\n", offset, inBuf[offset], inBuf[offset+1]);
        if (sscanf(&inBuf[offset], "%x", &val) != 1)
        {
            fprintf(stderr, "Error reading cmd bytes line: %d\n", lineNo);
            ret = -3;
            break;
        }
        //printf("%d: Val: %x\n", lineNo, val);
        if (val < 0x100)
            cmdBuf[idx] = val;
        else
        {
            fprintf(stderr, "Invalid value at line: %d\n", lineNo);
            ret = -4;
            break;
        }
        idx++;
        if (idx >= MAX_FPGA_CMD_BYTES)
        {
            fprintf(stderr, "CMD buffer overflow!\n");
            ret = -5;
            break;
        }
        SKIP_LINE(inBuf, offset);
    }
    if (ret == 0)
    {
        printf("Found %d cmd bytes\n", idx);
        return idx;
    }
    return ret;
}

int parseInputFile(char *file, unsigned char *cmdBuf, unsigned int *addr)
{
    int ret = 0;
    char fileBuf[512];
    char *inBuf;
    struct stat sBuf;
    int fd;
    if ((fd = open(file, O_RDONLY)) < 0)
    {
        perror("open");
        return -1;
    }
    if (fstat(fd, &sBuf) != 0)
    {
        perror("stat");
        close(fd);
        return -2;
    }
    if (sBuf.st_size < 8) /* one address and one command byte */
    {
        fprintf(stderr, "Error: input file too small(%d bytes)\n", (int)sBuf.st_size);
        close(fd);
        return -3;
    }
    if (sBuf.st_size >= 512)
    {
        if ((inBuf = malloc(sBuf.st_size+1)) == 0)
        {
            close(fd);
            fprintf(stderr, "Error: memory allocation failure\n");
            return -4;
        }
    }
    else
        inBuf = fileBuf;
    if (read(fd, inBuf, sBuf.st_size) != sBuf.st_size)
    {
        perror("read");
        if (sBuf.st_size >= 512)
            free(inBuf);
        close(fd);
        return -5;
    }
    close(fd);
    inBuf[sBuf.st_size] = '\0';
    printf("File sz: %d\n", (int)sBuf.st_size);
    //printf("%s\n", inBuf);
    ret = doParse(inBuf, (unsigned int)sBuf.st_size, cmdBuf, addr);
    if (ret > 0)
        printf("address: %x\n", *addr);
    if (sBuf.st_size >= 512)
        free(inBuf);
    return ret;
}

static int doLoadFile(int nargs, char **arg)
{
    uint32_t addr;
    uint8_t *cmdBytes = 0;
    int ret, numBytes;
    int regBank = 0;
    int i;
    volatile uint8_t *pMem;
    if (nargs != 1)
    {
        printf("%s", helpLoadFile);
        return -1;
    }
    if ((cmdBytes = (uint8_t *) malloc(MAX_FPGA_CMD_BYTES)) == 0)
    {
        printf("Error: memory allocation failure\n");
        return -1;
    }
    if ((numBytes = parseInputFile(arg[0], cmdBytes, &addr)) <= 0)
    {
        printf("ERROR: parse input file\n");
        free(cmdBytes);
        return -1;
    }
    // make sure we have a valid address
    if ((addr != 0x0000) && (addr != 0x0800) && (addr != 0xC00) &&
            (addr != 0x2000))
    {
        printf("Error: invalid address in input file (%x)\n", addr);
        free(cmdBytes);
        return -1;
    }
    // write the bytes to FPGA
    printf("Input file - ");
    switch (addr)
    {
        case 0x0:
            regBank = MACH_CTRL_CH;
            printf("Primary Machine Control\n");
            break;
        case 0x0800:
            regBank = LOOKUP_TBL_A_CH;
            printf("Angle Table A\n");
            break;
        case 0x0C00:
            regBank = LOOKUP_TBL_B_CH;
            printf("Angle Table B\n");
            break;
        case 0x2000:
            regBank = FPGA_UCODE_CH;
            printf("FPGA Micro Code\n");
            break;
        default:
            printf("Unknown\n");
            free(cmdBytes);
            return -1;
            break;
    }
    /* reset channel counter */
    pMem = (volatile uint8_t *)pFpgaMem;
    if (lockCtrlReg() != 0)
    {
        printf(errLockMsg, "ctrl reg");
        return -1;
    }
    RESET_CH_CTR(pMem);
    for (i = 0; i < numBytes; i++)
        pMem[regBank] = cmdBytes[i];
    if (regBank == MACH_CTRL_CH) // write sync so that micro-code picks up changes.
    {
        printf("Updating SYNC reg");
        updateSync(SYNC_REGS);
    }
    unlockCtrlReg();
    free(cmdBytes);
    return 0;
}

int computePCBTemp(float *pcbV, float *pcbR, float *pcbT);
int computePATemp(float *paV, float *paR, float *paT);
static int doTempCmd(int nargs, char **arg)
{
    int ret;
    double temp;
    float pcbV, pcbR, pcbT;
    float paV, paR, paT;
    if (lockI2CBus(I2C_MUX_BUS) != 0)
    {
        printf("Error: lock i2c bus\n");
        return -1;
    }
    if (i2cmux_select_chan(TMP421_MUX_CHAN) != 0)
    {
        unlockI2CBus(I2C_MUX_BUS);
        printf("Error: failed to select mux channel\n");
        return -1;
    }
    if ((ret = read_temperature(I2C_MUX_BUS, TEMP_SENSOR_ADDR, &temp, INTERNAL)) != 0)
    {
        printf("Error: failed to read temperature\n");
        unlockI2CBus(I2C_MUX_BUS);
        return -1;
    }
    unlockI2CBus(I2C_MUX_BUS);
    // read PCB and PA temperatures now.
    computePCBTemp(&pcbV, &pcbR, &pcbT);
    computePATemp(&paV, &paR, &paT);
    printf("Bing temp: %0.2f\n", temp);
    printf("PCB: V: %0.4f R: %0.2f Temp: %0.2f\n", pcbV, pcbR, pcbT);
    printf("PA:  V: %0.4f R: %0.2f Temp: %0.2f\n", paV, paR, paT);
    return 0;
}

static int eepromWriteEnable(int enable)
{
    bingMachineCtrl machCtrl;
    int numBytes;
    numBytes = (1+1)*2; // machine control is in first register
    if (lockCtrlReg() != 0)
    {
        printf(errLockMsg, "ctrl reg");
        return -1;
    }
    readMachCtrlRegs(&machCtrl, numBytes);
    // local eeprom write enable bit 11
    if (enable)
        machCtrl.opCtrl.hiByte |= 0x8;
    else
        machCtrl.opCtrl.hiByte &= ~(0x8);
    updateMachCtrlRegs(&machCtrl, numBytes);
    updateSync(SYNC_REGS);
    unlockCtrlReg();
    return 0;
}

static int doMfgDataOps(int nargs, char **arg)
{
    char company[LEN_COMPANY_NAME+1] = "Gatan Inc";
    ObreroMfgInfo mfgInfo;
    int ret;
    if (nargs < 1)
    {
        printf("%s\n", helpMfgData);
        return -1;
    }
    if (strcmp(arg[0], "read") == 0)
    {
        if (lockI2CBus(I2C_MUX_BUS) != 0)
        {
            printf(errLockMsg, "i2c bus");
            return -1;
        }
        ret = readMfgData(0, &mfgInfo);
        unlockI2CBus(I2C_MUX_BUS);
        if (ret == 0)
        {
            printf("Product:\n");
            printf("\tCompany: %16s\t Name: %16s\n\tModel: %16s\t Serial: %16s\n",
                    mfgInfo.mfgCompanyName, mfgInfo.mfgProdName, 
                    mfgInfo.mfgProdModel, mfgInfo.mfgProdSerial);
            printf("Assembly:\n");
            printf("\tPart No: %16s\t Serial: %16s\n\tMfg. Rev. %8s\t Eng. Rev: %8s\n\n", 
                    mfgInfo.mfgAssmPartNo, mfgInfo.mfgAssmSerial, 
                    mfgInfo.mfgAssmRev, mfgInfo.mfgAssmEngRev);
            printf("Board:\n");
            printf("\tPart No: %16s\t Serial: %16s\n\tMfg. Rev: %8s\t EngRev: %8s\n\n",
                    mfgInfo.mfgBrdPartNo, mfgInfo.mfgBrdSerial,
                    mfgInfo.mfgBrdRev, mfgInfo.mfgBrdEngRev);
        }
        else
            printf("Error: eeprom read, %d\n", ret);
        return 0;
    }
    else if ((strcmp(arg[0], "write-pca") == 0) ||
             (strcmp(arg[0], "write") == 0))
    {
        if (nargs < 5)
        {
            printf("Error: not enough data for write\n%s", helpMfgData);
            return -1;
        }
        eepromWriteEnable(1);
        if (lockI2CBus(I2C_MUX_BUS) != 0)
        {
            unlockCtrlReg();
            printf(errLockMsg, "i2c bus");
            return -1;
        }
        ret = writeMfgData(EEPROM_OFFSET_PCA, company, arg[1], arg[2], 
                                                            arg[3], arg[4]);
        unlockI2CBus(I2C_MUX_BUS);
        // enable write protection.
        eepromWriteEnable(0);
        if (ret != 0)
        {
            printf("Error updating MFG data, %d\n", ret);
            return -1;
        }
        return 0;
    }
    else if (strcmp(arg[0], "write-prod") == 0) 
    {
        // must be the product name, model and serial
        if (nargs < 4)
        {
            printf("Error: not enough data for write\n%s", helpMfgData);
            return -1;
        }
        eepromWriteEnable(1);
        if (lockI2CBus(I2C_MUX_BUS) != 0)
        {
            unlockCtrlReg();
            printf(errLockMsg, "i2c bus");
            return -1;
        }
        ret = writeMfgData2(EEPROM_OFFSET_PROD, arg[1], arg[2], arg[3]);
        unlockI2CBus(I2C_MUX_BUS);
        // enable write protection.
        eepromWriteEnable(0);
        if (ret != 0)
        {
            printf("Error updating MFG data, %d\n", ret);
            return -1;
        }
        return 0;
    }
    else if (strcmp(arg[0], "write-assm") == 0)
    {
        if (nargs < 5)
        {
            printf("Error: not enough data for write\n%s", helpMfgData);
            return -1;
        }
        eepromWriteEnable(1);
        if (lockI2CBus(I2C_MUX_BUS) != 0)
        {
            unlockCtrlReg();
            printf(errLockMsg, "i2c bus");
            return -1;
        }
        ret = writeMfgData3(EEPROM_OFFSET_ASSM, arg[1], arg[2], arg[3], arg[4]);
        unlockI2CBus(I2C_MUX_BUS);
        // enable write protection.
        eepromWriteEnable(0);
        if (ret != 0)
        {
            printf("Error updating MFG data, %d\n", ret);
            return -1;
        }
        return 0;
    }
    printf("%s\n", helpMfgData);
    return -1;
}

static int doPrintDbgCh(int nargs, char **arg)
{
    uint32_t numBytes;
    volatile uint8_t *pMem;
    uint8_t dbgBuf[1024];
    int i, j;
    if (nargs < 1)
    {
        printf("%s\n", helpDbgPrint);
        return -1;
    }
    if (cvtToUInt(arg[0], &numBytes) != 0)
        return -1;
    if ((numBytes < 4) || (numBytes > 1024))
    {
        printf("Invalid size\n");
        return -1;
    }
    // dump numByte bytes as 32bit values
    pMem = (volatile uint8_t *)pFpgaMem;
    RESET_CH_CTR(pMem);
    for (i = 0; i < numBytes; i++)
        dbgBuf[i] = pMem[DBG_CHAN];
    printf("%04d: ", 0);
    for (i = 0, j = 0; j < numBytes; i++, j+=4)
    {
        // display hibytes to lobytes to show 32bits
        printf("%02x%02x%02x%02x ", dbgBuf[j+3], dbgBuf[j+2], dbgBuf[j+1], dbgBuf[j]);
        if ((i) && (((i+1) % 8) == 0) && ((i+1) < (numBytes/4)))
            printf("\n%04d: ", (i+1));
    }
    printf("\n");
}

static int doPeekReg(int argc, char **argv)
{
    int ret = 0;
    uint32_t offset;
    uint16_t regSpace[sizeof(bingMachineCtrl)];
    uint16_t regVal;
    int numRegs = 1;
    int i;
    bingMachineCtrl *machCtrl = (bingMachineCtrl *)regSpace;
    if (argc < 1)
    {
        printf(helpPeekCmd);
        return -1;
    }
    if (cvtToUInt(argv[0], &offset) != 0)
        return -1;
    if (offset >= (sizeof(bingMachineCtrl)/2))
    {
        printf("Error: invalid offset, %d\n", offset);
        return -1;
    }
    if (argc > 1)
    {
        if (convToInt(argv[1], &numRegs) != 0)
            return -1;
    }
    if (numRegs <= 1)
    {
        if (lockCtrlReg() != 0)
        {
            printf(errLockMsg, "ctrl reg");
            return -1;
        }
        readMachCtrlRegs(machCtrl, ((offset+1)*2));
        unlockCtrlReg();
        regVal = bswap_16(regSpace[offset]);
        printf("%d: 0x%X\n", offset, (uint32_t)regVal);
    }
    else
    {
        uint8_t rBytes[64];
        if (numRegs > 64)
            numRegs = 64;
        if (lockCtrlReg() != 0)
        {
            printf(errLockMsg, "ctrl reg");
            return -1;
        }
        ret = readCtrlRegL(offset, numRegs, &rBytes[0]);
        unlockCtrlReg();
        if (ret != 0)
        {
            printf("Error reading machine control regs(%d)\n", ret);
            return -1;
        }
        printf("%d: ");
        for (i = 0; i < numRegs; i++)
            printf("0x%X  ", rBytes[i]);
        printf("\n");
    }
    return 0;
}

static int doPokeReg(int argc, char **argv)
{
    int ret = 0;
    uint32_t offset, value;
    uint16_t regSpace[sizeof(bingMachineCtrl)];
    uint16_t regVal;
    bingMachineCtrl *machCtrl = (bingMachineCtrl *)regSpace;
    if (argc < 2)
    {
        printf(helpPokeCmd);
        return -1;
    }
    if (cvtToUInt(argv[0], &offset) != 0)
        return -1;
    if (cvtToUInt(argv[1], &value) != 0)
        return -1;
    if (offset >= (sizeof(bingMachineCtrl)/2))
    {
        printf("Error: invalid offset, %d\n", offset);
        return -1;
    }
    if (lockCtrlReg() != 0)
    {
        printf(errLockMsg, "ctrl reg");
        return -1;
    }
    setCtrlRegBits(offset, 0xFFFF, value);
    unlockCtrlReg();
    return 0;
errPoke:
    unlockCtrlReg();
    return -1;
}

/* utility function for doMacAddr */
void printMACAddr(uint8_t *macAddr)
{
    int i;
    for (i = 0; i < EPROM_MAC_ADDR_LEN; i++)
        fprintf(stdout, "%02X%c", macAddr[i], (i < (EPROM_MAC_ADDR_LEN-1)) ? ':' : ' ');
    fprintf(stdout, "\n");
}

/*
 * Set/get mac address for the 1GigE interface on control board.
 */
static int doMacAddr(int nargs, char **arg)
{
    uint8_t macAddr[EPROM_MAC_ADDR_LEN];
    uint32_t  wrtOffset = 0;
    int ret = 0, i;
    if (nargs == 1) /* programming mac address.. */
    {
        if (parseMacStr(arg[0], macAddr) != 0)
        {
            fprintf(stdout, "macaddr: invalid mac addr.\n");
            return -1;
        }
        eepromWriteEnable(1); // disable write protect
        if (lockI2CBus(I2C_MUX_BUS) != 0)
        {
            unlockCtrlReg();
            printf(errLockMsg, "i2c bus");
            return -1;
        }
        ret = setMacAddress(macAddr);
        unlockI2CBus(I2C_MUX_BUS);
        eepromWriteEnable(0);
        if (ret != 0)
        {
            fprintf(stdout, "Error: update mac address, %d\n", ret);
            return ret;
        }
    }
    else if (nargs == 0)
    {
        if (lockI2CBus(I2C_MUX_BUS) != 0)
        {
            printf(errLockMsg, "i2c bus");
            return -1;
        }
        ret = getMacAddress(macAddr);
        unlockI2CBus(I2C_MUX_BUS);
        if (ret == 0)
            printMACAddr(macAddr);
        else
        {
            fprintf(stdout, "Error: read mac address, %d\n", ret);
        }
    }
    else
    {
        fprintf(stdout, "Error: invalid number of arguments\n");
        ret = -1;
    }
    return ret;
}

int mcp3424Read(int bus, int addr, int chan, int gain, float *val);
int mcp47x6Read(int bus, int devAddr, float *V);
int mcp47x6Write(int bus, int devAddr, float v);
int mcp4728Write(int bus, int devAddr, int chan, int gain, float v);
int mcp4728Read(int bus, int devAddr, uint8_t *dataBuf);
static int doMCP3424Cmd(int nargs, char **arg)
{
    float val;
    int ret = 0;
    int bus = 2, devAddr = 0x6E;
    int gain = 2;
    ret = mcp3424Read(bus, devAddr, 1, gain, &val);
    return ret;
}

static int doMCP4726SetCmd(int nargs, char **arg)
{
    float v;
    int ret = 0;
    int bus = 2;
    int devAddr = 0x60;
    int idx = 1;
    if (nargs < 2)
        return -1;
    if (convToInt(arg[0], &idx) != 0)
        return -1;
    if ((idx < 1) || (idx > 2))
    {
        printf("Error device is either 1 or 2\n");
        return -1;
    }
    if (cvtToFloat(arg[1], &v) != 0)
    {
        logPrint("Error: invalid number(%s)\n", arg[1]);
        return -1;
    }
    if (lockI2CBus(bus) != 0)
    {
        printf("Error: lock i2c bus\n");
        return -1;
    }
    if ((ret = i2cmux_select_chan(3)) != 0)
    {
        fprintf(stderr, "Error selecting channel %d\n", ret);
        unlockI2CBus(bus);
        return -1;
    }
    if (idx == 2)
        devAddr = 0x61;
    ret =  mcp47x6Write(bus, devAddr, v);
    unlockI2CBus(bus);
    if (ret != 0)
    {
        logPrint("Error: failed to update MCP4726\n");
        return -1;
    }
    return 0;
}

static int doMCP4726GetCmd(int nargs, char **arg)
{
    int idx = 1;
    int ret = 0;
    int devAddr = 0x60;
    float v;
    int bus = 2;
    //printf("%s: %d %s\n", __func__, nargs, arg[0]);
    if (nargs < 1)
        return -1;
    if (convToInt(arg[0], &idx) != 0)
        return -1;
    if ((idx < 1) || (idx > 2))
    {
        printf("Error device is either 1 or 2\n");
        return -1;
    }
    if (lockI2CBus(bus) != 0)
    {
        printf("Error: lock i2c bus\n");
        return -1;
    }
    if ((ret = i2cmux_select_chan(3)) != 0)
    {
        fprintf(stderr, "Error selecting channel %d\n", ret);
        unlockI2CBus(bus);
        return -1;
    }
    if (idx == 2)
        devAddr = 0x61;
    ret = mcp47x6Read(bus, devAddr, &v);
    unlockI2CBus(bus);
    if (ret != 0)
    {
        logPrint("Error: failed to read MCP4726\n");
        return -1;
    }
    printf("Aux Dac %d: %0.2f V\n", idx, v);
    return 0;
} 

static int doMCP4726Cmd(int nargs, char **arg)
{
    if (nargs >= 2)
    {
        if (strcmp(arg[0], "read") == 0)
            return doMCP4726GetCmd(--nargs, &arg[1]);
        else if (strcmp(arg[0], "write") == 0)
            return doMCP4726SetCmd(--nargs, &arg[1]);
    }
    else
    {
        printf("auxdac <read|write> <1|2> <value>\n");
        return -1;
    }
    return 0;
}

static int doTrimDacCmd(char *cmd, int chan, float *value)
{
    float v;
    uint8_t dataBuf[24];
    int ret = 0;
    int chanNo;
    int bus = 2;
    if (strcmp(cmd, "get") == 0)
    {
        if (lockI2CBus(bus) != 0)
        {
            printf("Error: lock i2c bus\n");
            return -1;
        }
        if ((ret = i2cmux_select_chan(1)) != 0)
        //if ((ret = i2cmux_select_chan(3)) != 0)   // bing_lite
        {
            fprintf(stderr, "Error selecting channel %d\n", ret);
            unlockI2CBus(bus);
            return -1;
        }
        //ret = (mcp4728Read(bus, 0x62, dataBuf);   // bing_lite
        ret = mcp4728Read(bus, 0x61, dataBuf);
        unlockI2CBus(bus);
        if (ret != 0)
        {
            logPrint("Error: failed to read MCP4728\n");
            return -1;
        }
    }
    else if (strcmp(cmd, "set") == 0)
    {
        if (lockI2CBus(bus) != 0)
        {
            printf("Error: lock i2c bus\n");
            return -1;
        }
        if ((ret = i2cmux_select_chan(1)) != 0)
        //if ((ret = i2cmux_select_chan(3)) != 0)   // bing_lite
        {
            fprintf(stderr, "Error selecting channel %d\n", ret);
            unlockI2CBus(bus);
            return -1;
        }
        ret = mcp4728Write(bus, 0x61, chan, 2, *value);
        //ret = mcp4728Write(bus, 0x62, chan, *value);
        unlockI2CBus(bus);
        if (ret != 0)
        {
            logPrint("Error: failed to update MCP4728\n");
            return -1;
        }
    }
    return -1;
}

/* START SI 5351 clkgen programming */

#define NUM_MAX_REG (255)

typedef struct {
    int offset;
    uint8_t value;
} Reg_Data;

static Reg_Data defaultRegStore[] =
{
	{   0,0x00 }, 
	{   1,0x00 }, 
	{   2,0x18 }, 
	{   3,0x00 }, 
	{   4,0x00 }, 
	{   5,0x00 }, 
	{   6,0x00 }, 
	{   7,0x00 }, 
	{   8,0x00 }, 
	{   9,0x00 }, 
	{  10,0x00 }, 
	{  11,0x00 }, 
	{  12,0x00 }, 
	{  13,0x00 }, 
	{  14,0x00 }, 
	{  15,0x00 }, 
	{  16,0x4F }, 
	{  17,0x80 }, 
	{  18,0x80 }, 
	{  19,0x80 }, 
	{  20,0x80 }, 
	{  21,0x80 }, 
	{  22,0x80 }, 
	{  23,0x80 }, 
	{  24,0x00 }, 
	{  25,0x00 }, 
	{  26,0x00 }, 
	{  27,0x87 }, 
	{  28,0x00 }, 
	{  29,0x0B }, 
	{  30,0x46 }, 
	{  31,0x00 }, 
	{  32,0x00 }, 
	{  33,0x16 }, 
	{  34,0x00 }, 
	{  35,0x00 }, 
	{  36,0x00 }, 
	{  37,0x00 }, 
	{  38,0x00 }, 
	{  39,0x00 }, 
	{  40,0x00 }, 
	{  41,0x00 }, 
	{  42,0x00 }, 
	{  43,0x01 }, 
	{  44,0x00 }, 
	{  45,0x0C }, 
	{  46,0x00 }, 
	{  47,0x00 }, 
	{  48,0x00 }, 
	{  49,0x00 }, 
	{  50,0x00 }, 
	{  51,0x00 }, 
	{  52,0x00 }, 
	{  53,0x00 }, 
	{  54,0x00 }, 
	{  55,0x00 }, 
	{  56,0x00 }, 
	{  57,0x00 }, 
	{  58,0x00 }, 
	{  59,0x00 }, 
	{  60,0x00 }, 
	{  61,0x00 }, 
	{  62,0x00 }, 
	{  63,0x00 }, 
	{  64,0x00 }, 
	{  65,0x00 }, 
	{  66,0x00 }, 
	{  67,0x00 }, 
	{  68,0x00 }, 
	{  69,0x00 }, 
	{  70,0x00 }, 
	{  71,0x00 }, 
	{  72,0x00 }, 
	{  73,0x00 }, 
	{  74,0x00 }, 
	{  75,0x00 }, 
	{  76,0x00 }, 
	{  77,0x00 }, 
	{  78,0x00 }, 
	{  79,0x00 }, 
	{  80,0x00 }, 
	{  81,0x00 }, 
	{  82,0x00 }, 
	{  83,0x00 }, 
	{  84,0x00 }, 
	{  85,0x00 }, 
	{  86,0x00 }, 
	{  87,0x00 }, 
	{  88,0x00 }, 
	{  89,0x00 }, 
	{  90,0x00 }, 
	{  91,0x00 }, 
	{  92,0x00 }, 
	{  93,0x00 }, 
	{  94,0x00 }, 
	{  95,0x00 }, 
	{  96,0x00 }, 
	{  97,0x00 }, 
	{  98,0x00 }, 
	{  99,0x00 }, 
	{ 100,0x00 }, 
	{ 101,0x00 }, 
	{ 102,0x00 }, 
	{ 103,0x00 }, 
	{ 104,0x00 }, 
	{ 105,0x00 }, 
	{ 106,0x00 }, 
	{ 107,0x00 }, 
	{ 108,0x00 }, 
	{ 109,0x00 }, 
	{ 110,0x00 }, 
	{ 111,0x00 }, 
	{ 112,0x00 }, 
	{ 113,0x00 }, 
	{ 114,0x00 }, 
	{ 115,0x00 }, 
	{ 116,0x00 }, 
	{ 117,0x00 }, 
	{ 118,0x00 }, 
	{ 119,0x00 }, 
	{ 120,0x00 }, 
	{ 121,0x00 }, 
	{ 122,0x00 }, 
	{ 123,0x00 }, 
	{ 124,0x00 }, 
	{ 125,0x00 }, 
	{ 126,0x00 }, 
	{ 127,0x00 }, 
	{ 128,0x00 }, 
	{ 129,0x00 }, 
	{ 130,0x00 }, 
	{ 131,0x00 }, 
	{ 132,0x00 }, 
	{ 133,0x00 }, 
	{ 134,0x00 }, 
	{ 135,0x00 }, 
	{ 136,0x00 }, 
	{ 137,0x00 }, 
	{ 138,0x00 }, 
	{ 139,0x00 }, 
	{ 140,0x00 }, 
	{ 141,0x00 }, 
	{ 142,0x00 }, 
	{ 143,0x00 }, 
	{ 144,0x00 }, 
	{ 145,0x00 }, 
	{ 146,0x00 }, 
	{ 147,0x00 }, 
	{ 148,0x00 }, 
	{ 149,0x00 }, 
	{ 150,0x00 }, 
	{ 151,0x00 }, 
	{ 152,0x00 }, 
	{ 153,0x00 }, 
	{ 154,0x00 }, 
	{ 155,0x00 }, 
	{ 156,0x00 }, 
	{ 157,0x00 }, 
	{ 158,0x00 }, 
	{ 159,0x00 }, 
	{ 160,0x00 }, 
	{ 161,0x00 }, 
	{ 162,0x00 }, 
	{ 163,0x00 }, 
	{ 164,0x00 }, 
	{ 165,0x00 }, 
	{ 166,0x00 }, 
	{ 167,0x00 }, 
	{ 168,0x00 }, 
	{ 169,0x00 }, 
	{ 170,0x00 }, 
	{ 171,0x00 }, 
	{ 172,0x00 }, 
	{ 173,0x00 }, 
	{ 174,0x00 }, 
	{ 175,0x00 }, 
	{ 176,0x00 }, 
	{ 177,0x00 }, 
	{ 178,0x00 }, 
	{ 179,0x00 }, 
	{ 180,0x00 }, 
	{ 181,0x30 }, 
	{ 182,0x00 }, 
	{ 183,0x92 }, 
	{ 184,0x60 }, 
	{ 185,0x60 }, 
	{ 186,0x00 }, 
	{ 187,0xC0 }, 
	{ 188,0x00 }, 
	{ 189,0x00 }, 
	{ 190,0x00 }, 
	{ 191,0x00 }, 
	{ 192,0x00 }, 
	{ 193,0x00 }, 
	{ 194,0x00 }, 
	{ 195,0x00 }, 
	{ 196,0x00 }, 
	{ 197,0x00 }, 
	{ 198,0x00 }, 
	{ 199,0x00 }, 
	{ 200,0x00 }, 
	{ 201,0x00 }, 
	{ 202,0x00 }, 
	{ 203,0x00 }, 
	{ 204,0x00 }, 
	{ 205,0x00 }, 
	{ 206,0x00 }, 
	{ 207,0x00 }, 
	{ 208,0x00 }, 
	{ 209,0x00 }, 
	{ 210,0x00 }, 
	{ 211,0x00 }, 
	{ 212,0x00 }, 
	{ 213,0x00 }, 
	{ 214,0x00 }, 
	{ 215,0x00 }, 
	{ 216,0x00 }, 
	{ 217,0x00 }, 
	{ 218,0x00 }, 
	{ 219,0x00 }, 
	{ 220,0x00 }, 
	{ 221,0x0D }, 
	{ 222,0x00 }, 
	{ 223,0x00 }, 
	{ 224,0x00 }, 
	{ 225,0x00 }, 
	{ 226,0x00 }, 
	{ 227,0x00 }, 
	{ 228,0x00 }, 
	{ 229,0x00 }, 
	{ 230,0x00 }, 
	{ 231,0x00 }, 
	{ 232,0x00 }, 
};

static int fillRegStore(char *file, Reg_Data *regStore)
{
    FILE *fp;
    char *line = 0;
    size_t len = 0;
    ssize_t read;
    int ret;
    int idx = 0;
    int addr, val;
    int foundStart;
    int okEntry = 0;

    fp  = fopen(file, "r");
    if (fp == 0)
    {
        ret = errno;
        logPrint("Error: file open(%s)\n", strerror(ret));
        return -1;
    }
    while ((read = getline(&line, &len, fp)) != -1)
    {
        if (strlen(line) < 1)
            continue;
        if (strncmp(line, "#REGISTER_MAP", 13) == 0)
        {
            foundStart = 1;
            logPrint("Found Start\n");
            continue;
        }
        if (line[0] == '#') 
            continue;
        if (foundStart)
        {
            okEntry = 0;
            if (sscanf(&line[0], "%d,%x", &addr, &val) == 2)
                okEntry = 1;
            else 
            {
                ret = errno;
                logPrint("Parse Error(%s): %s\n", strerror(ret), line);
            }
            if (okEntry)
            {
                regStore[idx].offset = addr;
                regStore[idx].value = val;
                idx++;
                if (idx >= NUM_MAX_REG)
                    break;
            }
        }
    }
    logPrint("== Found %d entries\n", idx);
    fclose(fp);
    if (line)
        free(line);
    return idx;
}

static void printRegStore(Reg_Data *regStore, int numEntries)
{
    int idx;
    if (!regStore)
        return;
    logPrint("[%d] registers\n", numEntries);
    for (idx = 0; idx < numEntries; idx++)
        logPrint("%d. %d, 0x%X\n", idx, regStore[idx].offset, 
                                        regStore[idx].value);
}

/*
  Steps:
1. Disable all outputs setting CLKx_DIS high
   Write Reg3 = 0xFF

2. Power down all output drivers
   Write Reg16 - Reg 23 = 0x80

3. Set interrupt masks as required (see Register 2 description in AN619).  By 
default, ClockBuilder Desktop, sets this register to 0x18.  Note that the 
least significant nibble must remain 0x8, but the most significant nibble 
may be modified to suit your needs.

4. Write new configuration to device using the contents of register map 
generated by ClockBuilder Desktop.
   Write Reg 15-92 and 149-170.

5. Apply soft reset
   Write Reg 177 = 0xAC

6. Enabled desired outputs (see Register 3).
*/
int readClkGenReg(int bus, int devAddr, int reg, uint8_t *value);
int writeClkGenReg(int bus, int devAddr, int reg, uint8_t value);
static int configClkGen(Reg_Data *regStore, int numEntries)
{
    int ret;
    int i, j;
    int pwrDnRegs[] = { 16, 17, 18, 19, 20, 21, 22, 23 };
    int cfgRegs[][2] = { { 15, 92}, { 149, 170 }, { 183, 183 } };

    if ((ret = i2cmux_select_chan(1)) < 0)
        logPrint("Error: failed to select mux channel\n");
    // disable outputs, Reg 3 = 0xff
    printf("Disable outputs\n");
    if (writeClkGenReg(2, 0x60, 0x3, 0xff) != 0)
    {
        ret = errno;
        logPrint("Error: failed to write to device(%s)\n", strerror(ret));
        return -1;
    }
    printf("Power down all output drivers\n");
    ret = 0;
    for (i = 0; i < sizeof(pwrDnRegs)/sizeof(int); i++)
    {
        if (writeClkGenReg(2, 0x60, pwrDnRegs[i], 0x80) != 0)
        {
            ret = errno;
            logPrint("Error: failed to write to device(%s)\n", strerror(ret));
            break;
        }
    }
    if (ret != 0)
        return -1;
    printf("Set interrupt mask\n");
    if (writeClkGenReg(2, 0x60, 0x2, 0x18) != 0)
    {
        ret = errno;
        logPrint("Error: failed to write to device(%s)\n", strerror(ret));
        return -1;
    }
    printf("Update registers\n");
    for (i = 0; i < 3; i++)
    {
        ret = 0;
        for (j = cfgRegs[i][0]; j <= cfgRegs[i][1]; j++)
        {
            if (writeClkGenReg(2,0x60,regStore[j].offset,regStore[j].value) != 0)
            {
                ret = errno;
                logPrint("Error: failed to write to device(%s)\n", 
                                                          strerror(ret));
                break;
            }
        }
        if (ret != 0)
            break;
    }
    if (ret != 0)
        return -1;
    printf("Apply soft reset\n");
    if (writeClkGenReg(2, 0x60, 177, 0xAC) != 0)
    {
        ret = errno;
        logPrint("Error: failed to write to device(%s)\n", strerror(ret));
        return -1;
    }
    printf("Done updating clkgen\n");
    return 0;
}

static int doClkGenConfig(char *file)
{
    int numEntries;
    int ret = -1;
    Reg_Data regStore[NUM_MAX_REG];
    Reg_Data *regStorePtr;
    int bus = CLK_GEN_BUS;
    if (file == 0)
    {
        regStorePtr = &defaultRegStore[0];
        numEntries = sizeof(defaultRegStore)/sizeof(Reg_Data);
    }
    else
    {
        regStorePtr = &regStore[0];
        numEntries = fillRegStore(file, regStore);
    }
    if (lockI2CBus(CLK_GEN_BUS) != 0)
    {
        printf("Error: failed to acquire i2c lock\n");
        return -1;
    }
    if (numEntries > 0)
        ret = configClkGen(regStore, numEntries);
    if (ret == 0) // enable output
    {
        usleep(100000);
        if ((ret = writeClkGenReg(CLK_GEN_BUS, 0x60, 0x3, (uint8_t)2)) != 0)
        {
            ret = errno;
            logPrint("Error: Enable output(%s)\n", strerror(ret));
        }
    }
    unlockI2CBus(CLK_GEN_BUS);
    //printRegStore(regStorePtr, numEntries);
    return ret;
}

/* END SI 5351 clkgen programming */

static int doClkGenCmd(int nargs, char **argv)
{
    int ret = 0;
    int value;
    uint8_t regVal;
    char *file = 0;
    if (nargs < 1)
        goto outClkGenHelp;
    if (strcmp(argv[0], "config") == 0)
    {
        if (nargs == 2)
            file = argv[1];
        ret = doClkGenConfig(file);
    }
    else if (strcmp(argv[0], "output") == 0)
    {
        if (nargs == 2)
        {
            if (convToInt(argv[1], &value) != 0)
                return -1;
            if (lockI2CBus(CLK_GEN_BUS) != 0)
            {
                printf("Error: failed to acquire i2c lock\n");
                return -1;
            }
            if ((ret = writeClkGenReg(CLK_GEN_BUS, 0x60, 0x3, (uint8_t)(value & 0xff))) != 0)
            {
                ret = errno;
                logPrint("Error: write to device(%s)\n", strerror(ret));
            }
            unlockI2CBus(CLK_GEN_BUS);
        }
        else
        {
            if (lockI2CBus(CLK_GEN_BUS) != 0)
            {
                printf("Error: failed to acquire i2c lock\n");
                return -1;
            }
            if (readClkGenReg(CLK_GEN_BUS, 0x60, 0x3, &regVal) != 0)
            {
                ret = errno;
                logPrint("Error: write to device(%s)\n", strerror(ret));
            }
            else
                printf("0x%X\n", regVal);
            unlockI2CBus(CLK_GEN_BUS);
        }
    }
    return ((ret) ? -1 : 0);
outClkGenHelp:
    printf("%s\n", helpClkGen);
    return -1;
}

static int setVRange(int vMax)
{
    int ret = 0;
    if (lockCtrlReg() != 0)
    {
        printf(errLockMsg, "ctrl reg");
        return -1;
    }
    ret = setVoltageRange(vMax);
    unlockCtrlReg();
    if (ret == 0)
        pCache->maxVRange = vMax;
    return ret;
}

static int setGrounding(char *gndStr)
{
    int gndMode = 0x0;
    int ret = 0;
    if (strcmp(gndStr, "floating") == 0)
        gndMode = GndModeFloating;
    else if (strcmp(gndStr, "socket") == 0)
        gndMode = GndModeSocket;
    else if (strcmp(gndStr, "mains") == 0)
        gndMode = GndModeMains;
    else if (strcmp(gndStr, "buffered") == 0)
        gndMode = GndModeBuffered;
    else
    {
        logPrint("Error: invalid option\n");
        return -1;
    }
    if (lockCtrlReg() != 0)
    {
        printf(errLockMsg, "ctrl reg");
        return -1;
    }
    ret = setGroundingMode(gndMode);
    if (ret == 0)
        pCache->grounding = gndMode;
    unlockCtrlReg();
    return ret;
}

static int setHDAC(float value)
{
    int ret = 0;
    if (lockCtrlReg() != 0)
    {
        printf(errLockMsg, "ctrl reg");
        return -1;
    }
    ret = setHeaterDAC(value);
    unlockCtrlReg();
    return 0;
}

// TODO: PROFILE ADC averaging time..
long long adcRdStart, adcRdEnd, adcRdTot;
#define AVG_MAX_SAMPLES (100000)
static int32_t avgAdcVals[AVG_MAX_SAMPLES];
int getAvgADCReading(int chan, int count, double *V)
{
    int64_t sum;
    uint32_t rVals[3] = { 0 };
    double fVals[3] = { 0.0 };
    int i;
    if (count > AVG_MAX_SAMPLES)
        return -1;
    adcRdStart = getTimeInMs();
    readT_ADCValues(chan, count, avgAdcVals);
    adcRdEnd = getTimeInMs();
    adcRdTot += (adcRdEnd - adcRdStart);
    sum = 0;
#if 0
    // sign extend the 24-bit signed values to 4byte signed integer.
    for (i = 0; i < count; i++)
    {
        if ((uint32_t)avgAdcVals[i] > 0x7FFFFF)
            avgAdcVals[i] |= 0xFF000000;
    }
    // find average of avgAdcVals[] and compute voltage based on the avg.
    // filter outliers..
    {
        int j, k, netCnt = 0;
        for (i = 0; i < count; i++)
        {
            //printf("ADC rd %d: 0x%X\n", i, avgAdcVals[i]);
            if (i == 0)
                { j = 1; k = 2; }
            else if (i == (count -1))
                { j = (count - 2); k = (count - 3); }
            else
                { j = i-1; k = i+1; }
            if ((abs(avgAdcVals[i] - avgAdcVals[j]) > 0x4FFF) &&
                (abs(avgAdcVals[i] - avgAdcVals[k]) > 0x4FFF))
            {
                printf("  SKIP %d\n", i);
                continue;
            }
            sum += avgAdcVals[i];
            netCnt++;
        }
        //printf("Sum %d, Count: %d\n", sum, netCnt);
        rVals[chan-1]  = (uint32_t)(sum / (int64_t)netCnt);
        if ((uint32_t)rVals[chan-1] > 0x7FFFFF)
            rVals[chan-1] &= 0xFFFFFF;
        //printf("Sum %lld, Count: %d rVals %X[%d]\n", sum, netCnt, 
        //                rVals[chan-1], rVals[chan-1]);
    }
#else
    // find average of avgAdcVals[] and compute voltage based on the avg.
    for (i = 0; i < count; i++)
    {
        //printf("ADC rd %d: 0x%X\n", i, avgAdcVals[i]);
        // sign extend the 24-bit signed values to a 32bit signed 
        if ((uint32_t)avgAdcVals[i] > 0x7FFFFF)
            avgAdcVals[i] |= 0xFF000000;
        sum += avgAdcVals[i];
    }
    //printf("Sum %d\n", sum);
    rVals[chan-1]  = sum / count;
#endif
    if ((uint32_t)rVals[chan-1] > 0x7FFFFF)
        rVals[chan-1] &= 0xFFFFFF;
    scaleTADC(rVals, fVals);
    *V = (fVals[chan-1]);
    return 0;
}

// NOTICE: caller MUST BE holding register lock
static int getAvgADCFPGAReading(int chan, double *V)
{
    int i;
    uint32_t adcAvg[3];
    double fVals[3];
    *V = 0;
    for (i = 0; i < 5; i++)
    {
       if (getAdcFpgaAvg(0, &(adcAvg[chan-1])) > 0)
           break;
    }
    if (i >= 5)
        return -1;
    scaleTADC(adcAvg, &fVals[0]);
    *V = fVals[chan-1];
    return 0;
}

static int calVerbose = 0;
/*
 * Fast RTD measurement(Obrero Analog Circuit Description, Alistair May):
 * 1. set stimulus current to 0
 * 2. For Pt100, set gain setting of 210(refer to table 7)
 * 3. Route current and amplififer to internal precision regiseter(table 6).
 *    For Pt100 use 100Ohms, rPrec = 100
 * 4. Wait 5ms for amp. to settle.
 * 5. Take average of 100K ADC samples(over 1s). Call this vPrecZero
 * 6. Apply stimulus current, 10uA for Pt100
 * 7. Wait 5ms for current source and amplifier to settle
 * 8. Take average of 100K ADC samples(over 1s). Call this vPrecSTim
 * 9. Set stimulus current to 0
 * 10. Route current and amplifier to RTD(table 6)
 * 11. Wait 5ms for amp. to settle
 * 12. Take a measurement of 100K ADC samples(over 1s). Call this vRtdZero
 * 13. Re-apply 10uA(Pt100)
 * 14. Wait 5 ms for amp. to settle
 * 15. Start making measurements, call each reading vRtdStim
 * 16. RTD resistance is given by the formula:
 *          rRtd = rPrec * ((vRtdStim - vRtdZero)/(vPrecStim - vPrecZero))
 */
double rPrec = 100.0;
#define PRT_CAL_SAMPLES (50000)
static int doPRTCalibration()
{
    // reset vG1Zero, applicable only to diodes???
    pCache->vG1Zero = 0.0;
    setStimulusCurrent(STIM_0uA, 1);
    setDRTDGain(GAIN_X210); 
    setDRTDCalSel1(DRTD_CAL_R100);
    mySleep(10);
    getAvgADCReading(T_ADC_CHAN_DRTD, PRT_CAL_SAMPLES, &pCache->vPrecZero);
    if (calVerbose)
        logPrint("%s: vPrecZero %0.6f\n", __func__, pCache->vPrecZero);

    //setStimulusCurrent(STIM_10uA);
    setStimulusCurrent(pCache->stimI[0], 1);
    mySleep(10);
    getAvgADCReading(T_ADC_CHAN_DRTD, PRT_CAL_SAMPLES, &pCache->vPrecStim);
    if (calVerbose)
        logPrint("%s: vPrecStim %0.6f\n", __func__, pCache->vPrecStim);

    setStimulusCurrent(STIM_0uA, 1);
    setDRTDCalSel1(DRTD_CAL_NORM);
    mySleep(10);
    getAvgADCReading(T_ADC_CHAN_DRTD, PRT_CAL_SAMPLES, &pCache->vRtdZero);
    if (calVerbose)
        logPrint("%s: vRtdZero %0.6f\n", __func__, pCache->vRtdZero);
    
    setStimulusCurrent(pCache->stimI[0], 1);
    mySleep(5); // wait for amp. to settle
    return 0;
}

// Calling function MUST Be holding REG lock
// Find vZero offset for thermo-couples. Based on section 2.4.5 of doc
// Analog Circuit Descripttion, Alistair M.
// Assume T-ADC is setup according to section 2.3.8 in above doc
#define MIN_TC_VZERO (-5.0 / 1000.0)  // -5mV
#define MAX_TC_VZERO (5.0 / 1000.0)   // +5mV
static int doTCCalibration()
{
    double v;
    int ret = 0;
    int numRetry = 3, try = 0;
    pCache->tcVZero = 0.0; // reset, will recompute here.
    setStimulusCurrent(pCache->stimI[0], 1);
    ret = setCtrlRegBits(3, 0x2, 0x2); // set TC_BIAS_EN high
    usleep(10000); // let it settle
retryCalTC:
    setCtrlRegBits(3, 0x4, 0x4); // set TC_ZERO high
    usleep(1000);                // wait min. 300uS
    getAvgADCReading(T_ADC_CHAN_TC, 100, &v); // get average ADC reading
    setCtrlRegBits(3, 0x4, 0x0); // set TC_ZERO high
    if (calVerbose)
        printf("VZERO: %0.7f\n", v);
    if ((v < MIN_TC_VZERO) || (v > MAX_TC_VZERO))
    {
        try++;
        if (try >= numRetry)
            return -2;
        usleep(50000);
        goto retryCalTC;
    }
    pCache->tcVZero = v;
    return 0;
}

// Calling function MUST Be holding REG lock
// Find Vg1Zero offset per section 2.10.1, Analog Circuit 
// Description(AlistairM)
static int doDiodeCalibration()
{
    uint8_t reg2Val;
    const int avgSamples = 10;
    int32_t adcVal[avgSamples];
    double adcVolts[avgSamples];
    uint32_t rVals[3] = { 0 };
    double fVals[3]   = { 0.0 };
    double sumV;
    int retry = 3;
    int ret = 0, i, j;

    setStimulusCurrent(pCache->stimI[0], 1);
    usleep(10000); // let it settle

    readCtrlRegL(2, 1, (void *)&reg2Val);
    setCtrlRegBits(2, 0x3, 0x3);     // set DRTD_CAL_SEL[1:0] = 11
    setCtrlRegBits(2, 0x38, 0x0);    // set gain to 1/1.01
    usleep(50000); // wait min. 10 mS
    for (i = 0; i < avgSamples; i++)
    {
        for (j = 0; j < retry; j++)
        {
            if ((ret = getAdcFpgaAvg(0, &(adcVal[i]))) > 0)
                break;
        }
        if (j >= retry) // failed to read Acc. value
            break;
        //printf("%d. 0x%X\n", (i+1), adcVal[i]);
        usleep(20000);
    }
    // done, restore register
    writeCtrlReg(2, 1, (void *)&reg2Val);
    // wait for ADC to settle, otherwise sensor-error may be triggered
    usleep(50000); 
    if (i < avgSamples)
        return -1;
    // compute voltages and find average..
    pCache->vG1Zero = 0.0; // reset so that scaleTADC() report right V
    if (calVerbose)
        printf("vG1Zero: %0.7f\n", pCache->vG1Zero);
    for (i = 0, sumV = 0.0; i < avgSamples; i++)
    {
        rVals[1] = adcVal[i];
        scaleTADC(rVals, fVals);
        adcVolts[i] = fVals[1];
        //printf("%d. %0.7f <- 0x%X\n", (i+1), adcVolts[i], adcVal[i]);
        sumV += adcVolts[i];
    }
    pCache->vG1Zero = sumV / (float)avgSamples;
    for (i = 0; i < avgSamples; i++) // check drift/noise
    {
        if (fabs(adcVolts[i] - pCache->vG1Zero) > (10.0/1000000.0))
            printf("%d. %0.7f - %0.7f > 10uV\n", (i+1), adcVolts[i], 
                                                    pCache->vG1Zero);
    }
    if (calVerbose)
        printf("vG1Zero: %0.7f\n", pCache->vG1Zero);
    return 0;
}

// MUST BE holding control register lock when calling this func.
static void verifySystemSetup()
{
    uint16_t reg;
    readCtrlReg(0, 1, &reg);
    if (!(reg & 0x200))
    {
        printf("Initializing..\n");
        setCtrlRegBits(0, 0x200, 0x200); // set BING_IO_DRIVE_EN
        doClkGenConfig("/fpga/register_map_clkgen.txt");
        //setCtrlRegBits(2, 0xC0, 0x80); // BING_DRTD_ISEL 0xb10
        setStimulusCurrent(pCache->stimI[0], 1); // 10uA by default
        setCtrlRegBits(3, 0x1, 0x1);   // BING_DRTD_SAMPLE
        printf("  Voltage range set to: 0 - 10V\n");
        setVoltageRange(10);
        pCache->maxVRange = 10;
        printf("   Done\n");
    }
}

// set scaling factor based on look-up table
static void setScaleFactor(int sType)
{
    uint8_t reg = 0;
    switch (sType)
    {
        case DT_470:
        case DT_670:
        case Type_K:
        case Type_T:
        case PRT:
            reg = 16;
            break;
        case Type_R:
            reg = 4;
            break;
    }
    writeCtrlReg(43, 1, (void *)&reg);
}

static void setFrontEndSel()
{
    uint8_t reg = 0;
    int sType = findSensorType(pCache->sensorType[0]);
    if (sType == SensorTypeDiode)
        reg = 0x20;
    setCtrlRegBits(37, 0x60, reg);
}

static int getSensorType(char *strArg)
{
    int sensType = 0;
    if (strcmp(strArg, "dt470") == 0)
        sensType = DT_470;
    else if (strcmp(strArg, "dt670") == 0)
        sensType = DT_670;
    else if (strcmp(strArg, "prt") == 0)
        sensType = PRT;
    else if (strcmp(strArg, "type_r") == 0)
        sensType = Type_R;
    else if (strcmp(strArg, "type_k") == 0)
        sensType = Type_K;
    else if (strcmp(strArg, "type_t") == 0)
        sensType = Type_T;
    else
    {
        logPrint("Error: invalid sensor type\n");
        sensType = -1;
    }
    return sensType;
}

// Validate sensor type: 2 sensor setup
static int validSensorType(int idx, int sType)
{
    int sKind = SensorTypeNone;
    int otherKind = SensorTypeNone, otherType;
    int otherIdx = 0;
    if ((idx < 0) || (idx > 1))
        return 0;
    // validate this one first
    if ((sType == Type_T) ||
        (sType == Type_K) ||
        (sType == Type_R))
       sKind = SensorTypeTC;
    if ((sType == PRT) ||
        (sType == DT_470) ||
        (sType == DT_670))
       sKind = SensorTypeDiode;
    if (sKind == SensorTypeNone)
        return 0;
    // make sure two sensors are valid combination:
    // sensor 1: TC,        sensor 2: Diode/PRT
    // sensor 1: Diode/PRT, sensor 2: TC
    if (idx == 0)
        otherIdx = 1;
    //printf("%s: new sKind %d, idx: %d other idx %d\n",__func__,
    //                                sKind, idx, otherIdx);
    // find out this fone..
    if ((otherType = pCache->sensorType[otherIdx]) == SensorTypeNone)
        return 1;
    if ((otherType == Type_T) ||
        (otherType == Type_K) ||
        (otherType == Type_R))
       otherKind = SensorTypeTC;
    if ((otherType == PRT) ||
        (otherType == DT_470) ||
        (otherType == DT_670))
       otherKind = SensorTypeDiode;
    if (otherKind == sKind)
        return 0;
    return 1;
}

static int getCJCEnable(int idx)
{ 
    // valid idx are 0 and 1
    int sensType1 = findSensorType(pCache->sensorType[idx]);
    int sensType2 = findSensorType(pCache->sensorType[!idx]);
    if (sensType1 == SensorTypeTC)
        pCache->cjcIdx = idx;
    else if (sensType2 == SensorTypeDiode)
        pCache->cjcIdx = (!idx);
    if ((sensType1 == SensorTypeTC) || (sensType2 == SensorTypeTC))
        return 1; 
    return 0;
}

double CalcTCTemp(int idx, double v)
{
    v += pCache->CJCFact; // do CJ compensation
    //printf("%s: v %0.5f\n", __func__, v);
    if (pCache->sensorType[idx] == Type_R)
        return tcTypeRTemp(v);
    else if (pCache->sensorType[idx] == Type_K)
        return tcCalTemp(CAL_TYPE_K, v);
    else if (pCache->sensorType[idx] == Type_T)
        return tcCalTemp(CAL_TYPE_T, v);
    return 0.0;
}

// MUST BE holding REGISTER LOCK when calling this function
// Compute temperature(used only for secondary sensor now, 07132015)
static float lookupTemp(int idx, int sType, double *volt)
{
    int dType = UnknownSensor;
    if (sType == SensorTypeNone)
        return 0.0; // 0 deg Kelvin
    //logPrint("%s: idx %d sType %d, V %0.7f\n", __func__, idx, sType, *volt);
    if (sType == SensorTypeTC)
        return CalcTCTemp(idx, *volt);
    else if (sType == SensorTypeDiode)
    {
        dType = pCache->sensorType[idx];
        if ((dType == DT_470) || (dType == DT_670))
        {
            //logPrint("1.  %s..\n",__func__);
            return findDiodeTemp(dType, *volt);
        }
        else
        {
            double R;
            R = rPrec * (((*volt) - pCache->vRtdZero) /
                            (pCache->vPrecStim - pCache->vPrecZero));
            return prtLookupTemp(R);
        }
    }
    else 
        return 0.0;
}

// MUST BE holding REGISTER LOCK when calling this function
static float getFPGATemp(int *fpgaT)
{
    uint16_t fpgaRegs[9]; // temp + accumulated ADC + number of samples 
    int64_t sumADC;
    int32_t avg;
    int numSamples;
    int sType, chan;
    float volt;
    readStatRegFull(53, 9, fpgaRegs);
    *fpgaT = ((fpgaRegs[2] & 0xff) << 16) |
                        ((fpgaRegs[1] & 0xFF) << 8) | (fpgaRegs[0] & 0xff);
    numSamples = (int)fpgaRegs[8];
    sumADC = ((((int64_t)(fpgaRegs[7]) & 0x0f) << 32) |
              (((int64_t)(fpgaRegs[6]) & 0xff) << 24) |
              (((int64_t)(fpgaRegs[5]) & 0xff) << 16) |
              ((fpgaRegs[4] & 0xff) << 8) | (fpgaRegs[3] & 0xff));
    avg = computeAccADCAvg(sumADC, numSamples);
    sType = findSensorType(pCache->sensorType[0]); // primary sensor
    if (sType == SensorTypeTC)
        chan = T_ADC_CHAN_TC;
    else if (sType == SensorTypeDiode)
        chan = T_ADC_CHAN_DRTD;
    else
        chan = T_ADC_CHAN_PREAMP;
    volt = scaleTADCVal(chan, avg);
    return volt;
}

// Reset sensor data; ONLY used for calibration

void resetSensors()
{
    pCache->enableCJC = 0;
    sleep(1);
    pCache->sensorType[0] = UnknownSensor;
    pCache->sensorType[1] = UnknownSensor;
}

char *diagGetSensorStr(int idx)
{
    static char sStr[32];
    sStr[0] = '\0';
    if ((idx < 0) || (idx > 1))
        return sStr;
    switch (pCache->sensorType[idx])
    {
        case DT_470:
            strcpy(sStr, "DT 470");
            break;
        case DT_670:
            strcpy(sStr, "DT 670");
            break;
        case Type_T:
            strcpy(sStr, "Type T");
            break;
        case Type_R:
            strcpy(sStr, "Type R");
            break;
        case Type_K:
            strcpy(sStr, "Type K");
            break;
        default:
            break;
    }
    return sStr;
}

// Caller MUST be holding control register lock
// idx: index into sensor table
int diagGetTemperature(int idx, float *temp, float *v)
{
    double mV;
    int fpgaT;
    int sType, ret = 0;
    int chan;

    if ((idx < 0) || (idx > 1))
        return -1;
    sType = findSensorType(pCache->sensorType[idx]);
    if (sType == SensorTypeTC)
        chan = T_ADC_CHAN_TC;
    else if (sType == SensorTypeDiode)
        chan = T_ADC_CHAN_DRTD;
    else
        return -1;

    if (idx == 0)
        *v = getFPGATemp(&fpgaT);
    else
        getAvgTADCVolt(chan, 1000, 0, 1, &mV);
        //getAvgTADCVolt(chan, 1000, 0, &mV);
    if (idx == 0)
    {
        *temp = fpgaT/1000.0; // milli-K -> K
        if (*temp >= 2096.0)
            *temp = 0.0;
    }
    else
    {
        *temp = lookupTemp(idx, sType, &mV);
        *v = (float)mV;
    }
    if (sType == SensorTypeTC)
        *v += pCache->CJCFact;
    return 0;
}

// caller MUST be holding control register lock
int diagSetSensorType(int idx, int sType)
{
    int ret = 0;
    int enable = 0;
    if (validSensorType(idx, sType) == 0)
        return -1;
    if ((sType == Type_R) || (sType == Type_K) || (sType == Type_T))
        enable = 1;
    pCache->sensorType[idx] = sType;
    verifySystemSetup();
    // set stimulus current to be routed to external diode/rtd
    setDRTDCalSel1(DRTD_CAL_NORM);
    if (pCache->sensorType[0] != PRT)
        setDRTDGain(GAIN_X1); // set gain to 1, except for PRT 
    // MUST be done before loading calibration table
    if (pCache->sensorType[0] == PRT)
        doPRTCalibration();
    else 
    {
        if (enable == 1) // it is a TC type
            doTCCalibration();
        else
        {
            ret = doDiodeCalibration();
            if (ret != 0)
                printf("ERROR: Failed to compute Vg1Zero\n");
        }
    }
    // load Calibration table, only if index is 0
    if (idx == 0)
    {
        if (loadCalData(pCache->sensorType[0]) != 0)
        {
            printf("Error: loading calibration data\n");
            return -1;
        }
    }
    if (idx == 0)
    {
        setScaleFactor(sType);
        setFrontEndSel();
        setCtrlRegBits(37, 0x1, 0x1); // enable averaging of ADC readings
    }
    // enable EEPROM PWR_EN for CJC read-back
    enable = getCJCEnable(idx);
    ret = setCtrlRegBits(0, 0x400, ((enable) ? 0x400 : 0x0));
    if (ret != 0)
        printf("Error: %sable EPROM PWR\n", ((enable) ? "en" : "dis"));
    // set CJC control and Scaling info
    // start/stop CJC task
    if (enable) 
        pCache->enableCJC = 1;
    else
        pCache->enableCJC = 0;
    return ret;
}

#define CJC_RAM_SZ (2048) // 4 byte locations
static int setCJCData(int loc, int data)
{
    uint16_t regSpace[sizeof(bingMachineCtrl)];
    bingMachineCtrl *machCtrl = (bingMachineCtrl *)regSpace;
    int regOffs = (41+1) * 2;
    int ret = 0;

    if ((loc < 0) || (loc >= CJC_RAM_SZ))
    {
        logPrint("Error: invalid location(%d)\n", loc);
        return -1;
    }
    data &= 0x1FFFFF; // make it 21 bit clean..
    machCtrl = (bingMachineCtrl *)regSpace;
    if (lockCtrlReg() != 0)
    {
        logPrint("Error: failed to acquire lock\n");
        return -1;
    }
    readMachCtrlRegs(machCtrl, regOffs);
    regSpace[35] = (loc & 0xff) << 8;
    regSpace[36] = ((loc >> 8) & 0xff) << 8;
    regSpace[38] = (data & 0xff) << 8;
    regSpace[39] = ((data >> 8) & 0xff) << 8;
    regSpace[40] = ((data >> 16) & 0xff) << 8;
    regSpace[41] = ((data >> 24) & 0xff) << 8;
    regSpace[37] = 0xC << 8;
    updateMachCtrlRegs(machCtrl, regOffs);
    updateSync(SYNC_REGS);
    regSpace[37] = 0x0;
    updateMachCtrlRegs(machCtrl, regOffs);
    updateSync(SYNC_REGS);
    unlockCtrlReg();
    return 0;
}

static int setStimulusVal(int sVal)
{
    int stimReg;
    int ret;
    switch (sVal)
    {
        case 0:
            stimReg = STIM_0uA;
            break;
        case 1:
            stimReg = STIM_1uA;
            break;
        case 10:
            stimReg = STIM_10uA;
            break;
        case 100:
            stimReg = STIM_100uA;
            break;
        case -1:
            stimReg = STIM_1uA_N;
            break;
        case -10:
            stimReg = STIM_10uA_N;
            break;
        case -100:
            stimReg = STIM_100uA_N;
            break;
        default:
            return -1;
    }
    if (lockCtrlReg() != 0)
    {
        printf(errLockMsg, "ctrl reg");
        return -1;
    }
    setStimulusCurrent(stimReg, 1);
    unlockCtrlReg();
    return 0;
}

static int doSetSensorType(int idx, int sensType)
{
    int ret = 0;
    calVerbose = 1;
    if (lockCtrlReg() != 0)
    {
        logPrint("Error: failed to acquire lock\n");
        return -1;
    }
    ret = diagSetSensorType(idx, sensType);
    unlockCtrlReg();
    calVerbose = 0;
    return ret;
}

/*
"set various device registers\n"\
"Usage: set <h-dac> <value>\n"\
"\th-dac: Heater DAC\n";
*/
static int doSetCmd(int nargs, char **arg)
{
    int ret = 0;
    int chan = -1;
    float value;
    int iVal;
    char *strArg = 0;
    if (nargs > 1)
    {
        //printf("%s: %s %d\n", __func__, arg[0], nargs);
        if (strcmp(arg[0], "relay") == 0)
        {
            nargs--;
            return doRelayCmd(nargs, &arg[1]);
        }
        if (strcmp(arg[0], "auxdac") == 0)
        {
            nargs--;
            return doMCP4726SetCmd(nargs, &arg[1]);
        }
        if (nargs == 3) // channel and value / location & data for cjc
        {
            if (convToInt(arg[1], &chan) != 0)
                return -1;
            if (cvtToFloat(arg[2], &value) != 0)
                return -1;
        }
        else
        {
            if ((strcmp(arg[0], "ground") == 0) ||
                (strcmp(arg[0], "diode") == 0) ||
                (strcmp(arg[0], "sensor") == 0))
                strArg = arg[1];
            else
            {
                if (strcmp(arg[0], "vrange") == 0)
                {
                    if (convToInt(arg[1], &iVal) != 0)
                        return -1;
                }
                else if (strcmp(arg[0], "stimulus") == 0)
                {
                    int absVal;
                    if (convToInt(arg[1], &iVal) != 0)
                        return -1;
                    absVal = abs(iVal);
                    if ((absVal != 0) && (absVal != 1) && (absVal != 10) &&
                            (absVal != 100))
                    {
                        printf("Invalid value for Stimulus\n");
                        return -1;
                    }
                }
                else if (cvtToFloat(arg[1], &value) != 0)
                    return -1;
            }
        }
        if (strcmp(arg[0], "h-dac") == 0)
            ret = setHDAC(value);
        else if (strcmp(arg[0], "trimdac") == 0)
        {
            if (nargs != 3)
            {
                printf("Incorrect number of arguments\n");
                return -1;
            }
            if ((chan < 1) || (chan > 4))
            {
                printf("Error: invalid channel number\n");
                return -1;
            }
            ret = doTrimDacCmd("set", chan, &value);
        }
        else if (strcmp(arg[0], "cjcdata") == 0)
            setCJCData(chan, value);
        else if (strcmp(arg[0], "vrange") == 0)
            ret = setVRange(iVal); 
        else if (strcmp(arg[0], "stimulus") == 0)
            ret = setStimulusVal(iVal);
        else if (strcmp(arg[0], "ground") == 0)
            ret = setGrounding(strArg);
        else if ((strcmp(arg[0], "diode") == 0) ||
                 (strcmp(arg[0], "sensor") == 0))
        {
            int sType = getSensorType(strArg);
            if (sType == -1)
            {
                printf("Error: invalid sensor type\n");
                return -1;
            }
            ret = doSetSensorType(0, sType);
        }
        else if (strcmp(arg[0], "vmax") == 0)
        {
            if (((int)value < 0) || ((int)value > pCache->maxVRange))
            {
                logPrint("Error: invalid value for vmax\n");
                return -1;
            }
            pCache->vMax = value;
        }
        else if (strcmp(arg[0], "pmax") == 0)
        {
            if ((value < 0) || (value > 100.00)) // max 100W?
            {
                printf("value out of limit[0 - 100]\n");
                return -1;
            }
            pCache->pMax = value;
        }
        else if (strcmp(arg[0], "imax") == 0)
        {
            if ((value < 0) || (value > 2.500)) // max 2.5A
            {
                printf("value out of limit[0 - 2.5]\n");
                return -1;
            }
            pCache->iMax = value;
        }
        else
        {
            logPrint("Error: invalid option\n");
            ret = -1;
        }
    }
    else
    {
        printf("%s\n", helpSetCmd);
        ret = -1;
    }
    return ret;
}

static int setCtrlRegBit(int reg, int bit, int bVal)
{
    int ret = 0;
    uint32_t offset, value;
    uint16_t regSpace[sizeof(bingMachineCtrl)];
    uint16_t regVal;
    bingMachineCtrl *machCtrl = (bingMachineCtrl *)regSpace;
    if (lockCtrlReg() != 0)
    {
        printf(errLockMsg, "ctrl reg");
        return -1;
    }
    offset = reg;
    readMachCtrlRegs(machCtrl, ((offset+1)*2));
    if (bVal)
        regSpace[offset] |= ((1 << bit) << 8);
    else
        regSpace[offset] &= ~((1 << bit) << 8);
    // update strobe bit in register 0
    updateMachCtrlRegs(machCtrl, ((offset+1)*2));
    updateSync(SYNC_REGS);
errSetCtrlReg:
    unlockCtrlReg();
    return ret;
}

static void scaleTADC(uint32_t *regVals, double *fVals)
{
    int i;
    for (i = 0; i < 3; i++)
        computeTADCVoltage(regVals[i], &(fVals[i]));
    // TC voltage calibration:
    fVals[0] -= (pCache->tcVZero + pCache->tcVMiss);
    fVals[0] /= pCache->tcGTC; // thermocouple: value is scaled
    // diode/DRTD voltage calibration:
    //fVals[1] *= 1.01;     // diode/prt: account for 100/101 divider
    fVals[1] /= pCache->G1drtd;   // diode/prt: account for 100/101 divider
    fVals[1] -= pCache->vG1Zero;
}

// rawV is the unscaled voltage
static void pScaleTADC(int chan, uint32_t *regVals, double *fVals, double 
                                    *rawV)
{
    int i;
    for (i = 0; i < 3; i++)
        computeTADCVoltage(regVals[i], &(fVals[i]));
    *rawV = fVals[chan-1];
    // TC voltage calibration:
    fVals[0] -= (pCache->tcVZero + pCache->tcVMiss);
    fVals[0] /= pCache->tcGTC; // thermocouple: value is scaled
    // diode/DRTD voltage calibration:
    //fVals[1] *= 1.01;     // diode/prt: account for 100/101 divider
    fVals[1] /= pCache->G1drtd;     // diode/prt: account for 100/101 divider
    fVals[1] -= pCache->vG1Zero;
}


float findTCTemp(int sType, double v);
static double R;
// sType: one of SensorTypeDiode, SensorTypeTC etc
// sIdx: index of sensor in sensorType[]
static float findSensorTemp(int sType, int sIdx, double *volts)
{
    float val;
    double v;
    if (sType == SensorTypeDiode)
    {
        if (pCache->sensorType[sIdx] == PRT)
        {
            R = rPrec * ((volts[1] - pCache->vRtdZero) / 
                                     (pCache->vPrecStim - pCache->vPrecZero)); 
            //printf("V: %0.6f -> R: %0.5f\n", volts[1], R);
            // now find temp corresponding to resistance
            val = prtLookupTemp((float)R);
        }
        else
        {
            val = findDiodeTemp(pCache->sensorType[sIdx], volts[1]);
        }
    }
    else if (sType == SensorTypeTC)
    {
        volts[0] += pCache->CJCFact;
        val = findTCTemp(pCache->sensorType[sIdx], volts[0]);
        //printf("%s: %0.5f = %0.5f + %0.5f val %0.5f\n", __func__, v, 
        //                           volts[0], pCache->CJCFact, val);
    }
    else
        val = 0.0;
    return val;
}

/*
 * Read t-ADC channel several times, compute average and return it.
 * Also read FPGA's corrected temp. for comparision.
 * Later we will use only FPGAs temperature read-back
 */
#define RD_NUM_SAMPLES (100)
static int getT_ADCAvg(int chan, int32_t *avg, float *fpgaTemp)
{
    int32_t adcVals[RD_NUM_SAMPLES];
    int32_t sum;
    uint8_t tFpga[3];
    int32_t avgTemp;
    int i;
    if (lockCtrlReg() != 0)
    {
        logPrint("Error: failed to acquire lock\n");
        return -1;
    }
    readT_ADCValues(chan, RD_NUM_SAMPLES, adcVals);
    readStatReg(53, 3, (void *)tFpga);
    unlockCtrlReg();
    avgTemp = (tFpga[2] << 16) | (tFpga[1] << 8) | tFpga[0];
    *fpgaTemp = (avgTemp / 1000.0); // change to K from  milli-K
    // sign extend the 24-bit signed values, we use a 4byte signed integer.
    for (i = 0; i < RD_NUM_SAMPLES; i++)
    {
        if ((uint32_t)adcVals[i] > 0x7FFFFF)
            adcVals[i] |= 0xFF000000;
    }
    sum = 0;
    // find average of adcVals[] and compute voltage based on the avg.
    // filter outliers..
    {
        int j, k, count = 0;
        for (i = 0; i < RD_NUM_SAMPLES; i++)
        {
            //printf("ADC rd %d: 0x%X\n", i, adcVals[i]);
            if (i == 0)
                { j = 1; k = 2; }
            else if (i == (RD_NUM_SAMPLES-1))
                { j = (RD_NUM_SAMPLES-2); k = (RD_NUM_SAMPLES-3); }
            else
                { j = i-1; k = i+1; }
            if ((abs(adcVals[i] - adcVals[j]) > 4096) &&
                (abs(adcVals[i] - adcVals[k]) > 4096))
            {
                //printf("  SKIP %d\n", i);
                continue;
            }
            sum += adcVals[i];
            count++;
        }
        //printf("Sum %d, Count: %d\n", sum, count);
        *avg  = (sum)/count;
        if ((uint32_t)(*avg) > 0x7FFFFF)
            (*avg) &= 0xFFFFFF;
    }
    //printf("Avg. ADC Val 0x%X\n", *avg);
    return 0;
}

// print channels of AD1274, fpga i/f
static int printT_ADC(int chan)
{
    int32_t avg;
    uint16_t statReg;
    uint32_t rVal[3];
    double fVals[3], unScaledV;
    float cVal, fpgaT;
    int sType, ret = 0;
    sType = findSensorType(pCache->sensorType[0]);
    if ((chan < 1) || (chan > 3))
    {
        if (sType == SensorTypeDiode)
            chan = 2;
        else if (sType == SensorTypeTC)
            chan = 1;
        else 
            chan = 3;
    }
    if (getT_ADCAvg(chan, &avg, &fpgaT) < 0)
        return -1;
//printf("t_adc_avg: chan %d, avg %u, fpgaT %0.6f\n", chan, avg, fpgaT);
    if (lockCtrlReg() != 0)
        return -1;
    ret = getAdcFpgaAvg(0, &(rVal[chan-1]));
//printf("fpga_avg: chan %d, avg %u, num_samples %d\n", 
//                          chan, rVal[chan-1], ret);
    readStatRegFull(0, 1, statReg);
    unlockCtrlReg();
    if (ret <= 0) 
    {
        printf("Unable to retrieve ADC values from FPGA\n");
        return -1;
    }
    //scaleTADC(rVal, &fVals[0]);
    pScaleTADC(chan, rVal, &fVals[0], &unScaledV);
//printf("scaled_vals: rVal: %u, fVal %0.6f, unscaled %0.6f\n",
//                    rVal[chan-1], fVals[chan-1], unScaledV);
    cVal = findSensorTemp(sType, 0, &fVals[0]);
    if (chan == 1)
    {
        if (sType == SensorTypeTC)
        {
            printf("Temp: %0.7f K  [ %0.7f : 0x%x , %0.6f ] S: %d\n", 
                                cVal, fVals[0], rVal[0], unScaledV, ret);
        }
        else
            printf("OUT1: %0.7f [ 0x%x , %0.6f] S: %d\n", fVals[0], rVal[0], unScaledV, ret);
    }
    if (chan == 2)
    {
        if (sType == SensorTypeDiode)
        {
            if (pCache->sensorType[0] == PRT)
            {
#if 0
            printf("Temp: %0.7f K  [ %0.7f : 0x%x ] [ R: %0.5f ] S: %d [fpgaT: %0.6f]\n",
                                    cVal, fVals[1], rVal[1], R, ret, (fpgaT/1000.0));
#else
            printf("Temp: %0.7f K  [ %0.7f : 0x%x , %0.6f ] [ R: %0.5f ] S: %d\n",
                                    cVal, fVals[1], rVal[1], unScaledV, R, ret);
#endif
            }
            else
            {
#if 0
            printf("Temp: %0.7f K  [ %0.7f : 0x%x ] S: %d [fpgaT: %0.6f]\n",
                                    cVal, fVals[1], rVal[1], ret, (fpgaT/1000.0));
#else
            printf("Temp: %0.7f K  [ %0.7f : 0x%x , %0.6f ] S: %d\n",
                                    cVal, fVals[1], rVal[1], unScaledV, ret);
#endif
            }
        }
        else
            printf("OUT2: %0.7f [ 0x%x , %0.6f ] S: %d\n", fVals[1], rVal[1], unScaledV, ret);
    }
    if (chan == 3) 
        printf("OUT3: %0.7f [ 0x%x , %0.6f ] S: %d\n", fVals[2], rVal[2], unScaledV, ret);
    if ((ret < 1428) || (ret > 2500))
        printf("\nWARNING: Uexpected number of Samples(%d)in mains cycle\n",
                                                ret);
    if ((statReg & 0x100)) // main sync, low when sync present
        printf("\nWARNING: No Mains SYNC\n");
    return 0;
}

static int fetchHeaterValues(float *V, uint32_t *rawV, float *I, uint32_t *rawI)
{
    int ret = 0;
    if (lockCtrlReg() != 0)
    {
        printf(errLockMsg, "ctrl reg");
        return -1;
    }
    ret = getHeaterValues(V, rawV, I, rawI);
    unlockCtrlReg();
    return ret;
}

// print channels of AD7683 - fpga i/f
static int printHeaterADC(int chan)
{
    int ret;
    float v, I;
    uint32_t rawV, rawI;
    ret = fetchHeaterValues(&v, &rawV, &I, &rawI);
    if (ret == 0)
        printf("h-dac: %0.6f V [0x%x]   %0.6f A  [0x%x]\n", v, rawV, I, rawI);
    return 0;
}

// max +ve value for 40 bit signed integer excl. sign bit
// max +ve and least -ve values changed for FPGA rev. 0x4b(09/28/2015)
#define MAX_INT40BIT  (0x7FFFFFFFFFLL)
// most -ve 40bit signed bit in 64bits
#define LEAST_INT40BIT (0xFFFFFF8000000000LL) 

// Make a 8Byte integer out of 40 bit values from FPGA. MSB is sign bit
static long long int getPIDErrITerm(int64_t rawVal)
{
    long long int tmpErr;
    long long int iErr;
    if (rawVal > MAX_INT40BIT)    //  integral error is 40 bit, MSB sign bit
    {
        //printf("0x%llX : ", rawVal);
        tmpErr = ((~(rawVal & MAX_INT40BIT)) + 1); // take two's complement
        iErr = tmpErr & MAX_INT40BIT; // extract  39 bits
        if (iErr)
            iErr *= -1; // apply sign
        else
            iErr = LEAST_INT40BIT; // make it largest 40 bit -ve value
    }
    else
        iErr = rawVal;
    //printf("I: 0x%llX\n", iErr);
    return iErr;
}

// Make a 4Byte integer out of 22 bit values from FPGA. MSB is sign bit
static int getPIDErrDTerm(int rawVal)
{
    int tmpErr;
    int iErr;
    if (rawVal > 0x1FFFFF)      //  D term is 22 bit, MSB sign bit
    {
        //printf("0x%X : ", rawVal);
        tmpErr = ((~(rawVal & 0x1FFFFF)) + 1); // take two's complement
        iErr = tmpErr & 0x1FFFFF; // extract  21 bits
        if (iErr)
            iErr *= -1; // apply sign
        else
            iErr = 0xFFE00000; // make it largest 21 bit -ve value
    }
    else
        iErr = rawVal;
    //printf("D: 0x%X\n", iErr);
    return iErr;
}

static int doStatsCmd(int verbose)
{
    uint32_t offset = 63;
    volatile uint8_t *pMem;
    uint8_t machStatBuf[MACH_STAT_BUF_SZ];
    uint16_t *machRPtr;
    int i, numBytes, sType;
    float curr, V;
    uint32_t rawCurr, rawV; 
    int64_t iErr = 0, rawIErr; // 40 bit integral error
    int dTerm = 0, rawD;
    int avgTemp, ret, val;
    uint16_t pidOP = 0;
    float T1;
    uint8_t rVals[32]; // all PID debug info..

    if (lockCtrlReg() != 0)
    {
        printf(errLockMsg, "ctrl reg");
        return -1;
    }
    getHeaterValues(&V, &rawV, &curr, &rawCurr);
    // read fpga registers for other stats
    pMem = (volatile uint8_t *)pFpgaMem;
    numBytes = (offset+1) * 2;
    updateSync(SYNC_REGS);
    RESET_CH_CTR(pMem);
    // read necessary number of status reg. bytes..
    for (i = 0; i < numBytes; i++)
         machStatBuf[i] = pMem[MACH_STAT_CH];
    unlockCtrlReg();

    machRPtr = (uint16_t *)machStatBuf;
    for (i = 0; i < numBytes/2; i++)
        machRPtr[i] = bswap_16(machRPtr[i]);

    // Integral error
    //printf("%X : %X : %X : %X: %s ", machRPtr[28], machRPtr[34], machRPtr[33],
    //        machRPtr[32], ((machRPtr[28] & 0x8) ? "-ve" : ""));
    rawIErr = (((int64_t)(machRPtr[28]) & 0xF) << 36) | 
                (((int64_t)(machRPtr[34]) & 0xFFF) << 24) |
                (((int64_t)(machRPtr[33]) & 0xFFF) << 12) | 
                ((int64_t)(machRPtr[32]) & 0xFFF);
    iErr = getPIDErrITerm(rawIErr);
    // D Term
    rawD = ((machRPtr[63] & 0x3FF) << 12) | (machRPtr[62] & 0xFFF);
    dTerm = getPIDErrDTerm(rawD);

    avgTemp = ((machRPtr[55] & 0xff) << 16) | 
                    ((machRPtr[54] & 0xff) << 8) | (machRPtr[53] & 0xff);
    T1 = (avgTemp / 1000.0); // milli-K -> K
    if (T1 > 2096.0) // sentinel value used by FPGA(2097.151)
        T1 = 0.0;
    // pid output
    pidOP = (((machRPtr[20] & 0xff) << 8) | (machRPtr[19] & 0xff));
    // set point
    //printf("[%X : %X : %X] ", machRPtr[26], machRPtr[25], machRPtr[24]);
    val = (((machRPtr[26] & 0xff) << 16) | 
           ((machRPtr[25] & 0xff) << 8) | 
           (machRPtr[24] & 0xff));
#if 1
    printf("T: %0.3f K  h-ADC: %0.4f V  %0.4f A  IErr: %lld[0x%llX]  DTerm: %d  O/P: %hu SetPt: %d\n", 
                     T1, V, curr, iErr, rawIErr, dTerm, pidOP, val);
#else
    printf("T: %0.3f K  Htr: %0.6f V  %0.5f A  I: %lld[0x%llX]  D: %d  O/P: %hu ", 
                                T1, V, curr, iErr, rawIErr, dTerm, pidOP);
//--------
    if (lockCtrlReg() != 0)
    {
        printf("Error: failed to obtain lock\n");
        return -1;
    }
    ret = readStatReg(19, ((47-19)+1), (void *)&rVals[0]);
    unlockCtrlReg();
    if (ret != 0)
    {
        printf("Error: reading status regs(%d)\n", ret);
        return ret;
    }
    // print params..
    i = 24-19;
    printf(" SetPt: %d ", 
                    ((rVals[i+2] << 16) | (rVals[i+1] << 8) | rVals[i]));
    // iGain(32bit, 05/14/2015)
    i = 36-19;
    val = ((rVals[i+2] & 0xff) << 24) | ((rVals[i+1] & 0xfff) << 12) | 
                            (rVals[i] & 0xfff);
    i = 27-19;
    printf(" P/I/D: %d/%d/%d ", rVals[i], val, rVals[i+2]);
    i = 30-19;
    printf(" OvflErr: 0x%X ", rVals[i]);
    printf(" COutErr: 0x%X ", rVals[i+1]);

    i = 35-19;
    printf(" CtrlReg: 0x%X ", rVals[i]);

    i = 39-19;
    printf(" CombErr: 0x%X ", rVals[i], rVals[i]);

    i = 45-19;
    printf("Corr[Ctrl|Stat|Err] : 0x%X 0x%X 0x%X\n", rVals[i], rVals[i+1], rVals[i+2]);
//--------
#endif
    return 0;
}

static int printGndVrange(int gnd)
{
    uint8_t bitVal;
    int offset;
    uint16_t regSpace[sizeof(bingMachineCtrl)];
    bingMachineCtrl *machCtrl = (bingMachineCtrl *)regSpace;
    if (lockCtrlReg() != 0)
    {
        printf(errLockMsg, "ctrl reg");
        return -1;
    }
    offset = 1;  // control reg 1
    readMachCtrlRegs(machCtrl, ((offset+1)*2));
    unlockCtrlReg();
    bitVal = bswap_16(regSpace[offset]) & 0xff;
    if (gnd)
    {
        printf("Ground Sel: ");
        switch (bitVal & 0x6)
        {
            case 0:
                printf("Floating\n");
                break;
            case 2:
                printf("4mm Back panel socket\n");
                break;
            case 4:
                printf("Mains(Chassis)\n");
                break;
            case 6:
                printf("Buffered ground input\n");
                break;
        }
    }
    else
    {
        float val = getMaxVRangeQuick(bitVal);
        //printf("val %0.3f reg 0x%X\n", val, bitVal);
        printf("Voltage Range: ");
        if (val == 0)
            printf("0");
        else printf("0 - %d", (int)val);
        printf("V\n");
    }
}

static int printCJCData(int loc, int numRegs)
{
    uint16_t statRegs[sizeof(bingMachStatus)];
    bingMachStatus *machStat;
    uint16_t regSpace[sizeof(bingMachineCtrl)];
    bingMachineCtrl *machCtrl;
    machStat = (bingMachStatus *)statRegs;
    machCtrl = (bingMachineCtrl *)regSpace;
    uint32_t regVal;
    int i, ret = 0;
    int regOffs = (37 + 1) * 2;
    int statOffs = (51 + 1) * 2;

    numRegs += loc; // make it a location
    if (lockCtrlReg() != 0)
    {
        logPrint("Error: failed to acquire lock\n");
        return -1;
    }
    readMachCtrlRegs(machCtrl, regOffs);
    for (i = loc; i < numRegs; i++)
    {
        regSpace[35] = (i & 0xff) << 8;
        regSpace[36] = ((i >> 8) & 0xff) << 8;
        regSpace[37] = 8 << 8;
        updateMachCtrlRegs(machCtrl, regOffs);
        updateSync(SYNC_REGS);
        readMachStatRegs(machStat, statOffs);
        regVal = ((statRegs[51] >> 8) & 0xff) << 24 |
                  ((statRegs[50] >> 8) & 0xff) << 16 |
                  ((statRegs[49] >> 8) & 0xff) << 8 |
                   (statRegs[48] >> 8) & 0xff;
        printf("%d: %d [0x%X]\n", i, regVal, regVal);
        regSpace[37] = 0;
        updateMachCtrlRegs(machCtrl, regOffs);
        updateSync(SYNC_REGS);
    }
    unlockCtrlReg();
    return 0;
}

extern int calReadCJCRawVal(int *rawVal);
static int printCJCVoltage()
{
    int ret = 0;
    float val;
    int rVal;
    double T = 0.0;
    int bus = 2, devAddr = 0x68;
    int gain = 2;
    // read i2c device channel 1 to get voltage.
    if ((ret = mcp3424Read(bus, devAddr, 1, gain, &val)) < 0)
    {
        printf("Error reading CJC voltage\n");
        goto errCJCVolt;
    }
    val = val/10.0; // 10mv/degree, @0degC, o/p is 0V
    printf("CJC: %0.5f C -> ", val);
    switch (pCache->sensorType[0])
    {
        case Type_K:
            T = cjcTemp2tcVolt(CAL_TYPE_K, (double)val);
            break;
        case Type_R:
            T = cjcTemp2tcTypeRVolt((double)val);
            break;
        case Type_T:
            T = cjcTemp2tcVolt(CAL_TYPE_T, (double)val);
            break;
    }
    printf("%0.5f V\n", T);
#if 1
    if ((ret = mcp3424ReadRaw(bus, devAddr, 1, gain, &rVal)) == 0)
        printf("1. CJC reg: 0x%X\n", rVal);
    if ((ret = calReadCJCRawVal(&rVal)) == 0)
        printf("2. CJC reg: 0x%X\n", rVal);
#endif
    return 0;
errCJCVolt:
    return -1;
}

static int printPreAmpVoltage()
{
    uint8_t sRegs[9];
    uint32_t rVals[3];
    double fVals[3];
    // set read strobe bit
    if (lockCtrlReg() != 0)
    {
        printf("Error: acuiring control reg lock\n");
        return -1;
    }
    if (readStatReg(6, 9, (void *)&sRegs[0]) != 0)
        goto errPAVolt;
    //setCtrlRegBits(0, 0x4, 0);
    unlockCtrlReg();
    rVals[0] = 0;
    rVals[1] = 0;
    rVals[2] = (sRegs[8] << 16) | (sRegs[7] << 8) | sRegs[6];
    scaleTADC(rVals, fVals);
    printf("Pre-Amp Voltage: %0.4f\n", fVals[2]);
    return 0;
errPAVolt:
    unlockCtrlReg();
    return -1;
}

static int doGetRelayCmd()
{
    uint8_t rVal;
    //printf("%s\n", __func__);
    if (lockCtrlReg() != 0)
    {
        printf("Error: Failed to acquire lock\n");
        return -1;
    }
    readCtrlRegL(5, 1, (void *)&rVal);
    unlockCtrlReg();
    printf("Relay 1: %s  2 : %s\n", ((rVal & 0x2) ? "closed" : "open"),
                                    ((rVal & 0x4) ? "closed" : "open"));
    return 0;
}

/*
"Read device registers\n" \
"Usage: get <t-adc|h-adc> [chan|reg]\n"\
"\tt-adc: ADS1274, h-adc: Heater ADC\n"\
"\tPrints all registers with no register argument\n";
*/
static int doGetCmd(int nargs, char **arg)
{
    int ret = 0;
    int chan = -1; // sentinel
    int range = 1;
    float value;
    if (nargs > 0)
    {
        if (strcmp(arg[0], "relay") == 0)
            return doGetRelayCmd();
        else if (strcmp(arg[0], "auxdac") == 0)
            return doMCP4726GetCmd(--nargs, &arg[1]);
        if (nargs == 3)
        {
            if (convToInt(arg[1], &chan) != 0)
                return -1;
            if (convToInt(arg[2], &range) != 0)
                return -1;
        }
        else if (nargs == 2)
        {
            if (strcmp(arg[0], "stats") != 0)
            {
                if (convToInt(arg[1], &chan) != 0)
                    return -1;
            }
        }
        if (strcmp(arg[0], "t-adc") == 0) // ADS1274
            ret = printT_ADC(chan);
        else if (strcmp(arg[0], "h-adc") == 0)
            ret = printHeaterADC(chan);
        else if (strcmp(arg[0], "trimdac") == 0)
            ret = doTrimDacCmd("get", chan, &value);
        else if (strcmp(arg[0], "vrange") == 0)
            ret = printGndVrange(0);
        else if (strcmp(arg[0], "ground") == 0)
            ret = printGndVrange(1);
        else if ((strcmp(arg[0], "diode") == 0) ||
                 (strcmp(arg[0], "sensor") == 0))
        {
            if (pCache->sensorType[0] == DT_470)
                printf("dt470\n");
            else if (pCache->sensorType[0] == DT_670)
                printf("dt670\n");
            else if (pCache->sensorType[0] == Type_R)
                printf("Type R\n");
            else if (pCache->sensorType[0] == Type_T)
                printf("Type T\n");
            else if (pCache->sensorType[0] == Type_K)
                printf("Type K\n");
            else if (pCache->sensorType[0] == PRT)
                printf("PRT\n");
            else
                printf("Unknown\n");
        }
#if 0
        else if (strcmp(arg[0], "cjcdata") == 0)
        {
            if (nargs == 1)
                { chan = 0; range = 2048; }
            else
            {
            if ((chan < 0) || (chan >= CJC_RAM_SZ))
            {
                logPrint("Invalid CJC location(%d)\n", chan);
                return -1;
            }
            if ((chan + range ) > CJC_RAM_SZ)
            {
                logPrint("Invalid range(%d)\n", range);
                return -1;
            }
            }
            printCJCData(chan, range);
        }
#endif
        else if (strcmp(arg[0], "stats") == 0)
        {
            int verbose = 0;
            if (nargs > 1)
                verbose = 1;
            doStatsCmd(verbose);
        }
        else if (strcmp(arg[0], "cjcdata") == 0) // print cjc voltage..
        {
            ret = printCJCVoltage();
        }
        else if (strcmp(arg[0], "preamp") == 0) // print pre-amp input voltage..
        {
            ret = printPreAmpVoltage();
        }
        else if (strcmp(arg[0], "maxvals") == 0)
        {
            printf("vMax: %0.3f   iMax: %0.3f   pMax: %0.3f\n", pCache->vMax,
                                pCache->iMax, pCache->pMax);
        }
        else
        {
            logPrint("Error: invalid option\n");
            ret = -1;
        }
    }
    else
    {
        printf("%s\n", helpGetCmd);
        ret = -1;
    }
    return ret;
}

static int doLCDBrightCmd(int nargs, char **arg)
{
    int step;
    int regVal;
    int ret = 0;
    int bus = 2;
    if (nargs == 1)
    {
        if (convToInt(arg[0], &step) != 0)
            return -1;
        if ((step < 0) || (step > 10))
        {
            printf("%s\n", helpBright);
            return -1;
        }
        regVal = step * 0x199;
        if (regVal > 0xFFF)
            regVal = 0xFFF;
        if (lockI2CBus(bus) != 0)
        {
            printf("Error: lock i2c bus\n");
            return -1;
        }
        if ((ret = i2cmux_select_chan(4)) != 0)
        {
            fprintf(stderr, "Error selecting channel, %d\n", ret);
            unlockI2CBus(bus);
            return -1;
        }
        ret = setLCDBrightness(2, 0x10, regVal); 
        unlockI2CBus(bus);
        if (ret != 0)
            printf("Error setting LCD brightness, %d\n", ret);
    }
    return ret;
}

// set voltage limit(mMax) for PID output, front-end etc
static int setupPIDRun()
{
    uint16_t dacVal;  // dac reg. value corresponding to vMax
    uint8_t vPtr[4];
    int ret = 0;
    int sType = findSensorType(pCache->sensorType[0]);
    // set max PID output: find h-DAC value corresponding to vMax
    if (pCache->vMax < 0.001)
    {
        printf("Warning: using %d as vMax\n", pCache->maxVRange);
        pCache->vMax = (float)pCache->maxVRange; 
    }
    // full DAC value is same maxVRange, 0xFFFF == maxVRange
    //dacVal = (pCache->vMax / (float) pCache->maxVRange) * 0xFFFF;
    dacVal = (0xFFFF/getHeaterDACRange()) * pCache->vMax;
    vPtr[0] = dacVal & 0xff;
    vPtr[1] = (dacVal >> 8) & 0xff;
    writeCtrlReg(26, 2, (void *)vPtr);
    return ret;
}

// PID command start
enum { PID_SETPT, PID_KP, PID_KI, PID_KD, PID_DELAY, PID_ENABLE, PID_AVG };
static int setPIDParam(int setType, int value)
{
    int numBytes;
    int offset;
    int ret = 0;
    uint8_t vPtr[4];
    uint16_t rPtr[3];
    int sType;
    switch (setType)
    {
        case PID_SETPT:
            offset = 16;
            numBytes = 3;
            break;
        case PID_KP:
        case PID_KD:
            offset = 19 + (setType - PID_KP);
            numBytes = 1;
            break;
        case PID_KI:
            offset = 23;
            numBytes = 3;
            break;
        case PID_ENABLE:
            offset = 22;
            numBytes = 1;
            break;
        case PID_AVG:
            offset = 37;
            numBytes = 1;
            break;
        default:
            return -1;
            break;
    }
    if (numBytes > 4)
    {
        printf("Error: invalid number of BYTES\n");
        return -1;
    }
    if (lockCtrlReg() != 0)
    {
        printf("Error: failed to get lock\n");
        return -1;
    }
    if (setType == PID_ENABLE)
    {
        // set max voltage based on vMax first..
        if (value) // if enabling
        {
            if (setupPIDRun() != 0)
            {
                printf("Error: failed to set enable\n");
                return -1;
            }
        }
        if ((ret = setCtrlRegBits(offset, 0x1, ((value) ? 0x1 : 0x0))) != 0)
            printf("Error: failed to set enable\n");
        //set H-DAC output t0 0.
        if (!value)
        {
            if (setHeaterDAC(0.0) != 0)
                fprintf(stderr, "ERROR: failed to set output to 0\n");
        }
    }
    else
    {
        if ((setType == PID_KP) || (setType == PID_KD))
        {
            // set 12 bit PID values
            value &= 0xFFF;
            ret = setCtrlRegBits(offset, 0xFFFF, value);
        }
        else if (setType == PID_KI)
        {
            rPtr[0] = value & 0xfff;
            rPtr[1] = (value >> 12) & 0xfff;
            rPtr[2] = (value >> 24) & 0xff;
            //printf("%X : %X : %X\n", rPtr[2], rPtr[1], rPtr[0]);
            writeCtrlRegFull(offset, numBytes, rPtr);
        }
        else if (setType == PID_AVG)
        {
            if (value) // enable averaging..
                ret = setCtrlRegBits(offset, 0x1, 0x1);
            else
                ret = setCtrlRegBits(offset, 0x1, 0x0);
        }
        else
        {
            vPtr[0] = value & 0xff;
            vPtr[1] = (value >> 8) & 0xff;
            vPtr[2] = (value >> 16) & 0xff;
            vPtr[3] = (value >> 24) & 0xff;
            writeCtrlReg(offset, numBytes, (void *)vPtr);
            if (setType == PID_SETPT) // reset error reg.
            {
                ret = setCtrlRegBits(22, 0x2, 0x2);
                usleep(1000);
                ret = setCtrlRegBits(22, 0x2, 0x0);
            }
        }
    }
    unlockCtrlReg();
    return ret;
}

static int printPIDParams()
{
    uint16_t regVals[10]; // all PID params..
    uint16_t ctrlReg;
    int ret;
    int rVal;
    if (lockCtrlReg() != 0)
    {
        printf("Error: failed to obtain lock\n");
        return -1;
    }
    ret = readCtrlReg(16, 10, (void *)&regVals[0]);
    ret = readCtrlReg(37, 1, (void *)&ctrlReg);
    unlockCtrlReg();
    if (ret != 0)
    {
        printf("Error: reading control regs(%d)\n", ret);
        return ret;
    }
    // print params..
    printf("PID loop parameters:\n");
    printf("  Enabled      : %s\n", ((regVals[6] & 0x1) ? "Yes" : "No"));
    rVal = ((regVals[2] & 0xff) << 16) | ((regVals[1] & 0xff) << 8) | 
                 (regVals[0] & 0xff);
    printf("  Set Point    : %0.2f\n", (rVal/1000.0));
    rVal = ((regVals[9] & 0xff) << 24) | ((regVals[8] & 0xfff) << 12) | 
                 (regVals[7] & 0xfff);
    printf("  kP / kI / kD : %d %d %d\n", (regVals[3] & 0xfff), 
                                rVal, (regVals[5] & 0xfff));
    printf("  Averaging    : %s\n", ((ctrlReg & 0x1) ? "On" : "Off"));
    printf("\n");
    return 0;
}

static int printPIDDbg()
{
    uint16_t rVals[32]; // all PID debug info..
    int ret, val;
    int64_t rawIErr, iErr;  // integral error is now 40bit
    int i;
    if (lockCtrlReg() != 0)
    {
        printf("Error: failed to obtain lock\n");
        return -1;
    }
    ret = readStatReg(19, ((47-19)+1), (void *)&rVals[0]);
    unlockCtrlReg();
    if (ret != 0)
    {
        printf("Error: reading status regs(%d)\n", ret);
        return ret;
    }
    // print params..
    printf("PID info:\n");
    i = 19-19;
    printf("  PID Output  : %d [ 0x%X ]\n", ((rVals[i+1] << 8) | rVals[i]),
                                        ((rVals[i+1] << 8) | rVals[i]));
    i = 24-19;
    printf("  SetPt       : %d [ 0x%X ]\n", 
                    ((rVals[i+2] << 16) | (rVals[i+1] << 8) | rVals[i]),
                    ((rVals[i+2] << 16) | (rVals[i+1] << 8) | rVals[i]));
    // iGain(32bit, 05/14/2015)
    i = 36-19;
    val = ((rVals[i+2] & 0xff) << 24) | ((rVals[i+1] & 0xfff) << 12) | 
                            (rVals[i] & 0xfff);
    //printf("val %d rVals[i+2] 0x%X rVals[i+1] 0x%x rVals[i] 0x%x\n",
    //        val, (int)rVals[i+2], (int)rVals[i+1], (int)rVals[i]);
    i = 27-19;
    printf("  P/I/D       : %d / %d / %d\n", rVals[i], val, rVals[i+2]);

    i = 30-19;
    printf("  Ovrflow Err : %d [ 0x%X ]\n", rVals[i], rVals[i]);
    printf("  COut Err    : %d [ 0x%X ]\n", rVals[i+1], rVals[i+1]);

    i = 32-19;
    rawIErr = (((int64_t)(rVals[28-19]) & 0xF) << 36) | 
                    (((int64_t)(rVals[i+2]) & 0xfff) << 24) |
                    (((int64_t)(rVals[i+1]) & 0xfff) << 12) | 
                    ((int64_t)(rVals[i]) & 0xfff);
    iErr = getPIDErrITerm(rawIErr);
    printf("  Integral Err: %lld [ 0x%llX ]\n", iErr, rawIErr);

    i = 35-19;
    printf("  Control Reg : %d [ 0x%X ]\n", rVals[i], rVals[i]);

    i = 39-19;
    printf("  Combined Err: %d [ 0x%X ]\n", rVals[i], rVals[i]);

    i = 45-19;
    printf("  Corr. Ctrl  : %d [ 0x%X ]\n", rVals[i], rVals[i]);
    printf("  Corr. Stat  : %d [ 0x%X ]\n", rVals[i+1], rVals[i+1]);
    printf("  Corr. Err   : %d [ 0x%X ]\n\n", rVals[i+2], rVals[i+2]);
    return 0;
}

/*
"Usage: pid < setpt | kp | ki | kd | delay > < value >\n"\
"       pid < enable | disable >\n"\
"  Just the command without arguments prints the current settings\n";
*/
// The set point is passed in units of milli-kelvin. So this need not be
// a float value or a -ve number. All other params are also integral values.
static int doPIDCmd(int nargs, char **arg)
{
    int intVal;
    float fVal;
    int ret = 0;
    int avg = 0;
    if (nargs == 2) // attribute, value pair
    {
        if (strcmp(arg[0], "setpt") == 0)
        {
            if (cvtToFloat(arg[1], &fVal) != 0)
                return -1;
            fVal *= 1000.0; // convert to mK
            // convert temp. to milli-kelvin
            ret = setPIDParam(PID_SETPT, (int)(fVal));
            if (ret == 0)
               pCache->setPt = (int)fVal;
        }
        else if (strcmp(arg[0], "avg") == 0)
        {
            if (strcmp(arg[1], "on") == 0)
                avg = 1;
            ret = setPIDParam(PID_AVG, avg);
            if (ret == 0)
                pCache->pidAvg = avg;
            else
                printf("Error: set averaging mode\n");
        }
        else
        {
            if (convToInt(arg[1], &intVal) != 0)
                return -1;
            if (strcmp(arg[0], "kp") == 0)
            {
                ret = setPIDParam(PID_KP, intVal);
                if (ret == 0)
                    pCache->Kp = intVal;
            }
            else if (strcmp(arg[0], "ki") == 0)
            {
                ret = setPIDParam(PID_KI, intVal);
                if (ret == 0)
                    pCache->Ki = intVal;
            }
            else if (strcmp(arg[0], "kd") == 0)
            {
                ret = setPIDParam(PID_KD, intVal);
                if (ret == 0)
                    pCache->Kd = intVal;
            }
            else
                goto pid_err;
        }
    }
    else if (nargs == 1) // enable/disable
    {
        if ((strcmp(arg[0], "enable") == 0) ||
            (strcmp(arg[0], "on") == 0))
        {
            ret = setPIDParam(PID_ENABLE, 1);
            if (ret == 0)
                pCache->pidEnable = 1;
        }
        else if ((strcmp(arg[0], "disable") == 0) ||
                 (strcmp(arg[0], "off") == 0))
        {
            ret = setPIDParam(PID_ENABLE, 0);
            if (ret == 0)
                pCache->pidEnable = 0;
        }
        else if ((strcmp(arg[0], "dbg") == 0) ||
                 (strcmp(arg[0], "debug") == 0))
            ret = printPIDDbg();
        else
            goto pid_err;
    }
    else // print current params..
    {
        ret = printPIDParams();
    }
    return ret;
pid_err:
    printf("%s\n", helpPIDCmd);
    return -1;
}
// PID command END

static int fillCJCRAM(uint32_t value)
{
    uint16_t regSpace[sizeof(bingMachineCtrl)];
    bingMachineCtrl *machCtrl = (bingMachineCtrl *)regSpace;
    int regOffs = (41+1) * 2;
    int i, ret = 0;

    machCtrl = (bingMachineCtrl *)regSpace;
    if (lockCtrlReg() != 0)
    {
        logPrint("Error: failed to acquire lock\n");
        return -1;
    }
    readMachCtrlRegs(machCtrl, regOffs); 
    for (i = 0; i < CJC_RAM_SZ; i++)
    {
        regSpace[37] = 0x8 << 8;
        updateMachCtrlRegs(machCtrl, regOffs);
        updateSync(SYNC_REGS);
        regSpace[35] = (i & 0xff) << 8;
        regSpace[36] = ((i >> 8) & 0xff) << 8;
        regSpace[38] = (value & 0xff) << 8;
        regSpace[39] = ((value >> 8) & 0xff) << 8;
        regSpace[40] = ((value >> 16) & 0xff) << 8;
        //regSpace[41] = ((value >> 24) & 0xff) << 8;
        updateMachCtrlRegs(machCtrl, regOffs);
        updateSync(SYNC_REGS);
        regSpace[37] = 0xC << 8;
        updateMachCtrlRegs(machCtrl, regOffs);
        updateSync(SYNC_REGS);
    }
    regSpace[37] = 0;
    updateMachCtrlRegs(machCtrl, regOffs);
    updateSync(SYNC_REGS);
    unlockCtrlReg();
    return 0;
}

static int verifyCJCRAM(uint32_t value)
{
    uint16_t statRegs[sizeof(bingMachStatus)];
    bingMachStatus *machStat;
    uint16_t regSpace[sizeof(bingMachineCtrl)];
    bingMachineCtrl *machCtrl;
    machStat = (bingMachStatus *)statRegs;
    machCtrl = (bingMachineCtrl *)regSpace;
    uint32_t regVal;
    int i, ret = 0;
    int regOffs = (37 + 1) * 2;
    int statOffs = (51 + 1) * 2;
    if (lockCtrlReg() != 0)
    {
        logPrint("Error: failed to acquire lock\n");
        return -1;
    }
    readMachCtrlRegs(machCtrl, regOffs);
    for (i = 0; i < CJC_RAM_SZ; i++)
    {
        regSpace[35] = (i & 0xff) << 8;
        regSpace[36] = ((i >> 8) & 0xff) << 8;
        regSpace[37] = 8 << 8;
        updateMachCtrlRegs(machCtrl, regOffs);
        updateSync(SYNC_REGS);
        readMachStatRegs(machStat, statOffs);
        regVal = ((statRegs[51] >> 8) & 0xff) << 24 |
                  ((statRegs[50] >> 8) & 0xff) << 16 |
                  ((statRegs[49] >> 8) & 0xff) << 8 |
                   (statRegs[48] >> 8) & 0xff;
        if (regVal != value)
        {
            printf("Error: loc %d, mis-match(r: 0x%X, v: 0x%X)\n",
                    i, regVal, value);
            break;
        }
    }
    regSpace[37] = 0;
    updateMachCtrlRegs(machCtrl, regOffs);
    updateSync(SYNC_REGS);
    unlockCtrlReg();
    if (i >= CJC_RAM_SZ)
        printf("pattern [%X] verified OK\n", value);
    return 0;
}

static int fillCJCSequence(int numRegs)
{
    uint16_t regSpace[sizeof(bingMachineCtrl)];
    bingMachineCtrl *machCtrl = (bingMachineCtrl *)regSpace;
    int regOffs = (41+1) * 2;
    int i, ret = 0;
    uint32_t value = 0xF;

    machCtrl = (bingMachineCtrl *)regSpace;
    if (lockCtrlReg() != 0)
    {
        logPrint("Error: failed to acquire lock\n");
        return -1;
    }
    readMachCtrlRegs(machCtrl, regOffs);
    for (i = 0; i < numRegs/*CJC_RAM_SZ*/; i++)
    {
        regSpace[37] = 8 << 8;
        updateMachCtrlRegs(machCtrl, regOffs);
        updateSync(SYNC_REGS);
        regSpace[35] = (i & 0xff) << 8;
        regSpace[36] = ((i >> 8) & 0xff) << 8;
        regSpace[38] = (value & 0xff) << 8;
        regSpace[39] = ((value >> 8) & 0xff) << 8;
        regSpace[40] = ((value >> 16) & 0xff) << 8;
        //regSpace[41] = ((value >> 24) & 0xff) << 8;
        updateMachCtrlRegs(machCtrl, regOffs);
        updateSync(SYNC_REGS);
        regSpace[37] = 0xC << 8;
        updateMachCtrlRegs(machCtrl, regOffs);
        updateSync(SYNC_REGS);
        value <<= 4;
        if (value == 0)
            value = 0xF;
        printf("<-- %d\n", i);
    }
    regSpace[37] = 0;
    updateMachCtrlRegs(machCtrl, regOffs);
    updateSync(SYNC_REGS);
    unlockCtrlReg();
    return 0;
}

static int printCJCRAM(int numRegs)
{
    uint16_t statRegs[sizeof(bingMachStatus)];
    bingMachStatus *machStat;
    uint16_t regSpace[sizeof(bingMachineCtrl)];
    bingMachineCtrl *machCtrl;
    machStat = (bingMachStatus *)statRegs;
    machCtrl = (bingMachineCtrl *)regSpace;
    uint32_t regVal;
    int i, ret = 0;
    int regOffs = (37 + 1) * 2;
    int statOffs = (51 + 1) * 2;
    if (lockCtrlReg() != 0)
    {
        logPrint("Error: failed to acquire lock\n");
        return -1;
    }
    readMachCtrlRegs(machCtrl, regOffs);
    for (i = 0; i < numRegs /*CJC_RAM_SZ*/; i++)
    {
        regSpace[35] = (i & 0xff) << 8;
        regSpace[36] = ((i >> 8) & 0xff) << 8;
        regSpace[37] = 8 << 8;
        updateMachCtrlRegs(machCtrl, regOffs);
        updateSync(SYNC_REGS);
        readMachStatRegs(machStat, statOffs);
        regVal = ((statRegs[51] >> 8) & 0xff) << 24 |
                  ((statRegs[50] >> 8) & 0xff) << 16 |
                  ((statRegs[49] >> 8) & 0xff) << 8 |
                   (statRegs[48] >> 8) & 0xff;
        printf("%d: 0x%X\n", i, regVal);
    }
    regSpace[37] = 0;
    updateMachCtrlRegs(machCtrl, regOffs);
    updateSync(SYNC_REGS);
    unlockCtrlReg();
    return 0;
}

/*
   CJC RAM uses only 21 bits. So test only that many..
 */
static int doCJCRAMTest(int nargs, char **arg)
{
    uint32_t testPat[] = { 0x001A5A5A, 0x0, 0x001FFFFF, 0x0005A5A5 };
    int i;
    for (i = 0; i < sizeof(testPat)/sizeof(uint32_t); i++)
    {
        printf("Fill Pattern: 0x%X.. ", testPat[i]);fflush(stdout);
        fillCJCRAM(testPat[i]);
        printf("Done, Verifying..\n");
        sleep(1);
        verifyCJCRAM(testPat[i]);
    }
    //fillCJCSequence(16);
    //printCJCRAM(16);
    return 0;
}

static int doRelayCmd(int nargs, char **arg)
{
    int rNo = 1;
    uint16_t rVal = 0x0;
    uint16_t mask = 0x6;
    int on = 0;
    //printf("%s: %d %s\n", __func__, nargs, arg[0]);
    if (nargs == 2)
    {
        if (strcmp(arg[0], "both") == 0)
            rNo = 0x3;
        else if (convToInt(arg[0], &rNo) != 0)
            return -1;
        if ((rNo != 1) && (rNo != 2) && (rNo != 3))
        {
            printf("Invalid relay no.\n");
            return -1;
        }
        if ((strcmp(arg[1], "on") == 0) || (strcmp(arg[1], "close") == 0))
            on=1;
        else
            on=0;
        switch (rNo)
        {
            case 1:
                mask = 0x2;
                rVal = ((on) ? 0x2 : 0);
                break;
            case 2:
                mask = 0x4;
                rVal = ((on) ? 0x4 : 0);
                break;
            case 3:
                mask = 0x6;
                rVal = ((on) ? 0x6 : 0);
                break;
        }
        if (lockCtrlReg() != 0)
        {
            printf("Error: Failed to acquire lock\n");
            return -1;
        }
        //printf("mask 0x%x rVal 0x%x\n", mask, rVal);
        setCtrlRegBits(5, mask, rVal);
        unlockCtrlReg();
    }
    else
        printf("%s\n", helpRelayCmd);
    return 0;
}


static int doCalibrateFunc(int nargs, char **arg)
{
    int dongleId, skips = 0, pause = 0;
    char *IPStr;
    if (nargs < 2)
    {
        printf("%s\n", helpCalibrate);
        return -1;
    }
    if (convToInt(arg[0], &dongleId) != 0)
        return -1;
    IPStr = arg[1];
    if (nargs >= 3)
    {
        if (convToInt(arg[2], &skips) != 0)
            return -1;
    }
    if (nargs >= 4)
    {
        if (strcmp(arg[3], "-p") == 0)
            pause = 1;
    }
    return doCalFunc(dongleId, IPStr, skips, pause);
}

static int doBakeCmd(int nargs, char **arg)
{
    uint8_t bake = 0;
    uint8_t reg;
    if (nargs == 1)
    {
        if (strcmp(arg[0], "on") == 0)
            bake = 1;
        else if (strcmp(arg[0], "off") == 0)
            bake = 0;
        else
        {
            printf("%s\n", helpBakeCmd);
            return -1;
        }
        if (lockCtrlReg() != 0)
        {
            logPrint("Error: failed to acquire lock\n");
            return -1;
        }
        if (setCtrlRegBits(1, 0x1, bake) != 0)
            logPrint("Error: failed to set bake mode\n");
        unlockCtrlReg();
    }
    else
    {
        if (lockCtrlReg() != 0)
        {
            logPrint("Error: failed to acquire lock\n");
            return -1;
        }
        if (readCtrlRegL(1, 1, (void *)&reg) == 0)
            printf("Bake : %sabled\n", ((reg & 0x1) ? "En" : "Dis"));
        else
            logPrint("Error: failed to read bake mode\n");
        unlockCtrlReg();
    }
    return 0;
}

static int doChillerCmd(int nargs, char **arg)
{
    uint8_t chill = 0;
    uint8_t reg;
    if (nargs == 1)
    {
        if (strcmp(arg[0], "on") == 0)
            chill = 1;
        else if (strcmp(arg[0], "off") == 0)
            chill = 0;
        else
        {
            printf("%s\n", helpChillerCmd);
            return -1;
        }
        if (lockCtrlReg() != 0)
        {
            logPrint("Error: failed to acquire lock\n");
            return -1;
        }
        if (setCtrlRegBits(5, 0x1, chill) != 0)
            logPrint("Error: failed to set chiller\n");
        unlockCtrlReg();
    }
    else
    {
        if (lockCtrlReg() != 0)
        {
            logPrint("Error: failed to acquire lock\n");
            return -1;
        }
        if (readCtrlRegL(5, 1, (void *)&reg) == 0)
            printf("Chiller : %sabled\n", ((reg & 0x1) ? "En" : "Dis"));
        else
            logPrint("Error: failed to read bake mode\n");
        unlockCtrlReg();
    }
    return 0;
}

static int doDebugCmd(int nargs, char **arg)
{
    if (nargs == 1)
    {
        if (strcmp(arg[0], "on") == 0)
            pCache->debug = 1;
        else
            pCache->debug = 0;
    }
    else 
        printf("Debug is %s\n", ((pCache->debug) ? "On" : "Off"));
}

// generate calibration entry table for FPGA
static int doDiodeTable(int nargs, char **arg)
{
    uint32_t stADCVal = 0x001000;
    uint32_t incr = 0x1000;
    uint32_t maxVal = 0x550000;
    uint32_t i;
    uint32_t rVals[3] = { 0, 0, 0 };
    double fVals[3];
    double prevVal = -2.000;
    double tempK;
    double diff;
    int dType;

    if (nargs == 1)
    {
    if (strcmp(arg[0], "dt670") == 0)
        dType = DT_670;
    else
        dType = DT_470;
    for (i = stADCVal; i <= maxVal; i+=incr)
    {
        rVals[1] = i;
        scaleTADC(&rVals[0], &fVals[0]);
        tempK = findDiodeTemp(dType, fVals[1]);
        tempK *= 1000; // convert to milli-kelvin..
        printf("0x%X  %0.5f  %0.5f   ", rVals[1], fVals[1], tempK);
        if (prevVal > 1000.00)
        {
            diff = (tempK - prevVal);
            //printf("%.06f -> %0.6f  %d", diff, (diff/4096.0),
            //                                 (int)((diff/4096.0)*100000));
            //printf("%.06f %d", diff, (int)((diff/4096.0)*100000));
            // change scaling to be a power of 2 for easy division in FPGA
            printf("%.06f %d", diff, (int)((diff/4096.0)*65536));
        }
        printf("\n");fflush(stdout);
        prevVal = tempK; 
    }
    }
    else
        printf("Usage: diodetbl <dt400|dt670>\n");
    return 0;
}

// thermo-couples: generate calibration table for FPGA
typedef struct
{
    float minTempVal;   // in deg C
    float maxempVal;    // in deg C
    uint32_t stADCVal;  // 24bit signed value
    uint32_t endADCVal; // 24bit signed value
    int  scaleFact;     // for FPGA, delta value must be less than 32K
} tcTblGenParams;
static int doThermoCoupleTbl(int nargs, char **arg)
{
    uint32_t incr = 0x1000; // LSB 12bits are masked off in FPGA
    uint32_t stADCVal;
    uint32_t i;
    uint32_t rVals[3] = { 0, 0, 0 };
    double fVals[3];
    tcTblGenParams typeRParams = 
                        { -55.0,  1780.0, 0xFF5000, 0x2D9000, 16384 };
    tcTblGenParams typeKParams = 
                        { -205.0,  1400.0, 0xF30000, 0x780000, (64*1024) };
    tcTblGenParams typeTParams = 
                        { -205.0,  400.0, 0xF3C000, 0x2D2000, (64*1024) };
    tcTblGenParams *pPtr;
    double prevVal;
    double limitVal;
    double tempK;
    double diff;
    int dType;

    if (nargs == 1)
    {
        if (strcmp(arg[0], "type_r") == 0)
        {
            printf("Type R: ");
            dType = Type_R;
            pPtr = &typeRParams;
        }
        else if (strcmp(arg[0], "type_k") == 0)
        {
            printf("Type K: ");
            dType = Type_K;
            pPtr = &typeKParams;
        }
        else if (strcmp(arg[0], "type_t") == 0)
        {
            printf("Type T: ");
            dType = Type_T;
            pPtr = &typeTParams;
        }
        else 
        {
            printf("Un-supported type\n");
            return -1;
        }
    }
    else
    {
        printf("Usage: tctbl <type_r|type_k|type_t>\n");
        return -1;
    }
    prevVal = ((K_OFFSET + pPtr->minTempVal) - 5.0) * 1000.0;  // milli-K
    limitVal = ((K_OFFSET + pPtr->minTempVal) - 1.0) * 1000.0; // milli-K
//printf("minT: %0.3f maxT: %0.3f stADC %X endADC %X scale: %u\n",
//        pPtr->minTempVal, pPtr->maxempVal, pPtr->stADCVal, 
//        pPtr->endADCVal, pPtr->scaleFact); fflush(stdout);
    if (pPtr->stADCVal > 0x7FFFFF)
    {
        for (i = pPtr->stADCVal; i <= 0xFFF000; i+=incr)
        {
            rVals[0] = i;
            scaleTADC(&rVals[0], &fVals[0]);
            if (dType == Type_R)
                tempK = tcTypeRTemp(fVals[0]);
            else
                tempK = tcCalTemp(((dType == Type_K) ? CAL_TYPE_K:CAL_TYPE_T), 
                                                                fVals[0]);
            tempK *= 1000; // convert to milli-kelvin..
            printf("0x%X  %0.5f  %0.5f   ", rVals[0], fVals[0], tempK);
            if (prevVal > limitVal)
            {
                diff = (tempK - prevVal);
                // change scaling to be a power of 2 for easy division in FPGA
                printf("%.06f %d", diff, (int)((diff/4096.0)*pPtr->scaleFact));
            }
            printf("\n");fflush(stdout);
            prevVal = tempK; 
        }
        stADCVal = 0x0;
    }
    else
        stADCVal = pPtr->stADCVal;
    for (i = stADCVal; i < pPtr->endADCVal; i+=incr)
    {
        rVals[0] = i;
        scaleTADC(&rVals[0], &fVals[0]);
        if (dType == Type_R)
            tempK = tcTypeRTemp(fVals[0]);
        else
            tempK = tcCalTemp(((dType == Type_K) ? CAL_TYPE_K : CAL_TYPE_T), 
                                                                fVals[0]);
        tempK *= 1000; // convert to milli-kelvin..
        printf("0x%X  %0.5f  %0.5f   ", rVals[0], fVals[0], tempK);
        if (prevVal > limitVal)
        {
            diff = (tempK - prevVal);
            // change scaling to be a power of 2 for easy division in FPGA
            printf("%.06f %d", diff, (int)((diff/4096.0)*pPtr->scaleFact));
        }
        printf("\n");fflush(stdout);
        prevVal = tempK; 
    }
    return 0;
}

static int genPrtTblForFPGA(fpgaCalData *calData, int count)
{
    uint32_t incr = 0x1000; // LSB 12bits are masked off in FPGA
    uint32_t stADCVal;
    uint32_t i;
    int j;
    uint32_t rVals[3] = { 0 };
    double fVals[3];
    tcTblGenParams prtParams = { -250.0,  520.0, 0x7000, 0x1F7000, (64*1024) };
    double prevVal, limitVal;
    double tempK, diff;
    double R;
    if (!calData)
        return -1;
    prevVal = ((K_OFFSET + prtParams.minTempVal) - 5.0) * 1000.0;  // milli-K
    limitVal = ((K_OFFSET + prtParams.minTempVal) - 1.0) * 1000.0; // milli-K
//    printf("minT: %0.3f maxT: %0.3f stADC %X endADC %X scale: %u\n",
//        prtParams.minTempVal, prtParams.maxempVal, prtParams.stADCVal, 
//        prtParams.endADCVal, prtParams.scaleFact); fflush(stdout);
    for (i = prtParams.stADCVal, j = 0; i <= prtParams.endADCVal; i+=incr)
    {
        rVals[1] = i;
        scaleTADC(&rVals[0], &fVals[0]);
        R = rPrec * ((fVals[1] - pCache->vRtdZero) / 
                                 (pCache->vPrecStim - pCache->vPrecZero)); 
        // now find temp corresponding to resistance
        tempK = prtLookupTemp((float)R);
        tempK *= 1000; // convert to milli-kelvin..
        if (prevVal > limitVal)
        {
            //logPrint("j %d %0.6f\n", j, tempK);
            if (j >= count)
                break;
            diff = (tempK - prevVal);
            calData[j].adcVal  = rVals[1];
            calData[j].voltage = fVals[1];
            calData[j].tempK   = tempK;
            calData[j].delta   = (int)((diff/4096.0) * prtParams.scaleFact);
            j++;
        }
        prevVal = tempK; 
    }
    calData[0].delta = 0.0;
    if (calVerbose)
        printf("%d. Min: %0.3f Max: %0.3f\n",j,calData[0].tempK,calData[j-1].tempK);
    return j;
}

static int genDiodeTblForFPGA(int dType, fpgaCalData *calData, int count)
{
    tcTblGenParams dt470Params = 
                { 0.0, 0.0, 0x49000, 0x560000, (64*1024) };
    tcTblGenParams dt670Params = 
                { 0.0, 0.0, 0x49000, 0x540000, (64*1024) };
    tcTblGenParams *tblPtr;
    uint32_t incr = 0x1000;
    uint32_t i, j;
    uint32_t rVals[3] = { 0 };
    double fVals[3] = { 0.0 };
    double tempK, diff;
    double prevVal = 0.0;
    if (dType == DT_470)
        tblPtr = &dt470Params;
    else if (dType == DT_670)
        tblPtr = &dt670Params;
    else
        return -1;
//printf("%s: dType %d calData %X count %d stADC %X endADC %X\n",
//        __func__, dType, calData, count, tblPtr->stADCVal, tblPtr->endADCVal);
    for (i = tblPtr->stADCVal, j = 0; i <= tblPtr->endADCVal; i+=incr)
    {
        rVals[1] = i;
        scaleTADC(&rVals[0], &fVals[0]);
//printf("   ADC: %u -> %0.6f V, ", rVals[1], fVals[1]);
        tempK = findDiodeTemp(dType, fVals[1]);
        tempK *= 1000; // convert to milli-kelvin..
//printf("-> %0.6f K\n", tempK);
        if (tempK > 0.00)
        {
            if (j >= count)
                break;
            diff = (tempK - prevVal);
            calData[j].adcVal  = rVals[1];
            calData[j].voltage = fVals[1];
            calData[j].tempK   = tempK;
            calData[j].delta   = (int)((diff/4096.0)*65536);
            //printf("%d: %x  %0.6f %0.6f %d\n", j, calData[j].adcVal, 
            //         calData[j].voltage, calData[j].tempK, calData[j].delta);
            j++;
        }
        prevVal = tempK; 
    }
    calData[0].delta = 0;
    if (calVerbose)
    printf("%d. Min: %0.3f Max: %0.3f\n",j,calData[j-1].tempK,calData[0].tempK);
    return j;
}

static int genTCTblForFpga(int dType, fpgaCalData *calData, int count)
{
    uint32_t incr = 0x1000; // LSB 12bits are masked off in FPGA
    uint32_t i, j;
    uint32_t rVals[3] = { 0, 0, 0 };
    double fVals[3];
    uint32_t stADCVal;
    tcTblGenParams typeRParams = 
                        { -55.0,  1780.0, 0xFF5000, 0x2D9000, 16384 };
    tcTblGenParams typeKParams = 
                        { -205.0,  1400.0, 0xF30000, 0x780000, (64*1024) };
    tcTblGenParams typeTParams = 
                        { -205.0,  400.0, 0xF3C000, 0x2D2000, (64*1024) };
    tcTblGenParams *pPtr;
    double prevVal, limitVal;
    double tempK, diff;

    if (dType == Type_R)
        pPtr = &typeRParams;
    else if (dType == Type_K)
        pPtr = &typeKParams;
    else if (dType == Type_T)
        pPtr = &typeTParams;
    else
        return -1;

    prevVal = ((K_OFFSET + pPtr->minTempVal) - 5.0) * 1000.0;  // milli-K
    limitVal = ((K_OFFSET + pPtr->minTempVal) - 1.0) * 1000.0; // milli-K
//    printf("minT: %0.3f maxT: %0.3f stADC %X endADC %X scale: %u\n",
//        pPtr->minTempVal, pPtr->maxempVal, pPtr->stADCVal, 
//        pPtr->endADCVal, pPtr->scaleFact); fflush(stdout);

    j = 0;
    if (pPtr->stADCVal > 0x7FFFFF)
    {
        for (i = pPtr->stADCVal; i <= 0xFFF000; i+=incr)
        {
            rVals[0] = i;
            scaleTADC(&rVals[0], &fVals[0]);
            if (dType == Type_R)
                tempK = tcTypeRTemp(fVals[0]);
            else
                tempK = tcCalTemp(((dType == Type_K) ? CAL_TYPE_K:CAL_TYPE_T), 
                                                                fVals[0]);
            tempK *= 1000; // convert to milli-kelvin..
            //if (prevVal > limitVal)
            if (tempK > 0.00)
            {
                if (j >= count)
                    break;
                diff = (tempK - prevVal);
                calData[j].adcVal  = rVals[0];
                calData[j].voltage = fVals[0];
                calData[j].tempK   = tempK;
                calData[j].delta   = (int)((diff/4096.0)*pPtr->scaleFact);
                j++;
            }
            prevVal = tempK; 
        }
        stADCVal = 0x0;
    }
    else
        stADCVal = pPtr->stADCVal;
    if (j >= count)
        return -1;
    for (i = stADCVal; i < pPtr->endADCVal; i+=incr)
    {
        rVals[0] = i;
        scaleTADC(&rVals[0], &fVals[0]);
        if (dType == Type_R)
            tempK = tcTypeRTemp(fVals[0]);
        else
            tempK = tcCalTemp(((dType == Type_K) ? CAL_TYPE_K : CAL_TYPE_T), 
                                                                fVals[0]);
        tempK *= 1000; // convert to milli-kelvin..
        if (tempK > 0.00)
        {
            if (j >= count)
                break;
            diff = (tempK - prevVal);
            calData[j].adcVal  = rVals[0];
            calData[j].voltage = fVals[0];
            calData[j].tempK   = tempK;
            calData[j].delta   = (int)((diff/4096.0)*pPtr->scaleFact);
            j++;
        }
        prevVal = tempK; 
    }
    calData[0].delta = 0.0;
    if (calVerbose)
        printf("Min: %0.3f Max: %0.3f\n", calData[0].tempK, calData[j-1].tempK);
    return j;
}

static int doRTDTable(int nargs, char **arg)
{
    uint32_t incr = 0x1000; // LSB 12bits are masked off in FPGA
    uint32_t stADCVal;
    uint32_t i;
    uint32_t rVals[3] = { 0, 0, 0 };
    double fVals[3];
    tcTblGenParams rdtParams =  //
                        { -250.0,  520.0, 0x7000, 0x1F7000, (/*16*/64*1024) };
    tcTblGenParams *pPtr;
    double prevVal;
    double limitVal;
    double tempK;
    double diff;
    int dType;
    double R;

    if (nargs == 1)
    {
        if (strcmp(arg[0], "prt") == 0)
        {
            printf("PT100: ");
            dType = PRT;
            pPtr = &rdtParams;
        }
        else 
        {
            printf("Un-supported type\n");
            return -1;
        }
    }
    else
    {
        printf("PT100: ");
        dType = PRT;
        pPtr = &rdtParams;
    }
    prevVal = ((K_OFFSET + pPtr->minTempVal) - 5.0) * 1000.0;  // milli-K
    limitVal = ((K_OFFSET + pPtr->minTempVal) - 1.0) * 1000.0; // milli-K
//printf("minT: %0.3f maxT: %0.3f stADC %X endADC %X scale: %u\n",
//        pPtr->minTempVal, pPtr->maxempVal, pPtr->stADCVal, 
//        pPtr->endADCVal, pPtr->scaleFact); fflush(stdout);
    for (i = pPtr->stADCVal; i <= pPtr->endADCVal; i+=incr)
    {
        rVals[1] = i;
        scaleTADC(&rVals[0], &fVals[0]);
        R = rPrec * ((fVals[1] - pCache->vRtdZero) / 
                                 (pCache->vPrecStim - pCache->vPrecZero)); 
        printf("%0.6f ", fVals[1]);
        printf("R: %0.6f -> ", R);
        // now find temp corresponding to resistance
        tempK = prtLookupTemp((float)R);
        tempK *= 1000; // convert to milli-kelvin..
        printf("0x%X  %0.5f  %0.5f   ", rVals[1], fVals[1], tempK);
        if (prevVal > limitVal)
        {
            diff = (tempK - prevVal);
            // change scaling to be a power of 2 for easy division in FPGA
            printf("%.06f %d", diff, (int)((diff/4096.0)*pPtr->scaleFact));
        }
        printf("\n");fflush(stdout);
        prevVal = tempK; 
    }
    return 0;
}

// convert delta to two's complement before loading..
#define LOADCALDATA(idx, val, delta)  do {       \
    dbgPrint("  %d. %d %d[0x%X]\n", idx, val, delta, delta);    \
    regSpace[37] = 0x8 << 8;                     \
    updateMachCtrlRegs(machCtrl, regOffs);       \
    updateSync(SYNC_REGS);             \
    regSpace[35] = (idx & 0xff) << 8;            \
    regSpace[36] = ((idx >> 8) & 0xff) << 8;     \
    regSpace[38] = (val & 0xff) << 8;            \
    regSpace[39] = ((val >> 8) & 0xff) << 8;     \
    regSpace[40] = ((val >> 16) & 0xff) << 8;    \
    regSpace[41] = (delta & 0xff) << 8;          \
    regSpace[42] = ((delta >> 8) & 0xff) << 8;   \
    updateMachCtrlRegs(machCtrl, regOffs);       \
    updateSync(SYNC_REGS);             \
    regSpace[37] = 0xC << 8;                     \
    updateMachCtrlRegs(machCtrl, regOffs);       \
    updateSync(SYNC_REGS);             \
    } while (0);

/* 
 */
static int loadCalData(int dType)
{
    uint16_t regSpace[sizeof(bingMachineCtrl)];
    bingMachineCtrl *machCtrl = (bingMachineCtrl *)regSpace;
    int regOffs = (42+1) * 2;
    uint8_t flagArr[CJC_RAM_SZ] = { 0 };
    int i, j, sz, ret = 0;
    uint32_t val;
    uint32_t delta;
    uint32_t regVal = 0x001000;
    uint32_t intvl = 0x1000;
    int offs;
    fpgaCalData *calPtr;

    switch (dType)
    {
        case DT_470:
        case DT_670:
            calPtr = malloc(sizeof(fpgaCalData)*1300);
            sz =  genDiodeTblForFPGA(dType, calPtr, 1300);
            if (sz <= 0)
            {
                printf("Error: generating look-up table for Diode(%d)\n", ret);
                free(calPtr);
                return -1;
            }
            break;
        case Type_R:
        case Type_K:
        case Type_T:
            calPtr = malloc(sizeof(fpgaCalData)*2048);
            sz =  genTCTblForFpga(dType, calPtr, 2048);
            if (sz <= 0)
            {
                printf("Error: generating look-up table for Diode\n");
                free(calPtr);
                return -1;
            }
            break;
        case PRT:
            calPtr = malloc(sizeof(fpgaCalData)*500);
            sz =  genPrtTblForFPGA(calPtr, 500);
            if (sz <= 0)
            {
                printf("Error: generating look-up table for PRT\n");
                free(calPtr);
                return -1;
            }
            break;
        default:
            printf("Invalid sensor type\n");
            return -1;
    }

    //printf("%s: dType %d sz %d\n", __func__, dType, sz);

    machCtrl = (bingMachineCtrl *)regSpace;
    readMachCtrlRegs(machCtrl, regOffs);
    // load available entries from calPtr, set rest to 0x1FFFFF;
    // first pass: populate valid entries in table and mark them in flag array
    for (i = 0; i < sz; i++)
    {
        // consider only bits 22-0(ignore sign bit for FPGA)
        offs = (calPtr[i].adcVal & 0x7FFFFF) / 0x1000;
        if ((offs < 0) || (offs > CJC_RAM_SZ))
        {
            logPrint("Error: offset %d, i %d\n", offs, i);
            break;
        }
        flagArr[offs] = 1;
        delta = (i < (sz-1)) ? (uint32_t)calPtr[i+1].delta : 0;
        val = calPtr[i].tempK;
        LOADCALDATA(offs, (int)calPtr[i].tempK, delta)
        j++;
    }
    // now populate reamaining slots with sentinel values
    for (i = 0; i < CJC_RAM_SZ; i++)
    {
        if (! flagArr[i])
        {
            dbgPrint("Fill %d\n", i);
            val = 0x1FFFFF;
            delta = 0;
            LOADCALDATA(i, val, delta);
        }
    }
    regSpace[37] = 0x0; 
    updateMachCtrlRegs(machCtrl, regOffs);
    updateSync(SYNC_REGS);
    free(calPtr);
    return 0;
}

static int printCalData(int offs, int count)
{
    uint16_t statRegs[sizeof(bingMachStatus)];
    bingMachStatus *machStat;
    uint16_t regSpace[sizeof(bingMachineCtrl)];
    bingMachineCtrl *machCtrl;
    machStat = (bingMachStatus *)statRegs;
    machCtrl = (bingMachineCtrl *)regSpace;
    uint32_t regVal;
    int16_t delta, realDelta;
    int i, ret = 0;
    int regOffs = (37 + 1) * 2;
    int statOffs = (52 + 1) * 2;
    if (lockCtrlReg() != 0)
    {
        logPrint("Error: failed to acquire lock\n");
        return -1;
    }
    readMachCtrlRegs(machCtrl, regOffs);
    if (offs != 0xFFFF)
    {
        for ( i = 0; i < count; i++)
        {
            if ((offs + i) >= CJC_RAM_SZ)
                break;
            regSpace[35] = ((offs+i) & 0xff) << 8;
            regSpace[36] = (((offs+i) >> 8) & 0xff) << 8;
            regSpace[37] = 8 << 8;
            updateMachCtrlRegs(machCtrl, regOffs);
            updateSync(SYNC_REGS);
            readMachStatRegs(machStat, statOffs);
            regVal = ((statRegs[50] >> 8) & 0xff) << 16 |
                     ((statRegs[49] >> 8) & 0xff) << 8 |
                     (statRegs[48] >> 8) & 0xff;
            delta =  ((statRegs[52] >> 8) & 0xff) << 8 | 
                     (statRegs[51] >> 8) & 0xff;
            //realDelta = (~(delta & 0x7fff)) + (uint16_t)1;
            printf("%d: %d %d [0x%04X]\n", (offs+i), regVal, delta, delta);
        }
        regSpace[37] = 0;
        updateMachCtrlRegs(machCtrl, regOffs);
        updateSync(SYNC_REGS);
        unlockCtrlReg();
        return 0;
    }
    for (i = 0; i < CJC_RAM_SZ; i++)
    {
        regSpace[35] = (i & 0xff) << 8;
        regSpace[36] = ((i >> 8) & 0xff) << 8;
        regSpace[37] = 8 << 8;
        updateMachCtrlRegs(machCtrl, regOffs);
        updateSync(SYNC_REGS);
        readMachStatRegs(machStat, statOffs);
        regVal = ((statRegs[50] >> 8) & 0xff) << 16 |
                 ((statRegs[49] >> 8) & 0xff) << 8 |
                   (statRegs[48] >> 8) & 0xff;
        delta =  ((statRegs[52] >> 8) & 0xfff) << 8 | 
                 (statRegs[51] >> 8) & 0xff;
        //realDelta = (~(delta & 0x7fff)) + (uint16_t)1;
        printf("%d: %d %d [0x%04X]\n", i, regVal, delta, delta);
    }
    regSpace[37] = 0;
    updateMachCtrlRegs(machCtrl, regOffs);
    updateSync(SYNC_REGS);
    unlockCtrlReg();
    return 0;
}

static int doLoadCalData(int dType)
{
    int ret = 0;
    if (lockCtrlReg() != 0)
    {
        logPrint("Error: failed to acquire lock\n");
        return -1;
    }
    ret = loadCalData(dType);
    unlockCtrlReg();
    return ret;
}

/*
"Usage: caldata load [ dt470 | dt670 ]\n"\
"       caldata print [ loc ], loc range 0-2047.\n";
 */
static int doCalDataCmd(int nargs, char **arg)
{
    int ret = 0;
    int dType;
    int count = 1;
    if (nargs > 0)
    {
        if (strcmp(arg[0], "load") == 0)
        {
            if (nargs == 2)
            {
                if (strcmp(arg[1], "dt470") == 0)
                    dType = DT_470;
                else if (strcmp(arg[1], "dt670") == 0)
                    dType = DT_670;
                else if (strcmp(arg[1], "type_r") == 0)
                    dType = Type_R;
                else if (strcmp(arg[1], "type_k") == 0)
                    dType = Type_K;
                else if (strcmp(arg[1], "type_t") == 0)
                    dType = Type_T;
                else if (strcmp(arg[1], "prt") == 0)
                    dType = PRT;
                else
                    goto calPrtUse;
            }
            ret = doLoadCalData(dType);
        }
        else if (strcmp(arg[0], "print") == 0)
        {
            dType = 0xFFFF;
            if (nargs >= 2)
            {
                if (convToInt(arg[1], &dType) != 0) // re-using var
                    return -1;
                if (nargs == 3) // count too
                {
                    if (convToInt(arg[2], &count) != 0)
                        return -1;
                }
            }
            ret = printCalData(dType, count);
        }
        else
            goto calPrtUse;
    }
    else
        goto calPrtUse;
    return ret;
calPrtUse:
    printf("%s\n", helpCalData);
    return -1;
}

static double getV4CalTemp(uint32_t regVal)
{
    uint32_t rVals[3] = { 0, 0, 0 };
    double fVals[3];
    rVals[0] = regVal;
    scaleTADC(rVals, fVals);
    return fVals[0];
}

static int doCalTempCmd(int nargs, char **arg)
{
    uint32_t stVal, count, intvl;
    uint32_t endVal;
    uint16_t statRegs[sizeof(bingMachStatus)];
    bingMachStatus *machStat;
    uint16_t regSpace[sizeof(bingMachineCtrl)];
    bingMachineCtrl *machCtrl;
    machStat = (bingMachStatus *)statRegs;
    machCtrl = (bingMachineCtrl *)regSpace;
    uint32_t regVal, tempVal;
    int ret = 0;
    int sType;
    int regOffs = (37 + 1) * 2; // offset of last reg we are interested in.
    int statOffs = (55 + 1) * 2;
    if (nargs == 1)
    {
        if (cvtToUInt(arg[0], &stVal) != 0)
            return -1;
        count = intvl = 1;
    }
    else if (nargs == 3)
    {
        if (cvtToUInt(arg[0], &stVal) != 0)
            return -1;
        if (cvtToUInt(arg[1], &count) != 0)
            return -1;
        if (cvtToUInt(arg[2], &intvl) != 0)
            return -1;
    }
    if (nargs == 0)
    {
        if (lockCtrlReg() != 0)
        {
            logPrint("Error: failed to acquire lock\n");
            return -1;
        }
        updateSync(SYNC_REGS);
        readMachStatRegs(machStat, statOffs);
        tempVal = ((statRegs[55] >> 8) & 0xff) << 16 |
                  ((statRegs[54] >> 8) & 0xff) << 8 |
                   (statRegs[53] >> 8) & 0xff;
        printf("T: %d\n", tempVal);
        unlockCtrlReg();
        return 0;
    }
    if ((nargs != 1) && (nargs != 3))
    {
        printf("%s\n", helpCalTemp);
        return -1;
    }
    if ((stVal + (count * intvl)) > 0xFFFFFF)
    {
        printf("Invalid parameters\n");
        return -1;
    }
    if (lockCtrlReg() != 0)
    {
        logPrint("Error: failed to acquire lock\n");
        return -1;
    }
    sType = findSensorType(pCache->sensorType[0]);
    readMachCtrlRegs(machCtrl, regOffs);
    printf("\nADC_Val     Voltage    Temp(K)\n\n");
    endVal = stVal + (count * intvl);
    setCtrlRegBits(37, 0x60, 0x00); // set ADC sel to output 0
    for (regVal = stVal; regVal < endVal; regVal+=intvl)
    {
        regSpace[32] = (regVal & 0xff) << 8;
        regSpace[33] = ((regVal >> 8) & 0xff) << 8;
        regSpace[34] = ((regVal >> 16) & 0xff) << 8;
        updateMachCtrlRegs(machCtrl, regOffs);
        updateSync(SYNC_REGS);
        readMachStatRegs(machStat, statOffs);
        tempVal = ((statRegs[55] >> 8) & 0xff) << 16 |
                  ((statRegs[54] >> 8) & 0xff) << 8 |
                   (statRegs[53] >> 8) & 0xff;
        printf("0x%X    %0.5f    %d\n", regVal, getV4CalTemp(regVal), tempVal);
    }
    if (sType == SensorTypeDiode)
        setCtrlRegBits(37, 0x60, 0x20);
    else
        setCtrlRegBits(37, 0x60, 0x00);
    unlockCtrlReg();
    return 0;
}

#define MAX_SAMPLES  (3000)
static int printADCAverage(int count, int chan, int verbose)
{
    volatile uint8_t *pMem;
    int32_t adcVals[MAX_SAMPLES]; 
    int64_t sum = 0;
    uint32_t rVals[3] = {  0 }; // based on channel specified
    double volts[3];
    int numBytes;
    int i, j;
    if (count > MAX_SAMPLES)
        count = MAX_SAMPLES;
    //printf("%s: %d %d\n", __func__, count, chan);
    if (lockCtrlReg() != 0)
    {
        logPrint("Error: failed to acquire lock\n");
        return -1;
    }
    readT_ADCValues(chan, count, adcVals);
    unlockCtrlReg();
    // find average of adcVals[] and compute voltage based on the avg.
    for (i = 0; i < count; i++)
    {
        if ((uint32_t)adcVals[i] > 0x7FFFFF)
            adcVals[i] |= 0xFF000000; // sign extend for sum
        if (verbose)
            printf("ADC rd %d: 0x%X\n", i, adcVals[i]);
        sum += adcVals[i];
        adcVals[i] &= 0xFFFFFF; //un-"sign extend"
    }
    rVals[chan-1]  = (uint32_t)(sum / count);
    if (rVals[chan-1] > 0x7FFFFF)
        rVals[chan-1] &= 0xFFFFFF;
    scaleTADC(rVals, volts);
    if (verbose)
    {
        printf("Avg. ADC Val 0x%X -> ", rVals[chan-1]);
    }
    printf("%0.7f [ %d/0x%X ]\n", volts[chan-1], rVals[chan-1], rVals[chan-1]);
    return 0;
}

// read T-ADC channel quickly and print average reading
static int doTADCAvg(int nargs, char **arg)
{
    int count = 100, chan = 2;
    int idx = 0, verbose = 0;
    if (nargs > 0) // check for verbose option..
    {
        if (strcmp(arg[0], "-v") == 0)
        {
            verbose = 1;
            idx = 1;
            nargs--;
        }
    }
    if (nargs > 0)
    {
        if (nargs == 1)
        {
            if (convToInt(arg[idx], &count) != 0)
                return -1;
        }
        else if (nargs == 2)
        {
            if (convToInt(arg[idx], &count) != 0)
                return -1;
            if (convToInt(arg[idx+1], &chan) != 0)
                return -1;
            if ((chan < 1) || (chan > 2))
            {
                printf("%s\n", helpTADCAvg);
                return -1;
            }
        }
        else
        {
            printf("%s\n", helpTADCAvg);
            return -1;
        }
    }
    if (count < 0)
        count = 100;
    return printADCAverage(count, chan, verbose);
}

static int doCJCTask(int nargs, char **arg)
{
    int start = 0;
    if (nargs == 1)
    {
        if (strcmp(arg[0], "start") == 0)
            start = 1;
        else 
            start = 0;
        pCache->enableCJC = start;
        if (!start)
            pCache->CJCFact = 0.0;
    }
    else
        printf("Usage: cjctask <start|stop>\n");
    return 0;
}

static void printTempLookup(int sensType, float v)
{
    float temp;
    switch (sensType)
    {
        case DT_670:
        case DT_470:
            temp = findDiodeTemp(sensType, (double)(v/1000.0));
            break;
        case PRT:
            temp = prtLookupTemp((float)v);
            break;
        case Type_R:
        case Type_K:
        case Type_T:
            temp = findTCTemp(sensType, (v/1000.0));
            break;
    }
    printf("%0.4f %s  %0.4f K  %0.4f C\n",
            v, ((sensType == PRT) ? "Ohms" : "mV"), temp, (temp - 273.15));
}

static int printTempTbl(int sensType, float stVal, float endVal, float incr)
{
    float range, cf;
    int c, i;
    if ((incr > 0.0001) && ((endVal - stVal) > incr))
    {
        range = (endVal - stVal);
        cf = range / incr;
        c =  ((int)cf + 1);
    }
    else
        c = 1;
    for (i = 0; i < c; i++)
    {
        printTempLookup(sensType, stVal);
        stVal += incr;
    }
    if ((stVal-incr) < endVal) // last one..
        printTempLookup(sensType, endVal);
    return 0;
}

// tblprt <sensor type> <start val> <end endVal> <incr>
static int doPrtLookup(int nargs, char **arg)
{
    int sensType;
    float stVal, endVal, incr;
    if (nargs == 2) // type and temp.
    {
        if ((sensType = getSensorType(arg[0])) == -1)
            return -1;
        if (cvtToFloat(arg[1], &stVal) != 0)
            return -1;
        endVal = stVal; incr = 0.0;
    }
    else if (nargs == 4)
    {
        if ((sensType = getSensorType(arg[0])) == -1)
            return -1;
        if (cvtToFloat(arg[1], &stVal) != 0)
            return -1;
        if (cvtToFloat(arg[2], &endVal) != 0)
            return -1;
        if (cvtToFloat(arg[3], &incr) != 0)
            return -1;
    }
    else
    {
        printf("%s\n", helpTblPrt);
        return -1;
    }
    return printTempTbl(sensType, stVal, endVal, incr);
}

// find register value corresponding to a voltage read-back for tc
// Opposite of function scaleTADC()
#define TADC_HALF_VAL (0x7FFFFF)
#define TADC_DIV      (8388607-1) // 2^23-1
#define TADC_VREF     (2.50)
static uint32_t voltageToADC(int ch, double fVal)
{
    int isNeg = 0;
    uint32_t rVal = 0;
    if (fVal < 0.000)
        isNeg = 1;
    fVal = fabs(fVal);
    // undo calibrations..
    if (T_ADC_CHAN_DRTD)
        fVal *= pCache->G1drtd; // divider
    else
    {
        fVal *= pCache->tcGTC; // scaling
        fVal += (pCache->tcVZero + pCache->tcVMiss);
    }
    rVal = (uint32_t)((fVal * TADC_DIV) / TADC_VREF);
    //printf("%s: %0.6f -> 0x%x ", __func__, fVal, rVal);
    if (isNeg)
        rVal -= TADC_HALF_VAL;
    //printf("-> 0x%x\n", rVal);
    return rVal;
}

static int printFPGATemp(float stVal, float endVal, float incr)
{
    float range, cf, temp;
    int c, i;
    int ch;
    uint32_t rVal;
    uint8_t regVal[3];
    int sType = findSensorType(pCache->sensorType[0]);
    if (sType == SensorTypeDiode)
        ch = T_ADC_CHAN_DRTD;
    else 
        ch = T_ADC_CHAN_TC;
    if ((incr > 0.0001) && ((endVal - stVal) > incr))
    {
        range = (endVal - stVal);
        cf = range / incr;
        c =  ((int)cf + 1);
    }
    else
        c = 1;
    if (lockCtrlReg() != 0)
    {
        printf("Error: failed to acquire lock\n");
        return -1;
    }
    setCtrlRegBits(37, 0x60, 0x60); // force front-end to input 0
    for (i = 0; i < c; i++)
    {
        rVal = voltageToADC(ch, (stVal/1000.0));
        regVal[0] = rVal & 0xFF;
        regVal[1] = (rVal >> 8) & 0xFF;
        regVal[2] = (rVal >> 16) & 0xFF;
        writeCtrlReg(32, 3, (void *)regVal);
        readStatReg(53, 3, (void *)regVal);
        rVal = (regVal[2] << 16) | (regVal[1] << 8) | regVal[0];
        temp = (rVal / 1000.0); // milli-K to K
        printf("%0.3f mV  %0.4f K  %0.4f C\n", stVal, temp, (temp - 273.15));
        stVal += incr;
    }
    regVal[0] = regVal[1] = regVal[2] =  0;
    writeCtrlReg(32, 3, (void *)regVal);  // reset CJC offset
    if (sType == SensorTypeDiode)  // set front-end to right one
        setCtrlRegBits(37, 0x60, 0x20); 
    else
        setCtrlRegBits(37, 0x60, 0x00);
    unlockCtrlReg();
    return 0;
}

static int doFPGATempLookup(int nargs, char **arg)
{
    float stVal, endVal, incr;
    if (nargs == 1) // type and temp.
    {
        if (cvtToFloat(arg[0], &stVal) != 0)
            return -1;
        endVal = stVal; 
        incr = 0.0;
    }
    else if (nargs == 3)
    {
        if (cvtToFloat(arg[0], &stVal) != 0)
            return -1;
        if (cvtToFloat(arg[1], &endVal) != 0)
            return -1;
        if (cvtToFloat(arg[2], &incr) != 0)
            return -1;
    }
    else
    {
        printf("fpgatemp <start val> <end val> <incr>, all in mV\n");
        return -1;
    }
    return printFPGATemp(stVal, endVal, incr);
}

// from sw_upgrade.c
extern int doSwUpgrade(char *imgName);
#define UPG_ERR_MAX        (20)
char *upg_stat_str[] = 
{
    " Successful",                                  // 0
    " Error opening upgrade file", 
    " Error retrieving system image details", 
    " File too big",
    " Incorrect size for upgrade image",
    " Failed to allocate app buffer",
    " Error reading upgrade image",
    " Upgrade image - invalid header",
    " Upgrade image - checksum error",
    " Upgrade image-not valid for SmartSet2",
    " Upgrade image - invalid ID",
    " Error opening system partition",
    " Error writing system upgrade image",
    " Error mounting app partition",
    " Error extracting files from cfg archive",
    " Error no contents file in archive",
    " Error reading content file",
    " Error opening content file",
    " Error in content file",
    " Error modifying temp. dir",
    " Unknown error",                                // 14
};
static int doUpgradeCmd(int nargs, char **arg)
{
    char *fileName;
    int ret;
    if (nargs == 1)
    {
        fileName = arg[0];
        ret = doSwUpgrade(fileName);
        if ((ret < 0) || (ret > UPG_ERR_MAX))
            ret = UPG_ERR_MAX; 
        printf("\nUpgrade: %s\n", upg_stat_str[ret]);
    }
    return 0;
}

static int doPwrSeqCmd(int nargs, char **arg)
{
    uint8_t t1, t2;
    int i;
    int ret = 0;
    int bus = I2C_MUX_BUS;
    int devAddr = UCD_9081_ADDR;
    uint16_t volts[UCD9081_NUM_RAIL_VOLTS];
    // Voltages expected UCD9081 monitor outputs. 
    // Channel 2 is scaled with a factor of 2.
    // All channels have 3.3V full scale.
    // Monitor registers are 10bit values.
    float scaleFact[UCD9081_NUM_RAIL_VOLTS] = 
        { 1.0, 2.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 };
    float ucdVolts[UCD9081_NUM_RAIL_VOLTS] = 
        { 3.3, 5.0, 3.3, 2.5, 1.8, 1.4, 1.2, 0.0 };
    float fsVal = 3.30;

    printf("Read UCD9081(0x%X)\n", devAddr);
    if (lockI2CBus(bus) < 0)
        return -2;
    if (i2cmux_select_chan(UCD_9081_CHAN) != 0)
    {
        unlockI2CBus(I2C_MUX_BUS);
        printf("Error: failed to select mux channel\n");
        return -1;
    }
    if ((ret = read_ucd9081_version(bus, devAddr, &t1))<0)
    {
        printf("%s: read ucd9081 ver(%d)\n", __func__, ret);
        goto errLSeq;
    }
    printf("UCD9081(0x%x)\n", devAddr);
    printf("   ver: 0x%X\n", t1);
    if ((ret = read_ucd9081_status(bus, devAddr, &t1))<0)
    {
        printf("%s: read ucd9081 stat(%d)\n", __func__, ret);
        goto errLSeq;
    }
    printf("   status: 0x%X\n", t1);

    if ((ret = read_ucd9081_railstatus(bus, devAddr , &t1, &t2)) < 0)
    {
        printf("%s: read ucd9081 status(%d)\n", __func__, ret);
        goto errLSeq;
    }
    printf("   rail status: 0x%X\n", t1);
    if ((ret = read_ucd9081_railvoltages(bus, devAddr, 
                                                (uint8_t *)&(volts[0]))) < 0)
    {
        printf("%s: read ucd9081 rail volts(%d)\n", __func__, ret);
        goto errLSeq;
    }
    unlockI2CBus(bus);
    for (i = 0; i < UCD9081_NUM_RAIL_VOLTS; i++)
    {
        volts[i] &= 0x3FF;  // 10 bit value
        printf("   %d. %0.3f V [%0.2f]\t[%d]\n", (i+1), 
                    (((fsVal/1024.0) * (float)volts[i]) * scaleFact[i]), 
                     ucdVolts[i], volts[i]);
    }
    printf("\n");
    return 0;
errLSeq:
    unlockI2CBus(bus);
    return -1;
}

// load factory calibration settings
static int doLoadFactoryCal(int nargs, char **arg)
{
    char sectStr[64];
    char keyStr[64];
    char strVal[64];
    char dfltVal[4];
    double val;
    int ret;

    dfltVal[0] = '\0';
    strncpy(sectStr, CAL_SECTION, 63);
    sectStr[63] = '\0';

    // trim_100u_p
    strncpy(keyStr, STIM_TRIM_100U_P, 63);
    keyStr[63] = '\0';
    ret = ini_gets(sectStr, keyStr, dfltVal, strVal, 63, SYS_INI_FILE);
    if (strVal[0])
    {
        if (cvtToDouble(strVal, &val) == 0)
        {
            logPrint("%s : %0.10f\n", keyStr, val);
            pCache->stimTrim100u_p = val;
        }
    }

    // trim_100u_n
    strncpy(keyStr, STIM_TRIM_100U_N, 63);
    keyStr[63] = '\0';
    ret = ini_gets(sectStr, keyStr, dfltVal, strVal, 63, SYS_INI_FILE);
    if (strVal[0])
    {
        if (cvtToDouble(strVal, &val) == 0)
        {
            logPrint("%s : %0.10f\n", keyStr, val);
            pCache->stimTrim100u_n = val;
        }
    }

    // trim_10u_p
    strncpy(keyStr, STIM_TRIM_10U_P, 63);
    keyStr[63] = '\0';
    ret = ini_gets(sectStr, keyStr, dfltVal, strVal, 63, SYS_INI_FILE);
    if (strVal[0])
    {
        if (cvtToDouble(strVal, &val) == 0)
        {
            logPrint("%s : %0.10f\n", keyStr, val);
            pCache->stimTrim10u_p = val;
        }
    }

    // trim_10u_n
    strncpy(keyStr, STIM_TRIM_10U_N, 63);
    keyStr[63] = '\0';
    ret = ini_gets(sectStr, keyStr, dfltVal, strVal, 63, SYS_INI_FILE);
    if (strVal[0])
    {
        if (cvtToDouble(strVal, &val) == 0)
        {
            logPrint("%s : %0.10f\n", keyStr, val);
            pCache->stimTrim10u_n = val;
        }
    }

    // trim_1u_p
    strncpy(keyStr, STIM_TRIM_1U_P, 63);
    keyStr[63] = '\0';
    ret = ini_gets(sectStr, keyStr, dfltVal, strVal, 63, SYS_INI_FILE);
    if (strVal[0])
    {
        if (cvtToDouble(strVal, &val) == 0)
        {
            logPrint("%s : %0.10f\n", keyStr, val);
            pCache->stimTrim1u_p = val;
        }
    }

    // trim_1u_n
    strncpy(keyStr, STIM_TRIM_1U_N, 63);
    keyStr[63] = '\0';
    ret = ini_gets(sectStr, keyStr, dfltVal, strVal, 63, SYS_INI_FILE);
    if (strVal[0])
    {
        if (cvtToDouble(strVal, &val) == 0)
        {
            logPrint("%s : %0.10f\n", keyStr, val);
            pCache->stimTrim1u_n = val;
        }
    }

    // TC Gain Gtc
    strncpy(keyStr, TC_GTC_GAIN, 63);
    keyStr[63] = '\0';
    ret = ini_gets(sectStr, keyStr, dfltVal, strVal, 63, SYS_INI_FILE);
    if (strVal[0])
    {
        if (cvtToDouble(strVal, &val) == 0)
        {
            logPrint("%s : %0.10f\n", keyStr, val);
            pCache->tcGTC = val;
        }
    }

    // CJC ADC gain
    strncpy(keyStr, CJC_ADC_GAIN, 63);
    keyStr[63] = '\0';
    ret = ini_gets(sectStr, keyStr, dfltVal, strVal, 63, SYS_INI_FILE);
    if (strVal[0])
    {
        if (cvtToDouble(strVal, &val) == 0)
        {
            logPrint("%s : %0.10f\n", keyStr, val);
            pCache->CJCADCGain = val;
        }
    }

    // TC VZero
    strncpy(keyStr, TC_ZERO_V, 63);
    keyStr[63] = '\0';
    ret = ini_gets(sectStr, keyStr, dfltVal, strVal, 63, SYS_INI_FILE);
    if (strVal[0])
    {
        if (cvtToDouble(strVal, &val) == 0)
        {
            logPrint("%s : %0.10f\n", keyStr, val);
            pCache->tcVZero = val;
        }
    }

    // Diode amp gain, G1drtd
    strncpy(keyStr, DIODE_SENSOR_GAIN, 63);
    keyStr[63] = '\0';
    ret = ini_gets(sectStr, keyStr, dfltVal, strVal, 63, SYS_INI_FILE);
    if (strVal[0])
    {
        if (cvtToDouble(strVal, &val) == 0)
        {
            logPrint("%s : %0.10f\n", keyStr, val);
            pCache->G1drtd = val;
        }
    }

    return 0;
}

static void printUARTStr(unsigned char *uartStr, int len, int hexPrt)
{
    int i, j, end;
    if (hexPrt)
    {
        i = 0;
        do {
            j = i;
            end = ((j+16) < len) ? (j+16) : len;
            for (j = i; j < end; j++)
                printf("%02x", uartStr[j]);
            printf("   ");
            for (j = i; j < end; j++)
            {
                if (isprint(uartStr[j]))
                    printf("%c", uartStr[j]);
                else
                    printf(" ", uartStr[i]);
            }
            printf("\n");
            i += 16;
        }
        while (i < len);
    }
    else
    {
        for (i = 0; i < len; i++)
        {
            if (isascii(uartStr[i]))
                printf("%c", uartStr[i]);
            else
                printf("(%02x)", uartStr[i]);
        }
    }
    printf("\n");
}

static int getUARTNo(char *uartStr, int *uartNo)
{
    if ((strcmp(uartStr, "4") == 0) ||
        (strcmp(uartStr, "cjc") == 0))
    {
        *uartNo = 4;
        return 0;
    }
    if ((strcmp(uartStr, "5") == 0) ||
        (strcmp(uartStr, "rs485") == 0))
    {
        *uartNo = 5;
        return 0;
    }
    return -1;
}

static int doReadUART(int nargs, char *arg[])
{
    int uartNo;
    uint32_t nChars = 0;
    uint8_t fifoStr[RX_FIFO_SZ];
    int len;
    if (nargs < 1)
    {
        printf("%s\n", helpReadUART);
        return -1;
    }
    if (getUARTNo(arg[0], &uartNo) != 0)
    {
        printf("Invalid uart specified, %s\n", arg[0]);
        return -1;
    }
    if (nargs == 2)
    {
        if (cvtToUInt(arg[1], &nChars) != 0)
        {
            printf("Invalid numerical value for nChars, %s\n", arg[2]);
            return -1;
        }
        if (nChars > RX_FIFO_SZ)
            nChars = RX_FIFO_SZ;
    }
    /*
    if (lockUART(uartNo) != 0)
    {
        printf(errLockMsg, "uart");
        return -1;
    }
    */
    len = readFromUART(uartNo, 1, fifoStr);
    //unlockUART(uartNo);
    if (nChars)
        printUARTStr(fifoStr, nChars, 0);
    else
        printUARTStr(fifoStr, len, 1);
    return 0;
}

/*
 * Enable power to CJC EEPROM and read cable eeprom, parse holder id from
 * eeprom contents.
 */
static int doHolderIdCmd(int nargs, char *arg[])
{
    uint16_t regZero, regVal;
    int cjcEnabled = 0;
    volatile uint8_t *pUart;
    char fifoStr[RX_FIFO_SZ];
    char *idPtr;
    int uartNo = 4; // CJC eeprom uart
    int len, i, ret = 0;

    pUart = (volatile uint8_t *)pFpgaUartBase;
    if (lockCtrlReg() != 0)
    {
        printf(errLockMsg, "ctrl reg");
        return -1;
    }
    readCtrlReg(0, 1, &regZero);
    if ((regZero & 0x400) != 0)
        cjcEnabled = 1;
    if (cjcEnabled)
    {
        printf("Disable CJC EEPROM power\n");
        regVal = 0x200;
        writeCtrlRegFull(0, 1, &regVal);
        usleep(25000);
    }
    printf("reset UARTS\n");
    pUart[6] = 0x30;  // reset UARTS - CJC & RS485
    usleep(2000);
    pUart[6] = 0x0;  
    usleep(25000);
    printf("Enable CJC EEPROM power\n");
    regVal = 0x600;
    writeCtrlRegFull(0, 1, &regVal);
    unlockCtrlReg();
    usleep(1100000); // a little more than 1 second.
    len = readFromUART(uartNo, 1, fifoStr);
    if (! cjcEnabled) // disable power to CJC eeprom
    {
        printf("Disable CJC EEPROM Power\n");
        for (i = 0; i < 3; i++)
        {
            if (lockCtrlReg() == 0)
                break;
            usleep(25000);
        }
        if (i >= 3)
        {
            printf("Error: failed to disable CJC EEPROM Power\n");
            return -1;
        }
        regVal = 0x200;
        writeCtrlRegFull(0, 1, &regVal);
        unlockCtrlReg();
    }
    if (fifoStr[0] != '\0')
    {
        idPtr = &(fifoStr[16]);
        idPtr[16] = '\0';
        printf("Holder Id: %s\n", idPtr);
    }
    else
    {
        printf("Failed to read cable eeprom\n");
        ret = -1;
    }
    return ret;
}

static int doProfileTADC(int nargs, char **arg)
{
    int count, i, j;
    int chan = 1;
    double V, fVal[3];
    float cVal;
    int sType, ret = 0;
    uint8_t tFpga[3];
    int32_t avgTemp;
    long long start, end;
    long long rdStart, rdEnd, rdTot;
    long long lkStart, lkEnd, lkTot;
    int netC;
    if (nargs > 0)
    {
        if (convToInt(arg[0], &chan) < 0)
            return -1;
        if (nargs > 1)
        {
            if (convToInt(arg[0], &count) < 0)
                return -1;
        }
    }
    else
        count = 1000;
    if ((chan  < 1) || (chan > 3))
    {
        chan = 1;
        printf("Ch: %d\n", chan);
    }
    if ((count < 0) || (count > 10000))
    {
        count = 1000;
        printf("Count: %d\n", count);
    }
    start = getTimeInMs();
    rdTot = lkTot = 0;
    adcRdTot = 0;
    netC = 0;
    for (i = 0; i < count; i++)
    {
        sType = findSensorType(pCache->sensorType[0]);
        if ((chan < 1) || (chan > 3))
        {
            if (sType == SensorTypeDiode)
                chan = 2;
            else if (sType == SensorTypeTC)
                chan = 1;
            else 
                chan = 3;
        }
        if (chan == 1)
            sType = SensorTypeTC;
        else 
            sType = SensorTypeDiode;
        if (lockCtrlReg() == 0)
        {
            //if (chan > 1)
            if (chan > 0)
            {
                rdStart = getTimeInMs();
                ret = getAvgADCReading(chan, 1000, &V);
                rdEnd = getTimeInMs();
                rdTot += (rdEnd - rdStart);
                fVal[chan-1] = V;
                lkStart = getTimeInMs();
                cVal = findSensorTemp(sType, 0, fVal);
                lkEnd = getTimeInMs();
                lkTot += (lkEnd - lkStart);
            }
            else
            {
                readStatReg(53, 3, (void *)tFpga);
                avgTemp = (tFpga[2] << 16) | (tFpga[1] << 8) | tFpga[0];
                cVal = (float)(avgTemp / 1000.0);
            }
            unlockCtrlReg();
            if (ret < 0) 
            {
                printf("Unable to retrieve ADC values from FPGA/Reg\n");
            }
            netC++;
        }
        else
            printf("LOCK-ERR(%d)\n",i);
    }
    end = getTimeInMs();
    printf("St: %lld mS, End: %lld mS, avg: %0.6f mS, Temp: %0.3f K\n",
            start, end, ((end - start) / (float)netC), cVal);
    printf("ADC read Total %lld, avg: %0.6f\n", adcRdTot, (adcRdTot/(float)netC));
    printf("read Total %lld, avg: %0.6f\n", rdTot, (rdTot/(float)netC));
    printf("lookup Total %lld, avg: %0.6f\n", lkTot, (lkTot/(float)netC));
    return 0;
}

static int doProfileTADCNew(int nargs, char **arg)
{
    int count, i, j;
    int chan = 3;
    double fVal[3] = { 0.0 };
    uint32_t rVal[3] = { 0 };
    float cVal;
    uint32_t avg;
    int sTypeSpec = -1, ret = 0;
    int sType;
    uint8_t tFpga[3];
    int32_t avgTemp;
    long long start, end;
    long long rdStart, rdEnd, rdTot;
    long long lkStart, lkEnd, lkTot;
    int accIdx;
    if (nargs > 0)
    {
        int usrSnsr;
        usrSnsr = getSensorType(arg[0]); // convert to enum
        if (usrSnsr == -1)
        {
            printf("Error: invalid sensor type\n");
            return -1;
        }
        sTypeSpec = findSensorType(usrSnsr); // convert to sensor kind
        if (nargs > 1)
        {
            if (convToInt(arg[1], &count) < 0)
                return -1;
        }
    }
    else
        count = 100;
    if ((count < 0) || (count > 10000))
        count = 1000;
    sType = findSensorType(pCache->sensorType[0]);
    if (sType == SensorTypeNone)
    {
        printf("Error: sensor type not set\n");
        return -1;
    }
    if (sTypeSpec == -1)
        sTypeSpec = sType;
    if (sType == sTypeSpec) // sensor 1
    {
        accIdx = 0;
    }
    else 
    {
        sType = findSensorType(pCache->sensorType[1]);
        if (sType != sTypeSpec)
        {
            printf("Error: Invalid sensor specified.\n");
            return -1;
        }
        accIdx = 1;
    }

    if (sType == SensorTypeDiode)
        chan = 2;
    else if (sType == SensorTypeTC)
        chan = 1;
    else 
        chan = 3;

    printf("== AccIdx: %d, Ch: %d, Count: %d == \n", accIdx, chan, count);
    start = getTimeInMs();
    rdTot = lkTot = 0;
    adcRdTot = 0;
    if (lockCtrlReg() != 0)
    {
        printf("LOCK-ERR\n");
        return -1;
    }
    for (i = 0; i < count; i++)
    {
        //if (chan > 1)
        if (chan > 0)
        {
            rdStart = getTimeInMs();
            ret = getAdcFpgaAvg(accIdx, &(rVal[chan-1]));
            scaleTADC(rVal, fVal);
//printf("Avg: %X [ %u ] -> %0.6f\n", rVal[chan-1], rVal[chan-1], fVal[chan-1]);
            rdEnd = getTimeInMs();
            rdTot += (rdEnd - rdStart);
            lkStart = getTimeInMs();
            if (chan != 3)
                cVal = findSensorTemp(sType, accIdx, fVal);
            else
                cVal = 0.0;
            lkEnd = getTimeInMs();
            lkTot += (lkEnd - lkStart);
        }
        else
        {
            readStatReg(53, 3, (void *)tFpga);
            avgTemp = (tFpga[2] << 16) | (tFpga[1] << 8) | tFpga[0];
            cVal = (float)(avgTemp / 1000.0);
        }
        if (ret < 0) 
        {
            printf("Unable to retrieve ADC values from FPGA/Reg(%d)\n",i);
        }
    }
    unlockCtrlReg();
    end = getTimeInMs();
    printf("St: %lld mS, End: %lld mS, avg: %0.6f mS, Temp: %0.3f K\n",
            start, end, ((end - start) / (float)count), cVal);
    printf("ADC read Total %lld, avg: %0.6f\n", adcRdTot, (adcRdTot/(float)count));
    printf("read Total %lld, avg: %0.6f\n", rdTot, (rdTot/(float)count));
    printf("lookup Total %lld, avg: %0.6f\n", lkTot, (lkTot/(float)count));
    printf("Temp: %0.4f\n", cVal);
    return 0;
}

static int doProfileStatRegs(int nargs, char **arg)
{
    volatile uint8_t *pMem;
    uint8_t machStatBuf[MACH_STAT_BUF_SZ];
    bingMachStatus *machStat;
    int i, j;
    long long start, end;
    int count, numBytes;
    numBytes = sizeof(bingMachStatus);
    pMem = (volatile uint8_t *)pFpgaMem;
    if (nargs > 0)
    {
        if (convToInt(arg[0], &count) < 0)
            return -1;
    }
    else
        count = 1000;
    if (lockCtrlReg() != 0)
    {
        printf(errLockMsg, "ctrl reg");
        return -1;
    }
    printf("Iterations: %d\n", count);
    start = getTimeInMs();
    for (j = 0; j < count; j++)
    {
        updateSync(SYNC_REGS);
        RESET_CH_CTR(pMem);
        // read necessary number of bytes..
        for (i = 0; i < numBytes; i++)
             machStatBuf[i] = pMem[MACH_STAT_CH];
    }
    end = getTimeInMs();
    printf("St: %lld mS, End: %lld mS, avg: %0.6f mS\n",
            start, end, ((end - start) / (float)count));
    unlockCtrlReg();
    return 0;
}

/* A generic function to get user input using readline */
static char *line_read = (char *)0;
static const char prompt[] = "Diag=> ";
char *getUserInput()
{
    if (line_read)
    {
        free(line_read);
        line_read = (char *)0;
    }
    /* get a line from user */
    line_read = readline(prompt);
    return line_read;
}

/*
 * parse given string into tokens and stuff pointers to each token into the 
 * argv vector. argSize contains number of valid pointers in argv vector
 */
static int parseTokens(char *string, char *argv[], int argSize)
{
    int found = 0;
    char *word;
    int idx = 0, i = 0;
    if (!string || !(*string))
        return -1;
    while (1)
    {
        while (string[i] && whitespace(string[i]))
            i++;
        word = string + i;
        if (!word || !(*word))
            break;
        while (string[i] && !whitespace(string[i]))
            i++;
        if (string[i])
            string[i++] = '\0';
        found++;
        if (idx < argSize)
        {
            argv[idx] = word;
            idx++;
        }
    }
    if (found > argSize)
        return 0;
    return found;
}

shellCmd *findCommand(char *cmd)
{
    register int i;
    for (i = 0; commands[i].name; i++)
        if (strcmp(cmd, commands[i].name) == 0)
            return (&commands[i]);
    return ((shellCmd *)0);
}

int executeCmd(int argc, char *argv[])
{
    shellCmd *cmdPtr;
    char **cmdArg;
    if (argc < 1)
        return -1;
    cmdPtr = findCommand(argv[0]);
    if (!cmdPtr)
    {
        fprintf(stdout, "Error: %s is not a known command\nType help to see a list of commands\n", argv[0]);
        return -1;
    }
    cmdArg = &(argv[1]);
    //fprintf(stdout, "exec: %s <%s>, %d\n", cmdPtr->name, cmdArg[0], (argc-1));
    return (((*cmdPtr->func))((argc-1), cmdArg));
}

static void displayHistory()
{
    HIST_ENTRY **list;
    int i;
    list = history_list();
    if (list)
    {
        for (i = 0; list[i]; i++)
            printf("%d. %s\n", (i+history_base), list[i]->line);
    }
}

#define MAX_ARGS 11
int main(int argc, char *argv[])
{
    char *reply, *cmd;
    struct sigaction sa;
    char *args[MAX_ARGS];
    char **cmdArgs;
    int numArgs, err;
    char prevCmd[128];
    int tmp;
    if (initLib() != 0)
    {
        syslog(LOG_ERR, "Failed to init library!");
        printf("Error: failed to init library!!\n");
        exit(1);
    }
    if (mapFpgaMem())
    {
        syslog(LOG_ERR, "Failed to map FPGA memory!");
        printf("Error: failed to map FPGA memory!!\n");
        exit(1);
    }

    if ((pCache = getSharedMemory(IPC_KEY_SHAREDMEM, sizeof(sysParams))) == 0)
    {
        fprintf(stderr, "Error attaching shared memory\n");
        return -1;
    }
    /* see if this is a non-interactive session */
    if (argc > 1)
    {
        gInteractive = 0;
        cmdArgs = &(argv[1]);
        if (executeCmd((argc-1), cmdArgs) == 0)
        {
#if 0
            if (g_paramCache)
                relSharedMemory(g_paramCache);
#endif
            exit(0);
        }
        else
        {
#if 0
            if (g_paramCache)
                relSharedMemory(g_paramCache);
#endif
            exit(1);
        }
    }
    /*  end non-interactive session */

    sa.sa_handler = sigHandler;
    sa.sa_flags = 0;
    sigaction(SIGINT, &sa, 0);
    /* install signal handlers */
    sa.sa_handler = SIG_IGN;
    sa.sa_flags = 0;
    sigemptyset(&sa.sa_mask);
    sigaction(SIGTERM, &sa, 0);

    /* init history stuff, keep only 256 entries in history */
    using_history();
    stifle_history(256);

    printf("\nDiagnostics shell for BING systems, Gatan Inc.\n");
    printf("Type help to see a list of available commands\n\n");

    prevCmd[0] = '\0';
    while (1)
    {
        reply = getUserInput();
        if (reply)
        {
            cmd = stripWhiteSpace(reply);
            /* if the line is non-empty, save it in history and exec */
            if (cmd && *cmd)
            {
                if (((strncasecmp(cmd, "quit", 4) == 0) ||
                     (strncasecmp(cmd, "exit", 4) == 0))  && (strlen(cmd) == 4))
                {
                        break;
                }
                if ((strcasecmp(cmd, "list") == 0) && (strlen(cmd) == 4))
                    displayHistory();
                else
                {
                    /* command history stuff, do not keep dup cmds in history list */
                    if (prevCmd[0] == '\0')
                    {
                        add_history(cmd);
                        strncpy(prevCmd, cmd, 128);
                        prevCmd[127] = '\0';
                    }
                    else
                    {
                        if (strcmp(cmd, prevCmd) != 0)
                        {
                            add_history(cmd);
                            strncpy(prevCmd, cmd, 128);
                            prevCmd[127] = '\0';
                        }
                    }
                    /* END command history stuff */
                    numArgs = parseTokens(reply, args, (MAX_ARGS-1));
                    if (numArgs <= 0)
                        fprintf(stdout, "Error in command\n", numArgs);
                    else
                    {
                        args[numArgs] = (char *)0; /* terminate argV array */
                        executeCmd(numArgs, args);
                        //if (executeCmd(numArgs, args) != 0)
                        //    fprintf(stdout, "Command failed\n");;
                    }
                }
            }
        }
        else
        {
                fprintf(stdout, "Quit\n");
                break;
        }
    }
#if 0
    if (g_paramCache)
        relSharedMemory(g_paramCache);
#endif
    exit(0);
}

