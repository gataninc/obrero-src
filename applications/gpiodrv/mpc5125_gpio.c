/*
   GPIO driver based on Ciro's driver for MPC8318.
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/ioport.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/delay.h>
#include <asm/io.h>
#include <asm/bitops.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <linux/platform_device.h>
#include <asm/of_platform.h>
#include "../include/mpc5125_gpio.h"

/* Constants */
#define MAXBIT    31
#define MAXREG    5

/* Macros */
#define GPIO_OP(cmd)    (cmd & 0xF0)
#define GPIO_REG(cmd)    (cmd & 0x0F)

/* GPIO control registers */
struct mpc5125_gpio_reg {
    __be32 gpdir;
    __be32 gpodr;
    __be32 gpdat;
    __be32 gpier;
    __be32 gpimr;
    __be32 gpicr;
};

static struct {
    volatile struct mpc5125_gpio_reg *gpio_reg;
    dev_t gpiodev;
    struct cdev dev;
} gdat;

static void dbgFPGAMaps(void)
{
    int i;
    volatile u32 *immr = 0;
    volatile u32 *regBase;
    u32 regV1;

    immr = (volatile u32 *)ioremap(0x80000000, 0x10100);

    printk("IMMR %X\n", (u32)immr);
    if (immr)
    {
        for (i = 0; i < 15; i++)
        {
            regBase = (immr + i);
            regV1 = in_be32(regBase);
            printk("IMMR[%d]: %X\n", (i*4), regV1);
        }
        regBase = (immr + (0x10000/4));
        for (i = 0; i < 15; i++)
        {
            regV1 = in_be32(regBase);
            printk("LPC[%d]: %X\n", (i*4), regV1);
            regBase++;
        }
    }
    msleep(200);
    iounmap(immr);
}

static int gpio_ioctl(struct inode *inode, struct file *file,
        unsigned int cmd, unsigned long arg)
{
    volatile __be32 *preg;
    unsigned int regnum = GPIO_REG(cmd);
    unsigned int op = GPIO_OP(cmd);
    int index;
    /* Validate register number */
    if(regnum > MAXREG)
        return -EINVAL;

    preg = ((volatile __be32 *)gdat.gpio_reg) + regnum;

    /* This operation is different from the others */
    /* Argument is a pointer */
    if(op == GPIO_GET)
    {
        if(!(file->f_mode & FMODE_READ))
            return -EPERM;
        if(copy_to_user((void __user *)arg,(void *)preg,sizeof(__be32)))
            return -EFAULT;
        else
            return 0;
    }

    /* Argument is a bit index */
    if(arg > MAXBIT)
        return -EINVAL;

    /* We need write permission to continue */
    if(!(file->f_mode & FMODE_WRITE))
        return -EPERM;

    /* We do this to match the Freescale docs, where bit 31 is the LSB */
    index = MAXBIT - arg;

    /* Execute */
    switch(op)
    {
        case GPIO_CLEAR:
            clear_bit(index,(void *)preg);
            break;
        case GPIO_SET:
            set_bit(index,(void *)preg);
            break;
        case GPIO_FLIP:
            /* Need read permission for this */
            if(!(file->f_mode & FMODE_READ))
                return -EPERM;
            change_bit(index,(void *)preg);
            break;
        case FPGA_LOAD:
            dbgFPGAMaps();
            break;
        default:
            return -EINVAL;
    }

    return 0;
}

static struct file_operations mpc5125_gpio_fops = {
    .owner = THIS_MODULE,
    .ioctl = gpio_ioctl,
};

static int __init mpc5125_gpio_init(void)
{
    struct device_node *np;
    struct resource r;
    int ret=0;
    u8 __iomem *ioctlPtr, *ioctlReg;

    /* map gpio memory */
    if ((np = of_find_compatible_node(NULL, NULL, "fsl,mpc5125-gpio")) == 0)
    {
        printk("Error: cannot find device mpc5125-gpio\n");
        return -ENODEV;
    }
    of_address_to_resource(np, 0, &r);
    of_node_put(np);

    /* Get access to it */
    gdat.gpio_reg = ioremap(r.start, (r.end - r.start + 1));
    if (gdat.gpio_reg == NULL) 
        return -ENOMEM;
    printk("gpio res: %X len %X, map %X\n",r.start,(r.end - r.start + 1),
            (u32)gdat.gpio_reg);
    memset(&r, 0, sizeof(struct resource));

    /* now map ioctl memory to access io_pads */
    if ((np = of_find_compatible_node(NULL, NULL, "fsl,mpc5125-ioctl")) == 0)
    {
        printk("Error: cannot find device mpc5125-ioctl\n");
        ret = -ENODEV;
        goto unmap_gpio;
    }
    of_address_to_resource(np, 0, &r);
    of_node_put(np);

    /* Get access to it */
    ioctlPtr = ioremap(r.start, (r.end - r.start + 1));
    if (ioctlPtr == NULL)
    {
        ret = -ENOMEM;
        goto unmap_gpio;
    }
    printk("ioctl res: %X len %X, map %X\n",r.start,(r.end - r.start + 1),
            (u32)ioctlPtr);
    printk("GPIO: DIR: 0x%X, ODR: 0x%X DAT: 0x%X\n", 
            (uint32_t)gdat.gpio_reg->gpdir,
            (uint32_t)gdat.gpio_reg->gpodr,
            (uint32_t)gdat.gpio_reg->gpdat);

    /* Since we are good citizens, get a dynamic device number */
    if(alloc_chrdev_region(&gdat.gpiodev,0,1,"mpc5125_gpio") < 0) 
    {
        ret = -ENODEV;
        goto unmap_ioctl;
    }

    /* Register with the kernel */
    cdev_init(&gdat.dev,&mpc5125_gpio_fops);
    if(cdev_add(&gdat.dev,gdat.gpiodev,1) < 0) 
    {
        ret = -ENODEV;
        goto unreg;
    }

    /* configure i/o pad for setting up GPIOs */
#if 1
    /* gpio 10, ConfigDone line */
    ioctlReg = (u8 __iomem *)(ioctlPtr + 0xE);
    out_8(ioctlReg, 0x63); /* enable ALT3 gpio10 */

    /* gpio 11, nStatus line */
    ioctlReg = (u8 __iomem *)(ioctlPtr + 0xF);
    out_8(ioctlReg, 0x60); /* enable ALT3 gpio11 */

    /* gpio 14, nConfig line */
    ioctlReg = (u8 __iomem *)(ioctlPtr + 0x75);
    out_8(ioctlReg, 0x64); /* enable ALT3 gpio14 */
#endif

    /* unmap this, we no longer need it */
    iounmap(ioctlPtr);
    printk("mpc5125 gpio driver loaded\n");

    return 0;

unreg:
    unregister_chrdev_region(gdat.gpiodev,1);
unmap_ioctl:
    iounmap(ioctlPtr);
unmap_gpio:
    iounmap(gdat.gpio_reg);
    return ret;
}

static void __exit mpc5125_gpio_exit(void)
{
    cdev_del(&gdat.dev);
    iounmap(gdat.gpio_reg);
    unregister_chrdev_region(gdat.gpiodev,1);
    return;
}

module_init(mpc5125_gpio_init);
module_exit(mpc5125_gpio_exit);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("FPGA control driver for Bing Ctlr board");
/*
   GPIO pin configuration:

   Pin         Pad I/O Ctrl      Alternate Functions          Reset Value
                 Reg Offs 
   ----------------------------------------------------------------------
   EMB_AD29     0x0F              Al10-LPC_AD29/LPC_A13          0x00
                STD_PU            Alt3-GPIO11

   EMB_AD30     0x0E              Alt0-LPC_AD30/LPC_A15          0x03
                STD_PU_ST         Alt1-CAN_CLK
                                  Alt3-GPIO10

   PSC_MCLK_IN  0x75              Alt0-PSC_MCLK_IN               0x04
                STD_PU_ST         Alt3-GPIO14

   // for Obrero: GPIO17 controls reset to i2c mux
   EMB_AD23     0x15              Alt0-LPC_AD23/LPC_A09          0x00
                                  Alt3-GPIO17
 */
