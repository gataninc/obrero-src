#! /bin/sh

#set -x

APPIMG=/mnt/nv/obrero_ui.img.gz
START_SCRIPT=/mnt/nfs/tempstart.sh

if [ ! -f ${APPIMG} ]; then
   echo "Error: UI image($APPIMG) not found"
   exit 1
fi
if [ ! -f ${START_SCRIPT} ]; then
    gunzip -c ${APPIMG} | dd of=/dev/ram2 bs=1024
    if [ $? -ne 0 ]; then
        echo "Error: failed to unpack ui image"
        exit 1
    fi
    mount /dev/ram2 /mnt/nfs
    if [ $? -ne 0 ]; then
        echo "Error: failed to mount ui partition"
        exit 1
    fi
fi
${START_SCRIPT} &
