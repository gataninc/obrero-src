#ifndef __THERMOCOUPLE_H
#define __THERMOCOUPLE_H

// type R routines
double tcTypeRTemp(double mV);
double cjcTemp2tcTypeRVolt(double cjcT);

// for type K & T routines
enum
{
    CAL_TYPE_K,
    CAL_TYPE_T,
};

double tcCalTemp(int type, double v);
double cjcTemp2tcVolt(int type, double cjcT);

#endif //__THERMOCOUPLE_H
