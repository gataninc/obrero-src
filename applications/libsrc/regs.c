#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <byteswap.h>
#include "bing_common.h"
#include "fpga.h"
#include "regs.h"

static volatile void *pFpgaMem = 0;
static volatile void *pFpgaUartBase = 0;

static int mapFpgaMem(void)
{
    int hMem;
    int err1, err2;
    if ((hMem = open("/dev/mem", O_RDWR)) == -1)
    {
        perror("open /dev/mem");
        return -1;
    }
    errno = 0;
    pFpgaMem = mmap(0, FPGA_MEMORY_SIZE, PROT_READ|PROT_WRITE,
                    MAP_SHARED, hMem, FPGA_MEMORY_BASE);
    err1 = errno;
    errno = 0;
    pFpgaUartBase = mmap(0, FPGA_MEMORY_SIZE, PROT_READ|PROT_WRITE,
                    MAP_SHARED, hMem, FPGA_UART_BASE);
    err2 = errno;
    close(hMem);
    if (pFpgaMem == MAP_FAILED)
    {
        fprintf(stderr, "Error memmap regs, %s\n", strerror(err1));
        pFpgaMem = 0;
        return -1;
    }
    if (pFpgaUartBase == MAP_FAILED)
    {
        fprintf(stderr, "Error memmap uart base, %s\n", strerror(err2));
        pFpgaUartBase = 0;
        return -1;
    }
    //printf("Mapped FPGA registers\n");fflush(stdout);
    return 0;
}

// NOTICE: MUST be holding register semaphore while calling this function.
void updateSync(int syncType)
{
    volatile uint8_t *pMem;
    uint8_t reg;
    int i, iCnt = 100;
    pMem = pFpgaMem;
    if (syncType == SYNC_REGS)
    {
        RESET_CH_CTR(pMem);
        reg = pMem[CPU_FPGA_SYNC_CH];
        RESET_CH_CTR(pMem);
        pMem[CPU_FPGA_SYNC_CH] = (reg | 0x1);
        // wait for FPGA to finish SYNC processing
        for (i = 0; i < iCnt; i++) // must not take more than 10ms
        {
            usleep(100);
            RESET_CH_CTR(pMem);
            reg = pMem[CPU_FPGA_SYNC_CH];
            if ((reg & 0x1) ==  0)
                break;
        }
        if (i >= iCnt)
            logPrint("!! %s: time out!!\n", __func__);
    }
    else if (syncType == LOOKUP_TBL_A_CH)
    {
        RESET_CH_CTR(pMem);
        reg = pMem[CPU_FPGA_SYNC_CH];
        RESET_CH_CTR(pMem);
        pMem[CPU_FPGA_SYNC_CH] = (reg | 0x2);
    }
    else
        logPrint("%s: Error: Invalid syncType %d\n", __func__, syncType);
}

/*
   Read only the bytes necessary(in offset) and return
 */
int readMachStatRegs(bingMachStatus *regs, int offset)
{
    volatile uint8_t *pMem;
    uint8_t *machStatPtr;
    int i;
    pMem = (volatile uint8_t *)pFpgaMem;
    RESET_CH_CTR(pMem);
    machStatPtr = (uint8_t *)regs;
    for (i = 0; i < offset; i++)
        machStatPtr[i] = pMem[MACH_STAT_CH];
    return 0;
}

/*
   Read only the bytes necessary(in offset) and return
 */
int readMachCtrlRegs(bingMachineCtrl *regs, int offset)
{
    volatile uint8_t *pMem;
    uint8_t *machCtrlPtr;
    int i;
    pMem = (volatile uint8_t *)pFpgaMem;
    RESET_CH_CTR(pMem);
    machCtrlPtr = (uint8_t *)regs;
    for (i = 0; i < offset; i++)
        machCtrlPtr[i] = pMem[MACH_CTRL_CH];
    return 0;
}

/*
   Write only the bytes necessary(in offset) and return
 */
int updateMachCtrlRegs(bingMachineCtrl *regs, int offset)
{
    volatile uint8_t *pMem;
    uint8_t *machCtrlPtr;
    int i;
    pMem = (volatile uint8_t *)pFpgaMem;
    RESET_CH_CTR(pMem);
    machCtrlPtr = (uint8_t *)regs;
    for (i = 0; i < offset; i++)
        pMem[MACH_CTRL_CH] = machCtrlPtr[i];
    return 0;
}

// mask is value with all relevant bits set 1. It is complemented and
// ANDed with read-value to clear afftected bits before ORing the new value
int setCtrlRegBits(int offset, uint16_t mask, uint16_t value)
{
    int readSz = offset + 1; // +1:account for 0 based index of offset
    uint16_t regSpace[sizeof(bingMachineCtrl)];
    bingMachineCtrl *machCtrl = (bingMachineCtrl *)regSpace;
    uint16_t regVal;
    if (offset >= (sizeof(bingMachineCtrl)/2))
    {
        logPrint("%s: Error: invalid offset(%d)\n", __func__, offset);
        return -1;
    }
    readSz *= 2; 
    readMachCtrlRegs(machCtrl, readSz);
    // update register values now..
    if (mask == 0xFFFF)
        regVal = value;
    else
    {
        regVal = bswap_16(regSpace[offset]);
        regVal &= (~mask);  // clear bits
        regVal |= value;    // set bits
    }
    //printf("off: %d mask %X value %X reg %X\n",offset,mask,value,regVal);fflush(stdout);
    regSpace[offset] = bswap_16(regVal);
    updateMachCtrlRegs(machCtrl, readSz);
    updateSync(SYNC_REGS);
    return 0;
}

// Write specified number of bytes starting at offset
// Writes ONLY the lower byte of each location
int writeCtrlReg(int offset, int size, void *value)
{
    int readSz = offset + 1 + size; // +1:account for 0 based index of offset
    uint16_t regSpace[sizeof(bingMachineCtrl)];
    bingMachineCtrl *machCtrl = (bingMachineCtrl *)regSpace;
    uint8_t *vPtr = (uint8_t *)value;
    int i;
    if ((offset+size) > (sizeof(bingMachineCtrl)/2))
    {
        logPrint("%s: Error: invalid offset(%d)\n", __func__, offset);
        return -1;
    }
    readSz *= 2; 
    readMachCtrlRegs(machCtrl, readSz);
    // update register values now, accounting for endian-difference.
    for (i = 0; i < size; i++)
        regSpace[offset+i] = (vPtr[i] << 8);
    updateMachCtrlRegs(machCtrl, readSz);
    updateSync(SYNC_REGS);
    return 0;
}

// Write specified number of bytes starting at offset
// Writes both lower and upper bytes of each register
int writeCtrlRegFull(int offset, int size, uint16_t *value)
{
    int readSz = offset + 1 + size; // +1:account for 0 based index of offset
    uint16_t regSpace[sizeof(bingMachineCtrl)];
    bingMachineCtrl *machCtrl = (bingMachineCtrl *)regSpace;
    int i;
    if ((offset+size) > (sizeof(bingMachineCtrl)/2))
    {
        logPrint("%s: Error: invalid offset(%d)\n", __func__, offset);
        return -1;
    }
    readSz *= 2; 
    readMachCtrlRegs(machCtrl, readSz);
    // update register values now, accounting for endian-difference.
    for (i = 0; i < size; i++)
        regSpace[offset+i] = bswap_16(value[i]);
    updateMachCtrlRegs(machCtrl, readSz);
    updateSync(SYNC_REGS);
    return 0;
}

// Write specified number of bytes starting at offset, set and unset the
// sBit in sReg(strobe)
int writeCtrlRegStrobe(int offset, int size, void *value, int sReg, int sBit)
{
    int readSz;
    uint16_t regSpace[sizeof(bingMachineCtrl)];
    bingMachineCtrl *machCtrl = (bingMachineCtrl *)regSpace;
    uint16_t rVal;
    int i;
    if (sReg > (offset + size))
        readSz = sReg + 1;
    else
        readSz = offset + 1 + size;
    if ((offset+size) > (sizeof(bingMachineCtrl)/2))
    {
        logPrint("%s: Error: invalid offset(%d)\n", __func__, offset);
        return -1;
    }
    readSz *= 2; 
    //logPrint("%s: off %d sz %d sR %d sB %d rdSz %d\n", __func__, offset, 
    //              size, sReg, sBit, readSz);
    readMachCtrlRegs(machCtrl, readSz);
    // update register values now..
    for (i = 0; i < size; i++)
        regSpace[offset+i] = (((uint8_t *)value)[i] << 8);
    updateMachCtrlRegs(machCtrl, readSz);
    updateSync(SYNC_REGS);
    // Set specified bit
    rVal = bswap_16(regSpace[sReg]);
    rVal |= (1 << sBit);
    regSpace[sReg] = bswap_16(rVal);
    updateMachCtrlRegs(machCtrl, readSz);
    updateSync(SYNC_REGS);
    // Un-Set specified bit
    rVal &= ~(1 << sBit);
    regSpace[sReg] = bswap_16(rVal);
    updateMachCtrlRegs(machCtrl, readSz);
    updateSync(SYNC_REGS);
    return 0;
}

// Read specified number of bytes from offset.
// Returns ONLY the lower bytes from the 16 bit registers..
int readCtrlRegL(int offset, int size, void *value)
{
    int readSz = offset + 1 + size; // +1:account for 0 based index of offset
    uint16_t regSpace[sizeof(bingMachineCtrl)];
    bingMachineCtrl *machCtrl = (bingMachineCtrl *)regSpace;
    int i;
    if ((offset+size) > (sizeof(bingMachineCtrl)/2))
    {
        logPrint("%s: Error: invalid offset(%d)\n", __func__, offset);
        return -1;
    }
    readSz *= 2; 
    readMachCtrlRegs(machCtrl, readSz);
    for (i = 0; i < size; i++)
        ((uint8_t *)value)[i] = (regSpace[offset+i] >> 8) & 0xff;
    return 0;
}

// Read specified number of registers from offset.
int readCtrlReg(int offset, int size, uint16_t *value)
{
    int readSz = offset + 1 + size; // +1:account for 0 based index of offset
    uint16_t regSpace[sizeof(bingMachineCtrl)];
    bingMachineCtrl *machCtrl = (bingMachineCtrl *)regSpace;
    int i;
    if ((offset+size) > (sizeof(bingMachineCtrl)/2))
    {
        logPrint("%s: Error: invalid offset(%d)\n", __func__, offset);
        return -1;
    }
    readSz *= 2; 
    readMachCtrlRegs(machCtrl, readSz);
    for (i = 0; i < size; i++)
        value[i] = bswap_16(regSpace[offset+i]);
    return 0;
}


// read lower byte of register
int readStatReg(int offset, int size, void *value)
{
    int readSz = offset + 1 + size;
    uint16_t regSpace[sizeof(bingMachStatus)];
    bingMachStatus *machStat = (bingMachStatus *)regSpace;
    int i;
    updateSync(SYNC_REGS);
    usleep(10);
    if ((offset+size) > (sizeof(bingMachStatus)/2))
    {
        logPrint("Error: invalid offset(%d)\n", offset);
        return -1;
    }
    updateSync(SYNC_REGS);
    usleep(1000);
    readSz *= 2;
    readMachStatRegs(machStat, readSz);
    for (i = 0; i < size; i++)
        ((uint8_t *)value)[i] = (regSpace[offset+i] >> 8) & 0xff;
    return 0;
}

// read both bytes of register, not just lower byte
int readStatRegFull(int offset, int size, uint16_t *bytes)
{
    int readSz = offset + size;
    uint16_t regSpace[sizeof(bingMachStatus)];
    bingMachStatus *machStat = (bingMachStatus *)regSpace;
    int i;
    if ((offset+size) > (sizeof(bingMachStatus)/2))
    {
        logPrint("Error: invalid offset(%d)\n", offset);
        return -1;
    }
    updateSync(SYNC_REGS);
    usleep(10);
    readSz *= 2;
    readMachStatRegs(machStat, readSz);
    for (i = 0; i < size; i++)
        bytes[i] = bswap_16(regSpace[offset+i]);
    return 0;
}

// read both bytes of register and return without byte-swap
int readStatRegNoSwap(int offset, int size, uint16_t *bytes)
{
    int readSz = offset + size;
    uint16_t regSpace[sizeof(bingMachStatus)];
    bingMachStatus *machStat = (bingMachStatus *)regSpace;
    int i;
    if ((offset+size) > (sizeof(bingMachStatus)/2))
    {
        logPrint("Error: invalid offset(%d)\n", offset);
        return -1;
    }
    updateSync(SYNC_REGS);
    usleep(10);
    readSz *= 2;
    readMachStatRegs(machStat, readSz);
    for (i = 0; i < size; i++)
        bytes[i] = regSpace[offset+i];
    return 0;
}

int libCheckRxUARTEmpty(int uart)
{
    bingFifoEmptyStat *statPtr;
    uint16_t statReg;
    readStatRegNoSwap(1, 1, &statReg);
    statPtr = (bingFifoEmptyStat *)(&statReg);
    switch (uart)
    {
        case 0:
            return (statPtr->RxFifo0Empty);
            break;
        case 1:
            return (statPtr->RxFifo1Empty);
            break;
        case 2:
            return (statPtr->RxFifo2Empty);
            break;
        case 3:
            return (statPtr->RxFifo3Empty);
            break;
        case 4:
            return (statPtr->RxFifo4Empty);
            break;
        case 5:
            return (statPtr->RxFifo5Empty);
            break;
        case 6:
        case 7:
            return 0; // do not have status flags for these...
        default:
            return 1;
    }
    return 1;
}

int libCheckTxUARTFull(int uart)
{
    bingFifoFullStat *statPtr;
    uint16_t statReg;
    readStatRegNoSwap(2, 1, &statReg);
    statPtr = (bingFifoFullStat *)(&statReg);
    switch (uart)
    {
        case 0:
            return (statPtr->RxFifo0Full);
            break;
        case 1:
            return (statPtr->RxFifo1Full);
            break;
        case 2:
            return (statPtr->RxFifo2Full);
            break;
        case 3:
            return (statPtr->RxFifo3Full);
            break;
        case 4:
            return (statPtr->RxFifo4Full);
            break;
        case 5:
            return (statPtr->RxFifo5Full);
            break;
        case 6:
        case 7:
            return 0; // do not have status flags for these...
        default:
            return 1;
    }
    return 1;
}

int libWriteToUART(int uartNo, uint8_t *str, int len, uint32_t delay)
{
    volatile uint8_t *pMem;
    int i;
    if ((uartNo < 0) || (uartNo > 7))
        return -1;
    pMem = (volatile uint8_t *)pFpgaUartBase;
    /* FPGA Fifo number is same as the uart no */
    for (i = 0; i < len; i++)
    {
        pMem[uartNo] = str[i];
        if (delay)
            usleep(delay*1000);
    }
    return 0;
}

/*
  Length of rdStr is ASSUMED to be RX_FIFO_SZ
 */
int libReadFromUART(int uartNo, uint32_t delay, char *rdStr, int len)
{
    uint8_t volatile *pMem;
    volatile unsigned char fifoStr;
    int i;
    logPrint("%s: uart %d\n", __func__, uartNo);
    if ((uartNo < 0) || (uartNo > 7))
        return -1;
    if ((len <= 0) || (len > RX_FIFO_SZ))
        len = RX_FIFO_SZ;
    pMem = (volatile uint8_t *)pFpgaUartBase;
    /* FPGA Fifo number is same as the uart no */
    for (i = 0; i < len; i++)
    {
        fifoStr = pMem[uartNo];
        rdStr[i] = fifoStr;
        usleep(delay*1000);
    }
    return i;
}

int initLib()
{
    if (mapFpgaMem())
        return -1;
    return 0;
}
