%define pfx /opt/freescale/rootfs/%{_target_cpu}

Summary         : uCLinux userspace debug agent for CF CodeWarrior
Name            : AppTRK-CF
Version         : 1.25
Release         : 1
License         : Freescale EULA
Vendor          : Freescale
Packager        : Tudor Stanescu
Group           : Development/Debuggers
Source          : %{name}-%{version}.tar.gz
BuildRoot       : %{_tmppath}/%{name}
Prefix          : %{pfx}

%Description
%{summary}

%Prep
%setup 

%Build
make all

%Install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{pfx}/usr/bin
make install DESTDIR=$RPM_BUILD_ROOT/%{pfx}

%Clean
rm -rf $RPM_BUILD_ROOT

%Files
%defattr(-,root,root)
%{pfx}/*
