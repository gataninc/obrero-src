#ifndef __FPGA_H
#define __FPGA_H

#define FPGA_MEMORY_BASE    0xE5000000
#define FPGA_UART_BASE      0xE6000000
#define FPGA_MEMORY_SIZE    (64*1024)

/* FPGA internal resources on CS3 */
#define  MACH_CTRL_CH     0x0 /* 1024B */
#define  MACH_STAT_CH     0x1 /* 1024B */
#define  LOOKUP_TBL_A_CH  0x2 /* 1024B */
#define  LOOKUP_TBL_B_CH  0x3 /* 1024B */
#define  CPU_FPGA_SYNC_CH 0x4 /* 1024B */
#define  DBG_CHAN         0x5 /* 1024B?? */
#define  RESET_CTRL_CHAN  0x6 /* one byte */
#define  FPGA_UCODE_CH    0x7

#define FPGA_STOP_UCODE      0x01
#define FPGA_START_UCODE     0x02
#define FPGA_RST_UCODE_PC    0x04
#define FPGA_RST_CHAN_CTR    0x08
#define FPGA_RST_UART        0x10  // obsolete
#define FPGA_RST_TOUCH_P     0x20

#define RESET_CH_CTR(mPtr)      (mPtr[RESET_CTRL_CHAN] = FPGA_RST_CHAN_CTR)
#define START_UCODE_EXEC(mPtr)  (mPtr[RESET_CTRL_CHAN] = FPGA_START_UCODE) 
#define STOP_UCODE_EXEC(mPtr)   (mPtr[RESET_CTRL_CHAN] = FPGA_STOP_UCODE)
#define RESET_UCODE_PC(mPtr)    (mPtr[RESET_CTRL_CHAN] = FPGA_RST_UCODE_PC)
#define RESET_FUSION_TP(mPtr)   (mPtr[RESET_CTRL_CHAN] = FPGA_RST_FUSION_TP)
#define RESET_OFF(mPtr)         (mPtr[RESET_CTRL_CHAN] &= ~(FPGA_RST_FUSION_TP))

/* FPGA internal resources on CS4 */
#define UART0FIFO     0
#define UART1FIFO     1
#define UART2FIFO     2
#define UART3FIFO     3
#define UART4FIFO     4
#define UART5FIFO     5

#define TX_FIFO_SZ    256
#define RX_FIFO_SZ    256

#define SYNC_REGS       1
#define SYNC_LOOKUP_TBL 2

typedef struct
{
    uint8_t  loByte;
    uint8_t  hiByte;
} bingFPGAReg;

typedef struct 
{
//Offset 0
    bingFPGAReg  opCtrl;
    bingFPGAReg  heaterCtrl;
    bingFPGAReg  digCtrlReg1;
    bingFPGAReg  digCtrlReg2;
//4
    bingFPGAReg  auxCtrlReg;
    bingFPGAReg  ancillaryCtrlReg;
    bingFPGAReg  calienteCtrlReg;
    bingFPGAReg  tbd1;
//8
    bingFPGAReg  heaterDACOut0;
    bingFPGAReg  heaterDACOut1;
    // strobe registers are used to initiate a read from SPI devices.
    // Set this register to non-zero value, FPGA will read the device
    // and make output available in the corresponding status registers.
    bingFPGAReg  NV3035C0;  // lcd access
    bingFPGAReg  NV3035C1;  // lcd access
//12
    bingFPGAReg  tbd2;
    bingFPGAReg  tbd3;
    bingFPGAReg  tbd4;
    bingFPGAReg  tbd5;
//16
    bingFPGAReg  heaterPIDSetPt0;
    bingFPGAReg  heaterPIDSetPt1;
    bingFPGAReg  heaterPIDSetPt2;
    bingFPGAReg  heaterPGain;
//20
    bingFPGAReg  heaterIGain;
    bingFPGAReg  heaterDGain;
    // 0: close PID loop, 1: reset integral err, 2: reset overflow & underflow
    bingFPGAReg  heaterPIDCtrl;  
    bingFPGAReg  heaterPIDLoopDelay0;
//24
    bingFPGAReg  heaterPIDLoopDelay1;
    bingFPGAReg  heaterPIDLoopDelay2;
    bingFPGAReg  tbd6;
    bingFPGAReg  tbd7;
//28
    bingFPGAReg  tbd8;
    bingFPGAReg  tbd9;
    bingFPGAReg  tbd10;
    bingFPGAReg  tbd11;
//32
    bingFPGAReg  CJCOffset0;
    bingFPGAReg  CJCOffset1;
    bingFPGAReg  CJCOffset2;
    bingFPGAReg  CJCCorrectionRAMAddr0;
//36
    bingFPGAReg  CJCCorrectionRAMAddr1;
    // 0: TBD, 1: reset err status, 2: write calibration RAM data
    // 3: write cal. data RAM address, 4: incr. RAM address on read/write
    // 5: LSB for ADC output selection 6: MSB for ADC output sel
    // ADC Ouput selects:
    // 00: Thermo-couple front end   01: Diode/RTD front end
    // 10: Pre-amp interface         11: output const. value of 0.
    bingFPGAReg  calibrationCtrl;
    // calibration data is 21 bits, so three byte are used
    bingFPGAReg  calDataByte0;
    bingFPGAReg  calDataByte1;
//40
    bingFPGAReg  calDataByte2;
    // calibration delta value is 16 bit signed.
    bingFPGAReg  calDataDelta0;
    bingFPGAReg  calDataDelta1;
    bingFPGAReg  interpolScale; // interpolation scaling - cal. look up
} bingMachineCtrl;

#define ADC_REG_VAL(adcReg) ((adcReg.hiByte << 8) | adcReg.loByte)

typedef struct
{
/* byte 1 MSB */
    uint16_t RxFifo1Empty : 1;
    uint16_t RxFifo0Empty : 1;
    uint16_t TxFifo5Empty : 1;    /* RS 485 channel */
    uint16_t TxFifo4Empty : 1;
    uint16_t TxFifo3Empty : 1;
    uint16_t TxFifo2Empty : 1;
    uint16_t TxFifo1Empty : 1;
    uint16_t TxFifo0Empty : 1;
/* byte 1 MSB */
    uint16_t nu           : 4;
    uint16_t RxFifo5Empty : 1;    /* RS 485 channel */
    uint16_t RxFifo4Empty : 1;    /* CJC EEPROM */
    uint16_t RxFifo3Empty : 1;
    uint16_t RxFifo2Empty : 1;
} bingFifoEmptyStat;

typedef struct
{
/* byte 1 MSB */
    uint16_t RxFifo1Full : 1;
    uint16_t RxFifo0Full : 1;
    uint16_t TxFifo5Full : 1;     /* RS 485 channel */
    uint16_t TxFifo4Full : 1;
    uint16_t TxFifo3Full : 1;
    uint16_t TxFifo2Full : 1;
    uint16_t TxFifo1Full : 1;
    uint16_t TxFifo0Full : 1;
/* byte 2 MSB */
    uint16_t nu : 4;
    uint16_t RxFifo5Full : 1;     /* RS 485 channel */
    uint16_t RxFifo4Full : 1;     /* CJC EEPROM */
    uint16_t RxFifo3Full : 1;
    uint16_t RxFifo2Full : 1;
}  bingFifoFullStat;

typedef struct 
{
/* byte 1 MSB */
    uint16_t Uart1ParErr : 1;
    uint16_t Uart0ParErr : 1;
    uint16_t Uart5FrErr  : 1;
    uint16_t Uart4FrErr  : 1;
    uint16_t Uart3FrErr  : 1;
    uint16_t Uart2FrErr  : 1;
    uint16_t Uart1FrErr  : 1;
    uint16_t Uart0FrErr  : 1;
/* byte 1 MSB */
    uint16_t nu          : 4;
    uint16_t Uart5ParErr : 1;
    uint16_t Uart4ParErr : 1;
    uint16_t Uart3ParErr : 1;
    uint16_t Uart2ParErr : 1;
} bingRxUartError1;

typedef struct
{
/* byte 1 MSB */
    uint16_t nu1         : 2;
    uint16_t Uart5OvrRun : 1;
    uint16_t Uart4OvrRun : 1;
    uint16_t Uart3OvrRun : 1;
    uint16_t Uart2OvrRun : 1;
    uint16_t Uart1OvrRun : 1;
    uint16_t Uart0OvrRun : 1;
/* byte 2 MSB */
    uint16_t nu2         : 8;
} bingRxUartError2;

typedef struct 
{
// idx 0
    bingFPGAReg  systemStatus;
    bingFifoEmptyStat FIFOEmptyStat;
    bingFifoFullStat  FIFOFullStat;
    bingRxUartError1   rxErrors1;      // parity and framing errors
//4
    bingRxUartError2   rxErrors2;      // over run errors
    bingFPGAReg  fpgaRev;   // verilog code version
    bingFPGAReg  adcDOUT1B0; // byte 0
    bingFPGAReg  adcDOUT1B1; // byte 1
//8
    bingFPGAReg  adcDOUT1B2; // byte 2
    bingFPGAReg  adcDOUT2B0; // byte 0
    bingFPGAReg  adcDOUT2B1; // byte 1
    bingFPGAReg  adcDOUT2B2; // byte 2
//12
    bingFPGAReg  adcDOUT3B0; // byte 0
    bingFPGAReg  adcDOUT3B1; // byte 1
    bingFPGAReg  adcDOUT3B2; // byte 2
    bingFPGAReg  heatHiADCB0; // byte 0
//16
    bingFPGAReg  heatHiADCB1; // byte 1
    bingFPGAReg  ClIADCB0; 
    bingFPGAReg  ClIADCB1;
    bingFPGAReg  heaterPIDDataB0;
//20
    bingFPGAReg  heaterPIDDataB1;
    bingFPGAReg  mCodeRev;   // micro-code revision
    bingFPGAReg  constVal1;   // 0x456
    bingFPGAReg  constVal2;   // 0x789
// 24
    bingFPGAReg  HeaterPIDSetPtB0;
    bingFPGAReg  HeaterPIDSetPtB1;
    bingFPGAReg  HeaterPIDSetPtB2;
    bingFPGAReg  heaterPIDPGain;
// 28
    bingFPGAReg  heaterPIDIGain;
    bingFPGAReg  heaterPIDDGain;
    // 0: ovrflow from 1st subtractor, 1: ovrflow from 1st adder
    // 2: ovrflow from 2nd subtractor, 3: ovrflow from 2st adder
    // 4: ovrflow from 3 adder
    bingFPGAReg  heaterOvrFlowErr;
    // 0: COUT from 1st subtractor, 1: COUT from 1st adder
    // 2: COUT from 2nd subtractor, 3: COUT from 2st adder
    // 4: COUT from 3 adder
    bingFPGAReg  heaterPIDCoutErr;
// 32
    bingFPGAReg HeaterPIDIntErrB0;   // Byte 0, integral error 
    bingFPGAReg HeaterPIDIntErrB1;
    bingFPGAReg HeaterPIDIntErrB2;
    bingFPGAReg HeaterPIDCtrlReg;
// 36
    bingFPGAReg HeaterPIDDelayB0;    // loop delay
    bingFPGAReg HeaterPIDDelayB1;
    bingFPGAReg HeaterPIDDelayB2;
    // 0: summed error from 1st subtractor  1: summed error from 1st adder
    // 2: summed error from 2nd subtractor  3: summed error from 2nd adder
    // 4: summed error from 3rd adder
    bingFPGAReg HeaterPIDCombErr;    // combined error reg
// 40
    bingFPGAReg CJCOffsetB0;
    bingFPGAReg CJCOffsetB1;
    bingFPGAReg CJCOffsetB2;
    bingFPGAReg CJCRAMAddrB0;
// 44
    bingFPGAReg CJCRAMAddrB1;
    bingFPGAReg tempCorrCtrl; // temperature correction control reg
    // 0: overflow from 1st adder  1: overflow from 2nd adder
    // 2: cout from 1st adder      3: cout from 2nd adder
    // 4: neg value from 1st adder
    bingFPGAReg tempCorrStat; // temperature correction status  reg
    // 0: summed from 1st adder  1: summed error from 2nd adder
    // 2: neg value from 1st adder
    bingFPGAReg tempCorrErr;  // temperature correction error reg
// 48
    bingFPGAReg calDataByte0;
    bingFPGAReg calDataByte1;
    bingFPGAReg calDataByte2;
    bingFPGAReg calDataDelta0;
// 52
    bingFPGAReg calDataDelta1;
    // read-back of corrected temp. from FPGA's PID loop, debug
    bingFPGAReg correctedTempB0;
    bingFPGAReg correctedTempB1;
    bingFPGAReg correctedTempB2;
// 56
    bingFPGAReg ADCMainsAccB0;
    bingFPGAReg ADCMainsAccB1;
    bingFPGAReg ADCMainsAccB2;
    bingFPGAReg ADCMainsAccB3;
// 60
// bits 4:0 is bits 36:32 of ADC Mains accumulation.
// bits 7:5 set to 0, bits 11:8 c_adc_data[15:12]. Caliente ADC not used now.
    bingFPGAReg ADCMainsAccB4;
    bingFPGAReg ADCMainsSamples;
    bingFPGAReg DerivateLowerTerm;
    bingFPGAReg DerivativeUpperTerm;
// 64
    bingFPGAReg Alt0ADCAccB0;
    bingFPGAReg Alt0ADCAccB1;
    bingFPGAReg Alt0ADCAccB2;
    bingFPGAReg Alt0ADCAccB3;
// 68
    bingFPGAReg Alt0ADCAccB4;
    bingFPGAReg Alt0Samples;
    bingFPGAReg AltAccStatus;
    bingFPGAReg constZero;    /* constant 0 */
// 72
    bingFPGAReg Alt1ADCAccB0;
    bingFPGAReg Alt1ADCAccB1;
    bingFPGAReg Alt1ADCAccB2;
    bingFPGAReg Alt1ADCAccB3;
// 76
    bingFPGAReg Alt1ADCAccB4;
    bingFPGAReg Alt1Samples;
    bingFPGAReg C7P3728MHzL; // 7.3728MHz Counter: lower 12 bits
    bingFPGAReg C7P3728MHzR; // 7.3728MHz Counter: higher 12 bits
} bingMachStatus;

#endif //__FPGA_H
