#ifndef __DMMFUNCS_H
#define __DMMFUNCS_H

int writeDMMString(int mySocket,char *string);
int readDMMString(int mySocket,char *buffer);
int getDMMString(int dmmSock, char *cmd, char *reply, int repLen);
int openDMM(char *IPStr);
int closeDMM(int desc);

#endif
