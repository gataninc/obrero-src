/*
 * Copyright (C) 2008 Freescale Semicondutor, Inc. All rights reserved.
 *
 * Author: Bruce Schmid <duck@freescale.com>
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */
/*
 * These routines are needed for i2c/serial transceivers
 * on other platforms
 */
static inline void
fsl_platform_set_device_mode(struct fsl_usb2_platform_data *pdata)
{}

static inline void
fsl_platform_pullup_enable(struct fsl_usb2_platform_data *pdata)
{}

static inline void
fsl_platform_pullup_disable(struct fsl_usb2_platform_data *pdata)
{}
