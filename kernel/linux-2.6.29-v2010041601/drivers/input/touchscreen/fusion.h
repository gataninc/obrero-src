/*
 *  "Fusion" touchscreen driver
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 */

#include <linux/ioctl.h>

/* I2C slave address */
#define FUSION_I2C_SLAVE_ADDR			0x10

/* I2C registers */
#define FUSION_DATA_INFO			0x00

/* First Point*/
#define FUSION_POS_X1_HI			0x01 	/* 16-bit register, MSB */
#define FUSION_POS_X1_LO			0x02 	/* 16-bit register, LSB */
#define FUSION_POS_Y1_HI			0x03 	/* 16-bit register, MSB */
#define FUSION_POS_Y1_LO			0x04 	/* 16-bit register, LSB */
#define FUSION_FIR_PRESS			0X05
#define FUSION_FIR_TIDTS			0X06

/* Second Point */
#define FUSION_POS_X2_HI			0x07 	/* 16-bit register, MSB */
#define FUSION_POS_X2_LO			0x08 	/* 16-bit register, LSB */
#define FUSION_POS_Y2_HI			0x09 	/* 16-bit register, MSB */
#define FUSION_POS_Y2_LO			0x0A 	/* 16-bit register, LSB */
#define FUSION_SEC_PRESS			0x0B
#define FUSION_SEC_TIDTS			0x0C

#define FUSION_VIESION_INFO_LO		0X0E
#define FUSION_VIESION_INFO			0X0F

#define FUSION_RESET				0x10
#define FUSION_SCAN_COMPLETE			0x11

struct fusion_data {
	struct i2c_client		*client;
	struct workqueue_struct		*workq;
	struct workqueue_struct		*dev_check_q;
	struct input_dev		*input;
    struct timer_list reset_timer;
    struct timer_list dev_check_timer;
	int				irq;
	u16				x1;
	u16				y1;
	u8				z1;
	u8				tip1;
	u8				tid1;
	u16				x2;
	u16				y2;
	u8				z2;
	u8				tip2;
	u8				tid2;
	u8				f_num;
	u8				save_points;
#define					FUSION_SAVE_PT1		0x1
#define					FUSION_SAVE_PT2		0x2
};

/* unused ioctl magic number, at least now.. */
#define TPDEV_IOCTL_MAGIC 0xC5

// return number touch screen resets
#define TPDEV_GET_TPRSTCNT    _IOR(TPDEV_IOCTL_MAGIC, 1, int)
// return whether user-land need to do a reset.
#define TPDEV_GET_TPRSTSTAT   _IOR(TPDEV_IOCTL_MAGIC, 2, int)
// set tp reset dbg flag
#define TPDEV_SET_DBGFLAG     _IOW(TPDEV_IOCTL_MAGIC, 3, int)
// set tp reset trace flag
#define TPDEV_SET_TRACEFLAG   _IOW(TPDEV_IOCTL_MAGIC, 4, int)
// set tp reset reset delay
#define TPDEV_SET_RESET_DELAY _IOW(TPDEV_IOCTL_MAGIC, 5, int)

#define TPDEV_IOCTL_MAX  5
