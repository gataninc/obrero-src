/*
 * Task to read cold junction temperature and compute the compensation
 * temperature corresponding to the thermocouple type used.
 * Work:
 * Do the following tasks when CJC is enabled once in a few seconds/minutes?:
 * Read LM35, compute cold junction compensation temperature.
 * Update FPGA register and shared memory with the comp. factor.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <syslog.h>
#include "bing_common.h"
#include "sharedmem.h"
#include "regs.h"
#include "utils.h"
#include "i2cutils.h"
#include "tc_calibrate.h"

volatile sysParams *pCache;

int mcp3424Read(int bus, int addr, int chan, int gain, float *val);
static int computeCJCVoltage(float *v)
{
    int ret = 0;
    float val;
    double T = 0.0;
    int bus = 2, devAddr = 0x68;
    int gain = 2;
    // read i2c device channel 1 to get voltage.
    if ((ret = mcp3424Read(bus, devAddr, 1, gain, &val)) < 0)
        return ret;
    val = val/10.0; // 10mv/degree, @0degC, o/p is 0V
    //fprintf(stderr, "CJC Voltage: %0.5f degC -> ", val);
    // Compute the voltage corresponding to this temperature that would
    // be produced by the TC type
    switch (pCache->sensorType[pCache->cjcIdx])
    {
        case Type_K:
            T = cjcTemp2tcVolt(CAL_TYPE_K, (double)val);
            break;
        case Type_R:
            T = cjcTemp2tcTypeRVolt((double)val);
            break;
        case Type_T:
            T = cjcTemp2tcVolt(CAL_TYPE_T, (double)val);
            break;
    }
    //logPrint("%0.5fmV\n", T);
    *v = T;
    return 0;
}

// find register value corresponding to a voltage read-back for tc
// Opposite of function scaleTADC()
#define TADC_HALF_VAL (0x7FFFFF)
#define TADC_DIV      ((float)(8388607-1)) // 2^23-1
#define TADC_VREF     (2.50)
static uint32_t unScaleTADC(double fVal)
{
    int isNeg = 0;
    uint32_t rVal = 0;
    if (fVal < 0.000)
        isNeg = 1;
    fVal *= 42.15226; // redo scaling
    fVal = fabs(fVal);
    //rVal = fVal/(TADC_VREF/TADC_DIV);
    rVal = (uint32_t)((fVal * TADC_DIV) / TADC_VREF);
    if (isNeg)
        rVal += TADC_HALF_VAL;
    return rVal;
}

// Update CJC offset bytes in FPGA
static int updateFPGAReg(uint32_t rVal)
{
    uint8_t vPtr[3];
    int offset = 32;
    int numBytes = 3;
    int ret = 0;

    rVal &= 0xFFFFFF; // 24Bit clean
    vPtr[0] = rVal & 0xff;
    vPtr[1] = (rVal >> 8) & 0xff;
    vPtr[2] = (rVal >> 16) & 0xff;
    if (lockCtrlReg() != 0)
    {
        logPrint("Error: failed to get lock\n");
        return -1;
    }
    if ((ret = writeCtrlReg(offset, numBytes, (void *)vPtr)) != 0)
        logPrint("Error: failed to set PID param\n");
    unlockCtrlReg();
    return ret;
}

#define UPDATE_INTERVAL  (1) // seconds
static int doCJCComp()
{
    float v;
    uint32_t rVal;
    int retFromCJC = 0;
    for (; ;)
    {
        // wait till CJC task is enabled.
        while (! pCache->enableCJC)
        {
            if (retFromCJC)
            {
                logPrint("== Clear CJC Offset Regs\n");
                pCache->CJCFact = 0.0;
                if (updateFPGAReg((uint32_t)0) == 0)
                    retFromCJC = 0;
            }
            sleep(1);
        }
        do 
        {
            if (computeCJCVoltage(&v) == 0)
            {
                pCache->CJCFact = (v/1000.0); // convert to volts..
                rVal = unScaleTADC(pCache->CJCFact);
                //logPrint("   RVal 0x%X\n", rVal);
                // Update FPGA registers ONLY if this is the primary
                // sensor.
                if (pCache->cjcIdx == 0)
                    updateFPGAReg(rVal);
            }         
            sleep(UPDATE_INTERVAL);
        } while (pCache->enableCJC);
        retFromCJC = 1;
    }
}

int main(int argc, char *argv[])
{
    if (initLib() != 0)
    {
        syslog(LOG_ERR, "Failed to init library!");
        logPrint("Error: failed to init library!!\n");
        exit(1);
    }
    if ((pCache = getSharedMemory(IPC_KEY_SHAREDMEM, sizeof(sysParams))) == 0)
    {
        syslog(LOG_ERR, "Error attaching shared memory");
        logPrint("Error attaching shared memory\n");
        exit(1);
    }
    syslog(LOG_NOTICE, "CJC task started");
    doCJCComp();
    syslog(LOG_ERR, "END CJC task");
    return 0;
}
