#ifndef __LINUX_I2C_MA17P0X_H
#define __LINUX_I2C_MA17P0X_H

/* linux/i2c/ma17p0x.h */

struct ma17p0x_platform_data {
	u16	model;				/* 2007. */
	int	gpio;

	int	(*get_pendown_state)(void);
	void	(*clear_penirq)(void);		/* If needed, clear 2nd level
						   interrupt source */
   	int	(*if_penirq)(void);
	void 	(*enable_irq)(int);
	int	(*init_platform_hw)(void);
	void	(*exit_platform_hw)(void);
	void 	(*gpio_attb)(int);
};

#endif
