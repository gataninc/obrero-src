%define pfx /opt/freescale/rootfs/%{_target_cpu}

Summary         : Universal Bootloader firmware
Name            : u-boot
Version         : 1.2.0
Release         : fsl20070622
License         : GPL
Vendor          : Freescale
Packager        : Ebony Zhu
Group           : Applications/System
Source          : u-boot-86xx-20070116.tar.gz
Patch0          : u-boot-fsl-1.2.0-MPC8641_PEX2_LAW.patch
Patch1          : u-boot-fsl-1.2.0-MPC8641_FIXUP_AHCI_OVERFLOW.patch
Patch2          : u-boot-fsl-1.2.0-MPC8641_FLASH.patch
Patch3          : u-boot-fsl-1.2.0-MPC8641_ULI_RTC.patch
Patch4          : u-boot-fsl-1.2.0-MPC8641_CW_DEBUG-2.patch
Patch5          : u-boot-fsl-1.2.0-MPC8641_RST_ALTBANK.patch
Patch6          : u-boot-fsl-1.2.0-MPC8641_EEPROM.patch
Patch7          : u-boot-fsl-1.2.0-MPC8641_PCIE_FIX.patch
Patch8          : u-boot-fsl-1.2.0-MPC8641_CONF_FIX.patch
Patch9          : u-boot-fsl-1.2.0-MPC8641_DDR600.patch
Patch10         : u-boot-fsl-1.2.0-MPC8641_DT_MAC.patch
Patch11         : u-boot-fsl-1.2.0-MPC8641_ASMP.patch
Patch12         : u-boot-fsl-1.2.0-MPC8641_DQ7.patch
Patch13         : u-boot-fsl-1.2.0-MPC8641_RIO.patch
BuildRoot       : %{_tmppath}/%{name}
Prefix          : %{pfx}

%Description
%{summary}

From Freescale ubootcpdref project plus Freescale patches

%Prep
%setup -n u-boot-86xx-20070116
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1
%patch6 -p1
%patch7 -p1
%patch8 -p1
%patch9 -p1
%patch10 -p1
%patch11 -p1
%patch12 -p1
%patch13 -p1

%Build
PKG_U_BOOT_CONFIG_TYPE=${PKG_U_BOOT_CONFIG_TYPE:-MPC8641HPCN_config}
make HOSTCC="$BUILDCC" CROSS_COMPILE=$TOOLCHAIN_PREFIX $PKG_U_BOOT_CONFIG_TYPE
make HOSTCC="$BUILDCC" HOSTSTRIP="$BUILDSTRIP" \
     CROSS_COMPILE=$TOOLCHAIN_PREFIX $PKG_U_BOOT_BUILD_ARGS all


%Install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{pfx}/boot
for i in u-boot.bin u-boot
do
    cp $i $RPM_BUILD_ROOT/%{pfx}/boot
done

%Clean
rm -rf $RPM_BUILD_ROOT

%Files
%defattr(-,root,root)
%{pfx}/*
