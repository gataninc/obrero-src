#! /bin/sh

DSH=/bin/dsh

if [ -n "$1" ]; then
    nC=$(echo -n $1|wc -c)
    if [ $nC -lt 3 ]; then
        echo "Invalid file name, too short."
        exit
    fi
    outFile=$1
else
    outFile=/dev/null
fi

delay=300
echo "press CTRL_C to stop..."; 
echo

echo -n "Diode    : " | tee $outFile
${DSH} get diode | tee -a $outFile
${DSH} get ground | tee -a $outFile
echo -n "Heater ADC: " | tee -a $outFile 
${DSH} get h-adc | tee -a $outFile
echo "Update Interval: ${delay}mS" | tee -a $outFile
echo | tee -a $outFile
endLoop=0
trap 'endLoop=1' SIGINT
while  :
do
#   ${DSH} get t-adc 2  | tee -a $outFile
    ${DSH} get stats  | tee -a $outFile
    usleep $((delay*1000))
    if [ $endLoop -eq 1 ]; then
        break
    fi
done
