/*******************************
*********************************/
#include <linux/module.h>

#include <linux/init.h>
#include <linux/fs.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/sched.h>
#include <linux/pm.h>
#include <linux/sysctl.h>
#include <linux/proc_fs.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/input.h>
#include <asm/mpc5121_struct.h>

#include <linux/io.h>
#include <linux/of.h>
#include <linux/of_platform.h>

#define	DRV_NAME	"piebox-keyboard"
struct mpc5121_gpio_struct  *get_mpc5121_gpio(void);

enum{
	GPIO_INTERRUPT_ANY_CHANGED=0,
	GPIO_INTERRUPT_LOW_TO_HIGHT,
	GPIO_INTERRUPT_HIGHT_TO_LOW,
	GPIO_INTERRUPT_PULSE2
	
};
#define	GPIO_INTERRUPT_MASK	3
static  unsigned char  gpio_number[]={GPIO_KEY_VOL,GPIO_KEY_MENU,GPIO_KEY_LCD,GPIO_SYSTEM_START_NORFLASH};
#define	PIE_BOX_KEY_NUMBES	sizeof(gpio_number)/sizeof(gpio_number[0])

#define	PIE_BOX_KEY_MASK		(GPIO_BIT_OFFSET(GPIO_KEY_VOL)|GPIO_BIT_OFFSET(GPIO_KEY_MENU)|GPIO_BIT_OFFSET(GPIO_KEY_LCD)|GPIO_BIT_OFFSET(GPIO_SYSTEM_START_NORFLASH))


/*
struct mpc5121_gpio_struct{
	u32	gpdir;
	u32	gpodr;
	u32	gpdat;
	u32	gpier;
	u32	gpier;
	u32	gpimr;
	u32	gpicr1;
	u32	gpicr2;
};
*/
struct piebox_keyboard_struct{	
	unsigned char 	*gpio_number;	
	char name[128];
	char phys[64];
	struct input_dev *dev;
	struct mpc5121_gpio_struct *gpio;
	u32	irq;
	u32	keymap[32];
	u32	keystatus;
	struct timer_list	keydetect_timer;
	
};

static irqreturn_t piebox_keyboard_int_server(int irq, void *dev_id)
{

	struct piebox_keyboard_struct *keyboard=dev_id;
	u32	iostatus,index,flags=0,ievent,ievent_dsable;
	iostatus=keyboard->gpio->gpdat;
	ievent=keyboard->gpio->gpier;
	ievent_dsable=0;
	/*printk("%s() line:%d keystatus=0x%08x\n",__FUNCTION__,__LINE__,iostatus);*/
	for(index=0;index<PIE_BOX_KEY_NUMBES;index++)
	{
		if(ievent&GPIO_BIT_OFFSET(keyboard->gpio_number[index]))
		{
			flags=1;
			break;
		}
	}
	if(!flags)
	{
		return IRQ_NONE;
	}
	for(index=0;index<PIE_BOX_KEY_NUMBES;index++)
	{	
		ievent_dsable|=GPIO_BIT_OFFSET(keyboard->gpio_number[index]);
	}
	keyboard->gpio->gpier=ievent_dsable;	
	mod_timer(&keyboard->keydetect_timer, jiffies + HZ/20);
	return IRQ_HANDLED;
}
static int piebox_keybaord_map_init(struct piebox_keyboard_struct *piebox_keyboard)
{
	int i;
	if(PIE_BOX_KEY_NUMBES>4)
	{
		printk(KERN_ERR"too many keys unmaped\n");
		return -1;
	}
	piebox_keyboard->keymap[GPIO_KEY_MENU]=KEY_F3;/* home */
	piebox_keyboard->keymap[GPIO_KEY_LCD]=KEY_F4;/* backlight */
	piebox_keyboard->keymap[GPIO_KEY_VOL]=KEY_F5;/* vol */
	piebox_keyboard->keymap[GPIO_SYSTEM_START_NORFLASH]=KEY_F6;/* recovery */
	for(i=0;i<32;i++)
	{
		if(piebox_keyboard->keymap[i])
		{
			set_bit(piebox_keyboard->keymap[i], piebox_keyboard->dev->keybit);
		}
	}
	return 0;
}
static void piebox_keyboard_int_config_enable(struct piebox_keyboard_struct *piebox_keyboard)
{
	struct mpc5121_gpio_struct *gpio=piebox_keyboard->gpio;
	unsigned char 	*gpios=piebox_keyboard->gpio_number;
	int index,number,icr1,icr2,imask;
	
	icr1=gpio->gpicr1;
	icr2=gpio->gpicr2;
	imask=gpio->gpimr;
	
	for(index=0;index<PIE_BOX_KEY_NUMBES;index++)
	{
		number=gpios[index];
		if(number<16)
		{
			icr1&=~(GPIO_INTERRUPT_MASK<<(30-number*2));
			 icr1|=(GPIO_INTERRUPT_HIGHT_TO_LOW<<(30-number*2));
		}
		else
		{
			icr2&=~(GPIO_INTERRUPT_MASK<<(30-(number-16)*2));
			icr2|=(GPIO_INTERRUPT_HIGHT_TO_LOW<<(30-(number-16)*2));
		}
		imask|=GPIO_BIT_OFFSET(number);		
	}
	gpio->gpicr1=icr1;
	gpio->gpicr2=icr2;
	gpio->gpimr=imask;

	
	
}
static void key_detect_timer(unsigned long data)
{
	struct piebox_keyboard_struct *keyboard= (struct esdhc_host *)data;	
	u32	iostatus,index;
	iostatus=keyboard->gpio->gpdat;	
	iostatus&=PIE_BOX_KEY_MASK;
	/*printk("iostatus:%08x\n",iostatus);*/
	if(iostatus!=keyboard->keystatus)
	{
		for(index=0;index<PIE_BOX_KEY_NUMBES;index++)
		{
		
			if(iostatus&GPIO_BIT_OFFSET(keyboard->gpio_number[index]))
			{
				input_report_key(keyboard->dev, keyboard->keymap[keyboard->gpio_number[index]],   0x00);
			}
			else
			{
				input_report_key(keyboard->dev, keyboard->keymap[keyboard->gpio_number[index]],   0x01);
			
			}	
		
		}
		input_sync(keyboard->dev);
		keyboard->keystatus=iostatus;
	}
	if((PIE_BOX_KEY_MASK&iostatus)!=PIE_BOX_KEY_MASK)
		mod_timer(&keyboard->keydetect_timer, jiffies + HZ/10);
	
}
static int piebox_keyboard_probe(struct of_device *op, const struct of_device_id *match)
{
	/*struct platform_device *pdev = to_platform_device(dev);*/
	
	
	int err=-1;
	
	struct piebox_keyboard_struct *piebox_keyboard;
	struct input_dev *inputdev;

	piebox_keyboard=kzalloc(sizeof(struct piebox_keyboard_struct), GFP_KERNEL);
	if(!piebox_keyboard)
	{
		printk(KERN_ERR"alloc memory failed.\n");
		return -ENOMEM;
	}

	piebox_keyboard->irq=irq_of_parse_and_map(op->node, 0);
	if(NO_IRQ==piebox_keyboard->irq)
	{
		printk(KERN_ERR"no irq.\n");
		return -ENOMEM;
	}
	printk("piebox-keyboard irq:%d\n",piebox_keyboard->irq);

	piebox_keyboard->gpio=get_mpc5121_gpio();/*ioremap(GENERAL_GPIO_BASE+MPC5121_CFG_IMMR,sizeof(struct mpc5121_gpio_struct));*/
	if(!piebox_keyboard->gpio)
	{
		printk(KERN_ERR"Unmaped gpio memory.\n");
		return -1;
	}
	
	strcpy(piebox_keyboard->name,"piebox-keyboard");
	strcpy(piebox_keyboard->phys,"keyboard/input0");



	piebox_keyboard->dev=input_allocate_device();
	
	if(!piebox_keyboard->dev)
	{
		printk(KERN_ERR"alloc memory failed.\n");
		err= -ENOMEM;
		goto failed1;
	}
	piebox_keyboard->gpio_number=gpio_number;
	
	inputdev=piebox_keyboard->dev;
	inputdev->name=piebox_keyboard->name;
	inputdev->phys=piebox_keyboard->phys;
	inputdev->dev.parent = &op->dev;
	inputdev->evbit[0] =BIT_MASK(EV_KEY) | BIT_MASK(EV_REL);
	set_bit(EV_MSC, inputdev->evbit);
	set_bit(MSC_SCAN, inputdev->mscbit);
	inputdev->id.bustype=BUS_HOST;
	inputdev->id.vendor=0x001;
	inputdev->id.product=0x002;
	inputdev->id.version=0x0100;
	input_set_drvdata(inputdev, piebox_keyboard);
	if(piebox_keybaord_map_init(piebox_keyboard))
	{
		err= -1;
		printk(KERN_ERR"keymap failed.\n");
		goto failed2;
	}	

	err = input_register_device(inputdev);
	if (err)
	{
			printk(KERN_ERR"input register failed.\n");
			goto failed2;
	}	
	
	
	err = request_irq(piebox_keyboard->irq, piebox_keyboard_int_server,
			  IRQF_SHARED|IRQF_DISABLED,
			 "piebox-keybaord", piebox_keyboard);
		if(err)
		{
			printk(KERN_ERR"Request irq:%d failed.\n",piebox_keyboard->irq);
			goto failed3;
		}
	init_timer(&piebox_keyboard->keydetect_timer);
	piebox_keyboard->keydetect_timer.data = (unsigned long)piebox_keyboard;
	piebox_keyboard->keydetect_timer.function =key_detect_timer;
	piebox_keyboard->keydetect_timer.expires = jiffies + HZ/20;
	add_timer(&piebox_keyboard->keydetect_timer);	
	dev_set_drvdata(&op->dev, (void *)piebox_keyboard);
	piebox_keyboard_int_config_enable(piebox_keyboard);
	return 0;
failed3:	
failed2:
	input_free_device(piebox_keyboard->dev);
 failed1:
 	iounmap(piebox_keyboard->gpio);
 	kfree(piebox_keyboard);
 	return err;
}

static int piebox_keyboard_remove(struct of_device *op)
{
	
	struct piebox_keyboard_struct *piebox_keyboard = dev_get_drvdata(&op->dev);
	
	free_irq(piebox_keyboard->irq,piebox_keyboard);
	del_timer(&piebox_keyboard->keydetect_timer);
	input_free_device(piebox_keyboard->dev);
	/*iounmap(piebox_keyboard->gpio);*/
	kfree(piebox_keyboard);
	return 0;
}


static struct of_device_id mpc512x_keybaord_of_match[] = {
	{	 .compatible = "piebox-keyboard",	 },
	{},
};
MODULE_DEVICE_TABLE(of, mpc512x_keybaord_of_match);

static struct of_platform_driver piebox_keyboard_of_platform_driver = {
	.owner = THIS_MODULE,
	.name =DRV_NAME ,
	.match_table = mpc512x_keybaord_of_match,
	.probe = piebox_keyboard_probe,
	.remove = piebox_keyboard_remove,
	.driver = {		   
	.name = DRV_NAME,
	.owner = THIS_MODULE,
	},
};

static int __init piebox_keyboard_init(void)
{
	 return of_register_platform_driver(&piebox_keyboard_of_platform_driver);
}

static void __exit piebox_keybaord_exit(void)
{
	of_unregister_platform_driver(&piebox_keyboard_of_platform_driver);
}

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("piebox keyboard driver");

module_init(piebox_keyboard_init);
module_exit(piebox_keybaord_exit);



MODULE_LICENSE("GPL");
MODULE_AUTHOR("Cloudy chen<chen_yunsong@mtcera.com>");
MODULE_DESCRIPTION("Keyboard driver for CPU GPIOs");
MODULE_ALIAS("platform:gpio-keys");

