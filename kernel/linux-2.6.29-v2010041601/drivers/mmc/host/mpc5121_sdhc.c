/*
 * drivers/mmc/host/mpc5121_sdhc.c
 *
 * Copyright (C) 2008 Freescale Semicondutor, Inc. All rights reserved.
 *
 * Author: <allgosystems.com>
 *
 * derived from sdhci.c by Pierre Ossman
 *
 * Description:
 * Freescale MPC5121 Secure Digital Host Controller driver.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/ioport.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/highmem.h>
#include <linux/dma-mapping.h>
#include <linux/scatterlist.h>
#include <linux/uaccess.h>
#include <linux/irq.h>
#include <linux/hardirq.h>
#include <linux/io.h>
#include <linux/clk.h>
#include <linux/mmc/mmc.h>
#include <linux/mmc/host.h>
#include <linux/mmc/card.h>
#include <linux/mmc/sd.h>
#include <linux/mmc/host.h>
#include <linux/module.h>
#include <asm/io.h>
#include <linux/ioport.h>
#include <linux/time.h>

#include <asm/of_platform.h>
#include <asm/dma.h>
#include <asm/page.h>
#include <asm/reg.h>
#include <sysdev/fsl_soc.h>

#include "mpc5121_sdhc.h"
#include <asm/fsldma.h>
#include <linux/wait.h>
#include <linux/sched.h>

#include <asm-powerpc/fsldma.h>

#define DATA_SKEW_RATE_STD_4 0x00000003

#define DRIVER_NAME "sdhc"

#define MPC512X_DMA_SDHC	30
#define RSP_TYPE(x)	((x) & ~(MMC_RSP_BUSY|MMC_RSP_OPCODE))

#ifdef DEBUG
#define DBG(fmt, args...)	printk(KERN_DEBUG "[%s]  " fmt "\n", \
			__FUNCTION__, ## args)
#else
#define DBG(fmt, args...)	do {} while (0)
#endif

#define J1_SDHC_CLK 0X00000080
#define K5_SDHC_CMD 0X00000080
#define J2_SDHC_D0 0X00000080
#define J3_SDHC_D1_IRQ 0X00000080
#define J4_SDHC_D2 0X00000080
#define H2_SDHC_D3_CD 0X00000080
#define CLK_LINE 0X0C4
#define CMD_LINE 0X0C8
#define D0_LINE 0X0CC
#define D1_LINE 0X0D0
#define D2_LINE 0X0D4
#define D3_CD_LINE 0X0D8


#define PU_ENABLE 0x00000018
#define PD_ENABLE 0x00000008
#define DATA_SKEW_RATE_STD_4 0x00000003
#define PU_ENABLE_PULL_DOWN 0x00000008

static u32 *write_buf;
static dma_addr_t sphyaddr;
static void  dma_correction(unsigned long *, unsigned long *, int len);

#ifdef CONFIG_MMC_DEBUG
static void dump_cmd(struct mmc_command *cmd)
{
	printk(KERN_INFO "%s: CMD: opcode: %d ", DRIVER_NAME, cmd->opcode);
	printk(KERN_INFO "arg: 0x%08x ", cmd->arg);
	printk(KERN_INFO "flags: 0x%08x\n", cmd->flags);
}

static void dump_status(const char *func, int sts)
{
	unsigned int bitset;
	printk(KERN_INFO "%s:status: ", func);
	while (sts) {
		/* Find the next bit set */
		bitset = sts & ~(sts - 1);
		switch (bitset) {
		case STATUS_CARD_INSERTION:
			printk(KERN_INFO "CARD_INSERTION|");
			break;
		case STATUS_CARD_REMOVAL:
			printk(KERN_INFO "CARD_REMOVAL |");
			break;
		case STATUS_YBUF_EMPTY:
			printk(KERN_INFO "YBUF_EMPTY |");
			break;
		case STATUS_XBUF_EMPTY:
			printk(KERN_INFO "XBUF_EMPTY |");
			break;
		case STATUS_YBUF_FULL:
			printk(KERN_INFO "YBUF_FULL |");
			break;
		case STATUS_XBUF_FULL:
			printk(KERN_INFO "XBUF_FULL |");
			break;
		case STATUS_BUF_UND_RUN:
			printk(KERN_INFO "BUF_UND_RUN |");
			break;
		case STATUS_BUF_OVFL:
			printk(KERN_INFO "BUF_OVFL |");
			break;
		case STATUS_READ_OP_DONE:
			printk(KERN_INFO "READ_OP_DONE |");
			break;
		case STATUS_WR_CRC_ERROR_CODE_MASK:
			printk(KERN_INFO "WR_CRC_ERROR_CODE |");
			break;
		case STATUS_READ_CRC_ERR:
			printk(KERN_INFO "READ_CRC_ERR |");
			break;
		case STATUS_WRITE_CRC_ERR:
			printk(KERN_INFO "WRITE_CRC_ERR |");
			break;
		case STATUS_SDIO_INT_ACTIVE:
			printk(KERN_INFO "SDIO_INT_ACTIVE |");
			break;
		case STATUS_END_CMD_RESP:
			printk(KERN_INFO "END_CMD_RESP |");
			break;
		case STATUS_WRITE_OP_DONE:
			printk(KERN_INFO "WRITE_OP_DONE |");
			break;
		case STATUS_CARD_BUS_CLK_RUN:
			printk(KERN_INFO "CARD_BUS_CLK_RUN |");
			break;
		case STATUS_BUF_READ_RDY:
			printk(KERN_INFO "BUF_READ_RDY |");
			break;
		case STATUS_BUF_WRITE_RDY:
			printk(KERN_INFO "BUF_WRITE_RDY |");
			break;
		case STATUS_RESP_CRC_ERR:
			printk(KERN_INFO "RESP_CRC_ERR |");
			break;
		case STATUS_TIME_OUT_RESP:
			printk(KERN_INFO "TIME_OUT_RESP |");
			break;
		case STATUS_TIME_OUT_READ:
			printk(KERN_INFO "TIME_OUT_READ |");
			break;
		default:
			printk(KERN_INFO "Invalid Status Register value0x%x\n",
			bitset);
			break;
		}
		sts &= ~bitset;
	}
	printk(KERN_INFO "\n");
}
static void sdhc_dumpregs(struct sdhc_host *host)
{
	printk(KERN_DEBUG DRIVER_NAME ": ========= REGISTER DUMP ==========\n");
	printk(KERN_DEBUG DRIVER_NAME ": Clock Control Register : 0x%08x\n",
		fsl_readl(host->ioaddr + MMC_STR_STP_CLK));
	printk(KERN_DEBUG DRIVER_NAME ": Status Register : 0x%08x\n",
		fsl_readl(host->ioaddr + MMC_STATUS));
	printk(KERN_DEBUG DRIVER_NAME ": Clock Rate Register : 0x%08x\n",
		fsl_readl(host->ioaddr + MMC_CLK_RATE));
	printk(KERN_DEBUG DRIVER_NAME
			": Command and data Control Register : 0x%08x\n",
		fsl_readl(host->ioaddr + MMC_CMD_DAT_CONT));
	printk(KERN_DEBUG DRIVER_NAME
			": Response and Timeout Register : 0x%08x\n",
		fsl_readl(host->ioaddr + MMC_RES_TO));
	printk(KERN_DEBUG DRIVER_NAME ": Read Timeout Register : 0x%08x\n",
		fsl_readl(host->ioaddr + MMC_READ_TO));
	printk(KERN_DEBUG DRIVER_NAME ": Block Length Register : 0x%08x\n",
		fsl_readl(host->ioaddr + MMC_BLK_LEN));
	printk(KERN_DEBUG DRIVER_NAME ": Number of Blocks Register : 0x%08x\n",
		fsl_readl(host->ioaddr + MMC_NOB));
	printk(KERN_DEBUG DRIVER_NAME ": Revision Number Register : 0x%08x\n",
		fsl_readl(host->ioaddr + MMC_REV_NO));
	printk(KERN_DEBUG DRIVER_NAME ": Interrupt Control Register : 0x%08x\n",
		fsl_readl(host->ioaddr + MMC_INT_CNTR));
	printk(KERN_DEBUG DRIVER_NAME ": Command Number Register : 0x%08x\n",
		fsl_readl(host->ioaddr + MMC_CMD));
	printk(KERN_DEBUG DRIVER_NAME ": Command Argument Register : 0x%08x\n",
		fsl_readl(host->ioaddr + MMC_ARG));
	printk(KERN_DEBUG DRIVER_NAME ": Command Response Register : 0x%08x\n",
		fsl_readl(host->ioaddr + MMC_RES_FIFO));
	printk(KERN_DEBUG DRIVER_NAME
			": Data Buffer Access Register : 0x%08x\n",
		fsl_readl(host->ioaddr + MMC_BUFFER_ACCESS));
}
#endif


static int sdhc_data_done(struct sdhc_host *host, unsigned int stat);

static void fsl_writel(u32 val, unsigned __iomem *addr)
{
	out_be32(addr, val);
}

static inline u32 fsl_readl(unsigned __iomem *addr)
{
	return in_be32(addr);
}
static void mpc5121_sdhc_io_pullup_d3_cd(void)
{
	struct device_node *np;
	np = of_find_compatible_node(NULL, NULL, "fsl,mpc5121-ioctl");
	if (np) {
		void __iomem *ioctl = of_iomap(np, 0);
		fsl_writel(H2_SDHC_D3_CD | DATA_SKEW_RATE_STD_4 | PU_ENABLE,
				ioctl + D3_CD_LINE);
		iounmap(ioctl);
	}
}


static void mpc5121_sdhc_io_pulldown_d3_cd(void)
{
	struct device_node *np;
	np = of_find_compatible_node(NULL, NULL, "fsl,mpc5121-ioctl");
	if (np) {
		void __iomem *ioctl = of_iomap(np, 0);
		fsl_writel(H2_SDHC_D3_CD | DATA_SKEW_RATE_STD_4 | PD_ENABLE,
				ioctl + D3_CD_LINE);
		iounmap(ioctl);
	}
}

/*!
 *This function resets the SDHC host.
 *
 * @param host  Pointer to MMC/SD  host structure
 */
static void sdhc_softreset(struct sdhc_host *host)
{
	/* reset sequence */

	fsl_writel(0x8, host->ioaddr + MMC_STR_STP_CLK);
	fsl_writel(0x9, host->ioaddr + MMC_STR_STP_CLK);
	fsl_writel(0x1, host->ioaddr + MMC_STR_STP_CLK);
	fsl_writel(0x1, host->ioaddr + MMC_STR_STP_CLK);
	fsl_writel(0x1, host->ioaddr + MMC_STR_STP_CLK);
	fsl_writel(0x1, host->ioaddr + MMC_STR_STP_CLK);
	fsl_writel(0x1, host->ioaddr + MMC_STR_STP_CLK);
	fsl_writel(0x1, host->ioaddr + MMC_STR_STP_CLK);
	fsl_writel(0x1, host->ioaddr + MMC_STR_STP_CLK);
	fsl_writel(0x1, host->ioaddr + MMC_STR_STP_CLK);
	fsl_writel(0x3f, host->ioaddr + MMC_CLK_RATE);

	fsl_writel(0xff, host->ioaddr + MMC_RES_TO);
	fsl_writel(512, host->ioaddr + MMC_BLK_LEN);
	fsl_writel(1, host->ioaddr + MMC_NOB);
#ifndef CONFIG_MMC_MPC5121_USE_CARD_INSERTION_INT
	mpc5121_sdhc_io_pullup_d3_cd();
#endif

}
#ifdef CONFIG_MMC_MPC5121_USE_DMA
/*!
 * After DMA completion during read operation this gets called in the DMA isr.
 * @param host pointer to host data structure
 */
static int dma_read_over(struct sdhc_host *host)
{
		unsigned int status = 0;
	struct mmc_data *data = host->data;

	status = fsl_readl(host->ioaddr + MMC_STATUS);
	if (status & STATUS_TIME_OUT_READ) {
		pr_debug("%s: Read time out occurred\n", DRIVER_NAME);
		data->error = -ETIMEDOUT;
		fsl_writel(STATUS_TIME_OUT_READ,
				host->ioaddr + MMC_STATUS);
	} else if (status & STATUS_READ_CRC_ERR) {
		pr_debug("%s: Read CRC error occurred\n", DRIVER_NAME);
		data->error = -EILSEQ;
		fsl_writel(STATUS_READ_CRC_ERR,
				host->ioaddr + MMC_STATUS);
	}
	fsl_writel(STATUS_READ_OP_DONE, host->ioaddr + MMC_STATUS);

	sdhc_data_done(host, status);
	return 1 ;

}

/*!
 * After DMA completion during write operation this gets called in the DMA isr.
 * @param host pointer to host data structure
 */
static int dma_write_over(struct sdhc_host *host)
{
	unsigned int count, status = 0;
	struct mmc_data *data = host->data;
	count = 0;

	while (!(fsl_readl(host->ioaddr + MMC_STATUS)
		    & STATUS_WRITE_OP_DONE)) {
		count++;
		if (count > 100000) {
			printk(KERN_ERR "%s: "
				"failed to get WRITE_OP_DONE\n",
				DRIVER_NAME);
			break;
		}
	}

		;

	/* check for CRC errors */
	status = fsl_readl(host->ioaddr + MMC_STATUS);
	if (status & STATUS_WRITE_CRC_ERR) {
		pr_debug("%s: Write CRC error occurred\n", DRIVER_NAME);

		data->error = -EILSEQ;
		fsl_writel(STATUS_WRITE_CRC_ERR,
		host->ioaddr + MMC_STATUS);
	}
	fsl_writel(STATUS_WRITE_OP_DONE, host->ioaddr + MMC_STATUS);
	/* complete the data transfer request */
	sdhc_data_done(host, status);
	return 1;
}

/*!
 * DMA completion callback routine, this gets called in the DMA isr, when
 * DMA is over the sleeping process is woken up.
 * @param error_status error info of DMA transfer
 */
static  void fsl_dma_sdhc_callback(void *arg, int error_status)
{
	struct sdhc_host *host = (struct sdhc_host *)arg;
	struct mmc_data *data = host->data;


	unsigned long *buf;
	buf = (unsigned long *)((u32)page_address((void *)data->sg->page_link)
			+ (u32) data->sg->offset);

	if (!error_status) {
		if ((data->flags&MMC_DATA_READ)) {
			dma_correction((unsigned long *)write_buf,
				buf, host->dma_size);
			dma_read_over(host);
			return ;
		} /* This is MMC_DATA_WRITE operation */
		else {
			dma_write_over(host);
			return;
		}
	} else  /* There was an error during DMA operation */
		printk(KERN_ERR "DMA error on SDHC channel\n");
}


/*!
 * Allocate buffer memory for DMA transfer
 * Request for DMA channel
 * and initialize the wait queue
 * @param host pointer to MMC/SD host structure
 */
static int fsl_sdhc_dma_init(struct sdhc_host *host)
{
	/*
	 * Allocate and setup the DMA channels
	 */
	int dma_chan;
	dma_chan = fsl_dma_chan_request(MPC512X_DMA_SDHC);
	if (dma_chan < 0) {
		printk(KERN_ERR "couldn't get SDHC  DMA channel\n");
		host->dma_available = 0;
		return -ENOMEM;
	}
	fsl_dma_callback_set(dma_chan, fsl_dma_sdhc_callback, host);

	write_buf = dma_alloc_coherent(NULL, sizeof(u32) * 1024,
			&sphyaddr, GFP_KERNEL);
	if (!write_buf)
		return -ENOMEM;
	return 0;
}

/*!
 * config information for DMA. The DMA transfer is between SDHC FIFO
 * and the DMA coherent buffer initialized in the beginning.
 * @param host pointer to MMC/SD host data structure
 * @param data pointer to MMC Data structure
 * @param MMC_OP operation - to or from SD FIFO
 * @param len data transfer length
 */
static int fsl_dma_priv_config(struct sdhc_host *host, struct mmc_data *data,
			int MMC_OP, int len)
{
	static struct fsl_dma_requestbuf	dma_priv;
	/* DMA_FROM_DEVICE = read the card by host*/
	if (MMC_OP == DMA_TO_DEVICE) {
		dma_priv.src = (dma_addr_t)(sphyaddr);
		dma_priv.soff = 4;
		dma_priv.dest = (dma_addr_t)host->addr + MMC_BUFFER_ACCESS;
		dma_priv.doff = 0;
		dma_priv.minor_loop = 16;
	} else {
		dma_priv.src = (dma_addr_t) host->addr + MMC_BUFFER_ACCESS;
		dma_priv.soff = 0;
		dma_priv.dest = (dma_addr_t)(sphyaddr) ;
		dma_priv.doff = 4;
		dma_priv.minor_loop = 16;
	}
	host->dma = &dma_priv;
	return 1;
}

/*!
 * function to enable byte swap operation whenever data is written or
 * read from FIFO so this fuction is called before DMA write and after
 * DMA read operation.
 * @params pointers to the two buffers
 * @param len the length of data in bytes
 */
static void  dma_correction(unsigned long *buf, unsigned long *temp_buf,
		int len)
{
	int i;
	unsigned long temp_data;

	for (i = 0; i < ((len+3)/4); i++) {
		temp_data = *(buf+i);
		temp_data = cpu_to_le32(temp_data);
		*(temp_buf+i) = temp_data;
	}
}

#endif

/* Wait count to start the clock */
#define CMD_WAIT_CNT 1000

/*!
 * This function sets the SDHC register to stop the clock and waits for the
 * clock stop indication.
 *
 * @param host Pointer to MMC/SD host structure
 * @param wait Boolean value to indicate whether to wait
 * 	for the clock to start or come out instantly
 */
static void sdhc_stop_clock(struct sdhc_host *host, bool wait)
{
	int wait_cnt = 0;
	while (1) {
		fsl_writel(STR_STP_CLK_STOP_CLK,
				host->ioaddr + MMC_STR_STP_CLK);


		if (!wait)
			break;

		wait_cnt = CMD_WAIT_CNT;

		while (wait_cnt--) {
			if (!(fsl_readl(host->ioaddr + MMC_STATUS) &
						STATUS_CARD_BUS_CLK_RUN))
				break;
		}
		if (!(fsl_readl(host->ioaddr + MMC_STATUS) &
					STATUS_CARD_BUS_CLK_RUN))
			break;
	}
}

/*!
 * This function sets the SDHC register to start the clock and waits for the
 * clock start indication. When the clock starts SDHC module starts processing
 * the command in CMD Register with arguments in ARG Register.
 *
 * @param host Pointer to MMC/SD host structure
 * @param wait Boolean value to indicate whether to wait for the
 * 	clock to start or come out instantly
 */
static void sdhc_start_clock(struct sdhc_host *host, bool wait)
{
	int wait_cnt;
	if (fsl_readl(host->ioaddr + MMC_STATUS) & STATUS_CARD_BUS_CLK_RUN)
		return;

	while (1) {
		setbits32(host->ioaddr + MMC_STR_STP_CLK,
				STR_STP_CLK_START_CLK);
		if (!wait)
			break;

		wait_cnt = CMD_WAIT_CNT;
		while (wait_cnt--) {
			if (!(fsl_readl(host->ioaddr + MMC_STATUS) &
				STATUS_CARD_BUS_CLK_RUN))
				setbits32(host->ioaddr + MMC_STR_STP_CLK,
						STR_STP_CLK_START_CLK);

		}
		if (fsl_readl(host->ioaddr + MMC_STATUS) &
			STATUS_CARD_BUS_CLK_RUN)
			break;
	}
}

/*!
 * This function is called to setup SDHC register for data transfer.
 * The function allocates DMA buffers, configures the DMA channel.
 * Start the DMA channel to transfer data. When DMA is not enabled this
 * function set ups only Number of Block and Block Length registers.
 *
 * @param host  Pointer to MMC/SD host structure
 * @param data  Pointer to MMC/SD data structure
 */
static void sdhc_setup_data(struct sdhc_host *host, struct mmc_data *data)
{
	unsigned int nob = data->blocks;

	if (data->flags & MMC_DATA_STREAM)
		nob = 0xffff;

	host->data = data;

	fsl_writel(nob, host->ioaddr + MMC_NOB);
	fsl_writel(data->blksz, host->ioaddr + MMC_BLK_LEN);

	host->dma_size = data->blocks * data->blksz;
	pr_debug("%s:Request bytes to transfer:%d\n", DRIVER_NAME,
		 host->dma_size);
}

/*!
 * This function is called by \b fslmci_request() function to setup the SDHC
 * register to issue command. This function disables the card insertion and
 * removal detection interrupt.
 *
 * @param host  Pointer to MMC/SD host structure
 * @param cmd   Pointer to MMC/SD command structure
 * @param cmdat Value to store in Command and Data Control Register
 */
static void sdhc_start_cmd(struct sdhc_host *host, struct mmc_command *cmd,
		unsigned int cmdat)
{
	WARN_ON(host->cmd != NULL);
	fsl_writel(INT_CNTR_END_CMD_RES, host->ioaddr + MMC_INT_CNTR);

	host->cmd = cmd;

	switch (RSP_TYPE(mmc_resp_type(cmd))) {
	case RSP_TYPE(MMC_RSP_R1):	/* r1, r1b, r6 */
		cmdat |= CMD_DAT_CONT_RESPONSE_FORMAT_R1;
		break;
	case RSP_TYPE(MMC_RSP_R3):
		cmdat |= CMD_DAT_CONT_RESPONSE_FORMAT_R3;
		break;
	case RSP_TYPE(MMC_RSP_R2):
		cmdat |= CMD_DAT_CONT_RESPONSE_FORMAT_R2;
		break;
	default:
		/* No Response required */
		break;
	}

	if (cmd->opcode == MMC_GO_IDLE_STATE)
		cmdat |= CMD_DAT_CONT_INIT;	/* This command needs init */

	if (host->mmc->ios.bus_width == MMC_BUS_WIDTH_4)
		cmdat |= CMD_DAT_CONT_BUS_WIDTH_4;
	fsl_writel(cmd->opcode, host->ioaddr + MMC_CMD);
	fsl_writel(cmd->arg, host->ioaddr + MMC_ARG);

	fsl_writel(cmdat, host->ioaddr + MMC_CMD_DAT_CONT);
	sdhc_start_clock(host, true);
}

/*!
 * This function is called to complete the command request.
 * This function enables insertion or removal interrupt.
 *
 * @param host Pointer to MMC/SD host structure
 * @param req  Pointer to MMC/SD command request structure
 */
static void sdhc_finish_request(struct sdhc_host *host,
				struct mmc_request *req)
{
	u32 intr_enable;
	host->mrq = NULL;
	host->cmd = NULL;
	host->data = NULL;


	mmc_request_done(host->mmc, req);
	intr_enable = (INT_CNTR_END_CMD_RES | INT_CNTR_CARD_INSERTION_EN
	| INT_CNTR_CARD_REMOVAL_EN);
	fsl_writel(intr_enable, host->ioaddr + MMC_INT_CNTR);
}

/*!
 * This function is called when the requested command is completed.
 * This function reads the response from the card and data if the command
 * is for data transfer. This function checks for CRC error in response FIFO or
 * data FIFO.
 *
 * @param host  Pointer to MMC/SD host structure
 * @param stat  Content of SDHC Status Register
 *
 * @return This function returns 0 if there is no pending command, otherwise 1
 * always.
 */
static int sdhc_cmd_done(struct sdhc_host *host, unsigned int stat)
{
	struct mmc_command *cmd = host->cmd;
	struct mmc_data *data = host->data;
	struct scatterlist *sg;
	int i, count;
	u32 a, b, c;
	u32 temp_data;
	unsigned int status = 0;
	unsigned long *buf;
	int no_of_bytes;
	int no_of_words;
	int num_buf = 1;

	if (!cmd) {
		/* There is no command for completion */
		return 0;
	}

	/* As this function finishes the command, initialize cmd to NULL */
	host->cmd = NULL;

	/* check for Time out errors */
	if (stat & STATUS_TIME_OUT_RESP) {
		fsl_writel(STATUS_TIME_OUT_RESP, host->ioaddr + MMC_STATUS);
		pr_debug("%s: CMD TIMEOUT\n", DRIVER_NAME);
		cmd->error = -ETIMEDOUT;
	} else if (stat & STATUS_RESP_CRC_ERR && cmd->flags & MMC_RSP_CRC) {
		fsl_writel(STATUS_RESP_CRC_ERR, host->ioaddr + MMC_STATUS);
		cmd->error = -EILSEQ;
	}

	/* Read response from the card */
	switch (RSP_TYPE(mmc_resp_type(cmd))) {
	case RSP_TYPE(MMC_RSP_R1):	/* r1, r1b, r6 */
		a = fsl_readl(host->ioaddr + MMC_RES_FIFO) & 0xffff;
		b = fsl_readl(host->ioaddr + MMC_RES_FIFO) & 0xffff;
		c = fsl_readl(host->ioaddr + MMC_RES_FIFO) & 0xffff;

		cmd->resp[0] = a << 24 | b << 8 | c >> 8;
		break;
	case RSP_TYPE(MMC_RSP_R3):	/* r3, r4 */
		a = fsl_readl(host->ioaddr + MMC_RES_FIFO) & 0xffff;
		b = fsl_readl(host->ioaddr + MMC_RES_FIFO) & 0xffff;
		c = fsl_readl(host->ioaddr + MMC_RES_FIFO) & 0xffff;
		cmd->resp[0] = a << 24 | b << 8 | c >> 8;

		break;
	case RSP_TYPE(MMC_RSP_R2):
		for (i = 0; i < 4; i++) {
			a = fsl_readl(host->ioaddr + MMC_RES_FIFO) & 0xffff;
			b = fsl_readl(host->ioaddr + MMC_RES_FIFO) & 0xffff;
			cmd->resp[i] = a << 16 | b;

		}
		break;
	default:
		break;
	}

	pr_debug("%s: 0x%08x, 0x%08x, 0x%08x, 0x%08x\n", DRIVER_NAME,
		 cmd->resp[0], cmd->resp[1], cmd->resp[2], cmd->resp[3]);

	if (!host->data || cmd->error) {
		/* complete the command */
		sdhc_finish_request(host, host->mrq);
#ifndef CONFIG_MMC_MPC5121_USE_CARD_INSERTION_INT
		mmc_detect_change(host->mmc, msecs_to_jiffies(100));
#endif
		return 1;
	}

	/* The command has a data transfer */
	no_of_bytes = data->blocks * data->blksz;
	host->dma_size = no_of_bytes;
	buf = (unsigned long *)((u32)page_address((void *)data->sg->page_link) +
			(u32) data->sg->offset);
	sg = data->sg ;

	/* calculate the number of bytes requested for transfer */
	no_of_words = (no_of_bytes + 3) / 4;
	pr_debug("no_of_words=%d\n", no_of_words);


#ifdef CONFIG_MMC_MPC5121_USE_DMA
	if (host->dma_size < (16 << host->mmc->ios.bus_width))
		goto pio;

	if ((data->flags & MMC_DATA_READ)) {
		fsl_dma_priv_config(host, data, DMA_FROM_DEVICE, no_of_bytes);
		host->dma->len = no_of_bytes ;
		fsl_dma_config(MPC512X_DMA_SDHC, (host->dma), num_buf);

		fsl_dma_enable(MPC512X_DMA_SDHC);
		return 1;
	}
	if (data->flags & MMC_DATA_WRITE) {
		dma_correction(buf, (unsigned long *)write_buf, host->dma_size);
		fsl_dma_priv_config(host, data, DMA_TO_DEVICE, no_of_bytes);
		host->dma->len = no_of_bytes ;
		fsl_dma_config(MPC512X_DMA_SDHC, (host->dma), num_buf);
		fsl_dma_enable(MPC512X_DMA_SDHC);
		return 1;
	}
pio:
#endif
	/* Use PIO tranfer of data */
	if (data->flags & MMC_DATA_READ) {
		for (i = 0; i < no_of_words; i++) {
			/* wait for buffers to be ready for read */

			count = 0;
			while (!(fsl_readl(host->ioaddr + MMC_STATUS) &
						(STATUS_BUF_READ_RDY |
						 STATUS_READ_OP_DONE))) {
			count++;
				if (count > 100000) {
					printk(KERN_ERR "%s: "
						"failed to get READ_OP_DONE\n",
						DRIVER_NAME);
					break;
				}
			}

				;

			/* read 32 bit data */
			temp_data = fsl_readl(host->ioaddr + MMC_BUFFER_ACCESS);
			temp_data = cpu_to_le32(temp_data);
			if (no_of_bytes >= 4) {
				*buf++ = temp_data;
				no_of_bytes -= 4;
			}
		}

		count = 0;
		/* wait for read operation completion bit */
		while (!(fsl_readl(host->ioaddr + MMC_STATUS)
					& STATUS_READ_OP_DONE)) {
			count++;
			if (count > 100000) {
				printk(KERN_ERR "%s: "
					"failed to get READ_OP_DONE\n",
					DRIVER_NAME);
				break;
			}
		}

			;
		/* check for time out and CRC errors */
		status = fsl_readl(host->ioaddr + MMC_STATUS);
		if (status & STATUS_TIME_OUT_READ) {
			pr_debug("%s: Read time out occurred\n", DRIVER_NAME);
			data->error = -ETIMEDOUT;
			fsl_writel(STATUS_TIME_OUT_READ,
			host->ioaddr + MMC_STATUS);
		} else if (status & STATUS_READ_CRC_ERR) {
			pr_debug("%s: Read CRC error occurred\n", DRIVER_NAME);
			data->error = -EILSEQ;
			fsl_writel(STATUS_READ_CRC_ERR,
			host->ioaddr + MMC_STATUS);
		}
		fsl_writel(STATUS_READ_OP_DONE, host->ioaddr + MMC_STATUS);

		pr_debug("%s: Read %u words\n", DRIVER_NAME, i);

		sdhc_data_done(host, status);
		return 1;
	}
	if (data->flags & MMC_DATA_WRITE) {
		for (i = 0; i < no_of_words; i++) {

			/* wait for buffers to be ready for write */
			while (!(fsl_readl(host->ioaddr + MMC_STATUS) &
						STATUS_BUF_WRITE_RDY)) ;

			/* write 32 bit data */
			/* ALLGO BUGFIX: MMC data is LE */
			fsl_writel(cpu_to_le32(*buf++),
					host->ioaddr + MMC_BUFFER_ACCESS);

			if (fsl_readl(host->ioaddr + MMC_STATUS)
					& STATUS_WRITE_OP_DONE)
				break;
		}

		/* wait for write operation completion bit */
		while (!(fsl_readl(host->ioaddr + MMC_STATUS) &
					STATUS_WRITE_OP_DONE))
			;

		/* check for CRC errors */
		status = fsl_readl(host->ioaddr + MMC_STATUS);
		if (status & STATUS_WRITE_CRC_ERR) {
			pr_debug("%s: Write CRC error occurred\n", DRIVER_NAME);

			data->error = -EILSEQ;
			fsl_writel(STATUS_WRITE_CRC_ERR,
					host->ioaddr + MMC_STATUS);
		}
		fsl_writel(STATUS_WRITE_OP_DONE, host->ioaddr + MMC_STATUS);
		pr_debug("%s: Written %u words\n", DRIVER_NAME, i);

	}

	/* complete the data transfer request */
	sdhc_data_done(host, status);
	return 1;
}


/*!
 * This function is called when the data transfer is completed either by DMA
 * or by core. This function is called to clean up the DMA buffer and to send
 * STOP transmission command for commands to transfer data. This function
 * completes request issued by the MMC/SD core driver.
 *
 * @param host   pointer to MMC/SD host structure.
 * @param stat   content of SDHC Status Register
 *
 * @return This function returns 0 if no data transfer otherwise return 1
 * always.
 */
static int sdhc_data_done(struct sdhc_host *host, unsigned int stat)
{
	struct mmc_data *data = host->data;

	if (!data)
		return 0;
#ifdef CONFIG_MMC_MPC5121_USE_DMA
	if (host->dma_size > (16 << host->mmc->ios.bus_width))
	fsl_dma_disable(MPC512X_DMA_SDHC);

#endif
	if (fsl_readl(host->ioaddr + MMC_STATUS) & STATUS_ERR_MASK) {
		pr_debug("%s: request failed. status: 0x%08x\n",
			 DRIVER_NAME, fsl_readl(host->ioaddr + MMC_STATUS));
	}

	host->data = NULL;
	data->bytes_xfered = host->dma_size;

	if (host->mrq->stop && (data->error == 0)) {
		sdhc_start_cmd(host, host->mrq->stop, 0);
	} else {
		sdhc_finish_request(host, host->mrq);
	}

	return 1;
}


static void sdhc_set_power(struct sdhc_host *host, unsigned short power)
{

	if (host->power == power)
	return;

	if (power == (unsigned short)-1)
	host->power = power;
}

/*!
 * This function is called by MMC/SD Bus Protocol driver to issue a MMC
 * and SD commands to the SDHC.
 *
 * @param  mmc  Pointer to MMC/SD host structure
 * @param  mrq  Pointer to MMC/SD command request structure
 */
static void sdhc_request(struct mmc_host *mmc, struct mmc_request *mrq)
{
	struct sdhc_host *host = mmc_priv(mmc);
	/* Holds the value of Command and Data Control Register */
	unsigned long cmdat;

	WARN_ON(host->mrq != NULL);

	host->mrq = mrq;
#ifdef CONFIG_MMC_DEBUG
	dump_cmd(req->cmd);
	dump_status(__FUNCTION__, __raw_readl(host->base + MMC_STATUS));
#endif

	cmdat = 0;
	if (mrq->data) {
	sdhc_setup_data(host, mrq->data);

	cmdat |= CMD_DAT_CONT_DATA_ENABLE;

	if (mrq->data->flags & MMC_DATA_WRITE)
	cmdat |= CMD_DAT_CONT_WRITE;

	if (mrq->data->flags & MMC_DATA_STREAM)
	printk(KERN_ERR
	"FSL MMC does not support stream mode\n");
	}
	sdhc_start_cmd(host, mrq->cmd, cmdat);
}

/*!
 * This function is called by MMC/SD Bus Protocol driver to change the clock
 * speed of MMC or SD card
 *
 * @param mmc Pointer to MMC/SD host structure
 * @param ios Pointer to MMC/SD I/O type structure
 */
static void sdhc_set_ios(struct mmc_host *mmc, struct mmc_ios *ios)
{
	struct sdhc_host *host = mmc_priv(mmc);
	/*This variable holds the value of clock prescaler */
	int prescaler;
	int clk_rate = clk_get_rate(host->sdhc_clk);
	if (ios->power_mode == MMC_POWER_OFF)
	sdhc_set_power(host, -1);
	else
	sdhc_set_power(host, ios->vdd);

	if (ios->clock) {
		unsigned int clk_dev = 0;

		if (ios->clock == mmc->f_min)
			prescaler = 16;
		else
			prescaler = 0;

		while (prescaler <= 0x800) {
			for (clk_dev = 1; clk_dev <= 0xF; clk_dev++) {
				int x;
				if (prescaler != 0) {
					x = (clk_rate / (clk_dev + 1)) /
					 (prescaler * 2);
				} else {
					x = clk_rate / (clk_dev + 1);
				}

				pr_debug("x=%d, clock=%d %d\n", x, ios->clock,
					 clk_dev);
				if (x <= ios->clock)
					break;
			}
			if (clk_dev < 0x10)
				break;
			if (prescaler == 0)
				prescaler = 1;
			else
				prescaler <<= 1;
		}

		pr_debug("prescaler = 0x%x, divider = 0x%x\n", prescaler,
			 clk_dev);
		sdhc_stop_clock(host, true);

		fsl_writel((prescaler << 4) | clk_dev,
		host->ioaddr + MMC_CLK_RATE);
		sdhc_start_clock(host, false);
	} else {
		sdhc_stop_clock(host, true);
	}
}

/*!
 * MMC/SD host operations structure.
 * These functions are registered with MMC/SD Bus protocol driver.
 */
static const struct mmc_host_ops sdhc_ops = {
	.request	= sdhc_request,
	.set_ios	= sdhc_set_ios,
};

/*!
 * Interrupt service routine registered to handle the SDHC interrupts.
 * This interrupt routine handles end of command, card insertion and
 * card removal interrupts. If the interrupt is card insertion or removal then
 * inform the MMC/SD core driver to detect the change in physical connections.
 * If the command is END_CMD_RESP read the Response FIFO.
 *
 * @param   irq    the interrupt number
 * @param   devid  driver private data
 * @param   regs   holds a snapshot of the processor's context before the
 *                 processor entered the interrupt code
 *
 * @return  The function returns \b IRQ_RETVAL(1) if interrupt was handled,
 *          returns \b IRQ_RETVAL(0) if the interrupt was not handled.
 */
static irqreturn_t sdhc_irq(int irq, void *dev_id)
{
	struct sdhc_host *host = dev_id;
	irqreturn_t result = IRQ_HANDLED;
	unsigned int status = 0;

	status = fsl_readl(host->ioaddr + MMC_STATUS);
	if (status & (STATUS_CARD_INSERTION | STATUS_CARD_REMOVAL)) {
		if (status & STATUS_CARD_INSERTION)
			fsl_writel(STATUS_CARD_INSERTION,
					host->ioaddr + MMC_STATUS);
		if (status & STATUS_CARD_REMOVAL)
			fsl_writel(STATUS_CARD_REMOVAL,
					host->ioaddr + MMC_STATUS);
			mmc_detect_change(host->mmc, msecs_to_jiffies(100));

	}

	if (status & STATUS_END_CMD_RESP) {
			setbits32(host->ioaddr + MMC_STATUS,
					STATUS_END_CMD_RESP);
		sdhc_cmd_done(host, status);

	}
	return result;
}


/*****************************************************************************\
 *                                                                           *
 * Device probing/removal                                                    *
 *                                                                           *
\*****************************************************************************/

static void sdhc_remove_slot(struct of_device *ofdev, int slot)
{
	struct sdhc_chip *chip;
	struct mmc_host *mmc;
	struct sdhc_host *host;

	chip = dev_get_drvdata(&(ofdev->dev));
	host = chip->hosts[slot];
	mmc = host->mmc;

	chip->hosts[slot] = NULL;
	mmc_remove_host(mmc);
	sdhc_softreset(host);

	free_irq(host->irq, host);

	iounmap(host->ioaddr);

	release_mem_region(host->addr, host->size);

	mmc_free_host(mmc);
}

#if CONFIG_PPC_MPC5125
#define SDHC1_IO_ADDR_BASE	(0x8000a000+0x6f)
static sdhc_io_init(void)
{
	unsigned char *sdhc_io_reg;
	unsigned int i=6;
	sdhc_io_reg=ioremap(SDHC1_IO_ADDR_BASE, 0x6);
	for(i=0;i<6;i++)
	{
		sdhc_io_reg[i]=0x1b;
	}
	iounmap(sdhc_io_reg);
}
#endif
static int __devinit sdhc_probe_slot(struct of_device *ofdev, int slot)
{
	struct device_node *np = ofdev->node;
	struct device_node *cpu;
	int ret;
	struct sdhc_chip *chip;
	struct mmc_host *mmc;
	struct sdhc_host *host;
	struct resource res;
	u32 intr_enable;

	chip = dev_get_drvdata(&(ofdev->dev));
	BUG_ON(!chip);

	mmc = mmc_alloc_host(sizeof(struct sdhc_host), &(ofdev->dev));
	if (!mmc)
		return -ENOMEM;

	host = mmc_priv(mmc);
	host->mmc = mmc;

	host->chip = chip;
	chip->hosts[slot] = host;

	ret = of_address_to_resource(np, 0, &res);
	if (ret)
		goto free;
	host->addr = res.start;

	host->size = res.end - res.start + 1;
	host->irq = irq_of_parse_and_map(np, 0);
	printk(KERN_DEBUG "slot %d at 0x%08lx, irq %d and size = %x\n",
			slot, host->addr, host->irq, host->size);

	snprintf(host->slot_descr, 20, "sdhc:slot%d", slot);

	if (!request_mem_region(host->addr, host->size, DRIVER_NAME)) {
		ret = -EBUSY;
		goto release;
	}
#if CONFIG_PPC_MPC5125
	sdhc_io_init();
#endif
	host->ioaddr = ioremap(host->addr, host->size);
	if (!host->ioaddr) {
		ret = -ENOMEM;
		goto release;
	}
	host->sdhc_clk = clk_get(&ofdev->dev, "sdhc_clk");
	clk_enable(host->sdhc_clk);
	sdhc_softreset(host);
#ifdef CONFIG_MMC_MPC5121_USE_DMA
	fsl_sdhc_dma_init(host);
#endif
	fsl_writel(READ_TO_VALUE, host->ioaddr + MMC_READ_TO);
	intr_enable = (INT_CNTR_END_CMD_RES | INT_CNTR_CARD_INSERTION_EN
			| INT_CNTR_CARD_REMOVAL_EN);
	fsl_writel(intr_enable, host->ioaddr + MMC_INT_CNTR);
	cpu = of_find_node_by_type(NULL, "cpu");
	if (cpu) {
		unsigned int size;
		const u32 *prop = of_get_property(cpu, "bus-frequency", &size);
		host->max_clk = *prop;
		of_node_put(cpu);
	} else
		host->max_clk = 396000000;
	mmc->ops = &sdhc_ops;
	mmc->f_min = 300000;
	mmc->f_max = min((int)host->max_clk, 25000000);
	mmc->caps = MMC_CAP_4_BIT_DATA;
	mmc->ocr_avail = MMC_VDD_32_33 |
			 MMC_VDD_33_34 |
			 MMC_VDD_29_30 |
			 MMC_VDD_30_31 |
			 MMC_VDD_165_195;
	spin_lock_init(&host->lock);
	/*
	 * Maximum number of segments. Hardware cannot do scatter lists.
	 */

#ifdef CONFIG_MMC_DEBUG
	sdhc_dumpregs(host);
#endif
	ret = request_irq(host->irq, sdhc_irq, IRQF_SHARED,
			host->slot_descr, host);
	if (ret)
		goto release;
	mmiowb();
	mmc_add_host(mmc);
	printk(KERN_INFO "%s: SDHC at 0x%08lx irq %d %s\n", mmc_hostname(mmc),
			host->addr, host->irq,
			(host->flags & SDHC_USE_DMA)?"DMA":"PIO");
	return 0;
release:
	release_mem_region(host->addr, host->size);
free:
	mmc_remove_host(mmc);
	return ret;
}


static int __devinit sdhc_probe(struct of_device *ofdev,
	const struct of_device_id *match)
{
	int ret = 1, i;
	u8 slots;
	struct sdhc_chip *chip;
	BUG_ON(ofdev == NULL);
	BUG_ON(match == NULL);
	slots = SDHC_SLOTS_NUMBER;
	DBG("found %d slot(s)", slots);
	if (slots == 0)
		return -ENODEV;
	chip = kmalloc(sizeof(struct sdhc_chip) +
		sizeof(struct sdhc_host *) * slots, GFP_KERNEL);
	if (!chip) {
		ret = -ENOMEM;
		goto err;
	}
	chip->ofdev = ofdev;
	chip->num_slots = slots;
	dev_set_drvdata(&(ofdev->dev), chip);
	for (i = 0; i < slots; i++) {
		ret = 0;
		ret = sdhc_probe_slot(ofdev, i);
		if (ret) {
			for (i--; i >= 0; i--)
				sdhc_remove_slot(ofdev, i);
			goto free;
		}
	}
	return 0;
free:
	dev_set_drvdata(&(ofdev->dev), NULL);
	kfree(chip);
err:
	return ret;
}

static int __devexit sdhc_remove(struct of_device *ofdev)
{
	int i;
	struct sdhc_chip *chip;
	chip = dev_get_drvdata(&(ofdev->dev));
	if (chip) {
		for (i = 0; i < chip->num_slots; i++)
			sdhc_remove_slot(ofdev, i);
		dev_set_drvdata(&(ofdev->dev), NULL);
		kfree(chip);
		mpc5121_sdhc_io_pulldown_d3_cd();
#ifdef CONFIG_MMC_MPC5121_USE_DMA
		fsl_dma_free_chan(MPC512X_DMA_SDHC);
		dma_free_coherent(NULL, sizeof(u32) * 1024, write_buf,
								sphyaddr);
#endif
	}
	return 0;
}


#define sdhc_suspend NULL
#define sdhc_resume NULL

/*-------------------------------------------------------------------------*/
static struct of_device_id fsl_sdhc_match[] = {
	{
#if CONFIG_PPC_MPC5125
		.compatible = "fsl,mpc5125-sdhc",
#else
		.compatible = "fsl,mpc5121-sdhc",
#endif
	},
	{},
};

MODULE_DEVICE_TABLE(of, fsl_sdhc_match);

static struct of_platform_driver sdhc_driver = {
	.owner = 	THIS_MODULE,
	.name =		DRIVER_NAME,
	.match_table =  fsl_sdhc_match,
	.probe =	sdhc_probe,
	.remove =	__devexit_p(sdhc_remove),
	.suspend =	sdhc_suspend,
	.resume =	sdhc_resume,
};


/*****************************************************************************\
 *                                                                           *
 * Driver init/exit                                                          *
 *                                                                           *
\*****************************************************************************/

static int __init sdhc_drv_init(void)
{
	printk(KERN_INFO DRIVER_NAME
		": Freescale Enhanced Secure Digital Host Controller driver\n");

	return of_register_platform_driver(&sdhc_driver);
}

static void __exit sdhc_drv_exit(void)
{
	DBG("Exiting\n");
	of_unregister_platform_driver(&sdhc_driver);
}
module_init(sdhc_drv_init);
module_exit(sdhc_drv_exit);

MODULE_AUTHOR("Freescale Semiconductor, Inc.");
MODULE_DESCRIPTION("Enhanced Secure Digital Host Controller driver");
MODULE_LICENSE("GPL");
